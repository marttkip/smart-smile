<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

error_reporting(0);
class Requisition extends MX_Controller
{

	function __construct()
	{
		parent:: __construct();
		$this->load->model('admin/users_model');
		$this->load->model('inventory_management/products_model');
		$this->load->model('orders_model');
		$this->load->model('suppliers_model');
		$this->load->model('categories_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/admin_model');
		$this->load->model('administration/personnel_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('accounting/creditors_model');
		$this->load->model('inventory/stores_model');
		$this->load->model('reception/database');
		$this->load->model('reception/reception_model');
		$this->load->model('inventory/requisition_model');
		$this->load->model('inventory_management/inventory_management_model');
	}

	/*
	*
	*	Default action is to show all the orders
	*
	*/
	public function index()
	{
		// get my approval roles

		$where = 'requisition_id > 0';
		$table = 'requisition';

		// $search = $this->session->userdata('search_orders');
		// $where .= $search;

		$search = $this->session->userdata('requisition_search');


		if(!empty($search))
		{
		$where .= $search;
		}
        

		//pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'procurement/requisitions';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = 4;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->requisition_model->get_all_requisitions($table, $where, $config["per_page"], $page);

		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		// $v_data['level_status'] = $this->orders_model->order_level_status();
		$v_data['title'] = "All Orders";
		$data['content'] = $this->load->view('requisition/requisition_list', $v_data, true);

		$data['title'] = 'All orders';

		$this->load->view('admin/templates/general_page', $data);
	}

	public function requisition_add($requisition_id = 0)
	{
		$v_data['requisition_id'] = $requisition_id;

		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		// $v_data['level_status'] = $this->orders_model->order_level_status();
		$v_data['title'] = "All Orders";
		$data['content'] = $this->load->view('requisition/requisition_view', $v_data, true);

		$data['title'] = 'All orders';

		$this->load->view('admin/templates/general_page', $data);

	}

	public function product_list($requisition_id=0)
	{
		$data = array('requisition_id'=>$requisition_id);


		$drug_name = $this->input->post('drug');
		$query = null;
		if(!empty($drug_name))
		{

			$ultra_sound_where = 'product_deleted = 0';


			$ultra_sound_table = 'product';

			$ultra_sound_where .= ' AND product_name LIKE \'%'.$drug_name.'%\'';

			$this->db->where($ultra_sound_where);
			$this->db->limit(1000);
			// $this->db->order_by('product.product_name','ASC');
			$query = $this->db->get($ultra_sound_table);

		}
		else
		{
			$ultra_sound_where = 'product_deleted = 0';
			$ultra_sound_table = 'product';
	

			$this->db->where($ultra_sound_where);
			$this->db->limit(1000);
			// $this->db->order_by('product.product_name','ASC');
			$query = $this->db->get($ultra_sound_table);
		}
		$data['query'] = $query;
		$page = $this->load->view('requisition/product_list',$data,true);
		$response['message'] = 'success';
		$response['results'] = $page;

		
		

		echo json_encode($response);
	}	

	public function selected_products($requisition_id= 0)
	{
		

		$data = array('requisition_id'=>$requisition_id);


		$product_name = $this->input->post('product_name');
		$query = null;
		if(!empty($product_name))
		{

			$ultra_sound_where = 'product_deleted = 0 AND product.product_id = requisition_item.product_id';
			$ultra_sound_table = 'product,requisition_item';
			$ultra_sound_where .= ' AND product_name LIKE \'%'.$product_name.'%\'';

			$this->db->where($ultra_sound_where);
			$this->db->limit(1000);
			// $this->db->order_by('product.product_name','ASC');
			$query = $this->db->get($ultra_sound_table);

		}
		else
		{
			$ultra_sound_where = 'product_deleted = 0 AND product.product_id = requisition_item.product_id AND requisition_item.requisition_item_delete = 0';
			$ultra_sound_table = 'product,requisition_item';

			$this->db->where($ultra_sound_where);
			$this->db->limit(1000);
			// $this->db->order_by('product.product_name','ASC');
			$query = $this->db->get($ultra_sound_table);
		}
		$data['query'] = $query;
		$page = $this->load->view('requisition/requisition_items',$data,true);

		$response['message'] = 'success';
		$response['results'] = $page;
		echo json_encode($response);
	}

	public function add_item_to_list($product_id,$requisition_id)
	{
		$requisition_add['requisition_id'] = $requisition_id;
		$requisition_add['product_id'] = $product_id;
		$requisition_add['created_on'] = date('Y-m-d');
		$requisition_add['created_by'] = $this->session->userdata('personnel_id');
		$requisition_add['current_stock'] = 0;
		

		$this->db->where('product_id = '.$product_id.' AND requisition_id = '.$requisition_id);
		$requisition_items = $this->db->get('requisition_item');

		if($requisition_items->num_rows() == 0)
		{
			$this->db->insert('requisition_item',$requisition_add);
		}
		else
		{

		}

		$response['message'] = 'success';
		$response['results'] = '';
		echo json_encode($response);
	}

	public function update_items($requisition_item_id,$requisition_id)
	{

		$units = $this->input->post('units');
		$requisition_add['units'] = $units;
		

		$this->db->where('requisition_item_id = '.$requisition_item_id);
		$this->db->update('requisition_item',$requisition_add);

		

		$response['message'] = 'success';
		$response['results'] = '';
		echo json_encode($response);
	}

	public function delete_requisition_item($requisition_item_id,$requisition_id)
	{

	
		$requisition_add['requisition_item_delete'] = 1;
		

		$this->db->where('requisition_item_id = '.$requisition_item_id);
		$this->db->update('requisition_item',$requisition_add);

		

		$response['message'] = 'success';
		$response['results'] = '';
		echo json_encode($response);
	}

	public function confirm_requisition($requisition_id)
	{

	
		$requisition_add['created'] = date('Y-m-d');
		$requisition_add['created_by'] = $this->session->userdata('personnel_id');
		$requisition_add['requisition_status'] = 0;
		
		$this->db->insert('requisition',$requisition_add);
		$requisition_idd = $this->db->insert_id();

		$requisition_update['requisition_id'] = $requisition_idd;
		$this->db->where('requisition_id = '.$requisition_id.' AND requisition_item_delete = 0 AND created_by = '.$this->session->userdata('personnel_id'));
		$this->db->update('requisition_item',$requisition_update);

		

		$response['message'] = 'success';
		$response['results'] = '';
		echo json_encode($response);
	}
}
?>