<div>
<section class="panel">
    <header class="panel-heading">
            <h5 class="pull-left"><i class="icon-reorder"></i><?php echo  $product_name.' Trail';?></h5>
          <div class="widget-icons pull-right">
              
          </div>
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">
        <div class="padd" style="height:80vh;overflow-y: scroll;">
            
            <div class="col-md-12" style="padding:10px; ">

            	<table class="table table-hover table-bordered ">
				 	<thead>
						<tr>
							<th>Date</th>						  
							<th>Store</th>
							<th>Type</th>
							<th>Description</th>
							<th>Total In</th>
							<th>Total Out</th>
							<th>Stock</th>						
						</tr>
					 </thead>
				  	<tbody>
				  		<?php echo $creditor_result['result']?>
					</tbody>
				</table>
			</div>
        </div>
    </div>
</section>
<div class="col-md-12">                             
    <div class="center-align">
        <a  class="btn btn-sm btn-warning" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR </a>
    </div>  
</div>
</div>