<?php

$result = '';

$result ='<table class="table table-hover table-condensed table-bordered ">
              <thead>
                <tr>
                  <th>Product Name</th>
                  <th>Reorder Level</th>
                  <th>Current Price</th>
                  <th>Current Stock</th>
                  <th>Supplier</th>
                </tr>
              </thead>
              <tbody>
            ';
if($query != null)
{
	if($query->num_rows() > 0)
	{
		foreach ($query->result() as $key => $value) {
			# code...
			$product_id = $value->product_id;
			$requisition_item_id = $value->requisition_item_id;
			$product_name = $value->product_name;
			$units = $value->units;

			if(empty($units))
			{
				$units = 0;
			}


			 $result .= 
                '
                    <tr >
                        <td>'.$product_name.'</td>
                        <td></td>
                        <td>'.number_format(0,2).'</td>
                      	<td>'.number_format(0,2).'</td>
                        <td><input type="text" name="units'.$requisition_item_id.'" id="units'.$requisition_item_id.'" class="form-control" value="'.$units.'" onkeyup="update_item('.$requisition_item_id.','.$requisition_id.')"></td>
                        <td><a class="btn btn-xs btn-success" onclick="remove_items('.$requisition_item_id.','.$requisition_id.')"><i class="fa fa-trash"></i></a></td>
                    </tr> 
                ';
			
		}
	}
}
 $result .= 
        '
          </tbody>
        </table>
        ';
echo $result;
if($requisition_id == 0)
{
	?>
	<div class="row">
		<div class="center-align">

			<a class="btn btn-xs btn-success" onclick="confirm_requisition(<?php echo $requisition_id?>)"> Confirm Requisition Items </a>
		</div>
	</div>
	<?php
}

?>