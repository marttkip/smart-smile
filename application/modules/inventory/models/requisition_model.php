<?php

class Requisition_model extends CI_Model 
{
	/*
	*	Retrieve all orders
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_requisitions($table, $where, $per_page, $page)
	{
		//retrieve all orders
		$this->db->from($table);
		$this->db->select('requisition.*');
		$this->db->where($where);
		// $this->db->order_by('orders.order_id','DESC');
		// $this->db->join('store', 'store.store_id = orders.store_id','left');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

}
?>