<?php

$rs2 = $this->nurse_model->get_visit_procedure_charges($visit_id);


echo "
<table align='center' class='table table-striped table-hover table-condensed'>
	<tr>
		
		<th>Procedure</th>
		<th>Units</th>
		<th>Unit Cost</th>
		<th>Total</th>
	</tr>		
";                     
		$total= 0;  
		if(count($rs2) >0){
			foreach ($rs2 as $key1):
				$v_procedure_id = $key1->visit_charge_id;
				$procedure_id = $key1->service_charge_id;
				$visit_charge_amount = $key1->visit_charge_amount;
				$units = $key1->visit_charge_units;
				$procedure_name = $key1->service_charge_name;
				$service_id = $key1->service_id;
			
				$total= $total +($units * $visit_charge_amount);
				
				echo"
						<tr> 
							<td >".$procedure_name."</td>
							<td >
								".$units."
							</td>
							<td >".$visit_charge_amount."</td>
							<td >".number_format($units*$visit_charge_amount)."</td>
							
						</tr>	
				";
				endforeach;

		}
echo"
<tr bgcolor='#D9EDF7'>
<td></td>
<td></td>
<th>Grand Total: </th>
<th colspan='1'><div id='grand_total'>".number_format($total)."</div></th>
</tr>
 </table>
";
?>