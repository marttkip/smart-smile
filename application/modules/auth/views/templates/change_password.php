<?php 
	
	$contacts = $this->site_model->get_contacts();
	
	if(count($contacts) > 0)
	{
		$email = $contacts['email'];
		$email2 = $contacts['email'];
		$logo = $contacts['logo'];
		$company_name = $contacts['company_name'];
		$phone = $contacts['phone'];
		
		if(!empty($facebook))
		{
			$facebook = '<li class="facebook"><a href="'.$facebook.'" target="_blank" title="Facebook">Facebook</a></li>';
		}
		
	}
	else
	{
		$email = '';
		$facebook = '';
		$twitter = '';
		$linkedin = '';
		$logo = '';
		$company_name = '';
		$google = '';
	}
?>
<!doctype html>
<html class="fixed">
	<head>
        <?php echo $this->load->view('admin/includes/header', $contacts, TRUE); ?>
    </head>

	<body>
    	<!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
        <![endif]-->
        	   <input type="hidden" id="base_url" value="<?php echo site_url();?>">
        <input type="hidden" id="config_url" value="<?php echo site_url();?>">
    	<section class="body-sign">
            <div class="center-sign">
				<a href="<?php echo site_url().'login';?>" class="logo pull-left">
					<img src="<?php echo base_url().'assets/logo/'.$logo;?>" height="35" alt="<?php echo $company_name;?>" class="img-responsive" />
				</a>

				<div class="panel panel-sign">
					<div class="panel-title-sign mt-xl text-right">
						<h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-personnel mr-xs"></i> CHANGE PASSWORD</h2>
					</div>
					<div class="panel-body">
						<?php
							$login_error = $this->session->userdata('login_error');
							$this->session->unset_userdata('login_error');
							
							if(!empty($login_error))
							{
								echo '<div class="alert alert-danger">'.$login_error.'</div>';
							}
						?>
							<form action="<?php echo site_url().$this->uri->uri_string();?>" method="post">
                        	
                                <div class="form-group mb-lg">
                                    <label>CURRENT PASSWORD</label>
                                    <div class="input-group input-group-icon">
                                        <input name="current_password" type="password" class="form-control input-lg" value="" autocomplete="off" />
                                        <span class="input-group-addon">
                                            <span class="icon icon-lg">
                                                <i class="fa fa-envelope"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group mb-lg">
                                    <label>NEW PASSWORD</label>
                                    <div class="input-group input-group-icon">
                                        <input name="new_password" type="password" id="new_password" onkeyup="check_password_strength()" class="form-control input-lg" value="" autocomplete="off" />
                                        <span class="input-group-addon">
                                            <span class="icon icon-lg">
                                                <i class="fa fa-envelope"></i>
                                            </span>
                                        </span>
                                    </div>
                                    <small id="emailHelp" class="form-text text-muted">
                                    	<div id="department_services">
											
										</div>
                                    </small>
                                </div>
                                <div class="form-group mb-lg">
                                    <label>CONFIRM NEW PASSWORD</label>
                                    <div class="input-group input-group-icon">
                                        <input name="confirm_new_password" id="confirm_new_password" onkeyup="check_password_match()" type="password" class="form-control input-lg" value="" autocomplete="off" />
                                        <span class="input-group-addon">
                                            <span class="icon icon-lg">
                                                <i class="fa fa-envelope"></i>
                                            </span>
                                        </span>
                                    </div>
                                    <small id="emailHelp" class="form-text text-muted">
                                    	<div id="department_services_new">
											
										</div>
                                    </small>
                                </div>
								
							

							<div class="row">
								
								<div class="col-sm-4 text-right">
									<button type="submit" class="btn btn-primary hidden-xs" id="password_button" style="display:none;">CHANGE PASSWORD</button>
									<button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">CHANGE PASSWORD</button>
								</div>
							</div>

						</form>
					</div>
				</div>

				<p class="text-center text-muted mt-md mb-md">&copy; Copyright <?php echo date('Y');?>. All Rights Reserved.</p>
			</div>
		</section>
		<!-- end: page -->
        		
		<!-- Vendor -->
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-cookie/jquery.cookie.js"></script>
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/javascripts/theme.init.js"></script>

		 <?php

        $date = date('Y-m-d H:i:s');

        $time = time();
        ?>

        <script type="text/javascript">
        	function check_password_strength()
        	{
        	
	            var config_url = $('#config_url').val();
	             var new_password = $('#new_password').val();
	            var url = config_url+"auth/confirm_strength";
	            // alert(url);
	            $.ajax({
	            type:'POST',
	            url: url,
	            data:{new_password: new_password},
	            dataType: 'text',
	            // processData: false,
	            // contentType: false,
	            success:function(data){
	              var data = jQuery.parseJSON(data);
	              // alert(data);
	              $( "#department_services" ).html( data );

	              var myTarget2 = document.getElementById("password_button");
	              myTarget2.style.display = 'none';
	             
	             

	            },
	            error: function(xhr, status, error) {
	            alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

	            }
	            });
        	}

        	function check_password_match()
        	{
        	
	            var config_url = $('#config_url').val();
	             var new_password = $('#new_password').val();
	             var confirm_new_password = $('#confirm_new_password').val();
	            var url = config_url+"auth/confirm_new_password";
	            // alert(current_page);
	            $.ajax({
	            type:'POST',
	            url: url,
	            data:{new_password: new_password,confirm_new_password:confirm_new_password},
	            dataType: 'text',
	            // processData: false,
	            // contentType: false,
	            success:function(data){
	              var data = jQuery.parseJSON(data);
	              // alert(data);

	              if(data.message == 'success')
	              {
	              	$( "#department_services_new" ).html( data.result );
	              	var myTarget2 = document.getElementById("password_button");
	              	myTarget2.style.display = 'block';
	              }
	              else
	              {
	              	$( "#department_services_new" ).html( data.result );
	              	var myTarget2 = document.getElementById("password_button");
	              	myTarget2.style.display = 'none';
	              }
	              
	             
	             

	            },
	            error: function(xhr, status, error) {
	            alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

	            }
	            });
        	}
        </script>
      
	</body>
</html>
