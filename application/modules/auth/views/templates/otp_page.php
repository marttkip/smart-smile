<?php 
	
	$contacts = $this->site_model->get_contacts();
	
	if(count($contacts) > 0)
	{
		$email = $contacts['email'];
		$email2 = $contacts['email'];
		$logo = $contacts['logo'];
		$company_name = $contacts['company_name'];
		$phone = $contacts['phone'];
		
		if(!empty($facebook))
		{
			$facebook = '<li class="facebook"><a href="'.$facebook.'" target="_blank" title="Facebook">Facebook</a></li>';
		}
		
	}
	else
	{
		$email = '';
		$facebook = '';
		$twitter = '';
		$linkedin = '';
		$logo = '';
		$company_name = '';
		$google = '';
	}
?>
<!doctype html>
<html class="fixed">
	<head>
        <?php echo $this->load->view('admin/includes/header', $contacts, TRUE); ?>
    </head>

	<body>
    	<!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
        <![endif]-->
       	<input type="hidden" id="base_url" value="<?php echo site_url();?>">
        <input type="hidden" id="config_url" value="<?php echo site_url();?>">
    	<section class="body-sign">
            <div class="center-sign">
				<a href="<?php echo site_url().'login';?>" class="logo pull-left">
					<img src="<?php echo base_url().'assets/logo/'.$logo;?>" height="35" alt="<?php echo $company_name;?>" class="img-responsive" />
				</a>

				<div class="panel panel-sign">
					<div class="panel-title-sign mt-xl text-right">
						<h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-personnel mr-xs"></i> CONFIRM OTP</h2>
					</div>
					<div class="panel-body">
						<?php
							$login_error = $this->session->userdata('login_error');
							$this->session->unset_userdata('login_error');
							
							if(!empty($login_error))
							{
								echo '<div class="alert alert-danger">'.$login_error.'</div>';
							}
						?>
							<form action="<?php echo site_url().$this->uri->uri_string();?>" method="post">
                        	
                                <div class="form-group mb-lg">
                                    <label>OTP CODE</label>
                                    <div class="input-group input-group-icon">
                                        <input name="otp_code" type="text" class="form-control input-lg" value="" autocomplete="off" />
                                        <span class="input-group-addon">
                                            <span class="icon icon-lg">
                                                <i class="fa fa-envelope"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
								
							

							<div class="row">
								
								<div class="col-sm-4 text-right">
									<button type="submit" class="btn btn-primary hidden-xs">VERIFY OTP</button>
									<button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">VERIFY OTP</button>
								</div>
							</div>

						</form>
					</div>
				</div>

				<p class="text-center text-muted mt-md mb-md">&copy; Copyright <?php echo date('Y');?>. All Rights Reserved.</p>
			</div>
		</section>
		<!-- end: page -->
        		
		<!-- Vendor -->
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-cookie/jquery.cookie.js"></script>
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/javascripts/theme.init.js"></script>

		 <?php

        $date = date('Y-m-d H:i:s');

        $time = time();
        ?>

        <script type="text/javascript">
        		countdown();
        	
				// var endTime = new Date().setTime(1362178800000);
				var items = <?php echo $time?>;
			
				var endTime = new Date().setTime();

				var currentTime = new Date().getTime();

				var remainingTime = endTime - currentTime;

				

				//var mins = 5;
				var mins = Math.floor((remainingTime/1000)/60);

				// calculate the seconds (don't change this! unless time progresses at a different speed for you...)
				//var secs = mins * 60;
				var secs = Math.floor(remainingTime/1000);
				function countdown() {
					setTimeout('Decrement()',1000);
				}
				function Decrement() {
					if (document.getElementById) {
						if(endTime < currentTime){
							alert('Finished');
							return;
						}
						minutes = document.getElementById("minutes");
						seconds = document.getElementById("seconds");
						// if less than a minute remaining
						if (seconds < 59) {
							seconds.value = secs;
						} else {
							minutes.value = getminutes();
							seconds.value = getseconds();
						}
						secs--;
						setTimeout('Decrement()',1000);
					}
				}
				function getminutes() {
					// minutes is seconds divided by 60, rounded down
					mins = Math.floor(secs / 60);
					return mins;
				}
				function getseconds() {
					// take mins remaining (as seconds) away from total seconds remaining
					return secs-Math.round(mins *60);
				}

        </script>
	</body>
</html>
