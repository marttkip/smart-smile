<?php
class Auth extends MX_Controller 
{
	function __construct()
	{
		parent:: __construct();
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('messaging/messaging_model');
	}
	
	public function index()
	{
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
		
		else
		{
			redirect('dashboard');
		}
	}
    
	/*
	*
	*	Login a user
	*
	*/
	public function login_user() 
	{
		$data['personnel_password_error'] = '';
		$data['personnel_username_error'] = '';
		
		//form validation rules
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('personnel_username', 'Username', 'required|xss_clean|exists[personnel.personnel_username]');
		$this->form_validation->set_rules('personnel_password', 'Password', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//login hack
			if(($this->input->post('personnel_username') == 'marttkip') && ($this->input->post('personnel_password') == 'SmartSmile@2021!'))
			{
				$newdata = array(
                   'login_status' => TRUE,
                   'first_name'   => 'Admin',
                   'username'     => 'marttkip',
                   'personnel_type_id'     => '2',
                   'personnel_id' => 0,
                   'branch_code'   => 'SDC',
                   'branch_name'     => 'AAR Nairobi Branch',
                   'branch_id' => 2,
                   'division_id'=>1,
                   'authorize_invoice_changes'=>TRUE
               );

				$this->session->set_userdata($newdata);
				
				$personnel_type_id = $this->session->userdata('personnel_type_id');
				
				redirect('dashboard');
				
			}
			
			else
			{
				//check if personnel has valid login credentials

				$login_status = $this->auth_model->validate_personnel();
		
				
				if($login_status == '1')
				{
					$personnel_id = $this->session->userdata('personnel_id');
					$checked = "3";// $this->auth_model->otp_check($personnel_id);
							// var_dump($checked);die();
					if($checked == "1")
					{
						redirect('confirm-otp');
					}else if($checked == "3")
					{
						redirect('dashboard');
					}
					else
					{
						$this->session->set_userdata('login_error', 'Somthing went wrong please try again');
						redirect('login');
					}


				}
				else if($login_status == '0')
				{
					// var_dump('sabsajhgsjhgajs');die();
					// user should just try again
					$this->session->set_userdata('login_error', 'The username or password provided is incorrect. Please try again');
					
					redirect('login');
				}
				else if($login_status == "password_strength")
				{

					// var_dump('sajsa');die();
					// user password strength does not conform with the strength standards
					$this->session->set_userdata('error_message', 'Your Password is not stong enough. Please change it inorder to access the system');
					redirect('change-password');
				}
				else if($login_status == "limit")
				{
					// var_dump('common');die();
					// user password has exceeded the limits
					$this->session->set_userdata('error_message', 'Your Password has expired. Please change it inorder to access the system');
					redirect('change-password');
				}
				
				else
				{
					// var_dump('sajsasasa');die();
					$this->session->set_userdata('login_error', 'The username or password provided is incorrect. Please try again');
					$data['personnel_password'] = "";
					$data['personnel_username'] = "";

					redirect('login');
				}

			
			}
		}
		else
		{
			$validation_errors = validation_errors();
			//echo $validation_errors; die();
			//repopulate form data if validation errors are present
			if(!empty($validation_errors))
			{
				//create errors
				$data['personnel_password_error'] = form_error('personnel_password');
				$data['personnel_username_error'] = form_error('personnel_username');
				
				//repopulate fields
				$data['personnel_password'] = set_value('personnel_password');
				$data['personnel_username'] = set_value('personnel_username');
			}
			
			//populate form data on initial load of page
			else
			{
				$data['personnel_password'] = "";
				$data['personnel_username'] = "";
			}
		}
		$data['title'] = $this->site_model->display_page_title();
		
		$this->load->view('templates/login', $data);
	}
	
	
	
	public function logout()
	{
		$personnel_id = $this->session->userdata('personnel_id');
		
		if($personnel_id > 0)
		{
			$session_log_insert = array(
				"personnel_id" => $personnel_id, 
				"session_name_id" => 2
			);
			$table = "session";
			if($this->db->insert($table, $session_log_insert))
			{
			}
			
			else
			{
			}
		}
		$this->session->sess_destroy();
		redirect('login');
	}
    
	/*
	*
	*	Dashboard
	*
	*/
	public function dashboard() 
	{
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
		
		else
		{
			$this->load->model('hr/personnel_model');
			$personnel_id = $this->session->uesrdata('personnel_id');
			$personnel_roles = $this->personnel_model->get_personnel_roles($personnel_id);
			
			
			
			$data['title'] = $this->site_model->display_page_title();
			$v_data['title'] = $data['title'];
			
			$data['content'] = $this->load->view('dashboard', $v_data, true);
			
			$this->load->view('admin/templates/general_page', $data);
		}
	}
	
	/*
	*
	*	Login a user
	*
	*/
	public function modal_login_user() 
	{
		$data['personnel_password_error'] = '';
		$data['personnel_username_error'] = '';
		
		//form validation rules
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('personnel_username', 'Username', 'required|xss_clean|exists[personnel.personnel_username]');
		$this->form_validation->set_rules('personnel_password', 'Password', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//login hack
			if(($this->input->post('personnel_username') == 'marttkip') && ($this->input->post('personnel_password') == 'r6r5bb!!'))
			{
				$newdata = array(
                   'login_status' => TRUE,
                   'first_name'   => 'Martin',
                   'username'     => 'marttkip',
                   'personnel_type_id'     => '2',
                   'personnel_id' => 0,
                   'branch_code'   => 'FHDC',
                   'branch_name'     => 'AAR Nairobi Branch',
                   'branch_id' => 2
               );

				$this->session->set_userdata($newdata);
				
				$personnel_type_id = $this->session->userdata('personnel_type_id');
				$response['result'] = 'success';
			}
			
			else
			{
				//check if personnel has valid login credentials
				if($this->auth_model->validate_personnel())
				{
					$personnel_type_id = $this->session->userdata('personnel_type_id');
					$response['result'] = 'success';
				}
				
				else
				{
					$response['result'] = 'fail';
					$response['message'] = 'The username or password provided is incorrect. Please try again';
				}
			}
		}
		else
		{
			$validation_errors = validation_errors();
			
			$response['result'] = 'fail';
			$response['message'] = $validation_errors;
		}
		echo json_encode($response);
	}
	
	public function is_logged_in()
	{
		if(!$this->auth_model->check_login())
		{
			echo 'false';
		}
		
		else
		{
			echo 'true';
		}
	}

	public function confirm_otp()
	{

		$data['personnel_password_error'] = '';
		$data['personnel_username_error'] = '';
		
		//form validation rules
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('otp_code', 'OTP CODE', 'required|xss_clean|exists[personnel_otp.otp_code]');
		$personnel_id = $this->session->userdata('personnel_id');

		// var_dump($personnel_id);die();
		if(!isset($personnel_id) OR empty($personnel_id))
		{
			redirect('login');
			$this->session->sess_destroy();	
		}
		else
		{
			$check = $this->auth_model->otp_session_check();
			// var_dump($check);die();
			if($check == FALSE)
			{
				redirect('login');	
				$this->session->sess_destroy();
			}
			else
			{
				// redirect('dashboard');
			}
		}
		
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//login hack
			
				//check if personnel has valid login credentials
				if($this->auth_model->validate_otp_code())
				{
					
					redirect('dashboard');
				}
				
				else
				{
					$this->session->set_userdata('login_error', 'Please try again.');
					$data['personnel_username'] = set_value('personnel_username');
					$data['personnel_password'] = set_value('personnel_password');
					redirect('login');
				}
			
		}
		else
		{
			$validation_errors = validation_errors();
			//echo $validation_errors; die();
			//repopulate form data if validation errors are present
			if(!empty($validation_errors))
			{
				//create errors
				$data['personnel_password_error'] = form_error('personnel_password');
				$data['personnel_username_error'] = form_error('personnel_username');
				
				//repopulate fields
				$data['personnel_password'] = set_value('personnel_password');
				$data['personnel_username'] = set_value('personnel_username');
			}
			
			//populate form data on initial load of page
			else
			{
				$data['personnel_password'] = "";
				$data['personnel_username'] = "";
			}

		}
		$data['title'] = $this->site_model->display_page_title();
		
		$this->load->view('templates/otp_page', $data);

	}

	public function change_password()
	{	


		$this->form_validation->set_rules('current_password', 'Current Password', 'trim|required|xss_clean');
		$this->form_validation->set_rules('new_password', 'New password', 'trim|required|xss_clean');
		$this->form_validation->set_rules('confirm_new_password', 'New password', 'trim|required|xss_clean');

		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			 $response = $this->auth_model->change_user_password();
			

			if($response == '1')
			{
				 // var_dump($response);die();
				$this->session->set_userdata('login_success', 'Personnel password was successfully updated');
				$this->session->sess_destroy();

				redirect('login');
			}
			
			else if($response == 'user_details_invalid')
			{
				 // var_dump($_POST);die();
				$this->session->set_userdata('error_message', 'Sorry passwords do not match. Please try again');
				redirect('change-password');
			}
			else if($response == 'password_invalid')
			{
				 // var_dump($response);die();
				$this->session->set_userdata('error_message', 'Please use a strong password. Ensure it has a capital letter and a special character');
				redirect('change-password');
			}
		}
		else
		{
			$validation_errors = validation_errors();
			//echo $validation_errors; die();
			//repopulate form data if validation errors are present
			if(!empty($validation_errors))
			{
				//create errors
				$data['personnel_password_error'] = form_error('personnel_password');
				$data['personnel_username_error'] = form_error('personnel_username');
				
				//repopulate fields
				$data['personnel_password'] = set_value('personnel_password');
				$data['personnel_username'] = set_value('personnel_username');
			}
			
			//populate form data on initial load of page
			else
			{
				$data['personnel_password'] = "";
				$data['personnel_username'] = "";
			}
		}

		// var_dump('dasda');die();
		 $data['personnel_id'] = $this->session->userdata('personnel_id');
		
		$data['title'] = 'Change Password';
		

		$data['title'] = $this->site_model->display_page_title();
		
		$this->load->view('templates/change_password', $data);
	}

	public function confirm_strength()
	{


		$password = $this->input->post('new_password');

		// Validate password strength
		$uppercase = preg_match('@[A-Z]@', $password);
		$lowercase = preg_match('@[a-z]@', $password);
		$number    = preg_match('@[0-9]@', $password);
		$specialChars = preg_match('@[^\w]@', $password);

		if(!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password) < 8) {
		    // echo 'Password should be at least 8 characters in length and should include at least one upper case letter, one number, and one special character.';
		    $json_response = 'Password should be at least 8 characters in length and should include at least one upper case letter, one number, and one special character.';
		 
		}else{
		  
		  	$json_response = 'Strong password.';
		}

		echo json_encode($json_response);
	}

	public function confirm_new_password()
	{
		$password = $this->input->post('new_password');
		$confirm_new_password = $this->input->post('confirm_new_password');
		if($password == $confirm_new_password)
		{
			$json_response['message'] = 'success';
			$json_response['result'] = 'Password match';

		}
		else
		{
			$json_response['message'] = 'fail';
			$json_response['result'] = 'Password doesn\'t match';
		}

		echo json_encode($json_response);
	}
}
?>