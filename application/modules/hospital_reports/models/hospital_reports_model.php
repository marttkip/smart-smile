<?php

class Hospital_reports_model extends CI_Model 
{


	public function get_all_patients_invoices($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
	
		$this->db->select('v_transactions_by_date.*, patients.*, payment_method.*, personnel.personnel_fname, personnel.personnel_onames,visit_invoice.visit_invoice_number,branch.branch_name,branch.branch_code,visit.visit_id,visit_invoice.visit_invoice_id,visit_invoice.preauth_status');
		$this->db->join('patients', 'patients.patient_id = v_transactions_by_date.patient_id', 'left');
		$this->db->join('payment_method', 'payment_method.payment_method_id = v_transactions_by_date.payment_method_id', 'left');
		$this->db->join('payments', 'payments.payment_id = v_transactions_by_date.transaction_id', 'left');
		$this->db->join('visit', 'visit.visit_id = visit_invoice.visit_id', 'left');
		$this->db->join('personnel', 'visit.personnel_id = personnel.personnel_id', 'left');
		$this->db->join('branch', 'branch.branch_id = v_transactions_by_date.branch_id', 'left');

		$this->db->where($where);
		$this->db->order_by('v_transactions_by_date.created_at','DESC');
		$this->db->group_by('v_transactions_by_date.reference_code');


		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}



	
	public function get_payment_methods()
	{
		$this->db->select('*');
		$query = $this->db->get('payment_method');
		
		return $query;
	}

	public function get_all_active_branches()
	{
		//retrieve all users
		$this->db->from('branch');
		$this->db->where('branch_status = 1');
		$this->db->order_by('branch_name','ASC');
		$query = $this->db->get();
		
		return $query;
	}

	function export_debtors()
	{
		$this->load->library('excel');
		
		
		$where = 'v_transactions_by_date.transactionCategory = "Revenue" AND visit_invoice.visit_invoice_id = v_transactions_by_date.transaction_id';
		
		$table = 'v_transactions_by_date,visit_invoice';
		$visit_search = $this->session->userdata('debtors_search_query');
		// var_dump($visit_search);die();
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		
			
			
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'" ';
			$where .= ' AND v_transactions_by_date.transaction_date = "'.date('Y-m-d').'" ';
		}
		

		$branch_session = $this->session->userdata('branch_id');

		if($branch_session > 0)
		{
			$where .= ' AND v_transactions_by_date.branch_id = '.$branch_session;
			// $where .= $visit_search;
		
		}
	
		$this->db->select('v_transactions_by_date.*, patients.*, payment_method.*, personnel.personnel_fname, personnel.personnel_onames,visit_invoice.visit_invoice_number,branch.branch_name,branch.branch_code,visit.visit_id,visit_invoice.visit_invoice_id');
		$this->db->join('patients', 'patients.patient_id = v_transactions_by_date.patient_id', 'left');
		$this->db->join('payment_method', 'payment_method.payment_method_id = v_transactions_by_date.payment_method_id', 'left');
		$this->db->join('payments', 'payments.payment_id = v_transactions_by_date.transaction_id', 'left');
		$this->db->join('visit', 'visit.visit_id = visit_invoice.visit_id', 'left');
		$this->db->join('personnel', 'visit.personnel_id = personnel.personnel_id', 'left');
		$this->db->join('branch', 'branch.branch_id = v_transactions_by_date.branch_id', 'left');

		$this->db->where($where);
		$this->db->order_by('v_transactions_by_date.created_at','DESC');
		$this->db->group_by('v_transactions_by_date.reference_code');

		$visits_query = $this->db->get($table);
		
		// var_dump($visits_query);die();
		
		$title = 'Transactions Export '.date('jS M Y H:i a',strtotime(date('Y-m-d H:i:s')));
		$col_count = 0;
		
		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/
			$row_count = 0;
			$report[$row_count][$col_count] = '#';
			$col_count++;
			$report[$row_count][$col_count] = 'Invoice Date';
			$col_count++;
			$report[$row_count][$col_count] = 'Patient No.';
			$col_count++;
			$report[$row_count][$col_count] = 'Patient';
			$col_count++;
			$report[$row_count][$col_count] = 'Category';
			$col_count++;
			$report[$row_count][$col_count] = 'Doctor';
			$col_count++;
			$report[$row_count][$col_count] = 'Invoice No.';
			$col_count++;
			$report[$row_count][$col_count] = 'Branch Code';
			$col_count++;
			$report[$row_count][$col_count] = 'Invoice Amount';
			$col_count++;
			$report[$row_count][$col_count] = 'Payments';
			$col_count++;
			$report[$row_count][$col_count] = 'Balance';
			$col_count++;	
			//display all patient data in the leftmost columns
			$total_waiver = 0;
			$total_payments = 0;
			$total_invoice = 0;
			$total_balance = 0;
			$total_rejected_amount = 0;
			$total_cash_balance = 0;
			$total_insurance_payments =0;
			$total_insurance_invoice =0;
			$total_payable_by_patient = 0;
			$total_payable_by_insurance = 0;
			$total_debit_notes = 0;
			$total_credit_notes= 0;
			foreach ($visits_query->result() as $row)
			{
				$row_count++;
				$total_invoiced = 0;
				$visit_date = date('jS M Y',strtotime($row->transaction_date));
				$visit_time = date('H:i a',strtotime($row->visit_time));
				if($row->visit_time_out != '0000-00-00 00:00:00')
				{
					$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
				}
				else
				{
					$visit_time_out = '-';
				}
				
				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$personnel_id = $row->personnel_id;
				$dependant_id = $row->dependant_id;
				$strath_no = $row->strath_no;
				$visit_type_id = $row->visit_type;
				$patient_number = $row->patient_number;
				$visit_type = $row->visit_type;
				$visit_table_visit_type = $visit_type;
				$patient_table_visit_type = $visit_type_id;
				$rejected_amount = $row->amount_rejected;
				$visit_invoice_number = $row->visit_invoice_number;
				$visit_invoice_id = $row->visit_invoice_id;
				$parent_visit = $row->parent_visit;
				$branch_code = $row->branch_code;

				if(empty($rejected_amount))
				{
					$rejected_amount = 0;
				}
				// $coming_from = $this->reception_model->coming_from($visit_id);
				// $sent_to = $this->reception_model->going_to($visit_id);
				$visit_type_name = $row->payment_type_name;
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
				$patient_date_of_birth = $row->patient_date_of_birth;

				$name = $patient_othernames.' '.$patient_surname;

				$doctor = $row->personnel_fname;
				$count++;
				$invoice_total = $row->dr_amount;
				$payments_value = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
				$credit_note = $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);

				$invoice_total -= $credit_note;
				$balance  = $this->accounts_model->balance($payments_value,$invoice_total);

				$total_payable_by_patient += $invoice_total;
				$total_payments += $payments_value;
				$total_balance += $balance;


				$report[$row_count][$col_count] = $count;
				$col_count++;
				$report[$row_count][$col_count] = $visit_date;
				$col_count++;
				$report[$row_count][$col_count] = $patient_number;
				$col_count++;
				$report[$row_count][$col_count] = ucwords(strtolower($name));
				$col_count++;
				$report[$row_count][$col_count] = $visit_type_name;
				$col_count++;
				$report[$row_count][$col_count] = $doctor;
				$col_count++;
				$report[$row_count][$col_count] = $visit_invoice_number;
				$col_count++;
				$report[$row_count][$col_count] = $branch_code;
				$col_count++;
				$report[$row_count][$col_count] = number_format($invoice_total,2);
				$col_count++;
				$report[$row_count][$col_count] = (number_format($payments_value,2));
				$col_count++;
				$report[$row_count][$col_count] = (number_format($balance,2));
				$col_count++;				
				
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}




	// payments Reports


	/*
	*	Retrieve visits
	*	@param string $table
	* 	@param string $where
	*	@param int $per_page
	* 	@param int $page
	*
	*/
	public function get_all_payments($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		// $this->db->from($table);
		// $this->db->select('visit.*, (visit.visit_time_out - visit.visit_time) AS waiting_time, patients.*, visit_type.visit_type_name, payments.*, payment_method.*, personnel.personnel_fname, personnel.personnel_onames, service.service_name,visit_invoice.visit_invoice_number,visit_invoice.created AS invoice_date');
		// $this->db->join('personnel', 'payments.payment_created_by = personnel.personnel_id', 'left');
		// $this->db->join('service', 'payments.payment_service_id = service.service_id', 'left');
		// $this->db->where($where);
		// $this->db->order_by('payments.time','DESC');
		// $query = $this->db->get('', $per_page, $page);

		//retrieve all users
		$this->db->from($table);
		$this->db->select('v_transactions_by_date.*, patients.*, payment_method.*, personnel.personnel_fname, personnel.personnel_onames,visit_invoice.visit_invoice_number,branch.branch_name,branch.branch_code,visit_invoice.created AS invoice_date');
		// $this->db->join('patients', 'patients.patient_id = v_transactions_by_date.patient_id', 'left');
		$this->db->join('payment_method', 'payment_method.payment_method_id = v_transactions_by_date.payment_method_id', 'left');
		$this->db->join('payments', 'payments.payment_id = v_transactions_by_date.transaction_id', 'left');
		$this->db->join('personnel', 'payments.payment_created_by = personnel.personnel_id', 'left');
		$this->db->join('visit_invoice', 'visit_invoice.visit_invoice_id = v_transactions_by_date.reference_id', 'left');
		$this->db->join('branch', 'branch.branch_id = v_transactions_by_date.branch_id', 'left');

		$this->db->where($where);
		$this->db->order_by('v_transactions_by_date.created_at','DESC');
		$query = $this->db->get('', $per_page, $page);


		
		return $query;
	}
	


	/*
	*	Retrieve total revenue
	*
	*/
	public function get_total_cash_collection($where, $table, $page = NULL)
	{
		//payments
		$table_search = $this->session->userdata('all_transactions_tables');
		
		if($page != 'cash')
		{
			$where .= ' AND visit.visit_id = payments.visit_id AND payments.cancel = 0';
		}
		if((!empty($table_search)) || ($page == 'cash'))
		{
			$this->db->from($table);
		}
		
		else
		{
			$this->db->from($table.', payments');
		}
		$this->db->select('SUM(payments.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		
		$cash = $query->row();
		$total_paid = $cash->total_paid;
		if($total_paid > 0)
		{
		}
		
		else
		{
			$total_paid = 0;
		}
		
		return $total_paid;
	}
	
	/*
	*	Retrieve total revenue
	*
	*/
	public function get_normal_payments($where, $table, $page = NULL)
	{
		if($page != 'cash')
		{
			$where .= ' AND visit.visit_id = payments.visit_id AND payments.cancel = 0';
		}
		//payments
		$table_search = $this->session->userdata('all_transactions_tables');
		if((!empty($table_search)) || ($page == 'cash'))
		{
			$this->db->from($table);
		}
		
		else
		{
			$this->db->from($table.', payments');
		}
		$this->db->select('*');
		$this->db->where($where);
		$query = $this->db->get();
		
		return $query;
	}
	/*
	*	Export Transactions
	*
	*/
	function export_cash_report()
	{
		$this->load->library('excel');
		
		$branch_code = $this->session->userdata('search_branch_code');
		
		if(empty($branch_code))
		{
			$branch_code = $this->session->userdata('branch_code');
		}
		
		$this->db->where('branch_code', $branch_code);
		$query = $this->db->get('branch');
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$branch_name = $row->branch_name;
		}
		
		else
		{
			$branch_name = '';
		}
		$v_data['branch_name'] = $branch_name;
		
		$where = 'v_transactions_by_date.transactionCategory = "Revenue Payment" AND patients.patient_id = v_transactions_by_date.patient_id';
		
		$table = 'v_transactions_by_date,patients';


		$visit_search = $this->session->userdata('visit_payments');
		
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		else
		{
			$where .=' AND v_transactions_by_date.transaction_date = "'.date('Y-m-d').'"';
		}

		$branch_session = $this->session->userdata('branch_id');

		if($branch_session > 0)
		{
			$where .= ' AND v_transactions_by_date.branch_id = '.$branch_session;
			// $where .= $visit_search;
		
		}

		$this->db->select('v_transactions_by_date.*, patients.*, payment_method.*, personnel.personnel_fname, personnel.personnel_onames,visit_invoice.visit_invoice_number,branch.branch_name,branch.branch_code,visit_invoice.created AS invoice_date');
		$this->db->join('payment_method', 'payment_method.payment_method_id = v_transactions_by_date.payment_method_id', 'left');
		$this->db->join('payments', 'payments.payment_id = v_transactions_by_date.transaction_id', 'left');
		$this->db->join('personnel', 'payments.payment_created_by = personnel.personnel_id', 'left');
		$this->db->join('visit_invoice', 'visit_invoice.visit_invoice_id = v_transactions_by_date.reference_id', 'left');
		$this->db->join('branch', 'branch.branch_id = v_transactions_by_date.branch_id', 'left');

		$this->db->where($where);
		$this->db->order_by('v_transactions_by_date.transaction_date','DESC');


		$query = $this->db->get($table);
		
		$title = 'Cash report '.date('jS M Y H:i a',strtotime(date('Y-m-d H:i:s')));
		$col_count = 0;
		
		if($query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/
			$row_count = 0;
			$report[$row_count][$col_count] = '#';
			$col_count++;
			$report[$row_count][$col_count] = 'Payment Type';
			$col_count++;
			$report[$row_count][$col_count] = 'Payment Date';
			$col_count++;
			$report[$row_count][$col_count] = 'Invoice Date';
			$col_count++;
			$report[$row_count][$col_count] = 'Patient';
			$col_count++;
			$report[$row_count][$col_count] = 'Payment Method';
			$col_count++;
			$report[$row_count][$col_count] = 'Type';
			$col_count++;
			$report[$row_count][$col_count] = 'Amount';
			$col_count++;
			$report[$row_count][$col_count] = 'Receipt No';
			$col_count++;
			$report[$row_count][$col_count] = 'Invoice Number';
			$col_count++;
			$report[$row_count][$col_count] = 'Branch Code';
			$col_count++;
			$report[$row_count][$col_count] = 'Recorded by';
			$col_count++;
			$current_column = $col_count ;
			
			foreach ($query->result() as $row)
			{
				$count++;
				$row_count++;
				$col_count = 0;
				
				$total_invoiced = 0;
				$payment_date = $row->transaction_date;
					$payment_created = date('jS M Y',strtotime($row->transaction_date));
				$time = date('H:i a',strtotime($row->created_at));
				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$personnel_id = $row->personnel_id;
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
				$payment_method = $row->payment_method;
				$cr_amount = $row->cr_amount;
				$transaction_code = $row->transaction_code;
				$reference_code = $row->reference_code;
				$transaction_description = $row->transaction_description;
				$transactionClassification = $row->transactionClassification;
				$visit_invoice_number = $row->visit_invoice_number;
				$branch_code = $row->branch_code;
				$invoice_date = $row->invoice_date;
				$created_by = $row->personnel_fname.' '.$row->personnel_onames;
				
				if(!empty($invoice_date))
				{
					$invoice = date('jS M Y',strtotime($row->invoice_date));
				}
				else
				{
					$invoice ='';
				}
				

				if($payment_date == $invoice_date)
				{
					$type = 'Period Payment';
				}
				else
				{
					$type = 'Debt repayment';
				}


				$report[$row_count][$col_count] = $count;
				$col_count++;
				$report[$row_count][$col_count] = $type;
				$col_count++;
				$report[$row_count][$col_count] = $payment_created;
				$col_count++;
				$report[$row_count][$col_count] = $invoice;
				$col_count++;
				$report[$row_count][$col_count] = ucwords(strtolower($patient_surname));
				$col_count++;
				$report[$row_count][$col_count] = $payment_method;
				$col_count++;
				$report[$row_count][$col_count] = $transactionClassification;
				$col_count++;
				$report[$row_count][$col_count] = number_format($cr_amount, 2);
				$col_count++;
				$report[$row_count][$col_count] = $reference_code;
				$col_count++;
				$report[$row_count][$col_count] = $visit_invoice_number;
				$col_count++;
				$report[$row_count][$col_count] = $branch_code;
				$col_count++;
				$report[$row_count][$col_count] = $created_by;
				$col_count++;
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}

	public function get_general_report_data($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('v_transactions_by_date.*, patients.*,branch.branch_name,branch.branch_code');
		// $this->db->join('patients', 'patients.patient_id = v_transactions_by_date.patient_id', 'left');
		$this->db->join('branch', 'branch.branch_id = v_transactions_by_date.branch_id', 'left');

		$this->db->where($where);
		$this->db->order_by('v_transactions_by_date.created_at','ASC');
		$this->db->group_by('v_transactions_by_date.transaction_id');

		


		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	public function export_general_report()
	{

		$this->load->library('excel');
		
		
		$branch_session = $this->session->userdata('branch_id');
		if($branch_session == 0)
		{
			$branch = '';
		}
		else
		{
			$branch = ' AND v_transactions_by_date.branch_id = '.$branch_session;
		}

		$where = '(v_transactions_by_date.transactionCategory = "Revenue" OR v_transactions_by_date.transactionCategory = "Revenue Payment")'.$branch;
		
		$table = 'v_transactions_by_date';


		$visit_search = $this->session->userdata('general_report_search');
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		
			
			
		}
		else
		{
			$where .= ' AND v_transactions_by_date.transaction_date = "'.date('Y-m-d').'" ';

		}

		$this->db->select('v_transactions_by_date.*, patients.*,branch.branch_name,branch.branch_code');
		$this->db->join('patients', 'patients.patient_id = v_transactions_by_date.patient_id', 'left');
		$this->db->join('branch', 'branch.branch_id = v_transactions_by_date.branch_id', 'left');
		$this->db->where($where);
		$this->db->order_by('v_transactions_by_date.created_at','ASC');

		$visits_query = $this->db->get($table);
		
		$title = 'Transactions Export '.date('jS M Y H:i a',strtotime(date('Y-m-d H:i:s')));
		$col_count = 0;
		
		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/

				
			$row_count = 0;
			$report[$row_count][$col_count] = '#';
			$col_count++;
			$report[$row_count][$col_count] = 'TYPE';
			$col_count++;
			$report[$row_count][$col_count] = 'DATE';
			$col_count++;
			$report[$row_count][$col_count] = 'INVOICE/RECEIPT NO.';
			$col_count++;
			$report[$row_count][$col_count] = 'PATIENTS\'s NAME';
			$col_count++;
			$report[$row_count][$col_count] = 'VISIT TYPE';
			$col_count++;
			$report[$row_count][$col_count] = 'PROCEDURES.';
			$col_count++;
			$report[$row_count][$col_count] = 'AMOUNT';
			$col_count++;
			$report[$row_count][$col_count] = 'PAID BY';
			$col_count++;
			$report[$row_count][$col_count] = 'TRANSACTION NO';
			$col_count++;
			$report[$row_count][$col_count] = 'BRANCH';
			$col_count++;	
			//display all patient data in the leftmost columns
			$total_waiver = 0;
			$total_payments = 0;
			$total_invoice = 0;
			$total_balance = 0;
			$total_rejected_amount = 0;
			$total_cash_balance = 0;
			$total_insurance_payments =0;
			$total_insurance_invoice =0;
			$total_payable_by_patient = 0;
			$total_payable_by_insurance = 0;
			$total_debit_notes = 0;
			$total_credit_notes= 0;
			foreach ($visits_query->result() as $row)
			{
				$row_count++;
				$count++;

				$parent_visit = $row->parent_visit;
				$branch_code = $row->branch_code;
				$visit_type_name = $row->payment_type_name;
				$transactionCategory = $row->transactionCategory;
				$transaction_date = $row->transaction_date;
				$dr_amount = $row->dr_amount;
				$cr_amount = $row->cr_amount;
				$payment_method_name = $row->payment_method_name;
				$payment_type_name = $row->payment_type_name;
				$patient_surname = $row->patient_surname;
				$reference_code = $row->reference_code;
				$transaction_code = $row->transactionCode;
				$payment_method_name = $row->payment_method_name;

				$branch_code = $row->branch_code;

				$visit_date = date('jS M Y',strtotime($row->transaction_date));

				$doctor = '';//$row->personnel_fname;
				$count++;
				// $invoice_total = $row->dr_amount;
				$payments_value = 0;//$this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
				// $balance  = $this->accounts_model->balance($payments_value,$invoice_total);

				$total_payable_by_patient += $invoice_total;
				$total_payments += $payments_value;
				$total_balance += 0;

				if($transactionCategory == "Revenue")
				{
					$amount = $dr_amount;
					$transactionCategory = "Invoice";
				}
				else if($transactionCategory == "Revenue Payment")
				{
					$amount = $cr_amount;
					$transactionCategory = "Receipt";
				}


				$report[$row_count][$col_count] = $count;
				$col_count++;
				$report[$row_count][$col_count] = $transactionCategory;
				$col_count++;
				$report[$row_count][$col_count] = $visit_date;
				$col_count++;
				$report[$row_count][$col_count] = $reference_code;
				$col_count++;
				$report[$row_count][$col_count] = ucwords(strtolower($patient_surname));
				$col_count++;
				$report[$row_count][$col_count] = $visit_type_name;
				$col_count++;
				$report[$row_count][$col_count] = '-';
				$col_count++;
				$report[$row_count][$col_count] = number_format($amount,2);
				$col_count++;
				$report[$row_count][$col_count] = ucwords(strtolower($payment_method_name));
				$col_count++;
				$report[$row_count][$col_count] = ucwords(strtolower($transaction_code));
				$col_count++;
				$report[$row_count][$col_count] = ucwords(strtolower($branch_code));
				$col_count++;			
				
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}

	public function get_all_unpaid_invoices($table, $where,$group_by=NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		// $this->db->order_by('visit_invoice.dentist_id','DESC');
		$query = $this->db->get('');
		
		return $query;
		
	}

	public function get_all_daily_sales($table, $where,$group_by=NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('patients.*, visit_type.visit_type_name,personnel.personnel_fname,personnel.personnel_onames,visit_invoice.dentist_id,visit_invoice.*,visit_charge.*,service_charge.service_charge_name,service_charge.service_charge_code,visit_invoice.dentist_id AS doctor,visit_charge.visit_charge_notes,service.service_name');
		$this->db->where($where);

		$this->db->join('service','service.service_id = service_charge.service_id','left');
		$this->db->order_by('visit_invoice.dentist_id','DESC');

		if($group_by == 1)
		{
			$this->db->group_by('visit_invoice.dentist_id');
		}
		

		$query = $this->db->get('');
		
		return $query;
		
	}

	public function get_all_other_daily_sales($table, $where,$group_by=NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('patients.*, visit_type.visit_type_name,visit_invoice.*,visit_charge.*,service_charge.service_charge_name,service_charge.service_charge_code,visit_invoice.dentist_id AS doctor,visit_charge.visit_charge_comment');
		$this->db->where($where);

		// $this->db->join('personnel','visit_invoice.dentist_id = personnel.personnel_id','left');
		$this->db->order_by('visit_invoice.dentist_id','DESC');

		if($group_by == 1)
		{
			$this->db->group_by('visit_invoice.dentist_id');
		}
		

		$query = $this->db->get('');
		
		return $query;
		
	}

	public function export_drb()
	{

		$this->load->library('excel');
		
		
		$where = 'visit_type.visit_type_id = visit_invoice.bill_to AND visit_invoice.visit_invoice_delete = 0 AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id AND patients.patient_id = visit_invoice.patient_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND visit_invoice.dentist_id = personnel.personnel_id';
		$table = 'patients, visit_type,visit_invoice,visit_charge,service_charge,personnel';
		$visit_search = $this->session->userdata('drb_search');
		// var_dump($visit_search);die();
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		
			
			
		}
		else
		{
			$where .= ' AND visit_invoice.created = "'.date('Y-m-d').'" ';
		
			$visit_payments = ' AND payments.payment_created = \''.date('Y-m-d').'\'';
			$visit_invoices = ' AND visit.visit_date = \''.date('Y-m-d').'\'';
			$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

			$this->session->set_userdata('visit_invoices', $visit_invoices);
			$this->session->set_userdata('visit_payments', $visit_payments);
		


			$where .= '';

		}
		
		
		$visits_query = $this->accounting_model->get_all_daily_sales($table, $where,1);
		// var_dump($query);die();
		
		$title = $search_title;
		$col_count = 0;
		$total_balance = 0;
		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/
			$row_count = 0;
			
			
			foreach($visits_query->result() as $value)
			{
				$row_count++;
				$total_invoiced = 0;



				$personnel_fname = $value->personnel_fname;
				$personnel_onames = $value->personnel_onames;
				$personnel_id = $value->doctor;
					

				$report[$row_count][$col_count] = '#';
				$col_count++;
				$report[$row_count][$col_count] = 'Dr. '.$personnel_fname.' '.$personnel_onames;
				$col_count++;
				$report[$row_count][$col_count] = '';
				$col_count++;
				$report[$row_count][$col_count] = '';
				$col_count++;
				$report[$row_count][$col_count] = '';
				$col_count++;
				$report[$row_count][$col_count] = '';
				$col_count++;

				$report[$row_count][$col_count] = '';
				$col_count++;

				$report[$row_count][$col_count] = '';
				$col_count++;
				$report[$row_count][$col_count] = '';
				$col_count++;
				$report[$row_count][$col_count] = '';
				$col_count++;
				$report[$row_count][$col_count] = '';
				$col_count++;


				$row_count++;

				$report[$row_count][$col_count] = '#';
				$col_count++;
				$report[$row_count][$col_count] = 'Type';
				$col_count++;
				$report[$row_count][$col_count] = 'Date';
				$col_count++;
				$report[$row_count][$col_count] = 'Num';
				$col_count++;
				$report[$row_count][$col_count] = 'Memo';
				$col_count++;
				$report[$row_count][$col_count] = 'Name';
				$col_count++;

				$report[$row_count][$col_count] = 'Item';
				$col_count++;

				$report[$row_count][$col_count] = 'Qty';
				$col_count++;
				$report[$row_count][$col_count] = 'Unit Price';
				$col_count++;
				$report[$row_count][$col_count] = 'Amount';
				$col_count++;
				$report[$row_count][$col_count] = 'Balance';
				$col_count++;

				
				// $personnel_id = $row->
				$where = 'visit_type.visit_type_id = visit_invoice.bill_to AND visit_invoice.visit_invoice_delete = 0 AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id AND patients.patient_id = visit_invoice.patient_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND visit_invoice.dentist_id = personnel.personnel_id AND visit_invoice.dentist_id ='.$personnel_id;
				$table = 'patients, visit_type,visit_invoice,visit_charge,service_charge,personnel';
				$visit_search = $this->session->userdata('drb_search');
				// var_dump($visit_search);die();
				if(!empty($visit_search))
				{
					$where .= $visit_search;
					
				}
				else
				{
					$where .= ' AND visit_invoice.created = "'.date('Y-m-d').'" ';
					$visit_payments = ' AND payments.payment_created = \''.date('Y-m-d').'\'';
					$visit_invoices = ' AND visit.visit_date = \''.date('Y-m-d').'\'';
					$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

					$this->session->set_userdata('visit_invoices', $visit_invoices);
					$this->session->set_userdata('visit_payments', $visit_payments);

					$where .= '';
				}
				
				
				$query_items = $this->accounting_model->get_all_daily_sales($table, $where);
				$total_invoiced = 0;
				if($query_items->num_rows() > 0)
				{
					foreach ($query_items->result() as $row)
					{
						$row_count++;


						$visit_date = date('d.m.Y',strtotime($row->created));
						$visit_time = date('H:i a',strtotime($row->visit_time));
						if($row->visit_time_out != '0000-00-00 00:00:00')
						{
							$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
						}
						else
						{
							$visit_time_out = '-';
						}
						
						$visit_id = $row->visit_id;
						$patient_id = $row->patient_id;
						$personnel_id = $row->doctor;
						$dependant_id = $row->dependant_id;
						$strath_no = $row->strath_no;
						$visit_type_id = $row->visit_type;
						$patient_number = $row->patient_number;
						$visit_type = $row->visit_type;
						$visit_table_visit_type = $visit_type;
						$patient_table_visit_type = $visit_type_id;
						$rejected_amount = $row->amount_rejected;
						$invoice_number = $row->invoice_number;
						$personnel_onames = $row->personnel_onames;
						$personnel_fname = $row->personnel_fname;
						$parent_visit = $row->parent_visit;

						if(empty($rejected_amount))
						{
							$rejected_amount = 0;
						}


						$visit_type_name = $row->visit_type_name;
						$patient_othernames = $row->patient_othernames;
						$patient_surname = $row->patient_surname;
						$patient_first_name = $row->patient_first_name;
						$patient_date_of_birth = $row->patient_date_of_birth;

						$service_charge_name = $row->service_charge_name;
						$service_charge_code = $row->service_charge_code;
						$visit_charge_amount = $row->visit_charge_amount;

						$visit_charge_units = $row->visit_charge_units;
						$visit_invoice_number = $row->visit_invoice_number;
						$visit_charge_notes = $row->visit_charge_notes;

						if($visit_type == 1)
						{
							$type = 'Non Insurance';
						}
						else
						{
							$type = 'Insurance';
						}

						if(!empty($visit_charge_notes))
						{
							$service_charge_name .= $visit_charge_notes;
						}
						// $payments_value = $this->accounts_model->total_payments($visit_id);

						$invoice_amount = $visit_charge_amount * $visit_charge_units;

						$total_balance += $visit_charge_amount * $visit_charge_units;
						$total_invoiced += $visit_charge_amoun * $visit_charge_units;

						$report[$row_count][$col_count] = $count;
						$col_count++;
						$report[$row_count][$col_count] = $type;
						$col_count++;
						$report[$row_count][$col_count] = $visit_date;
						$col_count++;
						$report[$row_count][$col_count] = $visit_invoice_number;
						$col_count++;
						$report[$row_count][$col_count] = $service_charge_name;
						$col_count++;
						$report[$row_count][$col_count] = $type.' - '.$visit_type_name.': '.$patient_surname.' '.$patient_othernames.' '.$patient_first_name;
						$col_count++;

						$report[$row_count][$col_count] = $visit_type_name.': '.$service_charge_code.' ('.$service_charge_name.')';
						$col_count++;

						$report[$row_count][$col_count] = $visit_charge_units;
						$col_count++;
						$report[$row_count][$col_count] = $visit_charge_amount;
						$col_count++;
						$report[$row_count][$col_count] = $invoice_amount;
						$col_count++;
						$report[$row_count][$col_count] = $total_balance;
						$col_count++;

						
					
						$count++;
						$db_personnel_id = $personnel_id;
				
					
					}
				}
				
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}

	

	public function get_days_collection()
	{
		$visit_payments = $this->session->userdata('drb_payments_search');
		$visit_type_id = $this->session->userdata('visit_type_id');
		$patient_number = $this->session->userdata('patient_number');
		$add ='';
		$table_add = '';

		if(!empty($visit_payments))
		{
			$add .= $visit_payments;
		}
		else
		{
			$add .= ' AND payments.payment_date = "'.date('Y-m-d').'" ';
		}
		// var_dump($visit_type_id);die();
		if(!empty($visit_type_id))
		{
			$add .= $visit_type_id;
		}
		if(!empty($patient_number))
		{
			$add .= $patient_number.' AND patients.patient_id = visit.patient_id';
			$table_add .= ',patients';
		}
		
		$this->db->where('cancel = 0 AND payment_item.payment_id = payments.payment_id AND payments.payment_type = 1  '.$add);
		$this->db->select('SUM(payment_item.payment_item_amount) AS total_payments');
		$query = $this->db->get('payments,payment_item'.$table_add);
		$total_payments = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_payments = $value->total_payments;
			}
		}
		return $total_payments;
	}


	public function all_payments_period()
	{
		$visit_payments = $this->session->userdata('drb_payments_search');
		$visit_type_id = $this->session->userdata('visit_type_id');
		$patient_number = $this->session->userdata('patient_number');
		$add ='';
		$table_add = '';
		if(!empty($visit_payments))
		{
			$add .= $visit_payments;
		}
		if(!empty($visit_type_id))
		{
			$add .= $visit_type_id;
		}
		if(!empty($patient_number))
		{
			$add .= $patient_number.' AND patients.patient_id = visit.patient_id';
			$table_add .= ',patients';
		}
		
		$this->db->where('cancel = 0 AND visit.visit_id = payments.visit_id AND visit.visit_delete = 0 AND payments.payment_type = 1 '.$add);
		$this->db->select('SUM(amount_paid) AS total_payments');
		$query = $this->db->get('payments,visit'.$table_add);
		$total_payments = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_payments = $value->total_payments;
			}
		}
		return $total_payments;
	}

	public function get_visit_waiver_totals()
	{
		$visit_invoices = $this->session->userdata('visit_invoices');
		$visit_type_id = $this->session->userdata('visit_type_id');
		$patient_number = $this->session->userdata('patient_number');
		$add ='';
		$table_add = '';
		if(!empty($visit_invoices))
		{
			$add .= $visit_invoices;
		}
		if(!empty($visit_type_id))
		{
			$add .= $visit_type_id;
		}
		if(!empty($patient_number))
		{
			$add .= $patient_number.' AND patients.patient_id = visit.patient_id';
			$table_add .= ',patients';
		}
		
		$this->db->where('cancel = 0 AND visit.visit_id = payments.visit_id AND visit.visit_delete = 0 AND payments.payment_type = 2 '.$add);
		$this->db->select('SUM(amount_paid) AS total_payments');
		$query = $this->db->get('payments,visit'.$table_add);
		$total_payments = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_payments = $value->total_payments;
			}
		}
		return $total_payments;
	}


	public function get_all_visit_waiver($visit_type)
	{
		$visit_invoices = $this->session->userdata('visit_invoices');
		$visit_type_id = $this->session->userdata('visit_type_id');
		$patient_number = $this->session->userdata('patient_number');
		$add ='';
		$table_add = '';
		if(!empty($visit_invoices))
		{
			$add .= $visit_invoices;
		}
		if(!empty($visit_type_id))
		{
			$add .= $visit_type_id;
		}
		if(!empty($patient_number))
		{
			$add .= $patient_number.' AND patients.patient_id = visit.patient_id';
			$table_add .= ',patients';
		}
		


		$this->db->where('cancel = 0 AND visit.visit_id = payments.visit_id AND visit.visit_delete = 0 AND payments.payment_type = 2 '.$add);
		$this->db->select('SUM(amount_paid) AS total_payments');
		$query = $this->db->get('payments,visit'.$table_add);
		$total_payments = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_payments = $value->total_payments;
			}
		}
		return $total_payments;
	}

	public function get_patients_visits($patient_visit)
	{
		$visit_invoices = $this->session->userdata('drb_visit_search');
		$visit_type_id = $this->session->userdata('visit_type_id');
		$patient_number = $this->session->userdata('patient_number');
		$add ='';
		$table_add = '';
		if(!empty($visit_invoices))
		{
			$add .= $visit_invoices;
		}
		else if(!empty($visit_type_id))
		{
			$add .= $visit_type_id;
		}
		else if(!empty($patient_number))
		{
			$add .= '';//$patient_number;
			$table_add .='patients';
		}
		else
		{
			$add = '';
		}
		if($patient_visit == 1)
		{
			$add .= ' AND patients.patient_id = visit.patient_id';
		}
		else if($patient_visit == 0)
		{
			$add .= ' AND patients.patient_id = visit.patient_id';
		}
		$this->db->where('visit.visit_delete = 0 AND visit.close_card <> 2 '.$add);
		$this->db->select('visit.patient_id');
		$query = $this->db->get('visit,patients');
		$total_new = 0;
		$total_old = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$patient_id = $value->patient_id;
				$revisit = $value->revisit;

				
				if($revisit == 1)
				{
					$total_old +=1;
				}
				else
				{
					$total_new +=1;
				}
			}
		}
		$response['total_new'] = $total_new;
		$response['total_old'] = $total_old;

		return $response;
	}

	public function get_amount_collected($payment_method_id)
	{
		$visit_payments = $this->session->userdata('drb_payments_search');
		$visit_type_id = $this->session->userdata('visit_type_id');
		$patient_number = $this->session->userdata('patient_number');
		$add ='';
		$table_add = '';
		if(!empty($visit_payments))
		{
			$add .= $visit_payments;
		}
		else
		{
			$add .= ' AND payments.payment_date = "'.date('Y-m-d').'" ';
		}
		if(!empty($visit_type_id))
		{
			$add .= $visit_type_id;
		}
		if(!empty($patient_number))
		{
			$add .= $patient_number.' AND patients.patient_id = visit.patient_id';
			$table_add .= ',patients';
		}
		
		$this->db->where('cancel = 0 AND payment_item.payment_id = payments.payment_id AND payments.payment_type = 1 AND payment_method_id = '.$payment_method_id.' '.$add);
		$this->db->select('SUM(payment_item.payment_item_amount) AS total_payments');
		$query = $this->db->get('payments,payment_item'.$table_add);
		$total_payments = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_payments = $value->total_payments;
			}
		}
		return $total_payments;
	}

	public function get_personnel_days_payments($personnel_id,$date=NULL,$period=NULL,$type=null)
	{

		if(empty($date))
		{
			$visit_payments = $this->session->userdata('drb_payments_search');
			$visit_type_id = $this->session->userdata('visit_type_id');
			$patient_number = $this->session->userdata('patient_number');
			$add ='';
			$table_add = '';
			if(!empty($visit_payments))
			{
				$add .= $visit_payments;
			}
			else
			{
				$add .= ' AND payments.payment_date = \''.date('Y-m-d').'\'';
			}
			if(!empty($visit_type_id))
			{
				$add .= $visit_type_id;
			}
		}
		else
		{
			$add = ' AND payments.payment_date = \''.$date.'\'';
		}
		

		if($period == 1)
		{
			$searched = $this->session->userdata('monthly_turnover_searched');
			$month = $this->session->userdata('monthly_turnover_month');
			$year = $this->session->userdata('monthly_turnover_year');


			if($searched AND !empty($month) AND !empty($year))
			{
				// start date

				$add = ' AND MONTH(payments.payment_date) = "'.$month.'" AND  YEAR(payments.payment_date) = "'.$year.'" ';
			}
			else
			{
				$add = ' AND MONTH(payments.payment_date) = "'.date('m').'" AND  YEAR(payments.payment_date) = "'.date('Y').'" ';
			}
		}
		
		$adding = '';
		if(!empty($type) AND $type == 1)
		{
			$adding .= ' AND visit_invoice.bill_to = 1';
		}
		else if(!empty($type) AND $type == 2)
		{
			$adding .= ' AND visit_invoice.bill_to <> 1';
		}
		
		$this->db->where('cancel = 0 AND payment_item.payment_id = payments.payment_id AND payment_item.visit_invoice_id = visit_invoice.visit_invoice_id  AND payments.payment_type = 1 AND visit_invoice.dentist_id = '.$personnel_id.' '.$add.$adding);
		$this->db->select('SUM(payment_item.payment_item_amount) AS total_payments');
		$query = $this->db->get('payments,payment_item,visit_invoice'.$table_add);
		$total_payments = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_payments = $value->total_payments;
			}
		}

		$this->db->where('cancel = 0 AND payment_item.payment_id = payments.payment_id AND payment_item.visit_invoice_id = 0 AND payments.payment_type = 1 AND payment_item.dentist_id = '.$personnel_id.' '.$add);
		$this->db->select('SUM(payment_item.payment_item_amount) AS total_payments');
		$query = $this->db->get('payments,payment_item'.$table_add);
		
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_payments += $value->total_payments;
			}
		}

		
		return $total_payments;
	}


	

	public function get_personnel_days_invoices($personnel_id,$date=NULL,$period=null,$type=null)
	{

		if(empty($date))
		{

			$visit_invoices = $this->session->userdata('drb_search');
			$visit_type_id = $this->session->userdata('visit_type_id');
			$patient_number = $this->session->userdata('patient_number');
			$add ='';
			$table_add = '';
			if(!empty($visit_invoices))
			{
				$add .= $visit_invoices;
			}
			else
			{
				$add .= ' AND visit_invoice.created = "'.date('Y-m-d').'" ';
			}
			if(!empty($visit_type_id))
			{
				$add .= $visit_type_id;
			}

		}
		else
		{
			$add = ' AND visit_invoice.created = "'.$date.'" ';
		}


		if($period == 1)
		{
			$searched = $this->session->userdata('monthly_turnover_searched');
			$month = $this->session->userdata('monthly_turnover_month');
			$year = $this->session->userdata('monthly_turnover_year');


			if($searched AND !empty($month) AND !empty($year))
			{
				// start date

				$add = ' AND MONTH(visit_invoice.created) = "'.$month.'" AND  YEAR(visit_invoice.created) = "'.$year.'" ';
			}
			else
			{
				$add = ' AND MONTH(visit_invoice.created) = "'.date('m').'" AND  YEAR(visit_invoice.created) = "'.date('Y').'" ';
			}
		}
		

		if(!empty($type) AND $type == 1)
		{
			$add .= ' AND visit_invoice.bill_to = 1';
		}
		else if(!empty($type) AND $type == 2)
		{
			$add .= ' AND visit_invoice.bill_to <> 1';
		}
		
		
		$this->db->where('visit_charge.visit_charge_delete = 0 AND visit_charge.visit_invoice_id = visit_invoice.visit_invoice_id  AND visit_invoice.dentist_id = '.$personnel_id.' '.$add);
		$this->db->select('SUM(visit_charge.visit_charge_amount*visit_charge.visit_charge_units) AS total_bill');
		$query = $this->db->get('visit_invoice,visit_charge'.$table_add);
		$total_bill = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_bill = $value->total_bill;
			}
		}
		return $total_bill;
	}


	public function get_drb_sales($table, $where,$group_by=NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('patients.*');
		$this->db->where($where);

		// $this->db->join('personnel','visit_invoice.dentist_id = personnel.personnel_id','left');
		// $this->db->order_by('v_transactions_by_date.patient_id','DESC');

		if($group_by == 1)
		{
			$this->db->group_by('v_transactions_by_date.patient_id');
		}
		

		$query = $this->db->get('');
		
		return $query;
		
	}


	public function get_drb_sales_items($table, $where,$group_by=NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('v_transactions_by_date.*');
		$this->db->where($where);

		// $this->db->join('personnel','visit_invoice.dentist_id = personnel.personnel_id','left');
		// $this->db->order_by('v_transactions_by_date.patient_id','DESC');

		if($group_by == 1)
		{
			$this->db->group_by('v_transactions_by_date.patient_id');
		}
		

		$query = $this->db->get('');
		
		return $query;
		
	}

	public function initials($name)
	{
		$words = array(
		    $name
		    );
		    
		$initials = implode('/', array_map(function ($name) { 
		    preg_match_all('/\b\w/', $name, $matches);
		    return implode('', $matches[0]);
		}, $words));

		return $initials;
	}

	public function get_total_revenue($date_from = NULL, $date_to = NULL,$branch_id)
	{

		$visit_search = $this->session->userdata('debtors_search_query');
		// var_dump($visit_search);die();
		$add = '';
		if(!empty($visit_search))
		{
			$add .= $visit_search;
		
			
			
		}
		else
		{
			$add .= ' AND v_transactions_by_date.transaction_date = "'.date('Y-m-d').'" ';
		
			$visit_payments = ' AND v_transactions_by_date.transaction_date = \''.date('Y-m-d').'\'';
			$visit_invoices = ' AND v_transactions_by_date.transaction_date = \''.date('Y-m-d').'\'';
			


		}
		

		
		$branch_session = $this->session->userdata('branch_id');

		if($branch_session > 0)
		{
			$add .= ' AND (v_transactions_by_date.branch_id = '.$branch_session.')';
			// $where .= $visit_search;
		
		}
		$table = 'v_transactions_by_date';
		
		
		$where = 'transactionClassification = "Invoice Patients" '.$add;

		
		$this->db->select('SUM(dr_amount) AS service_total');
		$this->db->where($where);
		$query = $this->db->get($table);
		
		// $result = $query->row();
		// $total = $result[0]->service_total;
		
		if($query->num_rows() > 0)
		{

			foreach ($query->result() as $key):
				# code...
				$total = $key->service_total;

				if(!is_numeric($total))
				{
					return 0;
				}
				else
				{
					return $total;
				}
			endforeach;
		}
		else
		{
			return 0;
		}
		
	}



	public function get_visit_type_amounts($date_from = NULL, $date_to = NULL,$visit_type_id = NULL,$branch_id)
	{
		

		$branch_session = $branch_id;
		$add = '';
		if($branch_session > 0)
		{
			$add .= ' AND branch_id = '.$branch_session.' ';
		
		}
		$table = 'v_transactions_by_date';
		
		$where = '(v_transactions_by_date.transaction_date BETWEEN \''.$date_from.'\' AND \''.$date_to.'\') AND v_transactions_by_date.payment_type = '.$visit_type_id.' AND transactionClassification = "Invoice Patients" '.$add;

		
		$this->db->select('SUM(dr_amount) AS service_total');
		$this->db->where($where);
		$query = $this->db->get($table);
		
		// $result = $query->row();
		// $total = $result[0]->service_total;
		$total_invoice = 0;
		if($query->num_rows() > 0)
		{

			foreach ($query->result() as $key):
				# code...
				$total = $key->service_total;

				if(!is_numeric($total))
				{
					$total_invoice = 0;
				}
				else
				{
				 	$total_invoice = $total;
				}
			endforeach;
		}
		
		
		$add = ' AND v_transactions_by_date.transaction_date BETWEEN "'.$date_from.'" AND "'.$date_to.'"  ';
	
		
		$branch_session = $this->session->userdata('branch_id');

		if($branch_session > 0)
		{
			$add .= ' AND (v_transactions_by_date.branch_id = '.$branch_session.')';
			// $where .= $visit_search;
		
		}
		
		$visit_type = $this->session->userdata('visit_type');
		
		$this->db->where('v_transactions_by_date.transactionCategory = "Revenue Payment" AND v_transactions_by_date.reference_id > 0 AND v_transactions_by_date.reference_id AND v_transactions_by_date.payment_type = '.$visit_type_id.' '.$add);
		$this->db->select('SUM(cr_amount) AS total_payments');
		$query = $this->db->get('v_transactions_by_date');
		$total_payments = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_payments = $value->total_payments;
			}
		}

		$response['total_payments'] = $total_payments;
		$response['total_invoices'] = $total_invoice;
		return $response;
		
	}

	public function get_debt_payment_totals($type_payment=NULL,$first_day=null,$last_day=null,$branch_id=2)
	{
		$visit_payments = $this->session->userdata('visit_payments');
		$visit_invoices = $this->session->userdata('visit_invoices');
		
		$visit_type_id = $this->session->userdata('visit_type_id');
		$visit_type = $this->session->userdata('visit_type');
		$patient_number = $this->session->userdata('patient_number');
		$add ='';
		$table_add = '';
		$add_debt = '';

		$visit_search = $this->session->userdata('visit_payments');
		
		if(!empty($visit_search))
		{
			$add .= $visit_search;
		}
		else
		{
			// $where .=' AND payments.payment_created = "'.date('Y-m-d').'"';
			$add .= ' AND v_transactions_by_date.transaction_date = "'.date('Y-m-d').'" ';
		}

		

		if($type_payment == 0)
		{
			$add .= ' AND v_transactions_by_date.transaction_date = v_transactions_by_date.invoice_date ';
		}
		else
		{
			$add .= ' AND v_transactions_by_date.transaction_date <> v_transactions_by_date.invoice_date ';
		}
		

		$branch_session = $this->session->userdata('branch_id');

		if($branch_session > 0)
		{
			$add .= ' AND (v_transactions_by_date.branch_id = '.$branch_session.')';
			// $where .= $visit_search;
		
		}
	


		$visit_type = $this->session->userdata('visit_type');
		
		$this->db->where('v_transactions_by_date.transactionCategory = "Revenue Payment" AND v_transactions_by_date.reference_id > 0 AND v_transactions_by_date.reference_id  IN (SELECT v_transactions_by_date.transaction_id FROM v_transactions_by_date WHERE  v_transactions_by_date.transactionCategory = "Revenue" '.$add_debt.' ) '.$add);
		$this->db->select('SUM(cr_amount) AS total_payments');
		$query = $this->db->get('v_transactions_by_date');
		$total_payments = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_payments = $value->total_payments;
			}
		}
		return $total_payments;
	}
	public function get_visit_invoice_totals()
	{
		$visit_invoices = $this->session->userdata('visit_invoices');
		$visit_type_id = $this->session->userdata('visit_type_id');
		$patient_number = $this->session->userdata('patient_number');
		$add ='';
		$table_add = '';

		$debtor_query = $this->session->userdata('debtors_search_query');

		// var_dump($visit_type_id); die();
		if(!empty($debtor_query))
		{
			$add .= $debtor_query;
		}
		else
		{
			$add .= ' AND v_transactions_by_date.transaction_date = \''.date('Y-m-d').'\'';
		}

		$branch_session = $this->session->userdata('branch_id');

		if($branch_session > 0)
		{
			$add .= ' AND v_transactions_by_date.branch_id = '.$branch_session;
		
		}
		// if(!empty($visit_type_id))
		// {
		// 	$add .= $visit_type_id;
		// }
		// if(!empty($patient_number))
		// {
		// 	$add .= $patient_number.' AND patients.patient_id = visit.patient_id';
		// 	$table_add .=',patients';
		// }
		
		$this->db->where('(v_transactions_by_date.transactionCategory = "Revenue") '.$add);
		$this->db->select('(SUM(dr_amount)) AS total_invoice');
		$query = $this->db->get('v_transactions_by_date');
		$total_invoice = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_invoice = $value->total_invoice;
			}
		}
		if(empty($total_invoice))
		{
			$total_invoice = 0;
		}
		return $total_invoice;
	}





	public function get_visit_credits_totals()
	{
		$visit_invoices = $this->session->userdata('visit_invoices');
		$visit_type_id = $this->session->userdata('visit_type_id');
		$patient_number = $this->session->userdata('patient_number');
		$add ='';
		$table_add = '';

		$debtor_query = $this->session->userdata('debtors_search_query');

		// var_dump($visit_type_id); die();
		if(!empty($debtor_query))
		{
			$add .= $debtor_query;
		}
		else
		{
			$add .= ' AND v_transactions_by_date.invoice_date = \''.date('Y-m-d').'\'';
		}

		$branch_session = $this->session->userdata('branch_id');

		if($branch_session > 0)
		{
			$add .= ' AND v_transactions_by_date.branch_id = '.$branch_session;
		
		}
		// if(!empty($visit_type_id))
		// {
		// 	$add .= $visit_type_id;
		// }
		// if(!empty($patient_number))
		// {
		// 	$add .= $patient_number.' AND patients.patient_id = visit.patient_id';
		// 	$table_add .=',patients';
		// }
		
		$this->db->where('(v_transactions_by_date.transactionCategory = "Credit Note") '.$add);
		$this->db->select('SUM(cr_amount) AS total_invoice');
		$query = $this->db->get('v_transactions_by_date');
		$total_invoice = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_invoice = $value->total_invoice;
			}
		}
		if(empty($total_invoice))
		{
			$total_invoice = 0;
		}
		return $total_invoice;
	}
	public function get_visit_payment_totals()
	{
		// $visit_payments = $this->session->userdata('visit_payments');
		$visit_invoices = $this->session->userdata('visit_invoices');
		
		$visit_type_id = $this->session->userdata('visit_type_id');
		$visit_type = $this->session->userdata('visit_type');
		$patient_number = $this->session->userdata('patient_number');
		$add ='';
		$table_add = '';

		$debtor_query = $this->session->userdata('debtors_search_query');

		// var_dump($visit_type_id); die();
		if(!empty($debtor_query))
		{
			$add .= $debtor_query;
		}
		else
		{
			$add .= ' AND v_transactions_by_date.transaction_date = \''.date('Y-m-d').'\'';
		}

		$branch_session = $this->session->userdata('branch_id');

		if($branch_session > 0)
		{
			$add .= ' AND v_transactions_by_date.branch_id = '.$branch_session;
		
		}
		// if(!empty($visit_invoices))
		// {
		// 	$add .= $visit_invoices;
		// }
		// if(!empty($visit_type_id))
		// {
		// 	$add .= $visit_type_id;
		// }
		// if(!empty($patient_number))
		// {
		// 	$add .= $patient_number.' AND patients.patient_id = visit.patient_id';
		// 	$table_add .= ',patients';
		// }
		$visit_type = $this->session->userdata('visit_type');
		// if($visit_type == 1 AND !empty($visit_type))
		// {
		// 	$add .= 'AND payments.payment_method_id < 9';
		// }
		// else if($visit_type != 1 AND !empty($visit_type))
		// {
		// 	$add .= ' AND payments.payment_method_id = 9';
		// }
		// else
		// {
		// 	$add .= '';
		// }

		// $this->db->where('v_transactions_by_date.transactionCategory = "Revenue Payment" AND v_transactions_by_date.reference_id > 0 AND v_transactions_by_date.reference_id  IN (SELECT v_transactions_by_date.transaction_id FROM v_transactions_by_date WHERE  v_transactions_by_date.transactionCategory = "Revenue" '.$add.' )');
		// $this->db->select('SUM(cr_amount) AS total_payments');


		$this->db->where('v_transactions_by_date.transactionCategory = "Revenue Payment" AND v_transactions_by_date.reference_id > 0  '.$add.'');
		$this->db->select('SUM(cr_amount) AS total_payments');
		$query = $this->db->get('v_transactions_by_date');
		$total_payments = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_payments = $value->total_payments;
			}
		}
		return $total_payments;
	}

	public function get_doctor($personnel_type_id=NULL)
	{

		$add = '';
		if(!empty($personnel_type_id))
		{
			$add = ' AND personnel.personnel_type_id = '.$personnel_type_id;
		}
		$table = "personnel, personnel_job,job_title";
		$where = "personnel_job.personnel_id = personnel.personnel_id AND personnel_job.job_title_id = job_title.job_title_id AND job_title.job_title_name = 'Dentist'".$add;
		$items = "personnel.personnel_onames, personnel.personnel_fname, personnel.personnel_id,personnel.professional_fees_percentage,personnel.personnel_type_id";
		$order = "personnel.personnel_id";

		$this->db->where($where);
		$this->db->select($items);
		$this->db->order_by($order,'ASC');
		
		$result = $this->db->get($table);
		
		return $result;
	}	


	public function get_personnel_days_credits($personnel_id,$date=NULL,$period=null,$type=null)
	{

		if(empty($date))
		{
			$visit_invoices = $this->session->userdata('drb_credit_search');
			$visit_type_id = $this->session->userdata('visit_type_id');
			$patient_number = $this->session->userdata('patient_number');
			$add ='';
			$table_add = '';
			if(!empty($visit_invoices))
			{
				$add .= $visit_invoices;
			}
			else
			{
				$add .= ' AND visit_credit_note.created = "'.date('Y-m-d').'" ';
			}
			if(!empty($visit_type_id))
			{
				$add .= $visit_type_id;
			}

		}
		else
		{
			$add .= ' AND visit_credit_note.created = "'.$date.'" ';
		}
		

		if($period == 1)
		{
			$searched = $this->session->userdata('monthly_turnover_searched');
			$month = $this->session->userdata('monthly_turnover_month');
			$year = $this->session->userdata('monthly_turnover_year');


			if($searched AND !empty($month) AND !empty($year))
			{
				// start date

				$add = ' AND MONTH(visit_credit_note.created) = "'.$month.'" AND  YEAR(visit_credit_note.created) = "'.$year.'" ';
			}
			else
			{
				$add = ' AND MONTH(visit_credit_note.created) = "'.date('m').'" AND  YEAR(visit_credit_note.created) = "'.date('Y').'" ';
			}
		}
		
		if(!empty($type) AND $type == 1)
		{
			$add .= ' AND visit_invoice.bill_to = 1';
		}
		else if(!empty($type) AND $type == 2)
		{
			$add .= ' AND visit_invoice.bill_to <> 1';
		}
		
		
		$this->db->where('visit_credit_note_item.visit_credit_note_id =visit_credit_note.visit_credit_note_id AND visit_credit_note.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_invoice.visit_invoice_delete = 0  AND visit_credit_note.visit_cr_note_delete = 0  AND visit_credit_note_item.visit_cr_note_item_delete = 0 AND visit_invoice.dentist_id = '.$personnel_id.' '.$add);
		$this->db->select('SUM(visit_credit_note_item.visit_cr_note_units*visit_credit_note_item.visit_cr_note_amount) AS total_bill');
		$query = $this->db->get('visit_credit_note,visit_credit_note_item,visit_invoice'.$table_add);
		$total_bill = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_bill = $value->total_bill;
			}
		}
		return $total_bill;
	}

	


	public function get_all_days_invoice($table, $where,$group_by=NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('SUM(visit_charge.visit_charge_amount * visit_charge.visit_charge_units) AS total_invoice_amount , visit_invoice.*,visit_invoice.created AS invoice_date,personnel.personnel_onames,personnel.personnel_fname,visit_type.visit_type_name AS payment_type_name,patients.*');
		$this->db->where($where);

		$this->db->join('personnel','visit_invoice.dentist_id = personnel.personnel_id','left');
		$this->db->join('visit_type','visit_invoice.bill_to = visit_type.visit_type_id','left');
		// $this->db->order_by('v_transactions_by_date.patient_id','DESC');

		
		$this->db->group_by('visit_invoice.visit_invoice_id');
		
	

		$query = $this->db->get('');
		
		return $query;
		
	}


	public function get_all_days_credit_note($table, $where,$group_by=NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('SUM(visit_credit_note.visit_cr_note_amount) AS total_credit_amount , visit_invoice.*,visit_invoice.created AS invoice_date,personnel.personnel_onames,personnel.personnel_fname,visit_type.visit_type_name AS payment_type_name');
		$this->db->where($where);

		$this->db->join('personnel','visit_invoice.dentist_id = personnel.personnel_id','left');
		$this->db->join('visit_type','visit_invoice.bill_to = visit_type.visit_type_id','left');
		// $this->db->order_by('v_transactions_by_date.patient_id','DESC');

		
		$this->db->group_by('visit_invoice.visit_invoice_id');
		
	

		$query = $this->db->get('');
		
		return $query;
		
	}

	public function get_doctors_sales($personnel_id)
	{
		$where = '(v_transactions_by_date.transactionCategory = "Revenue") AND visit_invoice.visit_invoice_id = v_transactions_by_date.transaction_id AND visit_invoice.dentist_id = '.$personnel_id;
		$table = 'v_transactions_by_date,visit_invoice';


		$searched = $this->session->userdata('monthly_turnover_searched');
		$month = $this->session->userdata('monthly_turnover_month');
		$year = $this->session->userdata('monthly_turnover_year');


		if($searched AND !empty($month) AND !empty($year))
		{
			// start date

			$where .= ' AND MONTH(visit_invoice.created) = "'.$month.'" AND  YEAR(visit_invoice.created) = "'.$year.'" ';
		}
		else
		{
			$where .= ' AND MONTH(visit_invoice.created) = "'.date('m').'" AND  YEAR(visit_invoice.created) = "'.date('Y').'" ';
		}

		$this->db->from($table);
	
		$this->db->select('v_transactions_by_date.*, patients.*, payment_method.*, personnel.personnel_fname, personnel.personnel_onames,visit_invoice.visit_invoice_number,branch.branch_name,branch.branch_code,visit.visit_id,visit_invoice.visit_invoice_id,visit_invoice.preauth_status');
		$this->db->join('patients', 'patients.patient_id = v_transactions_by_date.patient_id', 'left');
		$this->db->join('payment_method', 'payment_method.payment_method_id = v_transactions_by_date.payment_method_id', 'left');
		$this->db->join('payments', 'payments.payment_id = v_transactions_by_date.transaction_id', 'left');
		$this->db->join('visit', 'visit.visit_id = visit_invoice.visit_id', 'left');
		$this->db->join('personnel', 'visit.personnel_id = personnel.personnel_id', 'left');
		$this->db->join('branch', 'branch.branch_id = v_transactions_by_date.branch_id', 'left');

		$this->db->where($where);
		$this->db->order_by('v_transactions_by_date.created_at','DESC');
		$this->db->group_by('v_transactions_by_date.reference_code');


		$query = $this->db->get('');
		
		return $query;
	}
	public function get_doctors_payments($personnel_id)
	{
		$where = 'v_transactions_by_date.transactionCategory = "Revenue Payment" AND visit_invoice.visit_invoice_id = v_transactions_by_date.reference_id AND visit_invoice.dentist_id = '.$personnel_id;
		
		$table = 'v_transactions_by_date,visit_invoice';

		$searched = $this->session->userdata('monthly_turnover_searched');
		$month = $this->session->userdata('monthly_turnover_month');
		$year = $this->session->userdata('monthly_turnover_year');


		if($searched AND !empty($month) AND !empty($year))
		{
			// start date

			$where .= ' AND MONTH(v_transactions_by_date.transaction_date) = "'.$month.'" AND  YEAR(v_transactions_by_date.transaction_date) = "'.$year.'" ';
		}
		else
		{
			$where .= ' AND MONTH(v_transactions_by_date.transaction_date) = "'.date('m').'" AND  YEAR(v_transactions_by_date.transaction_date) = "'.date('Y').'" ';
		}


		$this->db->from($table);
		$this->db->select('v_transactions_by_date.*, patients.*, payment_method.*, personnel.personnel_fname, personnel.personnel_onames,visit_invoice.visit_invoice_number,branch.branch_name,branch.branch_code,visit_invoice.created AS invoice_date');
		$this->db->join('patients', 'patients.patient_id = v_transactions_by_date.patient_id', 'left');
		$this->db->join('payment_method', 'payment_method.payment_method_id = v_transactions_by_date.payment_method_id', 'left');
		$this->db->join('payments', 'payments.payment_id = v_transactions_by_date.transaction_id', 'left');
		$this->db->join('personnel', 'payments.payment_created_by = personnel.personnel_id', 'left');
		// $this->db->join('visit_invoice', 'visit_invoice.visit_invoice_id = v_transactions_by_date.reference_id', 'left');
		$this->db->join('branch', 'branch.branch_id = v_transactions_by_date.branch_id', 'left');

		$this->db->where($where);
		$this->db->order_by('v_transactions_by_date.created_at','DESC');
		$query = $this->db->get('');


		
		return $query;

	}



	




}
?>