<div class="row statistics">
    
    <div class="col-md-12 col-sm-12">
    	 <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	<h2 class="panel-title">DAILY SALES RECORD SUMMARY</h2>
            </header>           
        
              <!-- Widget content -->
              <div class="panel-body" style="padding: 10px !important;">
                <div class="col-md-12">
                   
                    <div class="col-md-3">
						<?php
                        	$total_invoices_revenue = $this->hospital_reports_model->get_visit_invoice_totals();
                            $total_transfers = 0;//$this->hospital_reports_model->get_visit_invoice_children_totals();


                            $total_payments_revenue = $this->hospital_reports_model->get_days_collection();
                            // var_dump($total_invoices_revenue);die();
                            $total_waiver_revenue = 0;//$this->hospital_reports_model->get_visit_waiver_totals();

                            $all_payments_period = $this->hospital_reports_model->all_payments_period();

                            $receivable = $all_payments_period - $total_payments_revenue;
                            $total_rejected_amounts = 0;//$this->hospital_reports_model->get_rejected_amounts();
                            // $total_invoices_revenue -= $total_rejected_amounts;

                            if($receivable < 0)
                            {
                                $receivable = ($receivable);
                            }

                        ?>
                        <?php
                        // $patients = $this->hospital_reports_model->get_patients_visits(1);
                        // $returning_patients = $this->hospital_reports_model->get_patients_visits(0);
                        ?>
                        <h5>VISIT TYPE BREAKDOWN</h5>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                                 <tr>
                                    <th>PATIENTS</th>
                                    <td><?php echo $total_patients;?></td>
                                </tr>
                                
                            </tbody>
                        </table>
                   

                        
                        <div class="clearfix"></div>
            		</div>
                    <!-- End Transaction Breakdown -->
                   
                   
                    <div class="col-md-3">
                       
                        <h5><strong>DAILY SALES RECORD</strong> </h5>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
								<?php
								$total_cash_breakdown = 0;
								$payment_methods = $this->hospital_reports_model->get_payment_methods();
                                if($payment_methods->num_rows() > 0)
                                {
                                    foreach($payment_methods->result() as $res)
                                    {
                                        $method_name = $res->payment_method;
                                        $payment_method_id = $res->payment_method_id;
                                        $total = $this->hospital_reports_model->get_amount_collected($payment_method_id);
                                 
                                        
                                        echo 
										'
										<tr>
											<th>'.strtoupper($method_name).'</th>
											<td>'.number_format($total, 2).'</td>
										</tr>
										';
										$total_cash_breakdown += $total;
                                    }
                                    
									echo 
									'
									<tr>
										<th>TOTAL</th>
										<td>'.number_format($total_cash_breakdown, 2).'</td>
									</tr>
									';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                   
                   <div class="col-print-6">
                       <h5><strong>DAILY SALES TURNOVER SUMMARY</strong> </h5>

                        <table class="table table-striped table-hover table-condensed">
                            <thead>
                                <th>Doctors</th>
                                <th>Billings</th>
                                <th>Collections</th>
                            </thead>
                            <tbody>
                                <?php
                                $total_cash_breakdown = 0;
                                $total_amount_invoiced = 0;
                                $total_amount_paid = 0;
                                
                                $doctors = $this->hospital_reports_model->get_doctor();
                                 // var_dump($doctors->num_rows());die();
                                 if($doctors->num_rows() > 0)
                                 {
                                    foreach($doctors->result() as $row):
                                        $fname = $row->personnel_fname;
                                        $onames = $row->personnel_onames;
                                        $personnel_id = $row->personnel_id;

                                        // var_dump($personnel_id);die();
                                       
                                            $amount_paid = $this->hospital_reports_model->get_personnel_days_payments($personnel_id);
                                            $amount_invoiced = $this->hospital_reports_model->get_personnel_days_invoices($personnel_id);
                                            $amount_credits = $this->hospital_reports_model->get_personnel_days_credits($personnel_id);
                                            
                                            $amount_invoiced -= $amount_credits;
                                            $total_amount_invoiced += $amount_invoiced;
                                            $total_amount_paid += $amount_paid;

                                            if($amount_invoiced > 0 OR $amount_paid > 0 OR $amount_credits > 0)
                                            {
                                                echo 
                                                    '
                                                    <tr>
                                                        <th>Dr. '.$onames.' '.$fname.'  </th>
                                                        <td>'.number_format($amount_invoiced, 2).'</td>
                                                        <td>'.number_format($amount_paid, 2).'</td>
                                                    </tr>
                                                    ';
                                            }
                                            
                                        
                                        
                                        
                                    endforeach;

                                    $amount_paid = $this->hospital_reports_model->get_personnel_days_payments(0);
                                    $amount_invoiced = $this->hospital_reports_model->get_personnel_days_invoices(0);
                                    $total_amount_invoiced += $amount_invoiced;
                                    $total_amount_paid += $amount_paid;
                                    echo 
                                        '
                                        <tr>
                                            <th>Auxilliary Dental Products</th>
                                            <td>'.number_format($amount_invoiced, 2).'</td>
                                            <td>'.number_format($amount_paid, 2).'</td>
                                        </tr>
                                        ';
                                    echo 
                                            '
                                            <tr>
                                                <th>Total</th>
                                                <th>'.number_format($total_amount_invoiced, 2).'</th>
                                                <th>'.number_format($total_amount_paid, 2).'</th>
                                            </tr>
                                            ';
                                }
                               
                                ?>
                            </tbody>
                        </table>
                     
                    </div>
                </div>
          	</div>
		</section>
    </div>
</div>