<!-- search -->
<?php echo $this->load->view('search_drb', '', TRUE);?>
<!-- end search -->
<?php
	$where = 'patients.patient_id = visit.patient_id AND patients.patient_delete = 0 AND visit.close_card >= 3  AND visit.patient_id NOT IN (SELECT v_transactions_by_date.patient_id FROM v_transactions_by_date WHERE v_transactions_by_date.patient_id = patients.patient_id AND visit.visit_date = v_transactions_by_date.transaction_date)   ';
	$table = 'patients, visit';
	$visit_search = $this->session->userdata('drb_visit_search');
	// var_dump($visit_search);die();
	if(!empty($visit_search))
	{
		$where .= $visit_search;
	
		
		
	}
	else
	{
		$where .= ' AND visit.visit_date = "'.date('Y-m-d').'" ';
	
		$visit_payments = ' AND payments.payment_created = \''.date('Y-m-d').'\'';
		$visit_invoices = ' AND v_transactions_by_date.transaction_date= \''.date('Y-m-d').'\'';
		$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

		// $this->session->set_userdata('drb_search', $visit_invoices);
		// $this->session->set_userdata('drb_payments_search', $visit_payments);
	


		$where .= '';

	}
	
	
	$query = $this->hospital_reports_model->get_drb_sales($table, $where,0);


	$where = 'patients.patient_id = visit.patient_id AND patients.patient_delete = 0 AND visit.close_card >= 3 ';
	$table = 'patients, visit';
	$visit_search = $this->session->userdata('drb_visit_search');
	// var_dump($visit_search);die();
	if(!empty($visit_search))
	{
		$where .= $visit_search;
	
		
		
	}
	else
	{
		$where .= ' AND visit.visit_date = "'.date('Y-m-d').'" ';
	
		$visit_payments = ' AND payments.payment_created = \''.date('Y-m-d').'\'';
		$visit_invoices = ' AND v_transactions_by_date.transaction_date= \''.date('Y-m-d').'\'';
		$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

		// $this->session->set_userdata('drb_search', $visit_invoices);
		// $this->session->set_userdata('drb_payments_search', $visit_payments);
	


		$where .= '';

	}
	
	
	$query_new = $this->hospital_reports_model->get_drb_sales($table, $where,0);
	$v_data['total_patients'] = $query_new->num_rows();

	// var_dump($query->num_rows());die();

?>
<?php echo $this->load->view('transaction_statistics', $v_data, TRUE);?>
 
<div class="row">
    <div class="col-md-12">

        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title">DRB</h2>
            	  <div class="widget-icons pull-right" style="margin-top: -24px !important;">
            	  	<a href="<?php echo site_url().'print-drb'?>" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Print DRB</a>
            	  
            	</div>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
          <h5 class="center-align"><?php echo $this->session->userdata('search_title');?></h5>
<?php
		$result = '';
		$search = $this->session->userdata('drb_search');

		// var_dump($search);die();
		if(!empty($search))
		{
			echo '<a href="'.site_url().'hospital_reports/reports/close_drb_search" class="btn btn-sm btn-warning">Close Search</a>';
		}
		// var_dump($query);die();
		//if users exist display them
		//if users exist display them
		$total_invoiced = 0;
		$total_balance = 0;

	

		$result .= '
						<table class="table table-hover table-bordered table-striped table-responsive col-md-12">

							<thead>
								<th>#</th>
								<th>FILE NUMBER</th>
								<th>DOCTOR</th>
								<th>PATIENT NAME</th>
								<th>INVOICE NUMBER</th>
								<th>AMOUNT CHARGED</th>
								<th>AMOUNT PAID</th>
								<th>BALANCE</th>
								<th></th>
						  </thead>
						  <tbody>

						';
			$count = 0;
			$total_payments = 0;
	

			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $row)
				{

					$visit_date = date('d.m.Y',strtotime($row->transaction_date));
					
					$patient_id = $row->patient_id;
					$dependant_id = $row->dependant_id;
					// $transaction_category = $row->transaction_category;
					$patient_surname = $row->patient_surname;
					$patient_othernames = $row->patient_othernames;
					$patient_first_name = $row->patient_first_name;
					$patient_number = $row->patient_number;
					$visit_type_name = $row->visit_type_name;
					$personnel_onames = $row->personnel_onames;
					$personnel_fname = $row->personnel_fname;
					$personnel_name2 = $personnel_fname.' '.$personnel_onames;

					$visit_id = $row->visit_id;

					$initials = $this->hospital_reports_model->initials($personnel_name2);
					$count++;
					// check if the patient has on accont payment
					$payments_query = $this->accounts_model->get_visit_invoice_payments('null',1,$patient_id);

					$add_payments = '';
					$payments_value = 0;
					// var_dump($payments_query->num_rows());die();
				
					$result .= 
									'
										<tr>
											<td>'.$count.'</td>
											<td>'.$patient_number.'</td>
											<td>'.$initials.' </td>
											<td>'.$patient_surname.' '.$patient_othernames.' '.$patient_first_name.'</td>
											<td>-</td>
											<td>-</td>
											<td>-</td>
											<td>-</td>
											<td>'.strtoupper($visit_type_name).'</td>
										</tr> 
									';
				}
			}

			
			


			$where = 'patients.patient_id = v_transactions_by_date.patient_id  AND (v_transactions_by_date.transaction_date = v_transactions_by_date.invoice_date) AND v_transactions_by_date.reference_id > 0 ';
			$table = 'patients,v_transactions_by_date';
			$visit_search = $this->session->userdata('drb_report_search');
		
			if(!empty($visit_search))
			{
				$where .= $visit_search;
			
				
				
			}
			else
			{
				$where .= ' AND  v_transactions_by_date.transaction_date = "'.date('Y-m-d').'" ';
			
				$visit_payments = ' AND payments.payment_created = \''.date('Y-m-d').'\'';
				$visit_invoices = ' AND v_transactions_by_date.transaction_date= \''.date('Y-m-d').'\'';
				$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

				
			


				$where .= '';

			}
		
		
			$query_two = $this->hospital_reports_model->get_drb_sales_items($table, $where,2);
			// var_dump($query_two);die();

			if($query_two->num_rows() > 0)
			{
				foreach ($query_two->result() as $key => $value2) {
					# code...
					$dr_amount = $value2->dr_amount;
					$cr_amount = $value2->cr_amount;
					$transaction_id = $value2->transaction_id;
					$visit_invoice_id = $value2->reference_id;
					$reference_code = $value2->reference_code;
					$personnel_name = $value2->personnel_name;
					$payment_type_name = $value2->payment_type_name;
					$patient_surname = $value2->patient_surname;
					$patient_othernames = $value2->patient_othernames;
					$patient_first_name = $value2->patient_first_name;
					$patient_number = $value2->patient_number;
					// $payment_type_name = $value2->payment_type_name;
					$count++;

					$initials = $this->hospital_reports_model->initials($personnel_name);
					$dr_amount = $this->accounts_model->get_visit_invoice_total($visit_invoice_id);
					$visit_invoice_number = $this->accounts_model->get_visit_invoice_number($visit_invoice_id);
					// $payments_query = $this->accounts_model->get_payment_details($transaction_id);
					$payments_query = $this->accounts_model->get_visit_payment_details($visit_invoice_id);
					$credit_note = 0;//$this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);
					$add_payments = '';
					$payments_value = 0;

					if(empty($visit_invoice_number))
					{
						$reference_code = '';
					}
					else
					{
						$reference_code = $visit_invoice_number;
					}

					// var_dump($payments_query->result());die();
					if($payments_query->num_rows() > 0)
					{
						foreach ($payments_query->result() as $key => $value4) {
							# code...
							$payment_method = $value4->payment_method;
							$confirm_number = $value4->confirm_number;
							$total_amount = $value4->payment_item_amount;
							$payment_method_id = $value4->payment_method_id;

							$payments_value += $total_amount;
							if($payment_method_id == 5)
							{
								$confirm_number = $value4->transaction_code;
							}

							$add_payments .= 
											'
												<tr>
													<td colspan="2"></td>
													<td colspan="5"><strong>METHOD : </strong>'.$payment_method.' '.$payment_method_id.'<strong>REF : </strong>'.$confirm_number.' <strong>AMOUNT:</strong>'.number_format($payments_value,2).'  </td>
													
													<td colspan="2"></td>
												</tr> 
											';
						}
					}

					$total_payments += $payments_value;
					$dr_amount -= $credit_note;
					$total_invoiced += $dr_amount;

					$balance  = $this->accounts_model->balance($payments_value,$dr_amount);

					$total_balance += $balance;
			

					$result .= 
								'
									<tr>
										<td>'.$count.'</td>
										<td>'.$patient_number.' </td>
										<td>'.$personnel_name.' </td>
										<td>'.$patient_surname.' '.$patient_othernames.' '.$patient_first_name.'</td>
										<td>'.$reference_code.'</td>
										<td>'.number_format($dr_amount,2).'</td>
										<td>('.number_format($payments_value,2).')</td>
										<td>'.number_format($balance,2).'</td>
										<td>'.strtoupper($payment_type_name).'</td>
									</tr> 
								';
					$result .= $add_payments;
				}
				
			}


			// Add patients that have  payments without invoices for the day

			$where = 'patients.patient_id = v_transactions_by_date.patient_id  AND (v_transactions_by_date.transaction_date <> v_transactions_by_date.invoice_date) AND v_transactions_by_date.reference_id > 0 ';
			$table = 'patients,v_transactions_by_date';
			$visit_search = $this->session->userdata('drb_report_search');
		
			if(!empty($visit_search))
			{
				$where .= $visit_search;
			
				
				
			}
			else
			{
				$where .= ' AND  v_transactions_by_date.transaction_date = "'.date('Y-m-d').'" ';
			
				$visit_payments = ' AND payments.payment_created = \''.date('Y-m-d').'\'';
				$visit_invoices = ' AND v_transactions_by_date.transaction_date= \''.date('Y-m-d').'\'';
				$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

				
			


				$where .= '';

			}
		
		
			$query_two = $this->hospital_reports_model->get_drb_sales_items($table, $where,2);
			// var_dump($query_two);die();

			if($query_two->num_rows() > 0)
			{
				foreach ($query_two->result() as $key => $value2) {
					# code...
					$dr_amount = $value2->dr_amount;
					$cr_amount = $value2->cr_amount;
					$transaction_id = $value2->transaction_id;
					$reference_code = $value2->reference_code;
					$personnel_name = $value2->personnel_name;
					$payment_type_name = $value2->payment_type_name;
					$patient_surname = $value2->patient_surname;
					$patient_othernames = $value2->patient_othernames;
					$patient_first_name = $value2->patient_first_name;
					$patient_number = $value2->patient_number;
					$transactionCategory = $value2->transactionCategory;
					// $payment_type_name = $value2->payment_type_name;


					if($transactionCategory =="Credit Note" )
					{
						$dr_amount = -$cr_amount;
					}
					$count++;

					$initials = $this->hospital_reports_model->initials($personnel_name);

					$payments_query = $this->accounts_model->get_payment_details($transaction_id);
					$credit_note = $this->accounts_model->get_visit_invoice_credit_notes($transaction_id);
					$add_payments = '';
					$payments_value = 0;

					// var_dump($payments_query->result());die();
					if($payments_query->num_rows() > 0)
					{
						foreach ($payments_query->result() as $key => $value4) {
							# code...
							$payment_method = $value4->payment_method;
							$confirm_number = $value4->confirm_number;
							$total_amount = $value4->payment_item_amount;
							$payment_method_id = $value4->payment_method_id;

							$payments_value += $total_amount;
							if($payment_method_id == 5)
							{
								$confirm_number = $value4->transaction_code;
							}

							$add_payments .= 
											'
												<tr>
													<td colspan="2"></td>
													<td colspan="5"><strong>METHOD : </strong>'.$payment_method.' '.$payment_method_id.'<strong>REF : </strong>'.$confirm_number.' <strong>AMOUNT:</strong>'.number_format($payments_value,2).'  </td>
													
													<td colspan="2"></td>
												</tr> 
											';
						}
					}
					$dr_amount -= $credit_note;
					$total_invoiced += $dr_amount;
					if($transactionCategory =="Credit Note" )
					{
						$payments_value = 0;
						$balance = 0;
					}
					else
					{
						$total_payments += $payments_value;
						$balance  = $this->accounts_model->balance($payments_value,$dr_amount);
					}

					
					

					

					$total_balance += $balance;



					$result .= 
								'
									<tr>
										<td>'.$count.'</td>
										<td>'.$patient_number.' </td>
										<td>'.$personnel_name.' </td>
										<td>'.$patient_surname.' '.$patient_othernames.' '.$patient_first_name.'</td>
										<td>'.$reference_code.'</td>
										<td>'.number_format($dr_amount,2).'</td>
										<td>('.number_format($payments_value,2).')</td>
										<td>('.number_format($payments_value,2).')</td>
										<td>'.strtoupper($payment_type_name).'</td>
									</tr> 
								';
					$result .= $add_payments;
				}
				
			}

			// Add patients that have just made payments without invoices for the day

			$where = 'patients.patient_id = v_transactions_by_date.patient_id AND v_transactions_by_date.transactionCategory = "Revenue Payment" AND (v_transactions_by_date.transaction_date > v_transactions_by_date.invoice_date OR transactionClassification ="On account Patients Payment") AND v_transactions_by_date.reference_id = 0 ';
			$table = 'patients,v_transactions_by_date';
			$visit_search = $this->session->userdata('drb_report_search');
		
			if(!empty($visit_search))
			{
				$where .= $visit_search;
			
				
				
			}
			else
			{
				$where .= ' AND  v_transactions_by_date.transaction_date = "'.date('Y-m-d').'" ';
			
				$visit_payments = ' AND payments.payment_created = \''.date('Y-m-d').'\'';
				$visit_invoices = ' AND v_transactions_by_date.transaction_date= \''.date('Y-m-d').'\'';
				$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

				
			


				$where .= '';

			}
			
			
			$query_two = $this->hospital_reports_model->get_drb_sales_items($table, $where,0);


			if($query_two->num_rows() > 0)
			{
				foreach ($query_two->result() as $key => $value2) {
					# code...

					$cr_amount = $value2->cr_amount;
					$transaction_id = $value2->transaction_id;
					$reference_code = $value2->reference_code;
					$personnel_name = $value2->personnel_name;
					$payment_type_name = $value2->payment_type_name;
					$patient_surname = $value2->patient_surname;
					$patient_othernames = $value2->patient_othernames;
					$patient_first_name = $value2->patient_first_name;
					$patient_number = $value2->patient_number;
					// $payment_type_name = $value2->payment_type_name;
					$count++;

					$initials = $this->hospital_reports_model->initials($personnel_name);

					$payments_query = $this->accounts_model->get_payment_details($transaction_id);
					$add_payments = '';
					$payments_value = 0;

					// var_dump($payments_query->result());die();
					if($payments_query->num_rows() > 0)
					{
						foreach ($payments_query->result() as $key => $value4) {
							# code...
							$payment_method = $value4->payment_method;
							$confirm_number = $value4->confirm_number;
							$total_amount = $value4->payment_item_amount;
							$payment_method_id = $value4->payment_method_id;

							$payments_value += $total_amount;
							if($payment_method_id == 5)
							{
								$confirm_number = $value4->transaction_code;
							}

							$add_payments .= 
											'
												<tr>
													<td colspan="2"></td>
													<td colspan="5"><strong>METHOD : </strong>'.$payment_method.' '.$payment_method_id.'<strong>REF : </strong>'.$confirm_number.' <strong>AMOUNT:</strong>'.number_format($payments_value,2).'  </td>
													
													<td colspan="2"></td>
												</tr> 
											';
						}
					}
					$total_payments += $payments_value;

					$total_balance -= $payments_value;

					$result .= 
								'
									<tr>
										<td>'.$count.'</td>
										<td>'.$patient_number.'</td>
										<td>'.$initials.' </td>
										<td>'.$patient_surname.' '.$patient_othernames.' '.$patient_first_name.'</td>
										<td>-</td>
										<td>-</td>
										<td>('.number_format($payments_value,2).')</td>
										<td>'.number_format($payments_value,2).'</td>
										<td>'.strtoupper($payment_type_name).'</td>
									</tr> 
								';
					$result .= $add_payments;
				}
				
			}




			// add visit credit notes for today


			$where = 'patients.patient_id = v_transactions_by_date.patient_id  AND (v_transactions_by_date.transaction_date = v_transactions_by_date.invoice_date) AND v_transactions_by_date.reference_id > 0 AND v_transactions_by_date.transactionCategory = "Credit Note" ';
			$table = 'patients,v_transactions_by_date';
			$visit_search = $this->session->userdata('drb_report_search');
		
			if(!empty($visit_search))
			{
				$where .= $visit_search;
			
				
				
			}
			else
			{
				$where .= ' AND  v_transactions_by_date.transaction_date = "'.date('Y-m-d').'" ';
			
				$visit_payments = ' AND payments.payment_created = \''.date('Y-m-d').'\'';
				$visit_invoices = ' AND v_transactions_by_date.transaction_date= \''.date('Y-m-d').'\'';
				$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

				
			


				$where .= '';

			}
		
		
			$query_two = $this->hospital_reports_model->get_drb_sales_items($table, $where,2);
			// var_dump($query_two);die();

			if($query_two->num_rows() > 0)
			{
				foreach ($query_two->result() as $key => $value2) {
					# code...
					$dr_amount = $value2->dr_amount;
					$cr_amount = $value2->cr_amount;
					$transaction_id = $value2->transaction_id;
					$reference_code = $value2->reference_code;
					$personnel_name = $value2->personnel_name;
					$payment_type_name = $value2->payment_type_name;
					$patient_surname = $value2->patient_surname;
					$patient_othernames = $value2->patient_othernames;
					$patient_first_name = $value2->patient_first_name;
					$patient_number = $value2->patient_number;
					$transactionCategory = $value2->transactionCategory;
					// $payment_type_name = $value2->payment_type_name;


					if($transactionCategory =="Credit Note" )
					{
						$dr_amount = -$cr_amount;
					}
					$count++;

					$initials = $this->hospital_reports_model->initials($personnel_name);

					$payments_query = $this->accounts_model->get_payment_details($transaction_id);
					$credit_note = $this->accounts_model->get_visit_invoice_credit_notes($transaction_id);
					$add_payments = '';
					$payments_value = 0;

					// var_dump($payments_query->result());die();
					if($payments_query->num_rows() > 0)
					{
						foreach ($payments_query->result() as $key => $value4) {
							# code...
							$payment_method = $value4->payment_method;
							$confirm_number = $value4->confirm_number;
							$total_amount = $value4->payment_item_amount;
							$payment_method_id = $value4->payment_method_id;

							$payments_value += $total_amount;
							if($payment_method_id == 5)
							{
								$confirm_number = $value4->transaction_code;
							}

							$add_payments .= 
											'
												<tr>
													<td colspan="2"></td>
													<td colspan="5"><strong>METHOD : </strong>'.$payment_method.' '.$payment_method_id.'<strong>REF : </strong>'.$confirm_number.' <strong>AMOUNT:</strong>'.number_format($payments_value,2).'  </td>
													
													<td colspan="2"></td>
												</tr> 
											';
						}
					}
					$dr_amount -= $credit_note;
					$total_invoiced += $dr_amount;
					if($transactionCategory =="Credit Note" )
					{
						$payments_value = 0;
						$balance = 0;
					}
					else
					{
						$total_payments += $payments_value;
						$balance  = $this->accounts_model->balance($payments_value,$dr_amount);
					}

					
					

					

					$total_balance += $balance;



					$result .= 
								'
									<tr>
										<td>'.$count.'</td>
										<td>'.$patient_number.' </td>
										<td>'.$initials.' </td>
										<td>'.$patient_surname.' '.$patient_othernames.' '.$patient_first_name.'</td>
										<td>'.$reference_code.'</td>
										<td>'.number_format($dr_amount,2).'</td>
										<td>('.number_format($payments_value,2).')</td>
										<td>('.number_format($payments_value,2).')</td>
										<td>'.strtoupper($payment_type_name).'</td>
									</tr> 
								';
					$result .= $add_payments;
				}
				
			}

							
		$result .= 
				'
				
						<tr>
						  <th colspan="4"></th>
						  <th>Total</th>
						  <th>'.number_format($total_invoiced,2).'</th>
						  <th>'.number_format($total_payments,2).'</th>
						  <th>'.number_format($total_invoiced-$total_payments,2).'</th>
					
						</tr>
			';
		$result .= '
			</tbody>
			</table>';
		
		echo $result;
		
?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        
		</section>
    </div>
  </div>