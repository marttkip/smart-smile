
<!DOCTYPE html>
<html lang="en">

    <head>
          <title><?php echo $contacts['company_name'];?> | DAILY SALES RECORD</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
            body
            {
                font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
                font-size:10px;                
            }
            .receipt_spacing{letter-spacing:0px; font-size: 10px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            
            .col-md-6 {
                width: 50%;
             }
        
            h3
            {
                font-size: 30px;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{ margin:0 auto;}
            .col-print-1 {width:8%;  float:left;}
            .col-print-2 {width:16%; float:left;}
            .col-print-3 {width:25%; float:left;}
            .col-print-4 {width:33%; float:left;}
            .col-print-5 {width:42%; float:left;}
            .col-print-6 {width:50%; float:left;}
            .col-print-7 {width:58%; float:left;}
            .col-print-8 {width:66%; float:left;}
            .col-print-9 {width:75%; float:left;}
            .col-print-10{width:83%; float:left;}
            .col-print-11{width:92%; float:left;}
            .col-print-12{width:100%; float:left;}
            table td
            {
            	border: 2px solid grey !important;
            }
            table th
            {
            	border: 2px solid grey !important;
            }
          	@media print {

          		
          	}
            .padd
            {
                padding:10px;
            }
            
        </style>

        <style media="print">
		/*Specific CSS rules for the print version of the webpage */
			table td
	            {
	            	border: 2px solid grey !important;
	            }
	            table th
	            {
	            	border: 2px solid grey !important;
	            }
		</style>
    </head>
    <body class="receipt_spacing">
        <div class="padd">
             <div class="row">
                <div class="col-md-12">
                    <div class="col-print-6" style="text-align: left;">
                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive"/>
                        <!-- <p style="margin-top: 20px !important;"> Dr. Lorna Sande | Dr. Kinoti M, Dental Surgeons</p> -->
                    </div>
                    <div class="col-print-6 " style="text-align: right;">
                        <!-- <strong>
                            Professor Nelson Awori Centre, Ground Floor<br/>
                            Suite A3 & A1, Ralph Bunche Rd No. 7<br/>
                            Telephone: +254 20 2430110 <br/>
                            +254 706 706 000 +254 700 033 337 <br/>
                            accounts@upperhilldentalcentre.com <br/>
                            P.O. Box 1998600202 Nairobi
                         
                        </strong> -->
                        <div  class="align-right">
			            	  <?php echo $contacts['company_name'];?><br/>
		                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
		                    E-Mail:<?php echo $contacts['email'];?>.<br> Tel : <?php echo $contacts['phone'];?><br/>
		                    <?php echo $contacts['location'];?><br/>
                         
			            </div>
                    </div>
                    
                </div>
		       <div class="col-md-12">
		       		<div style="margin-top: 10px">
		       		<h4 class="left-align"><?php echo 'DAILY SALES RECORD';?></h4>
		       		<h4 class="left-align"><?php echo strtoupper($this->session->userdata('drb_search_title'));?></h4>
		       		<br>
		       		<?php
						$where = 'patients.patient_id = visit.patient_id AND patients.patient_delete = 0 AND visit.close_card >= 3  AND visit.patient_id NOT IN (SELECT v_transactions_by_date.patient_id FROM v_transactions_by_date WHERE v_transactions_by_date.patient_id = patients.patient_id AND visit.visit_date = v_transactions_by_date.transaction_date)   ';
						$table = 'patients, visit';
						$visit_search = $this->session->userdata('drb_visit_search');
						// var_dump($visit_search);die();
						if(!empty($visit_search))
						{
							$where .= $visit_search;
						
							
							
						}
						else
						{
							$where .= ' AND visit.visit_date = "'.date('Y-m-d').'" ';
						
							$visit_payments = ' AND payments.payment_created = \''.date('Y-m-d').'\'';
							$visit_invoices = ' AND v_transactions_by_date.transaction_date= \''.date('Y-m-d').'\'';
							$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

							// $this->session->set_userdata('drb_search', $visit_invoices);
							// $this->session->set_userdata('drb_payments_search', $visit_payments);
						


							$where .= '';

						}
						
						
						$query = $this->hospital_reports_model->get_drb_sales($table, $where,0);


						$where = 'patients.patient_id = visit.patient_id AND patients.patient_delete = 0 AND visit.close_card >= 3 ';
						$table = 'patients, visit';
						$visit_search = $this->session->userdata('drb_visit_search');
						// var_dump($visit_search);die();
						if(!empty($visit_search))
						{
							$where .= $visit_search;
						
							
							
						}
						else
						{
							$where .= ' AND visit.visit_date = "'.date('Y-m-d').'" ';
						
							$visit_payments = ' AND payments.payment_created = \''.date('Y-m-d').'\'';
							$visit_invoices = ' AND v_transactions_by_date.transaction_date= \''.date('Y-m-d').'\'';
							$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

							// $this->session->set_userdata('drb_search', $visit_invoices);
							// $this->session->set_userdata('drb_payments_search', $visit_payments);
						


							$where .= '';

						}
						
						
						$query_new = $this->hospital_reports_model->get_drb_sales($table, $where,0);
						$total_patients = $query_new->num_rows();


					?>
					<?php
							$result = '';
							$search = $this->session->userdata('drb_search');

							
							$total_invoiced = 0;
							$total_balance = 0;

						

							
		$result .= '
						<table class="table table-hover table-bordered table-striped table-responsive col-md-12">

							<thead>
								<th>#</th>
								<th>FILE NUMBER</th>
								<th>DOCTOR</th>
								<th>PATIENT NAME</th>
								<th>INVOICE NUMBER</th>
								<th>AMOUNT CHARGED</th>
								<th>AMOUNT PAID</th>
								<th>BALANCE</th>
								<th></th>
						  </thead>
						  <tbody>

						';
			$count = 0;
			$total_payments = 0;
	

			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $row)
				{

					$visit_date = date('d.m.Y',strtotime($row->transaction_date));
					
					$patient_id = $row->patient_id;
					$dependant_id = $row->dependant_id;
					// $transaction_category = $row->transaction_category;
					$patient_surname = $row->patient_surname;
					$patient_othernames = $row->patient_othernames;
					$patient_first_name = $row->patient_first_name;
					$patient_number = $row->patient_number;
					$visit_type_name = $row->visit_type_name;
					$personnel_onames = $row->personnel_onames;
					$personnel_fname = $row->personnel_fname;
					$personnel_name2 = $personnel_fname.' '.$personnel_onames;

					$visit_id = $row->visit_id;

					$initials = $this->hospital_reports_model->initials($personnel_name2);
					$count++;
					// check if the patient has on accont payment
					$payments_query = $this->accounts_model->get_visit_invoice_payments('null',1,$patient_id);

					$add_payments = '';
					$payments_value = 0;
					// var_dump($payments_query->num_rows());die();
				
					$result .= 
									'
										<tr>
											<td>'.$count.'</td>
											<td>'.$patient_number.'</td>
											<td>'.$initials.' </td>
											<td>'.$patient_surname.' '.$patient_othernames.' '.$patient_first_name.'</td>
											<td>-</td>
											<td>-</td>
											<td>-</td>
											<td>-</td>
											<td>'.strtoupper($visit_type_name).'</td>
										</tr> 
									';
				}
			}

			
			


			$where = 'patients.patient_id = v_transactions_by_date.patient_id  AND (v_transactions_by_date.transaction_date = v_transactions_by_date.invoice_date) AND v_transactions_by_date.reference_id > 0 ';
			$table = 'patients,v_transactions_by_date';
			$visit_search = $this->session->userdata('drb_report_search');
		
			if(!empty($visit_search))
			{
				$where .= $visit_search;
			
				
				
			}
			else
			{
				$where .= ' AND  v_transactions_by_date.transaction_date = "'.date('Y-m-d').'" ';
			
				$visit_payments = ' AND payments.payment_created = \''.date('Y-m-d').'\'';
				$visit_invoices = ' AND v_transactions_by_date.transaction_date= \''.date('Y-m-d').'\'';
				$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

				
			


				$where .= '';

			}
		
		
			$query_two = $this->hospital_reports_model->get_drb_sales_items($table, $where,2);
			// var_dump($query_two);die();

			if($query_two->num_rows() > 0)
			{
				foreach ($query_two->result() as $key => $value2) {
					# code...
					$dr_amount = $value2->dr_amount;
					$cr_amount = $value2->cr_amount;
					$transaction_id = $value2->transaction_id;
					$visit_invoice_id = $value2->reference_id;
					$reference_code = $value2->reference_code;
					$personnel_name = $value2->personnel_name;
					$payment_type_name = $value2->payment_type_name;
					$patient_surname = $value2->patient_surname;
					$patient_othernames = $value2->patient_othernames;
					$patient_first_name = $value2->patient_first_name;
					$patient_number = $value2->patient_number;
					// $payment_type_name = $value2->payment_type_name;
					$count++;

					$initials = $this->hospital_reports_model->initials($personnel_name);
					$dr_amount = $this->accounts_model->get_visit_invoice_total($visit_invoice_id);
					$visit_invoice_number = $this->accounts_model->get_visit_invoice_number($visit_invoice_id);
					// $payments_query = $this->accounts_model->get_payment_details($transaction_id);
					$payments_query = $this->accounts_model->get_visit_payment_details($visit_invoice_id);
					$credit_note = 0;//$this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);
					$add_payments = '';
					$payments_value = 0;

					if(empty($visit_invoice_number))
					{
						$reference_code = '';
					}
					else
					{
						$reference_code = $visit_invoice_number;
					}

					// var_dump($payments_query->result());die();
					if($payments_query->num_rows() > 0)
					{
						foreach ($payments_query->result() as $key => $value4) {
							# code...
							$payment_method = $value4->payment_method;
							$confirm_number = $value4->confirm_number;
							$total_amount = $value4->payment_item_amount;
							$payment_method_id = $value4->payment_method_id;

							$payments_value += $total_amount;
							if($payment_method_id == 5)
							{
								$confirm_number = $value4->transaction_code;
							}

							$add_payments .= 
											'
												<tr>
													<td colspan="2"></td>
													<td colspan="5"><strong>METHOD : </strong>'.$payment_method.' '.$payment_method_id.'<strong>REF : </strong>'.$confirm_number.' <strong>AMOUNT:</strong>'.number_format($payments_value,2).'  </td>
													
													<td colspan="2"></td>
												</tr> 
											';
						}
					}

					$total_payments += $payments_value;
					$dr_amount -= $credit_note;
					$total_invoiced += $dr_amount;

					$balance  = $this->accounts_model->balance($payments_value,$dr_amount);

					$total_balance += $balance;
			

					$result .= 
								'
									<tr>
										<td>'.$count.'</td>
										<td>'.$patient_number.' </td>
										<td>'.$initials.' </td>
										<td>'.$patient_surname.' '.$patient_othernames.' '.$patient_first_name.'</td>
										<td>'.$reference_code.'</td>
										<td>'.number_format($dr_amount,2).'</td>
										<td>('.number_format($payments_value,2).')</td>
										<td>'.number_format($balance,2).'</td>
										<td>'.strtoupper($payment_type_name).'</td>
									</tr> 
								';
					$result .= $add_payments;
				}
				
			}


			// Add patients that have  payments without invoices for the day

			$where = 'patients.patient_id = v_transactions_by_date.patient_id  AND (v_transactions_by_date.transaction_date <> v_transactions_by_date.invoice_date) AND v_transactions_by_date.reference_id > 0 ';
			$table = 'patients,v_transactions_by_date';
			$visit_search = $this->session->userdata('drb_report_search');
		
			if(!empty($visit_search))
			{
				$where .= $visit_search;
			
				
				
			}
			else
			{
				$where .= ' AND  v_transactions_by_date.transaction_date = "'.date('Y-m-d').'" ';
			
				$visit_payments = ' AND payments.payment_created = \''.date('Y-m-d').'\'';
				$visit_invoices = ' AND v_transactions_by_date.transaction_date= \''.date('Y-m-d').'\'';
				$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

				
			


				$where .= '';

			}
		
		
			$query_two = $this->hospital_reports_model->get_drb_sales_items($table, $where,2);
			// var_dump($query_two);die();

			if($query_two->num_rows() > 0)
			{
				foreach ($query_two->result() as $key => $value2) {
					# code...
					$dr_amount = $value2->dr_amount;
					$cr_amount = $value2->cr_amount;
					$transaction_id = $value2->transaction_id;
					$reference_code = $value2->reference_code;
					$personnel_name = $value2->personnel_name;
					$payment_type_name = $value2->payment_type_name;
					$patient_surname = $value2->patient_surname;
					$patient_othernames = $value2->patient_othernames;
					$patient_first_name = $value2->patient_first_name;
					$patient_number = $value2->patient_number;
					$transactionCategory = $value2->transactionCategory;
					// $payment_type_name = $value2->payment_type_name;


					if($transactionCategory =="Credit Note" )
					{
						$dr_amount = -$cr_amount;
					}
					$count++;

					$initials = $this->hospital_reports_model->initials($personnel_name);

					$payments_query = $this->accounts_model->get_payment_details($transaction_id);
					$credit_note = $this->accounts_model->get_visit_invoice_credit_notes($transaction_id);
					$add_payments = '';
					$payments_value = 0;

					// var_dump($payments_query->result());die();
					if($payments_query->num_rows() > 0)
					{
						foreach ($payments_query->result() as $key => $value4) {
							# code...
							$payment_method = $value4->payment_method;
							$confirm_number = $value4->confirm_number;
							$total_amount = $value4->payment_item_amount;
							$payment_method_id = $value4->payment_method_id;

							$payments_value += $total_amount;
							if($payment_method_id == 5)
							{
								$confirm_number = $value4->transaction_code;
							}

							$add_payments .= 
											'
												<tr>
													<td colspan="2"></td>
													<td colspan="5"><strong>METHOD : </strong>'.$payment_method.' '.$payment_method_id.'<strong>REF : </strong>'.$confirm_number.' <strong>AMOUNT:</strong>'.number_format($payments_value,2).'  </td>
													
													<td colspan="2"></td>
												</tr> 
											';
						}
					}
					$dr_amount -= $credit_note;
					$total_invoiced += $dr_amount;
					if($transactionCategory =="Credit Note" )
					{
						$payments_value = 0;
						$balance = 0;
					}
					else
					{
						$total_payments += $payments_value;
						$balance  = $this->accounts_model->balance($payments_value,$dr_amount);
					}


					$total_balance += $balance;



					$result .= 
								'
									<tr>
										<td>'.$count.'</td>
										<td>'.$patient_number.' </td>
										<td>'.$initials.' </td>
										<td>'.$patient_surname.' '.$patient_othernames.' '.$patient_first_name.'</td>
										<td>'.$reference_code.'</td>
										<td>'.number_format($dr_amount,2).'</td>
										<td>('.number_format($payments_value,2).')</td>
										<td>('.number_format($payments_value,2).')</td>
										<td>'.strtoupper($payment_type_name).'</td>
									</tr> 
								';
					$result .= $add_payments;
				}
				
			}

			// Add patients that have just made payments without invoices for the day

			$where = 'patients.patient_id = v_transactions_by_date.patient_id AND v_transactions_by_date.transactionCategory = "Revenue Payment" AND (v_transactions_by_date.transaction_date > v_transactions_by_date.invoice_date OR transactionClassification ="On account Patients Payment") AND v_transactions_by_date.reference_id = 0 ';
			$table = 'patients,v_transactions_by_date';
			$visit_search = $this->session->userdata('drb_report_search');
		
			if(!empty($visit_search))
			{
				$where .= $visit_search;
			
				
				
			}
			else
			{
				$where .= ' AND  v_transactions_by_date.transaction_date = "'.date('Y-m-d').'" ';
			
				$visit_payments = ' AND payments.payment_created = \''.date('Y-m-d').'\'';
				$visit_invoices = ' AND v_transactions_by_date.transaction_date= \''.date('Y-m-d').'\'';
				$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

				
			


				$where .= '';

			}
			
			
			$query_two = $this->hospital_reports_model->get_drb_sales_items($table, $where,0);


			if($query_two->num_rows() > 0)
			{
				foreach ($query_two->result() as $key => $value2) {
					# code...

					$cr_amount = $value2->cr_amount;
					$transaction_id = $value2->transaction_id;
					$reference_code = $value2->reference_code;
					$personnel_name = $value2->personnel_name;
					$payment_type_name = $value2->payment_type_name;
					$patient_surname = $value2->patient_surname;
					$patient_othernames = $value2->patient_othernames;
					$patient_first_name = $value2->patient_first_name;
					$patient_number = $value2->patient_number;
					// $payment_type_name = $value2->payment_type_name;
					$count++;

					$initials = $this->hospital_reports_model->initials($personnel_name);

					$payments_query = $this->accounts_model->get_payment_details($transaction_id);
					$add_payments = '';
					$payments_value = 0;

					// var_dump($payments_query->result());die();
					if($payments_query->num_rows() > 0)
					{
						foreach ($payments_query->result() as $key => $value4) {
							# code...
							$payment_method = $value4->payment_method;
							$confirm_number = $value4->confirm_number;
							$total_amount = $value4->payment_item_amount;
							$payment_method_id = $value4->payment_method_id;

							$payments_value += $total_amount;
							if($payment_method_id == 5)
							{
								$confirm_number = $value4->transaction_code;
							}

							$add_payments .= 
											'
												<tr>
													<td colspan="2"></td>
													<td colspan="5"><strong>METHOD : </strong>'.$payment_method.' '.$payment_method_id.'<strong>REF : </strong>'.$confirm_number.' <strong>AMOUNT:</strong>'.number_format($payments_value,2).'  </td>
													
													<td colspan="2"></td>
												</tr> 
											';
						}
					}
					$total_payments += $payments_value;

					$total_balance -= $payments_value;

					$result .= 
								'
									<tr>
										<td>'.$count.'</td>
										<td>'.$patient_number.'</td>
										<td>'.$initials.' </td>
										<td>'.$patient_surname.' '.$patient_othernames.' '.$patient_first_name.'</td>
										<td>-</td>
										<td>-</td>
										<td>('.number_format($payments_value,2).')</td>
										<td>'.number_format($payments_value,2).'</td>
										<td>'.strtoupper($payment_type_name).'</td>
									</tr> 
								';
					$result .= $add_payments;
				}
				
			}


			$where = 'patients.patient_id = v_transactions_by_date.patient_id  AND (v_transactions_by_date.transaction_date = v_transactions_by_date.invoice_date) AND v_transactions_by_date.reference_id > 0 AND v_transactions_by_date.transactionCategory = "Credit Note" ';
			$table = 'patients,v_transactions_by_date';
			$visit_search = $this->session->userdata('drb_report_search');
		
			if(!empty($visit_search))
			{
				$where .= $visit_search;
			
				
				
			}
			else
			{
				$where .= ' AND  v_transactions_by_date.transaction_date = "'.date('Y-m-d').'" ';
			
				$visit_payments = ' AND payments.payment_created = \''.date('Y-m-d').'\'';
				$visit_invoices = ' AND v_transactions_by_date.transaction_date= \''.date('Y-m-d').'\'';
				$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

				
			


				$where .= '';

			}
		
		
			$query_two = $this->hospital_reports_model->get_drb_sales_items($table, $where,2);
			// var_dump($query_two);die();

			if($query_two->num_rows() > 0)
			{
				foreach ($query_two->result() as $key => $value2) {
					# code...
					$dr_amount = $value2->dr_amount;
					$cr_amount = $value2->cr_amount;
					$transaction_id = $value2->transaction_id;
					$reference_code = $value2->reference_code;
					$personnel_name = $value2->personnel_name;
					$payment_type_name = $value2->payment_type_name;
					$patient_surname = $value2->patient_surname;
					$patient_othernames = $value2->patient_othernames;
					$patient_first_name = $value2->patient_first_name;
					$patient_number = $value2->patient_number;
					$transactionCategory = $value2->transactionCategory;
					// $payment_type_name = $value2->payment_type_name;


					if($transactionCategory =="Credit Note" )
					{
						$dr_amount = -$cr_amount;
					}
					$count++;

					$initials = $this->hospital_reports_model->initials($personnel_name);

					$payments_query = $this->accounts_model->get_payment_details($transaction_id);
					$credit_note = $this->accounts_model->get_visit_invoice_credit_notes($transaction_id);
					$add_payments = '';
					$payments_value = 0;

					// var_dump($payments_query->result());die();
					if($payments_query->num_rows() > 0)
					{
						foreach ($payments_query->result() as $key => $value4) {
							# code...
							$payment_method = $value4->payment_method;
							$confirm_number = $value4->confirm_number;
							$total_amount = $value4->payment_item_amount;
							$payment_method_id = $value4->payment_method_id;

							$payments_value += $total_amount;
							if($payment_method_id == 5)
							{
								$confirm_number = $value4->transaction_code;
							}

							$add_payments .= 
											'
												<tr>
													<td colspan="2"></td>
													<td colspan="5"><strong>METHOD : </strong>'.$payment_method.' '.$payment_method_id.'<strong>REF : </strong>'.$confirm_number.' <strong>AMOUNT:</strong>'.number_format($payments_value,2).'  </td>
													
													<td colspan="2"></td>
												</tr> 
											';
						}
					}
					$dr_amount -= $credit_note;
					$total_invoiced += $dr_amount;
					if($transactionCategory =="Credit Note" )
					{
						$payments_value = 0;
						$balance = 0;
					}
					else
					{
						$total_payments += $payments_value;
						$balance  = $this->accounts_model->balance($payments_value,$dr_amount);
					}

					
					

					

					$total_balance += $balance;



					$result .= 
								'
									<tr>
										<td>'.$count.'</td>
										<td>'.$patient_number.' </td>
										<td>'.$initials.' </td>
										<td>'.$patient_surname.' '.$patient_othernames.' '.$patient_first_name.'</td>
										<td>'.$reference_code.'</td>
										<td>'.number_format($dr_amount,2).'</td>
										<td>('.number_format($payments_value,2).')</td>
										<td>('.number_format($payments_value,2).')</td>
										<td>'.strtoupper($payment_type_name).'</td>
									</tr> 
								';
					$result .= $add_payments;
				}
				
			}
							
		$result .= 
				'
				
						<tr>
						  <th colspan="4"></th>
						  <th>Total</th>
						  <th>'.number_format($total_invoiced,2).'</th>
						  <th>'.number_format($total_payments,2).'</th>
						  <th>'.number_format($total_invoiced-$total_payments,2).'</th>
					
						</tr>
			';
		$result .= '
			</tbody>
			</table>';
		
							
							echo $result;
							
							
					?>
		       			
		       		</div>
		       </div>
		       <p style="page-break-after: always;">&nbsp;</p>
		       <div class="col-md-12">
		       		<div class="col-print-6">
		       			<table class="table table-striped table-hover table-condensed">
                            <tbody>
                                 <tr>
                                    <th>NUMBER OF PATIENTS</th>
                                    <td><?php echo $total_patients;?></td>
                                </tr>
                                
                            </tbody>
                        </table>

                        
                        <h5><strong>DAILY SALES RECORD</strong> </h5>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
								<?php
								$total_cash_breakdown = 0;
								$payment_methods = $this->hospital_reports_model->get_payment_methods();
                                if($payment_methods->num_rows() > 0)
                                {
                                    foreach($payment_methods->result() as $res)
                                    {
                                        $method_name = $res->payment_method;
                                        $payment_method_id = $res->payment_method_id;
                                        $total = $this->hospital_reports_model->get_amount_collected($payment_method_id);
                                 
                                        
                                        echo 
										'
										<tr>
											<th>'.strtoupper($method_name).'</th>
											<td>'.number_format($total, 2).'</td>
										</tr>
										';
										$total_cash_breakdown += $total;
                                    }
                                    
									echo 
									'
									<tr>
										<th>TOTAL</th>
										<td>'.number_format($total_cash_breakdown, 2).'</td>
									</tr>
									';
                                }
                                ?>
                            </tbody>
                        </table>

                         <h5><strong>DAILY SALES TURNOVER</strong> </h5>

                        <table class="table table-striped table-hover table-condensed">
                            <thead>
                                <th>Doctor's</th>
                                <th>Billings</th>
                                <th>Collections</th>
                            </thead>
                            <tbody>
                                <?php
                               
                                $total_cash_breakdown = 0;
                                $total_amount_invoiced = 0;
                                $total_amount_paid = 0;
                                
                                $doctors = $this->hospital_reports_model->get_doctor();
                                 // var_dump($doctors->num_rows());die();
                                 if($doctors->num_rows() > 0)
                                 {
                                    foreach($doctors->result() as $row):
                                        $fname = $row->personnel_fname;
                                        $onames = $row->personnel_onames;
                                        $personnel_id = $row->personnel_id;

                                        // var_dump($personnel_id);die();
                                       
                                             $amount_paid = $this->hospital_reports_model->get_personnel_days_payments($personnel_id);
                                            $amount_invoiced = $this->hospital_reports_model->get_personnel_days_invoices($personnel_id);
                                            $amount_credits = $this->hospital_reports_model->get_personnel_days_credits($personnel_id);
                                            
                                            $amount_invoiced -= $amount_credits;
                                            $total_amount_invoiced += $amount_invoiced;
                                            $total_amount_paid += $amount_paid;

                                            
                                            if($amount_invoiced > 0 OR $amount_paid > 0)
                                            {
                                                echo 
                                                    '
                                                    <tr>
                                                        <th>Dr. '.$onames.' '.$fname.' </th>
                                                        <td>'.number_format($amount_invoiced, 2).'</td>
                                                        <td>'.number_format($amount_paid, 2).'</td>
                                                    </tr>
                                                    ';
                                            }
                                            
                                        
                                        
                                        
                                    endforeach;

                                    $amount_paid = $this->hospital_reports_model->get_personnel_days_payments(0);
                                    $amount_invoiced = $this->hospital_reports_model->get_personnel_days_invoices(0);
                                    $total_amount_invoiced += $amount_invoiced;
                                    $total_amount_paid += $amount_paid;
                                    echo 
                                        '
                                        <tr>
                                            <th>Auxilliary Dental Products</th>
                                            <td>'.number_format($amount_invoiced, 2).'</td>
                                            <td>'.number_format($amount_paid, 2).'</td>
                                        </tr>
                                        ';
                                    echo 
                                            '
                                            <tr>
                                                <th>Total</th>
                                                <th>'.number_format($total_amount_invoiced, 2).'</th>
                                                <th>'.number_format($total_amount_paid, 2).'</th>
                                            </tr>
                                            ';
                                }
                               
                                ?>
                            </tbody>
                        </table>

		       		</div>
		       </div>
		    </div>
		</div>
	</body>
</html>
   		