
<div class="row">

    	<section class="panel">
            <header class="panel-heading">						
                <h2 class="panel-title">Search monthly doctor turnover</h2>
            </header>
            <div class="panel-body">
            	<?php 
				echo form_open('hospital-reports/search-doctors-turnover');
				?>
				<div class="col-md-4">
					<div class="form-group">
	                    <label class="col-lg-5 control-label">Year: </label>
	                    
	                    <div class="col-lg-7">
	                        <input type="text" name="year" class="form-control" size="54" value="<?php echo date("Y");?>" />
	                    </div>
	                </div>
					
				</div>
				<div class="col-md-4">
					<div class="form-group">
	                    <label class="col-lg-5 control-label">Month: </label>
	                    
	                    <div class="col-lg-7">
	                        <select name="month" class="form-control">
	                            <?php
	                                if($month->num_rows() > 0){
	                                    foreach ($month->result() as $row):
	                                        $mth = $row->month_name;
	                                        $mth_id = $row->month_id;
	                                        if($mth == date("M")){
	                                            echo "<option value=".$mth_id." selected>".$row->month_name."</option>";
	                                        }
	                                        else{
	                                            echo "<option value=".$mth_id.">".$row->month_name."</option>";
	                                        }
	                                    endforeach;
	                                }
	                            ?>
	                        </select>
	                    </div>
	                </div>
				</div>
				<div class="col-md-4">
                
                
                
                
	          
	                    <div class="col-lg-7 col-lg-offset-5">
	                        <div class="form-actions center-align">
	                            <button class="submit btn btn-primary" type="submit">
	                                <i class='fa fa-search'></i> Search
	                            </button>
	                        </div>
	                    </div>
	                </div>
                <?php echo form_close();?>
            </div>
        </section>

    
	
</div>

<?php

$month_search = $this->session->userdata('doctors_turnover_month');
$year_search = $this->session->userdata('doctors_turnover_year');


		if(empty($year_search) OR empty($month_search))
		{
			$first_day_this_month = date('m-01-Y'); // hard-coded '01' for first day
		$last_day_this_month  = date('m-t-Y');
		}
		else
		{

			if($month_search < 10)
			{
				$month_search = '0'.$month_search;
			}
			$first_day_this_month = date('01-'.$month_search.'-'.$year_search); // hard-coded '01' for first day

			$last_date = date("Y-m-t", strtotime($first_day_this_month));

			$last_day_this_month  = date("m-t-Y", strtotime($last_date));
			// $last_day_this_month  = date($month_search.'-t-'.$year_search);
		}
		// var_dump($last_day_this_month);die();
// $first_day_this_month = date('m-01-Y'); // hard-coded '01' for first day
// $last_day_this_month  = date('m-t-Y');
			// var_dump($last_day_this_month);die();

$last_day_date_explode = explode('-', $last_day_this_month);



$count = $last_day_date_explode[1];
$month = $last_day_date_explode[0];
$year = $last_day_date_explode[2];
$todays_date = $year.'-'.$month.'-01';

$report_date= date('M Y',strtotime($todays_date));
?>                        
 
<div class="row">


        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title"><?php echo $title.' '.$report_date.' Report';?></h2>
            	  <div class="widget-icons pull-right" style="margin-top: -24px !important;">
            	  	<a href="<?php echo site_url().'print-doctors-turnover'?>" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Print Turnover</a>
            	  	<!-- <a href="<?php echo site_url().'export-doctors-turnover'?>" target="_blank" class="btn btn-sm btn-success"><i class="fa fa-arrow-up"></i> Export Turnover</a> -->
            	</div>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
          
<?php
		$result = '';
		$search = $this->session->userdata('doctors_turnover_searched');
		if(!empty($search))
		{
			echo '<a href="'.site_url().'hospital_reports/close_doctors_turnover_search" class="btn btn-sm btn-warning">Close Search</a>';
		}


		

	       		

			// var_dump($count);die();
			$result = '
						<h4 class="center-align"> '.$report_date.'</h4>
						<br>
						<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
						  <tbody>
							';


            $doctors = $this->hospital_reports_model->get_doctor();
             // var_dump($doctors->num_rows());die();
            $list = '';
           	$list_two = '';
             if($doctors->num_rows() > 0)
             {
                foreach($doctors->result() as $row):
                    $fname = $row->personnel_fname;
                    $onames = $row->personnel_onames;
                    $personnel_id = $row->personnel_id;

                    $list .= '<th colspan="2">Dr. '.$fname.' '.$onames.'</th>';
                     $list_two .= '<th >Billings</th>
                     			   <th >Collections</th>';
                endforeach;
              }
              $list .= '<th colspan="2">ADP</th>';
              $list_two .= '<th >Billings</th>
                     			   <th >Collections</th>';
			$result .= 
							'
							
									<tr>
									  <th>#</th>
									  '.$list.'
									</tr>
						';


			$result .= 
							'
							
									<tr>
									  <th></th>
									  '.$list_two.'
									</tr>
						';
						for ($i=1; $i <=  $count; $i++) { 
							# code...

							$date = $i.'.'.$month;


							if($i < 10)
							{
								$day = '0'.$i;
							}
							else
							{
								$day = $i;
							}
							$todays_date = $year.'-'.$month.'-'.$day;

							// var_dump($todays_date);die();
						
				           	$list_two = '';
				             if($doctors->num_rows() > 0)
				             {
				                foreach($doctors->result() as $row):
				                    $fname = $row->personnel_fname;
				                    $onames = $row->personnel_onames;
				                    $personnel_id = $row->personnel_id;


				                    $amount_paid = $this->hospital_reports_model->get_personnel_days_payments($personnel_id,$todays_date);
                                    $amount_invoiced = $this->hospital_reports_model->get_personnel_days_invoices($personnel_id,$todays_date);
                                    $amount_credits = $this->hospital_reports_model->get_personnel_days_credits($personnel_id,$todays_date);
                                     $amount_paid += $amount_credits;


                                    
                                    $amount_invoiced -= $amount_credits;


                                    $array['collection'.$personnel_id] += $amount_paid;
                                    $array['invoice'.$personnel_id] += $amount_invoiced;
                                  

				                     $list_two .= '<td >'.number_format($amount_invoiced,2).'</td>
				                     			   <td >'.number_format($amount_paid,2).'</td>';



				                endforeach;
				              }


				              $amount_paid = $this->hospital_reports_model->get_personnel_days_payments(0,$todays_date);
                              $amount_invoiced = $this->hospital_reports_model->get_personnel_days_invoices(0,$todays_date);
                               $array['collection0'] += $amount_paid;
                                $array['invoice0'] += $amount_invoiced;

                              $list_two .= '<td >'.number_format($amount_invoiced,2).'</td>
				                     		<td >'.number_format($amount_paid,2).'</td>';

							 $result .= 
										'
										
												<tr>
												  <th>'.$date.'</th>
												  '.$list_two.'
												</tr>
										';
							
						}

						$list_two = '';
						$grand_invoices = 0;
						$grand_collections = 0;

						$grand_director_collections = 0;
						$grand_director_invoices = 0;
						$grand_associates_invoices = 0;
						$grand_associates_collections = 0;


				             if($doctors->num_rows() > 0)
				             {
				                foreach($doctors->result() as $row):
				                    $fname = $row->personnel_fname;
				                    $onames = $row->personnel_onames;
				                    $personnel_id = $row->personnel_id;
				                    $personnel_type_id = $row->personnel_type_id;

				                    if($personnel_type_id == 4)
				                    {
				                    	$grand_director_invoices += $array['invoice'.$personnel_id];
				                    	$grand_director_collections += $array['collection'.$personnel_id];
				                    }
				                    else if($personnel_type_id == 2)
				                    {
				                    	$grand_associates_invoices += $array['invoice'.$personnel_id];
				                    	$grand_associates_collections += $array['collection'.$personnel_id];
				                    }
				                    $total_amount_collection = $array['collection'.$personnel_id];
									$total_amount_invoiced = $array['invoice'.$personnel_id];

									$grand_invoices += $total_amount_invoiced;
									$grand_collections += $total_amount_collection;

									 $list_two .= '<th >'.number_format($total_amount_invoiced,2).'</th>
				                     			   <th >'.number_format($total_amount_collection,2).'</th>';
				                 endforeach;
				             }
				            $total_amount_collection = $array['collection0'];
							$total_amount_invoiced = $array['invoice0'];

							$grand_invoices += $total_amount_invoiced;
							$grand_collections += $total_amount_collection;
				             $list_two .= '<th >'.number_format($total_amount_invoiced,2).'</th>
				                     			   <th >'.number_format($total_amount_collection,2).'</th>';
						 $result .= 
										'
										
												<tr>
												  <th>Total</th>
												  '.$list_two.'
												</tr>
										';


					$result .= 
				'
							  </tbody>
							</table>
						

				';
		
		echo $result;
		
?>

			<div class="row">
	         	<div class="col-md-4">
	         		<table class="table table-striped table-bordered">
	         			<thead>
	         				<tr>
	         					<th>TOTAL BILLINGS</th>
	         					<th><?php echo number_format($grand_invoices,2)?></th>
	         				</tr>
	         				<tr>
	         					<th>TOTAL COLLECTIONS</th>
	         					<th><?php echo number_format($grand_collections,2)?></th>
	         				</tr>
	         			</thead>
	         		</table>
	         	</div>
	         	
	         </div>

	         <div class="row">
	         	<div class="col-md-12">
	         		<?php

	         			$result = '
						<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
						  <tbody>
							';


			       
			            $list = '';
			           	$list_two = '';
			           	$list_three = '';
						if($doctors->num_rows() > 0)
						{
			                foreach($doctors->result() as $row):
			                    $fname = $row->personnel_fname;
			                    $onames = $row->personnel_onames;
			                    $personnel_id = $row->personnel_id;
			                    $professional_fees_percentage = $row->professional_fees_percentage;



			                    $total_amount_collection = $array['invoice'.$personnel_id];

			                    $amount = $total_amount_collection * ($professional_fees_percentage/100);
			                    $amount_vat =$amount - ($amount * 0.05);


			                    $list .= '<th colspan="1">Dr. '.$fname.' '.$onames.'</th>
			                    		 ';
			                   	$list_two .= '<td colspan="1">'.number_format($amount,2).'</td>
			                    		 ';
			                    $list_three .= '<td colspan="1">'.number_format($amount_vat,2).'</td>
			                    		 ';
			                     
			                endforeach;
			              }
			               $list .= '<th colspan="1">ADP</th>';


			               $list_two .= '<td colspan="1">-</td>';
		                   $list_three .= '<td colspan="1">-</td>';

			              $result .= 
										'
										
												<tr>
												  <th></th>
												  '.$list.'
												</tr>
									';
						$result .= 
										'
										
												<tr>
												  <th>30% PROF FEES</th>
												  '.$list_two.'
												</tr>
									';
						$result .= 
										'
										
												<tr>
												  <th>5% WHT</th>
												  '.$list_three.'
												</tr>
									';
						
						$result .= 
						'
									  </tbody>
									</table>
								

						';

						echo $result;

						$total_amount_collection = $array['collection0'];
						$total_amount_invoiced = $array['invoice0'];

						$totals_invoice = $grand_director_invoices+$grand_associates_invoices+$total_amount_invoiced;
						$totals_payments = $grand_director_collections+$grand_associates_collections+$total_amount_collection;
	         		?>

	         </div>
	     </div>

	     <div class="row">
	         	<div class="col-md-4">
	         		<table class="table table-striped table-bordered">
	         			<thead>
	         				<tr>
	         					<th></th>
	         					<th>Billings</th>
	         					<th>Collections</th>
	         				</tr>
	         				<tr>
	         					<th>Directors</th>
	         					<td><?php echo number_format($grand_director_invoices,2)?></td>
	         					<td><?php echo number_format($grand_director_collections,2)?></td>
	         				</tr>
	         				<tr>
	         					<th>Associates</th>
	         					<td><?php echo number_format($grand_associates_invoices,2)?></td>
	         					<td><?php echo number_format($grand_associates_collections,2)?></td>
	         				</tr>
	         				<tr>
	         					<th>ADP</th>
	         					<td><?php echo number_format($total_amount_invoiced,2)?></td>
	         					<td><?php echo number_format($total_amount_collection,2)?></td>
	         				</tr>


	         				<tr>
	         					<th>TOTAL</th>
	         					<th><?php echo number_format($grand_invoices,2)?></th>
	         					<th><?php echo number_format($grand_collections,2)?></th>
	         				</tr>
	         			</thead>
	         		</table>
	         	</div>
	         	
	         </div>


          </div>
          
         
        
		</section>
  </div>
