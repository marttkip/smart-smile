<!-- search -->
<?php echo $this->load->view('search_payments', '', TRUE);?>
<!-- end search -->
<?php echo $this->load->view('payments_statistics', '', TRUE);?>
 
<div class="row">
    <div class="col-md-12">

        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title"><?php echo $title;?></h2>
            </header>             

          <!-- Widget content -->
          <div class="panel-body">
          <h5 class="center-align"><?php echo $this->session->userdata('cash_search_title');?></h5>
<?php
		$result = '<a href="'.site_url().'hospital_reports/export_cash_report" class="btn btn-sm btn-success pull-right">Export</a>';
		if(!empty($search))
		{
			echo '<a href="'.site_url().'hospital_reports/close_cash_search" class="btn btn-sm btn-warning">Close Search</a>';
		}
		$period_payment =0;
		$debt_payment = 0;
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
				'
					<table class="table table-bordered table-striped table-responsive col-md-12">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Payment Type</th>
						  <th>Payment Date</th>
						  <th>Invoice Date</th>
						  <th>Patient</th>
						  <th>Payment Method</th>
						  <th>Type</th>
						  <th>Amount</th>
						  <th>Receipt No</th>
						  <th>Paid to</th>
						  <th>Branch</th>
						  <th>Recorded by</th>
						</tr>
					  </thead>
					  <tbody>
			';
			$total_payments = 0;
			foreach ($query->result() as $row)
			{
				$count++;
				$total_invoiced = 0;
				$payment_date = $row->transaction_date;
				$payment_created = date('jS M Y',strtotime($row->transaction_date));
				$time = date('H:i a',strtotime($row->created_at));
				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$personnel_id = $row->personnel_id;
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
				$payment_method = $row->payment_method;
				$cr_amount = $row->cr_amount;
				$transaction_code = $row->transaction_code;
				$reference_code = $row->reference_code;
				$transaction_description = $row->transaction_description;
				$transactionClassification = $row->transactionClassification;
				$visit_invoice_number = $row->visit_invoice_number;
				$invoice_date = $row->invoice_date;
				$branch_code = $row->branch_code;
				$created_by = $row->personnel_fname.' '.$row->personnel_onames;

				if(!empty($invoice_date))
				{
					$invoice = date('jS M Y',strtotime($row->invoice_date));
				}
				else
				{
					$invoice ='';
				}
				

				if($payment_date == $invoice_date)
				{
					$type = 'Period Payment';
					$color = 'success';
					$period_payment += $cr_amount;
				}
				else
				{
					$type = 'Debt repayment';
					$color = 'info';
					$debt_payment += $cr_amount;
				}
				
				$total_payments += $cr_amount;

					$result .= 
						'
							<tr>
								<td class="'.$color.'">'.$count.'</td>
								<td class="'.$color.'">'.$type.'</td>
								<td>'.$payment_created.'</td>
								<td>'.$invoice.'</td>
								<td>'.ucwords(strtolower($patient_surname)).'</td>
								<td>'.$payment_method.'</td>
								<td>'.$transactionClassification.'</td>
								<td>'.number_format($cr_amount, 2).'</td>
								<td>'.$reference_code.'</td>
								<td>'.$visit_invoice_number.'</td>
								<td>'.$branch_code.'</td>
								<td>'.$created_by.'</td>
							</tr> 
					';

			}
			$result .= 
						'
							<tr>
								<th></th>
								<th></th>
								<th></th>

								<th></th>
								<th></th>
								<th></th>
								<th>Total</th>
								<th>'.number_format($total_payments, 2).'</th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr> 
					';
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no payments";
		}
		
		echo $result;

		$data_checked['period_payment'] = $period_payment;
		$data_checked['debt_payment'] = $debt_payment;
?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        
		</section>
    </div>
  </div>

 