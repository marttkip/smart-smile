<?php
// $group_incomplete_rs = $this->accounts_model->get_incomplete_invoices($patient_id,1);
// $incomplete_result = '';
// $date_visit = '';
// if($group_incomplete_rs->num_rows() > 0)
// {
// 	foreach ($group_incomplete_rs->result() as $key => $value2) {

// 		$visit_date = $value2->date;
		// $visit_id = $value2->visit_id;
		$visit_date = date('Y-m-d');

		$incomplete_rs = $this->accounts_model->get_incomplete_invoices($patient_id,NULL,$visit_date);

	

		if($incomplete_rs->num_rows() > 0)
		{
			$incomplete_result .= '<tr class="primary">
									<td colspan="4">'.date('jS M Y',strtotime($visit_date)).'</td>
								</tr>';
			foreach ($incomplete_rs->result() as $key => $value) {
				# code...
				
				$visit_charge_amount = $value->visit_charge_amount;
				$visit_charge_qty = $value->visit_charge_qty;
				$visit_charge_units = $value->visit_charge_units;
				$service_charge_name = $value->service_charge_name;

				
				$total_visit_charge = $visit_charge_units*$visit_charge_amount;
				$incomplete_result .= '<tr>
											<td>'.$service_charge_name.'</td>
											<td>'.number_format($visit_charge_units).'</td>
											<td>'.number_format($visit_charge_amount,2).'</td>
											<td>'.number_format($total_visit_charge,2).'</td>
										</tr>';
				

			}
			$incomplete_result .= '<tr class="warning">
									<td colspan="4"><a class="btn btn-xs btn-success pull-right" onclick="add_invoice('.$patient_id.','.$visit_id.','.$visit_date.')"> Complete Invoice</a></td>
								</tr>';
		}
		
// 	}
// }

?>
<table class="table table-bordered table-condensed">
	<thead>
		<th>Service Charge</th>
		<th>Units</th>
		<th>Amount</th>
		<th>Total</th>
	</thead>
	<tbody>
		<?php echo $incomplete_result;?>
	</tbody>
</table>