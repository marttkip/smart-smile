<?php 

// var_dump($query);die();
if($query->num_rows() > 0)
{
	foreach ($query->result() as $key => $value) {
		# code...
		$patient_id = $value->patient_id;
		$patient_surname = $value->patient_surname;
		$patient_first_name = $value->patient_first_name;
		$patient_othernames = $value->patient_othernames;
		$patient_phone1 = $value->patient_phone1;
		$patient_email = $value->patient_email;
		$patient_number = $value->patient_number;
		$patient_date_of_birth = $value->patient_date_of_birth;
		$patient_age = $value->patient_age;
		$gender_id = $value->gender_id;
		$total_balance = $value->total_balance;

		
			$patient_name = $patient_othernames.' '.$patient_surname;
		
	}
}

if($gender_id == 1)
{
	$gender = 'Male';
}
else if($gender_id == 2)
{
	$gender = 'Female';
}

if($patient_date_of_birth == "0000-00-00" OR empty($patient_date_of_birth))
{
   $patient_date_of_birth = '';

   if(!empty($patient_age))
   {
   	$age = $patient_age;
   }
   else
   {
   	 $age = '';
   }
   $dob = '';
}
else
{
	$dob= date('jS M Y',strtotime($patient_date_of_birth));
	$age = $this->reception_model->calculate_age($patient_date_of_birth);
}

$items = '';
if($visit_id > 0)
{

	// $all_departments_rs = $this->reception_model->get_patient_departments($visit_id);



	// if($all_departments_rs->num_rows() > 0)
	// {
	// 	foreach ($all_departments_rs->result() as $key => $value) {
	// 		# code...

	// 		$department_name = $value->department_name;
	// 		$created = $value->created;
	// 		$picked = $value->picked;
	// 		$personnel_onames = $value->personnel_onames;


	// 		if($picked == 0)
	// 		{
	// 			$color = 'red';
	// 		}
	// 		else if($picked == 1)
	// 		{
	// 			$color = 'green';
	// 		}

	// 		else if($picked == 2)
	// 		{
	// 			$color = 'orange';
	// 		}

	// 		$items .= ' <li class="'.$color.'">
	// 						<span class="title">'.$department_name.'</span>
	// 						<span class="description truncate">'.date('jS M Y H:i A',strtotime($created)).'. ~ '.$personnel_onames.'</span>
	// 					</li>';
	// 	}
	// }
}
// $payments = $this->accounts_model->get_patient_payments($patient_id);
// $invoices = $this->accounts_model->get_patient_invoice($patient_id);
// $balance = $invoices - $payments;


$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND (appointments.appointment_status = 4 OR appointments.appointment_status = 7)  AND visit.patient_id ='.$patient_id;
$table = 'patients,visit,appointments';
$total_appointments = 0;//$this->reception_model->count_items($table, $where);

?>
<section class="card" style="margin-top: 50px;">
	<div class="card-body">
		<div class="thumb-info mb-3 " >
			<img src="https://via.placeholder.com/100" class="rounded img-fluid" alt="<?php echo $patient_name?>">
			<div class="thumb-info-title" >
				<span class="thumb-info-inner"><?php echo $patient_name?></span>
				<span class="thumb-info-type"><?php echo $patient_number;?></span>
			</div>
		</div>

		<div class="widget-toggle-expand mb-3">
			
			<div class="widget-content-expanded">
				<ul class="mt-3" style="list-style: none;padding: 0px !important;">
					<li><strong>Gender: </strong><br><?php echo $gender;?></li>
					<li><strong>Birth Date: </strong><br> <?php echo $dob;?></li>
					<li><strong>Age: </strong><br> <?php echo $age;?></li>

				</ul>
			</div>
		</div>


	</div>
</section>

<hr class="dotted short">
<h4 class="mb-3 mt-0">Patient Info</h4>
<ul class="simple-card-list mb-3">
	<li class="primary">
		<h4>Next Appointment</h4>
		<p class="text-light">Appointments.</p>
	</li>
	<li class="danger">
		<h4></h4>
		<p class="text-light">Visit Balance</p>
	</li>
	
</ul>

<h4 class="mb-3 mt-4 pt-2"> Departments Visits </h4>
<ul class="simple-bullet-list mb-3" style="height: 20vh;overflow-y: scroll;">

	<?php echo $items;?>
	<!-- <li class="red">
		<span class="title">Laboratory</span>
		<span class="description truncate">2020-01-02 14:00. ~ Asbel</span>
	</li>
	<li class="green">
		<span class="title">Tucson HTML5 Template</span>
		<span class="description truncate">Lorem ipsom dolor sit amet</span>
	</li>
	<li class="blue">
		<span class="title">Porto HTML5 Template</span>
		<span class="description truncate">Lorem ipsom dolor sit.</span>
	</li>
	<li class="orange">
		<span class="title">Tucson Template</span>
		<span class="description truncate">Lorem ipsom dolor sit.</span>
	</li> -->
</ul>
<!-- 
<h4 class="mb-3 mt-4 pt-2">Messages</h4>
<ul class="simple-user-list mb-3">
	<li>
		<figure class="image rounded">
			<img src="img/!sample-user.jpg" alt="Joseph Doe Junior" class="rounded-circle">
		</figure>
		<span class="title">Joseph Doe Junior</span>
		<span class="message">Lorem ipsum dolor sit.</span>
	</li>
	<li>
		<figure class="image rounded">
			<img src="img/!sample-user.jpg" alt="Joseph Junior" class="rounded-circle">
		</figure>
		<span class="title">Joseph Junior</span>
		<span class="message">Lorem ipsum dolor sit.</span>
	</li>
	<li>
		<figure class="image rounded">
			<img src="img/!sample-user.jpg" alt="Joe Junior" class="rounded-circle">
		</figure>
		<span class="title">Joe Junior</span>
		<span class="message">Lorem ipsum dolor sit.</span>
	</li>
	<li>
		<figure class="image rounded">
			<img src="img/!sample-user.jpg" alt="Joseph Doe Junior" class="rounded-circle">
		</figure>
		<span class="title">Joseph Doe Junior</span>
		<span class="message">Lorem ipsum dolor sit.</span>
	</li>
</ul> -->