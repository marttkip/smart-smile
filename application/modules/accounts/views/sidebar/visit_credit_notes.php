<?php
$patient_items = '';
$visit_type_id = null;
$close_card = 3;
$visit_type_id = 1;
if($query->num_rows() > 0)
{
    foreach ($query->result() as $key => $res) 
    {
        # code...

        $visit_date = date('D M d Y',strtotime($res->visit_date)); 
        $insurance_number = $res->insurance_number;
        $insurance_description = $res->insurance_description;
        $insurance_limit = $res->insurance_limit;
        $visit_type = $res->visit_type;
        $patient_id = $res->patient_id;
        $visit_id = $res->visit_id;
        $visit_type_id = $res->visit_type;
         $close_card = $res->close_card;
        

    }

}
?>
<div>
<section class="panel panel-info">
    <header class="panel-heading">
            <h5 class="pull-left"><i class="icon-reorder"></i><?php echo $title?></h5>
          <div class="widget-icons pull-right">
              
          </div>
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">
        <div class="padd">
        	<div class="center-align">
        		<h3>Credit Note</h3>
        	</div>
            <?php
            // if($close_card == 3)
            // {
                ?>
                    <div class="row" id="invoice-div" style="padding:10px;">
                            <input type="text" name="search_procedures" id="search_procedures" class="form-control" onkeyup="get_procedures_credit_note(<?php echo $patient_id;?>,<?php echo $visit_id;?>,<?php echo $visit_type_id;?>,<?php echo $visit_invoice_id;?>)" placeholder="Search procedures.... root canal,extraction">
                            
                    </div>
                    <input type="hidden" name="visit_type_id" id="visit_type_id" value="<?php echo $visit_type_id?>">
                     <!-- <input type="hidden" name="visit_invoice" id="visit_invoice" value="<?php echo $visit_invoice_id?>"> -->
                    <div class="col-md-12" style="padding:10px;" id="charges-div">
                            <ul  id="searched-procedures" style="list-style: none;">

                            </ul>
                    </div>
                <?php
            // }
            ?>
            <div class="col-md-12" style="padding:10px;">
                    <div id="visit-charges"></div>
            </div>
            <br>
             <div class="col-md-12" style="padding:10px;">
                    <div id="all-credit-notes"></div>
            </div>
        </div>
    </div>
</section>
<div class="col-md-12">                             
    <div class="center-align">
        <a  class="btn btn-sm btn-warning" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR </a>
    </div>  
</div>
</div>
<script type="text/javascript">
        //Calculate procedure total
         function calculatecredittotal(amount, id, procedure_id, v_id,patient_id,visit_credit_note_id){
               
            var units = document.getElementById('units'+id).value;  
            var billed_amount = document.getElementById('billed_amount'+id).value;  

           // alert(billed_amount);
            grand_credit_total(id, units, billed_amount, v_id,patient_id,visit_credit_note_id);

        }
        function grand_credit_total(procedure_id, units, amount, v_id,patient_id,visit_credit_note_id){



             var config_url = document.getElementById("config_url").value;
             var visit_charge_notes = document.getElementById('visit_cr_note_comments'+procedure_id).value;  
             var data_url = config_url+"accounts/update_credit_total/"+procedure_id+"/"+units+"/"+amount+"/"+v_id;
           
              // var tooth = document.getElementById('tooth'+procedure_id).value;
             // alert(data_url);
            $.ajax({
            type:'POST',
            url: data_url,
            data:{procedure_id: procedure_id,visit_charge_notes: visit_charge_notes},
            dataType: 'text',
            success:function(data){
             // get_medication(visit_id);
                 // display_billing(v_id);
                 get_visit_credit_note(v_id,patient_id,visit_credit_note_id);
             // alert('You have successfully updated your billing');
            //obj.innerHTML = XMLHttpRequestObject.responseText;
            },
            error: function(xhr, status, error) {
            //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                get_visit_credit_note(v_id,patient_id,visit_credit_note_id);
                alert(error);
            }

            });
        }


        function delete_procedure(id, visit_id){
            var XMLHttpRequestObject = false;
                
            if (window.XMLHttpRequest) {
            
                XMLHttpRequestObject = new XMLHttpRequest();
            } 
                
            else if (window.ActiveXObject) {
                XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
            }
             var config_url = document.getElementById("config_url").value;
            var url = config_url+"nurse/delete_procedure/"+id;
            // alert(url);
            if(XMLHttpRequestObject) {
                        
                XMLHttpRequestObject.open("GET", url);
                        
                XMLHttpRequestObject.onreadystatechange = function(){
                    
                    if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                     get_visit_charges(visit_id);
                    }
                }
                        
                XMLHttpRequestObject.send(null);
            }
        }
        function change_payer(visit_charge_id, service_charge_id, v_id)
        {
                var res = confirm('Do you want to change who is being billed ? ');

                if(res)
                {

                    var config_url = document.getElementById("config_url").value;
                    var data_url = config_url+"accounts/change_payer/"+visit_charge_id+"/"+service_charge_id+"/"+v_id;
                   
                      // var tooth = document.getElementById('tooth'+procedure_id).value;
                     // alert(data_url);
                    $.ajax({
                    type:'POST',
                    url: data_url,
                    data:{visit_charge_id: visit_charge_id},
                    dataType: 'text',
                    success:function(data){
                     // get_medication(visit_id);
                         get_visit_charges(v_id)
                     alert('You have successfully updated your billing');
                    //obj.innerHTML = XMLHttpRequestObject.responseText;
                    },
                    error: function(xhr, status, error) {
                    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                        get_visit_charges(v_id)
                        alert(error);
                    }

                    });

                }

        }
</script>
        