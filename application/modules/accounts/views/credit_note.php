<?php

//patient details
$visit_type = $patient['visit_type'];
$patient_type = $patient['patient_type'];
$patient_othernames = $patient['patient_othernames'];
$patient_surname = $patient['patient_surname'];
$patient_surname = $patient['patient_surname'];
$patient_firstname = $patient['patient_firstname'];
$patient_number = $patient['patient_number'];
$gender = $patient['gender'];
$patient_insurance_number = $patient['patient_insurance_number'];
$account_balance = $patient['account_balance'];
$inpatient = $patient['inpatient'];
$visit_type_name = $patient['visit_type_name'];
$visit_status = 0;


$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));



// var_dump($visit_credit_note_id);die();
$visit_invoice_detail = $this->accounts_model->get_visit_credit_note_details($visit_credit_note_id);

if($visit_invoice_detail->num_rows() > 0)
{
	foreach ($visit_invoice_detail->result() as $key => $value) {
		# code...
		$visit_cr_note_number = $value->visit_cr_note_number;
		$created = date('d/m/Y',strtotime($value->created));
		$visit_cr_note_amount = $value->visit_cr_note_amount;
		$visit_invoice_number = $value->visit_invoice_number;

		$bill_to  = $value->bill_to;
		$patient_id  = $value->patient_id;



	}
}

// var_dump($patient_id);die();



?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Credit Note</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
        	body
        	{
        		font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
        	}
			.receipt_spacing{letter-spacing:0px; font-size: 14px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			
			.col-md-6 {
			    width: 50%;
			 }
		
			h3
			{
				font-size: 30px;
			}
			p
			{
				margin: 0 0 0px !important;
			}
			
			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{ margin:0 auto;}
			.col-print-1 {width:8%;  float:left;}
			.col-print-2 {width:16%; float:left;}
			.col-print-3 {width:25%; float:left;}
			.col-print-4 {width:33%; float:left;}
			.col-print-5 {width:42%; float:left;}
			.col-print-6 {width:50%; float:left;}
			.col-print-7 {width:58%; float:left;}
			.col-print-8 {width:66%; float:left;}
			.col-print-9 {width:75%; float:left;}
			.col-print-10{width:83%; float:left;}
			.col-print-11{width:92%; float:left;}
			.col-print-12{width:100%; float:left;}

			table.table{
			    border-spacing: 0 !important;
			    font-size:14px;
				margin-top:0px;
				margin-bottom: 10px !important;
				border-collapse: inherit !important;
				font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
			}
			th, td {
			    border: 1.5px solid #000 !important;
			    padding: 0.5em 1em !important;
			}
			/* the first 'th' within the first 'tr' of the 'thead': */
			thead tr:first-child th:first-child {
			    border-radius: 10px 0 0 0 !important;
			}
			/* the last 'th' within the first 'tr' of the 'thead': */
			thead tr:first-child th:last-child {
			    border-radius: 0 10px 0 0 !important;
			}
			/* the first 'td' within the last 'tr' of the 'tbody': */
			tbody tr:last-child td:first-child {
			    border-radius: 0 0 0 10px !important;
			}
			/* the last 'td' within the last 'tr' of the 'tbody': */
			tbody tr:last-child td:last-child {
			    border-radius: 0 0 10px 10px !important;
			}
			tbody tr:first-child td:first-child {
			    border-radius: 10px 10px 0 0 !important;
			    border-bottom: 0px solid #000 !important;
			}
			.padd
			{
				padding:10px;
			}
			
		</style>
    </head>
    <body>
    	
 		

    	<div class="padd " >
    		<div class="col-print-12">
	    		<div class="col-print-6" style="margin-bottom: 10px;">
	            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive" />
	            	<!-- <p style="font-size: 13px !important;margin-top:10px !important;">Dr. Lorna Sande | Dr. Kinoti M. Dental Surgeons</p> -->
	            	
	            	<!-- <p style="font-size: 13px;">Pin. PO51455684R </p> -->
	            </div>
	            <div class="col-print-6" style="margin-bottom: 10px;padding: 10px">
	            	<h3 class="pull-right"><strong>Credit Note</strong></h3>
	            </div>
	        	
	        </div>
    
        
	      	<div class="row" >
	      		<div class="col-md-12">
	      			<div class="col-print-6 left-align">
	      				<p style="font-size: 12px;">FOR PROFESSIONAL SERVICES RENDERED </p>
		            	<table class="table">
		            		<tr>
		            			<td><strong>Bill To</strong></td>
		            		</tr>
		            		<tr>
		            			
		            			<td>
		            				
		            				<?php echo $patient_othernames.' '.$patient_firstname.' '.$patient_surname; ?>

		            			</td>
		            		</tr>
		            	</table>
		            </div>
		            <div class="col-print-3">
		            	&nbsp;
		            </div>
		            <div class="col-print-3">
		            	<p style="font-size: 12px;"> &nbsp;</p>
		            	<table class="table">
		            		<tr>
		            			<td>
		            				<span class="pull-left">Date:</span> 
		            				<span class="pull-right"> <strong><?php echo $created; ?> </strong></span>
		            			</td>
		            		</tr>
		            		<tr>
		            			<td>
		            				<span class="pull-left">Invoice Number:</span> 
		            				<span class="pull-right"> <strong><?php echo $visit_invoice_number; ?> </strong></span>
		            			</td>
		            		</tr>
		            		<tr>
		            			
		            			<td style="border-top: 0px solid #000 !important;">
		            				
		            					<span class="pull-left"><strong>Credit Note No.</strong> </span> 
		            					<span class="pull-right" ><strong><?php echo $visit_cr_note_number; ?> </strong> </span>
		            				

		            			</td>
		            		</tr>
		            	</table>
		            </div>
	      			
	      		</div>
	        	
	        </div>
	       
		        <div class="row" >
		      		<div class="col-md-12">
		      			<div class="col-print-5 left-align">
			            	&nbsp;
			            </div>
			            <div class="col-print-2">
			            	&nbsp;
			            </div>
			            <div class="col-print-5">
			            	
			            	<table class="table">
			            		<tr>
			            			<td  style="border-radius: 10px 10px 10px 10px !important;border-bottom: 1px solid #000 !important;">
				            			<span class="pull-left">FILE No. </span> 
				            			<span class="pull-right" ><?php echo $patient_number; ?>  </span>
				            		
				            		</td>
			            		</tr>
			            		
			            		
			            	</table>
			            </div>
		      		</div>
		      	</div>
		    
	      	<div class="row">
	      		<div class="col-md-12">
	      			<table class="table">
		            		<tr>
		            			<td  style="border-radius: 10px 10px 10px 10px !important;border-bottom: 0px solid #000 !important; width: 10%;">
			            			<span class="pull-left">Item </span> 
			            		
			            		</td>
			            		<td  style="border-radius: 10px 10px 10px 10px !important;border-bottom: 0px solid #000 !important; width: 60%;">
			            			<span class="pull-left">Description </span> 
			            		
			            		</td>
			            		<td  style="border-radius: 10px 10px 10px 10px !important;border-bottom: 0px solid #000 !important;  width: 10%;">
			            			<span class="pull-left">Qty </span> 
			            		
			            		</td>
			            		<td  style="border-radius: 10px 10px 10px 10px !important;border-bottom: 0px solid #000 !important;  width: 10%;">
			            			<span class="pull-left">Rate </span> 
			            		
			            		</td>
			            		<td  style="border-radius: 10px 10px 10px 10px !important;border-bottom: 0px solid #000 !important;  width: 10%;">
			            			<span class="pull-left">Amount </span> 
			            		
			            		</td>
		            		</tr>

		            		<?php
                                $total = 0;

                                $visit__rs1 = $this->accounts_model->get_visit_credit_note($visit_id,$visit_credit_note_id,$patient_id);

                                $item_list = '';
								$description_list = "";
								$quantity_list = "";
								$rate_list = "";
								$amount_list = "";

								// var_dump($visit__rs1);die();
                                if($visit__rs1->num_rows() > 0)
								{						
									foreach ($visit__rs1->result() as $key1 => $value) :
										$v_procedure_id = $value->visit_credit_note_item_id;
										$procedure_id = $value->service_charge_id;
										$visit_cr_note_amount = $value->visit_cr_note_amount;
										$visit_cr_note_comments = $value->visit_cr_note_comment;
										$units = $value->visit_cr_note_units;
										$procedure_name = $value->service_charge_name;
										$service_id = $value->service_id;
										$visit_credit_note_id = $value->visit_credit_note_id;

										// $visit_type_id = 1;
										$total= $total +($units * $visit_cr_note_amount);

										if($visit_credit_note_id > 0)
										{
											$text_color = "success";
										}
										else
										{
											$text_color = 'default';
										}

									
									
										$checked="";
										$number++;
										

									$item_list .= $service_charge_code.'<br> ';
									$description_list .= $procedure_name."<br>";
									$quantity_list .= $units."<br>";
									$rate_list .= number_format($visit_cr_note_amount,2)."<br>";
									$amount_list .= number_format($units * $visit_cr_note_amount,2)."<br>";

									$count++;
									// var_dump($item_list);die();
									$visit_date_day = $visit_date;
									endforeach;



									
								}
								?>

									<tr>
				            			<td  style="border-radius: 10px 10px 0px 10px !important;border-bottom: 1px solid #000 !important;border-right: 0px solid #000 !important; height: 250px !important;">
					            			<span class="pull-left"> <?php echo $item_list;?> </span> 
					            		
					            		</td>
					            		<td  style="border-radius: 10px 10px 0px 0px !important;border-bottom: 1px solid #000 !important;border-right: 0px solid #000 !important;height: 250px !important;">
					            			<span class="pull-left">  <?php echo $description_list;?>  </span> 
					            		
					            		</td>
					            		<td  style="border-radius: 10px 10px 0px 0px !important;border-bottom: 1px solid #000 !important;border-right: 0px solid #000 !important;height: 250px !important;">
					            			<span class="pull-left"> <?php echo $quantity_list;?>  </span> 
					            		
					            		</td>
					            		<td  style="border-radius: 10px 10px 0px 0px !important;border-bottom: 1px solid #000 !important;border-right: 0px solid #000 !important;height: 250px !important;">
					            			<span class="pull-left"> <?php echo $rate_list;?> </span> 
					            		
					            		</td>
					            		<td  style="border-radius: 10px 10px 10px 0px !important;border-bottom: 1px solid #000 !important; height: 250px !important;">
					            			<span class="pull-left"> <?php echo $amount_list;?> </span> 
					            		
					            		</td>
				            		</tr>
									
		            		<tr>
		            			<td style="border:none !important;"></td>
		            			<td  style="border:none !important;"></td>
		            			<td colspan="3"  style="border:none !important; padding:0px !important;">
		            				<table class="table">
					            		<tr>
					            			<td  style="border-radius: 10px 10px 10px 10px !important;border-bottom: 1px solid #000 !important;border-top:0px !important;">
						            			<span class="pull-left">Total. </span> 
						            			<span class="pull-right" ><?php echo number_format($total,2);; ?>  </span>
						            		
						            		</td>
					            		</tr>
					            		
					            		
					            	</table>
		            				
		            			</td>
		            		</tr>
		            		

		            		
		            	</table>
	      		</div>
	      	</div>
	      	<div class="row">
	      		<div  class="center-align">
	            	<p style="font-size: 12px;">
	                	<strong style="text-decoration: underline;"><?php echo $contacts['company_name'];?></strong> <br/>
	                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
	                    Address :<strong> P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></strong> <br/>
	                    Office Line: <strong> <?php echo $contacts['phone'];?></strong> <br/>
	                    E-mail: <strong><?php echo $contacts['email'];?>.</strong><br/>
	                </p>
	            </div>
	      	</div>
	      
	    </div>


    </body>
    
</html>