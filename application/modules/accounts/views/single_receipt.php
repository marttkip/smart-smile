<?php

//patient details
$visit_type = $patient['visit_type'];
$patient_type = $patient['patient_type'];
$patient_othernames = $patient['patient_othernames'];
$patient_surname = $patient['patient_surname'];
$patient_surname = $patient['patient_surname'];
$patient_number = $patient['patient_number'];
$gender = $patient['gender'];
$patient_insurance_number = $patient['patient_insurance_number'];
$account_balance = $patient['account_balance'];
$inpatient = $patient['inpatient'];
$visit_type_name = $patient['visit_type_name'];
$visit_status = 0;


$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
$visit_date = date('d/m/Y',strtotime($this->accounts_model->get_visit_date($visit_id)));

//doctor

$doctor = $this->accounts_model->get_att_doctor($visit_id);

//served by
$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));

//services details
$item_invoiced_rs = $this->accounts_model->get_patient_visit_charge_items($visit_id);
$credit_note_amount = $this->accounts_model->get_sum_credit_notes($visit_id);
$debit_note_amount = $this->accounts_model->get_sum_debit_notes($visit_id);

$visit_rs = $this->accounts_model->get_visit_details($visit_id);
$visit_type_id = 0;
if($visit_rs->num_rows() > 0)
{
    foreach ($visit_rs->result() as $key => $value) {
        # code...
        $close_card = $value->close_card;
        $visit_type_id = $value->visit_type;
        $invoice_number = $value->invoice_number;
        $visit_time_out = $value->visit_time_out;
        $parent_visit = $value->parent_visit;
        $insurance_description = $value->insurance_description;
        $insurance_number = $value->insurance_number;

        $visit_invoice_number = $value->invoice_number;
        $preauth_date = date('d/m/Y');
        $preauth_amount = 0;;
        
        $visit_time_out = date('jS F Y',strtotime($visit_time_out));
    }
}
// var_dump($visit_invoice_id);die();

// var_dump($visit_type_id);die();
//payments
$payments_rs = $this->accounts_model->payments($visit_id);
$total_payments = 0;
$s = 0;
$total_amount = 0;



//payments
$payments_rs = $this->accounts_model->payment_detail($receipt_payment_id);
$total_payments = 0;
$payment_created = $visit_date; 
$confirm_number = '';

if(count($payments_rs) > 0)
{
    $x=0;
    
    foreach ($payments_rs as $key_items):
        $x++;
        $payment_type = $key_items->payment_type;
         $confirm_number = $key_items->confirm_number;
        $payment_created = date('d/m/Y',strtotime($key_items->payment_created));
        $payment_method = $key_items->payment_method;
       $receip_date = $key_items->payment_created;


    endforeach;
    
}


$previous_payment = $this->accounts_model->previous_payment($visit_id,$receip_date);
$all_payments_rs = $this->accounts_model->get_all_visit_transactions($visit_id);



?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Invoice</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
            body
            {
                font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
            }
            .receipt_spacing{letter-spacing:0px; font-size: 12px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            
            .col-md-6 {
                width: 50%;
             }
        
            h3
            {
                font-size: 30px;
            }
            p
            {
                margin: 0 0 0px !important;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{ margin:0 auto;}
            .col-print-1 {width:8%;  float:left;}
            .col-print-2 {width:16%; float:left;}
            .col-print-3 {width:25%; float:left;}
            .col-print-4 {width:33%; float:left;}
            .col-print-5 {width:42%; float:left;}
            .col-print-6 {width:50%; float:left;}
            .col-print-7 {width:58%; float:left;}
            .col-print-8 {width:66%; float:left;}
            .col-print-9 {width:75%; float:left;}
            .col-print-10{width:83%; float:left;}
            .col-print-11{width:92%; float:left;}
            .col-print-12{width:100%; float:left;}

            table.table{
                border-spacing: 0 !important;
                font-size:12px;
                margin-top:0px;
                margin-bottom: 10px !important;
                border-collapse: inherit !important;
                font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
            }
            th, td {
                border: 1px solid #000 !important;
                padding: 0.5em 1em !important;
            }
            /* the first 'th' within the first 'tr' of the 'thead': */
            thead tr:first-child th:first-child {
                border-radius: 10px 0 0 0 !important;
            }
            /* the last 'th' within the first 'tr' of the 'thead': */
            thead tr:first-child th:last-child {
                border-radius: 0 10px 0 0 !important;
            }
            /* the first 'td' within the last 'tr' of the 'tbody': */
            tbody tr:last-child td:first-child {
                border-radius: 0 0 0 10px !important;
            }
            /* the last 'td' within the last 'tr' of the 'tbody': */
            tbody tr:last-child td:last-child {
                border-radius: 0 0 10px 10px !important;
            }
            tbody tr:first-child td:first-child {
                border-radius: 10px 10px 0 0 !important;
                border-bottom: 0px solid #000 !important;
            }
            .padd
            {
                padding:10px;
            }
            
        </style>
    </head>
    <body onLoad="window.print();return false;">
        
        

        <div class="padd " >
            <div class="col-print-12">
                <div class="col-print-6" style="margin-bottom: 10px;">
                    <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive" style="margin-bottom: 10px;" />
                    <!-- <p style="font-size: 11px !important;">Dr. Lorna Sande | Dr. Kinoti M. Dental Surgeons</p> -->
                    
                    <p style="font-size: 12px;">Pin. PO51455684R </p>
                </div>
                <div class="col-print-6" style="margin-bottom: 10px;padding: 10px">
                    <h4 class="pull-right"><strong>Cash Receipt</strong></h4>
                </div>
                <!-- <div  class="center-align">
                    <p style="font-size: 40px;">
                        <h3 style="text-decoration: underline;"><?php echo $contacts['company_name'];?></h3> <br/>
                        <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                        Address :<strong> P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></strong> <br/>
                        Office Line: <strong> <?php echo $contacts['phone'];?></strong> <br/>
                        Website: <strong> http://www.irisdental.co.ke </strong> E-mail: <strong><?php echo $contacts['email'];?>.</strong><br/>
                    </p>
                </div> -->
            </div>
    
        
            <div class="row" >
                <div class="col-md-12">
                    <div class="col-print-6 left-align">
                        <p style="font-size: 12px;">FOR PROFESSIONAL SERVICES RENDERED </p>
                        <table class="table">
                            <tr>
                                <td><strong>Received From</strong></td>
                            </tr>
                            <tr>
                                
                                <td>
                                    
                                    <?php echo $patient_surname.' '.$patient_othernames; ?>

                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-print-3">
                        &nbsp;
                    </div>
                    <div class="col-print-3">
                        <p style="font-size: 12px;"> &nbsp;</p>
                        <table class="table">
                            <tr>
                                <td>
                                    <span class="pull-left">Date:</span> 
                                    <span class="pull-right"> <strong><?php echo $payment_created; ?> </strong></span>
                                </td>
                            </tr>
                            <tr>
                                
                                <td style="border-top: 0px solid #000 !important;">
                                    
                                        <span class="pull-left"><strong>Receipt No.</strong> </span> 
                                        <span class="pull-right" ><strong><?php echo $confirm_number; ?> </strong> </span>
                                    

                                </td>
                            </tr>
                        </table>
                    </div>
                    
                </div>
                
            </div>
            
            <div class="row" >
                <div class="col-md-12">
                    <div class="col-print-7">
                        &nbsp; 

                    </div>
                    <div class="col-print-5">
                        
                       <table class="table center-align">
                            <tr>
                                <td  style="border-radius: 10px 10px 10px 10px !important;border-bottom: 0px solid #000 !important; ">
                                    <span class="center-align"><strong> Ref No. </strong> </span> 
                                
                                </td>
                                <td  style="border-radius: 10px 10px 10px 10px !important;border-bottom: 0px solid #000 !important; ">
                                    <span class="center-align"><strong>Payment Method</strong> </span> 
                                
                                </td>
                            </tr>
                            <tr>
                                <td  style="border-radius: 10px 10px 10px 10px !important;  ">
                                    <span class="center-align"> <?php echo $invoice_number?> </span> 
                                
                                </td>
                                <td  style="border-radius: 10px 10px 10px 10px !important;  ">
                                    <span class="center-align"><?php echo $payment_method;?> </span> 
                                
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
                   
            <div class="row">
                <div class="col-md-12">
                    <table class="table">
                            <tr>
                                <td  style="border-radius: 10px 10px 10px 10px !important;border-bottom: 0px solid #000 !important; width: 10%;">
                                    <span class="pull-left">Item </span> 
                                
                                </td>
                                <td  style="border-radius: 10px 10px 10px 10px !important;border-bottom: 0px solid #000 !important; width: 60%;">
                                    <span class="pull-left">Description </span> 
                                
                                </td>
                                <td  style="border-radius: 10px 10px 10px 10px !important;border-bottom: 0px solid #000 !important;  width: 10%;">
                                    <span class="pull-left">Qty </span> 
                                
                                </td>
                                <td  style="border-radius: 10px 10px 10px 10px !important;border-bottom: 0px solid #000 !important;  width: 10%;">
                                    <span class="pull-left">Rate </span> 
                                
                                </td>
                                <td  style="border-radius: 10px 10px 10px 10px !important;border-bottom: 0px solid #000 !important;  width: 10%;">
                                    <span class="pull-left">Amount </span> 
                                
                                </td>
                            </tr>

                            <?php
                                $total = 0;
                                $item_invoiced_rs = $this->accounts_model->get_patient_visit_charge_items_tree($visit_id,$visit_invoice_id);
                                $total_amount= 0; 
                                $days = 0;
                                $count = 0;
                                $item_list = '';
                                $description_list = "";
                                $quantity_list = "";
                                $rate_list = "";
                                $amount_list = "";
                                if($item_invoiced_rs->num_rows() > 0)
                                {
                                    foreach ($item_invoiced_rs->result() as  $value) {
                                        # code...
                                        $service_id= $value->service_id;
                                        $service_name = $value->service_name;

                                    

                                        $rs2 = $this->accounts_model->get_visit_procedure_charges_per_service($visit_id,$service_id,$visit_invoice_id); 

                                        // if($service_name == "Bed charge")
                                        // {
                                        //  $days = count($rs2);
                                        // }
                                        
                                        // var_dump($service_name); die();
                                        $sub_total= 0; 
                                        $personnel_query = $this->personnel_model->retrieve_personnel();
                                            
                                        if(count($rs2) >0){
                                        
                                            $visit_date_day = '';
                                            
                                            foreach ($rs2 as $key1):
                                                $v_procedure_id = $key1->visit_charge_id;
                                                $procedure_id = $key1->service_charge_id;
                                                $date = $key1->date;
                                                $time = $key1->time;
                                                $visit_charge_timestamp = $key1->visit_charge_timestamp;
                                                $visit_charge_amount = $key1->visit_charge_amount;
                                                $units = $key1->visit_charge_units;
                                                $procedure_name = $key1->service_charge_name;
                                                $service_id = $key1->service_id;
                                                $provider_id = $key1->provider_id;
                                                $service_charge_code = $key1->service_charge_code;
                                            
                                                $sub_total= $sub_total +($units * $visit_charge_amount);
                                                $visit_date = date('l d F Y',strtotime($date));
                                                $visit_time = date('H:i A',strtotime($visit_charge_timestamp));
                                                if($visit_date_day != $visit_date)
                                                {
                                                    
                                                    $visit_date_day = $visit_date;
                                                }
                                                else
                                                {
                                                    $visit_date_day == $visit_date;
                                                }

                                                

                                                if($personnel_query->num_rows() > 0)
                                                {
                                                    $personnel_result = $personnel_query->result();
                                                    
                                                    foreach($personnel_result as $adm)
                                                    {
                                                        $personnel_id = $adm->personnel_id;
                                                        
                                                        
                                                            if($personnel_id == $provider_id)
                                                            {
                                                                $provider_id = ' [ Dr. '.$adm->personnel_fname.' '.$adm->personnel_lname.']';

                                                                $procedure_name = $procedure_name.$provider_id;
                                                            }
                                                        
                                                        

                                                        
                                                        
                                                        
                                                    }

                                                }
                                                
                                                else
                                                {
                                                    $provider_id = '';
                                                    
                                                }
                                                if($procedure_name == 'General Female Ward' || $procedure_name == 'General Male Ward' || $procedure_name == 'Private Ward' || $procedure_name == 'Semi-Private Female Ward' || $procedure_name == 'High Dependancy Unit Ward'  || $procedure_name == 'Intensive Care Unit Ward' )
                                                {
                                                    $days++;
                                                }


                                                $item_list .= $service_charge_code.'<br> ';
                                                $description_list .= $procedure_name."<br>";
                                                $quantity_list .= $units."<br>";
                                                $rate_list .= number_format($visit_charge_amount,2)."<br>";
                                                $amount_list .= number_format($units * $visit_charge_amount,2)."<br>";

                                                $count++;
                                            
                                                $visit_date_day = $visit_date;
                                                endforeach;
                                                

                                        }
                                    
                                        $total_amount = $total_amount + $sub_total;

                                    }
                                }
                                
                                
                              
                                

                              ?>
                            <tr>
                                <td  style="border-radius: 10px 10px 0px 10px !important;border-bottom: 1px solid #000 !important;border-right: 0px solid #000 !important; height: 200px !important;">
                                    <span class="pull-left"> <?php echo $item_list;?> </span> 
                                
                                </td>
                                <td  style="border-radius: 10px 10px 0px 0px !important;border-bottom: 1px solid #000 !important;border-right: 0px solid #000 !important;height: 200px !important;">
                                    <span class="pull-left">  <?php echo $description_list;?>  </span> 
                                
                                </td>
                                <td  style="border-radius: 10px 10px 0px 0px !important;border-bottom: 1px solid #000 !important;border-right: 0px solid #000 !important;height: 200px !important;">
                                    <span class="pull-left"> <?php echo $quantity_list;?>  </span> 
                                
                                </td>
                                <td  style="border-radius: 10px 10px 0px 0px !important;border-bottom: 1px solid #000 !important;border-right: 0px solid #000 !important;height: 200px !important;">
                                    <span class="pull-left"> <?php echo $rate_list;?> </span> 
                                
                                </td>
                                <td  style="border-radius: 10px 10px 10px 0px !important;border-bottom: 1px solid #000 !important; height: 200px !important;">
                                    <span class="pull-left"> <?php echo $amount_list;?> </span> 
                                
                                </td>
                            </tr>

                               <?php
            

                                $rs_rejection = $this->dental_model->get_rejection_info($visit_id);
                                $rejected_amount = 0;
                                $rejected_reason ='';
                                $close_card = 0;
                                if(count($rs_rejection) >0){
                                  foreach ($rs_rejection as $r2):
                                    # code...
                                    $rejected_amount = $r2->rejected_amount;
                                    $rejected_date = $r2->rejected_date;
                                    $rejected_reason = $r2->rejected_reason;
                                    $close_card = $r2->close_card;
                                    $parent_visit = $r2->parent_visit;

                                    // get the visit charge

                                  endforeach;
                                }


                                $rs_rejection = $this->dental_model->get_visit_rejected_updates_sum($invoice_number,$visit_type_id);
                                $total_rejected = 0;
                                if(count($rs_rejection) >0){
                                  foreach ($rs_rejection as $r2):
                                    # code...
                                    $total_rejected = $r2->total_rejected;

                                  endforeach;
                                }

                                $rejected_amount += $total_rejected;

                                $total_amount -= $debit_note_pesa;
                                $payments_value = $this->accounts_model->total_payments($visit_id);

                                $invoice_total = $this->accounts_model->total_invoice($visit_id);

                                $balance = $this->accounts_model->balance($payments_value,$invoice_total);



                                // var_dump($rejected_amount); die();
                                if($parent_visit == 0 OR $patient_visit == NULL AND $visit_type_id == 1)
                                {
                                    $payable_by_patient = $rejected_amount;
                                }
                                else
                                {
                                    $payable_by_patient = $total_amount - $rejected_amount;
                                }

                            ?>

                            <tr>
                                <td style="border:none !important;"></td>
                                <td  style="border:none !important;"></td>
                                <td colspan="3"  style="border:none !important; padding:0px !important;">
                                    <table class="table" style="margin:none !important; padding: none !important;margin-top:0px !important;">
                                        <tr>
                                            <td style="border-radius: 10px 0px 0 0 !important;border-right: 0px solid #000 !important;border-top: 0px solid #000 !important;">
                                                <span class="pull-left"><strong>Total</strong> </span> 
                                            </td>
                                            <td style="border-radius: 0px 10px 0 0 !important;border-bottom: 0px solid #000 !important;border-top: 0px solid #000 !important;">
                                                <span class="pull-right" >KES <?php echo number_format($total_amount,2); ?>  </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-radius: 0px 0px 0 0 !important;border-right: 0px solid #000 !important; border-bottom: 0px solid #000 !important;">
                                                <span class="pull-left"><strong>Payments</strong> </span> 
                                            </td>
                                            <td style="border-radius: 0px 0px 0 0 !important;border-bottom: 0px solid #000 !important;">
                                                <span class="pull-right" >KES <?php echo number_format($payments_value,2); ?> </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-right: 0px solid #000 !important;">
                                                <span class="pull-left"><strong>Balance</strong> </span> 
                                            </td>
                                            <td style="border-radius: 0px 0px 10px 0 !important;">
                                                <span class="pull-right" >KES <?php echo number_format($balance,2); ?> </span>
                                            </td>
                                        </tr>
                                        
                                    </table>
                                </td>
                            </tr>
                            

                            
                        </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" style="margin-bottom: 0px !important;font-size: 12px !important;">
                    <!-- <p>Bill Payable to : <strong>Upper Hill Dental Centre</strong></p>
                    <p>Modes of Payment; Cash,Cheque,EFT, Visa card,M-pesa (Pay bill No. 967966. Account No. UHDC)</p>
                    
                    <div >
                        <div class="col-print-4">
                            For Self Paying Patients
                            <div class="col-print-12">
                                <div class="col-print-4">
                                    Bank Name
                                </div>
                                <div class="col-print-1" style="width:2% !important;">
                                    :
                                </div>
                                <div class="col-print-7 pull-left">
                                    Stanbic Bank
                                </div>
                            </div>
                            <div class="col-print-12">
                                <div class="col-print-4">
                                    Bank Bank
                                </div>
                                <div class="col-print-1" style="width:2% !important;">
                                    :
                                </div>
                                <div class="col-print-7 pull-left">
                                    Upper Hill
                                </div>
                            </div>
                            <div class="col-print-12">
                                <div class="col-print-4">
                                    Account Number
                                </div>
                                <div class="col-print-1" style="width:2% !important;">
                                    :
                                </div>
                                <div class="col-print-7 pull-left">
                                    0100002224008
                                </div>
                            </div>
                        </div>
                        <div class="col-print-4">
                            For Insurance Patients
                            <div class="col-print-12">
                                <div class="col-print-4">
                                    Bank Name
                                </div>
                                <div class="col-print-1" style="width:2% !important;">
                                    :
                                </div>
                                <div class="col-print-7">
                                    Stanbic Bank
                                </div>
                            </div>
                            <div class="col-print-12">
                                <div class="col-print-4">
                                    Bank Bank
                                </div>
                                <div class="col-print-1" style="width:2% !important;">
                                    :
                                </div>
                                <div class="col-print-7">
                                    Upper Hill
                                </div>
                            </div>
                            <div class="col-print-12">
                                <div class="col-print-4">
                                    Account Number
                                </div>
                                <div class="col-print-1" style="width:2% !important;">
                                    :
                                </div>
                                <div class="col-print-7">
                                    0100005465944
                                </div>
                            </div>
                        </div>
                        <div class="col-print-4">
                            <div class="col-print-12">
                                <div class="col-print-4">
                                    Swift Code
                                </div>
                                <div class="col-print-1" style="width:2% !important;">
                                    :
                                </div>
                                <div class="col-print-7">
                                    SBICKENX
                                </div>
                            </div>
                            <div class="col-print-12">
                                <div class="col-print-4">
                                    Bank Code
                                </div>
                                <div class="col-print-1" style="width:2% !important;">
                                    :
                                </div>
                                <div class="col-print-7">
                                    31
                                </div>
                            </div>
                            <div class="col-print-12">
                                <div class="col-print-4">
                                    Branch Code
                                </div>
                                <div class="col-print-1" style="width:2% !important;">
                                    :
                                </div>
                                <div class="col-print-7">
                                    010
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-print-12">
                    <p style="font-size: 10px !important;">Mode of payment: <?php echo $payment_method;?></p>
                        
                    <strong>TERMS AND CONDITIONS:</strong>
                    <br>
                    <p style="font-size: 10px !important;">All accounts are due for payment strictly 30 days from the date of invoice or on demand. Interest of 3% per month will be surcharged on all overdue debts.</p>
                    <!-- <p style="font-size: 10px !important; text-align: center;">Professor Nelson Awori Centre, Ground Floor - Suite A3 | Ralph Bunche Rd No. 7 (Next to The Nairobi Hospital) | P.O. Box 19986 00202 KNH Nairobi, Kenya</p>
                    <p style="font-size: 10px !important;text-align: center;">info@upperhilldentalcentre.com | +254 736 579375 +254 20 2430110 +254 710 569959 | Admin/Accounts +254 706 706000 admin@upperhilldentalcentre.com</p> -->
                    </div>
                </div>
            </div>
        </div>


    </body>
    
</html>