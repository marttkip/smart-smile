<?php
// get visit details


$visit_rs = $this->reception_model->get_visit_details($visit_id);

foreach ($visit_rs as $key) {
	# code...

	$time_end=$key->time_end;
	$time_start=$key->time_start;
	$visit_date=$key->visit_date;
	$doctor_id=$key->personnel_id;
	$room_id=$key->room_id;
	$patient_id=$key->patient_id;
}

$personnel_id = $this->session->userdata('personnel_id');
$is_dentist = $this->reception_model->check_if_admin($personnel_id,4);
$is_admin = $this->reception_model->check_if_admin($personnel_id,3);
$personnel_id = $this->session->userdata('personnel_id');
			
$personnel_id = $this->session->userdata('personnel_id');
			
			$is_dentist = $this->reception_model->check_personnel_department_id($personnel_id,4);
			


			// var_dump($is_physician);die();

			if(($is_dentist) AND $doctor_id > 0 OR $personnel_id == 0)
			{
				
				
			}
			else
			{
				?>
				<div class="col-md-12">
					<div class="center-align">
						<?php echo form_open("dental/attend_to_patient/".$visit_id, array("class" => "form-horizontal"));?>
						<input type="submit" class="btn btn-lg btn-success center-align" value="Attend to patient" onclick="return confirm('Are you sure you want to attend to patient ?');"/>
						<?php echo form_close();?>
					</div>
				</div>
				<?php
				
			}
			


?>
<input type="hidden" name="patient_id" id="patient_id" value="<?php echo $patient_id;?>">
		 <input type="hidden" name="current_date" id="current_date" value="<?php echo $visit_date;?>">
          <input type="hidden" name="visit_id" id="visit_id" value="<?php echo $visit_id;?>">

<?php
if(($is_dentist) AND $doctor_id > 0 OR $personnel_id == 0)
{
?>
<div class="col-print-2" style="padding:5px;">
	<div id="sidebar-detail"></div>
	<?php

    if($doctor_id > 0)
    {
    	?>
    	
           <div class="col-md-12">
              <h4> - NEXT APPOINTMENT <br>  </h4>
              <br>
              <div class="center-align">
                   <a class="btn btn-sm btn-warning "   onclick='appointment_sidebar(<?php echo $patient_id;?>,<?php echo $visit_id;?>,10,<?php echo $doctor_id;?>)' > <i class="fa fa-plus"></i> Book Appointment </a>               
              </div>
              <br>
              <div id="next-appointment-view"></div>
              

           </div>
     
    	<?php
    }
    ?>
      
</div>
<div class="col-print-10">
		<div class="center-align">
			<?php
				$error = $this->session->userdata('error_message');
				$validation_error = validation_errors();
				$success = $this->session->userdata('success_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger">'.$error.'</div>';
					$this->session->unset_userdata('error_message');
				}
				
				if(!empty($validation_error))
				{
					echo '<div class="alert alert-danger">'.$validation_error.'</div>';
				}
				
				if(!empty($success))
				{
					echo '<div class="alert alert-success">'.$success.'</div>';
					$this->session->unset_userdata('success_message');
				}
			?>
		</div>
		
		<?php echo $this->load->view("nurse/allergies_brief", '', TRUE);?>
			
		<div class="clearfix"></div>
		<input type="hidden" name="patient_id" id="patient_id" value="<?php echo $patient_id;?>">
		<input type="hidden" name="visit_id" id="visit_id" value="<?php echo $visit_id;?>">
		 <input type="hidden" name="current_date" id="current_date" value="<?php echo $visit_date;?>">
          <input type="hidden" name="visit_id" id="visit_id" value="<?php echo $visit_id;?>">
		<div class="tabbable" style="margin-bottom: 18px;">
			<ul class="nav nav-tabs nav-justified">
				<li class="active" ><a href="#patient-history" data-toggle="tab">Patient card history</a></li>
				<li><a href="#dentine" data-toggle="tab">Dental Chart</a></li>
				<li><a href="#prescription" data-toggle="tab">Prescription</a></li>
				<li><a href="#history-patients" data-toggle="tab">Sick Leave</a></li>
				<li><a href="#diary" data-toggle="tab">Patient's Appointments</a></li>
				<li><a href="#uploads" data-toggle="tab">Uploads</a></li>
				<li><a href="#profoma-form" data-toggle="tab">Profoma Invoice</a></li>
				<li><a href="#billing-form" data-toggle="tab">Visit Billing</a></li>
				<li><a href="#patient_details" data-toggle="tab">Patient Details</a></li>
				<li><a href="#visit_trail" data-toggle="tab">Visit Trail</a></li>

			
			</ul>
			<div class="tab-content" style="padding-bottom: 9px; border-bottom: 1px solid #ddd;">
				<div class="tab-pane active" id="patient-history" style="height: 75vh !important;overflow-y: scroll;overflow-x: none;">
					<?php echo $this->load->view("patient_history", '', TRUE);?>
				</div>
				<div class="tab-pane " id="dentine" style="height: 75vh !important;overflow-y: scroll;overflow-x: none;">
					<?php echo $this->load->view("dental_chart", $v_data, TRUE);?>
				</div>
				<div class="tab-pane " id="prescription" style="height: 75vh !important;overflow-y: scroll;overflow-x: none;">

					<?php echo $this->load->view("prescription_view", '', TRUE); ?>
					<div id="visit-prescription"></div>

					 <!--  <h4 class="center-align"> PRESCRIPTION</h4>
                     	<a class="btn btn-sm btn-warning pull-right" onclick="prescription_sidebar(<?php echo $visit_id;?>)" >  Add Prescription </a>    
                    <br> -->
                    <!-- <div id="visit_prescription"></div> -->
					<!-- <div id="visit_prescription"></div> -->
				</div>
				<div class="tab-pane " id="history-patients" style="height: 75vh !important;overflow-y: scroll;overflow-x: none;">
					<?php echo $this->load->view("sick_leave", '', TRUE); //echo $this->load->view("nurse/patients/lifestyle", '', TRUE);?>
				</div>
				<div class="tab-pane " id="uploads" style="height: 75vh !important;overflow-y: scroll;overflow-x: none;">
					<?php echo $this->load->view("uploads", '', TRUE);?>
				</div>
				<div class="tab-pane " id="diary" style="height: 75vh !important;overflow-y: scroll;overflow-x: none;">
					<?php echo $this->load->view("patient_appointments", '', TRUE);?>
				</div>
				<div class="tab-pane" id="patient_details" style="height: 75vh !important;overflow-y: scroll;overflow-x: none;">

					<?php 
					$v_data['patient_details'] = $patient_details;
					echo $this->load->view("patient_details", '', TRUE);?>
				</div>
				<div class="tab-pane" id="profoma-form" style="height: 75vh !important;overflow-y: scroll;overflow-x: none;">

					<?php echo $this->load->view("profoma", '', TRUE);?>
				</div>
				<div class="tab-pane" id="billing-form" style="height: 75vh !important;overflow-y: scroll;overflow-x: none;">

					<?php echo $this->load->view("billing", '', TRUE);?>
				</div>
				<div class="tab-pane" id="visit_trail" style="height: 75vh !important;overflow-y: scroll;overflow-x: none;">
					<?php echo $this->load->view("visit_trail", '', TRUE);?>
				</div>


			</div>
		</div>
				  
		<div class="col-md-12">
			
			  <div class="center-align">
				
				 <a href="<?php echo site_url().'dental/send_to_accounts/'.$visit_id.'/'.$mike;?>" onclick="return confirm('Are you sure you want to send to accounts ? ')" class="btn btn-large btn-danger fa fa-lock" > SEND TO ACCOUNTS </a>
				 <a href="<?php echo site_url().'queue'?>"  class="btn btn-large btn-info fa fa-arrow-left" > Back to Queue </a>
			  </div>
	
		
		</div>
			
	</div>
        
<?php

}
?>
  
 
  <script type="text/javascript">
  	
	var config_url = $("#config_url").val();
		
	$(document).ready(function(){
		

	 //  	$.get( config_url+"nurse/get_family_history/<?php echo $visit_id;?>", function( data ) {
		// 	$("#new-nav").html(data);
		// 	$("#checkup_history").html(data);
		// });

		// get_medication(<?php echo $visit_id;?>);
		prescription_view();

		// get_surgeries(<?php echo $visit_id;?>);

		get_page_item(1,<?php echo $patient_id;?>);
		visit_display_billing(<?php echo $visit_id;?>);

		procedure_lists(<?php echo $visit_id;?>,<?php echo $patient_id;?>);
		window.localStorage.setItem('tooth_item_checked',0);

		get_days_bill(<?php echo $visit_id;?>,<?php echo $patient_id;?>);
		get_sidebar_details(<?php echo $patient_id;?>,<?php echo $visit_id;?>);
		display_next_appointment(<?php echo $patient_id;?>,<?php echo $visit_id;?>);
		// display_inpatient_prescription(<?php echo $visit_id;?>,0);
		// prescription_view();
		



	});

	function get_sidebar_details(patient_id,visit_id)
	{

		var config_url = $('#config_url').val();
	 	var url = config_url+"reception/get_sidebar_details/"+patient_id+"/"+visit_id;
	 	// alert(url);
		$.ajax({
			type:'POST',
			url: url,
			data:{query: null},
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
			var data = jQuery.parseJSON(data);
			  // alert(data.content);
			if(data.message == "success")
			{
				$("#sidebar-detail").html(data.result);
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});

	}


	function get_medication(visit_id){
    
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }

	    var config_url = document.getElementById("config_url").value;
	    var url = config_url+"nurse/load_medication/"+visit_id;

	    if(XMLHttpRequestObject) {
	        
	        var obj = document.getElementById("medication");
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                obj.innerHTML = XMLHttpRequestObject.responseText;
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}
	function get_surgeries(visit_id){
    
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var config_url = document.getElementById("config_url").value;
	    var url = config_url+"nurse/load_surgeries/"+visit_id;
	    
	    if(XMLHttpRequestObject) {
	        
	        var obj = document.getElementById("surgeries");
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                obj.innerHTML = XMLHttpRequestObject.responseText;
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}
	function save_surgery(visit_id){
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var date = document.getElementById("datepicker").value;
	    var description = document.getElementById("surgery_description").value;
	    var month = document.getElementById("month").value;
	    var config_url = document.getElementById("config_url").value;
	    var url = config_url+"nurse/surgeries/"+date+"/"+description+"/"+month+"/"+visit_id;
	   
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                get_surgeries(visit_id);
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}

	function delete_surgery(id, visit_id){
	    //alert(id);
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	      var config_url = document.getElementById("config_url").value;
	    var url = config_url+"nurse/delete_surgeries/"+id;
	    
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                get_surgeries(visit_id);
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}
	function save_medication(visit_id){
	    var config_url = document.getElementById("config_url").value;
	    var data_url = config_url+"nurse/medication/"+visit_id;
	   
	     var patient_medication = $('#medication_description').val();
	     var patient_medicine_allergies = $('#medicine_allergies').val();
	     var patient_food_allergies = $('#food_allergies').val();
	     var patient_regular_treatment = $('#regular_treatment').val();
	     
	    $.ajax({
	    type:'POST',
	    url: data_url,
	    data:{medication: patient_medication,medicine_allergies: patient_medicine_allergies, food_allergies: patient_food_allergies, regular_treatment: patient_regular_treatment },
	    dataType: 'text',
	    success:function(data){
	     get_medication(visit_id);
	    //obj.innerHTML = XMLHttpRequestObject.responseText;
	    },
	    error: function(xhr, status, error) {
	    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	    alert(error);
	    }

	    });

	       
	}
  </script>

<script type="text/javascript">
		$(document).ready(function() {
		
		// check_date();
		// load_patient_appointments();

		// tinymce.init({
  //               selector: ".cleditor",
  //               height: "300"
  //           });
  	// alert('dasda');
	});

	function check_date(){
	     var datess=document.getElementById("scheduledate").value;
	     load_schedule();
	     load_patient_appointments_two();
	     if(datess){
		  $('#show_doctor').fadeToggle(1000); return false;
		 }
		 else{
		  alert('Select Date First')
		 }
	}

	function load_schedule(){
		var config_url = $('#config_url').val();
		var datess=document.getElementById("scheduledate").value;
		var doctor= <?php echo $this->session->userdata('personnel_id');?>//document.getElementById("doctor").value;

		var url= config_url+"reception/doc_schedule/"+doctor+"/"+datess;
	
		  $('#doctors_schedule').load(url);
		  $('#doctors_schedule').fadeIn(1000); return false;	
	}
	function load_patient_appointments(){
		var patient_id = $('#patient_id').val();
		var current_date = $('#current_date').val();

		var url= config_url+"reception/patient_schedule/"+patient_id+"/"+current_date;
		
		$('#patient_schedule').load(url);
		$('#patient_schedule').fadeIn(1000); return false;	

		$('#patient_schedule2').load(url);
		$('#patient_schedule2').fadeIn(1000); return false;	
	}
	function load_patient_appointments_two(){
		var patient_id = $('#patient_id').val();
		var current_date = $('#current_date').val();

		var url= config_url+"reception/patient_schedule/"+patient_id+"/"+current_date;
		
		$('#patient_schedule2').load(url);
		$('#patient_schedule2').fadeIn(1000); return false;	
	}
	function schedule_appointment(appointment_id)
	{
		if(appointment_id == '1')
		{
			$('#appointment_details').css('display', 'block');
		}
		else
		{
			$('#appointment_details').css('display', 'none');
		}
	}

	var config_url = $("#config_url").val();
		
	// $(document).ready(function(){
		
	//   	$.get( config_url+"nurse/get_family_history/<?php echo $visit_id;?>", function( data ) {
	// 		$("#new-nav").html(data);
	// 		$("#checkup_history").html(data);
	// 	});
	// });

	function pass_tooth()
    {

     var tooth_item_checked = window.localStorage.getItem('tooth_item_checked');


    
   

		var cavity_status = 0;
		var procedure_status = 2;
		var tooth_id ='';
		var visit_id = document.getElementById("visit_id").value;
		var patient_id = document.getElementById("patient_id").value;
		var teeth_section = '';
		var surface_id = '';

		// alert(tooth_item_checked);

      if(tooth_item_checked == 1)
      {
	      	 var tooth_id = document.getElementById("tooth_id").value;
		     
      	    var teeth_section = document.getElementById("teeth_section").value;
      		var surface_id = document.getElementById("surface_id").value;

      		var radios = document.getElementsByName('cavity_status');
		    var cavity_status = null;
		    for (var i = 0, length = radios.length; i < length; i++)
		    {
		     if (radios[i].checked)
		     {
		      // do whatever you want with the checked radio
		       cavity_status = radios[i].value;
		      // only one radio can be logically checked, don't check the rest
		      break;
		     }
		    }   


		    var radios = document.getElementsByName('procedure_status');
		    var procedure_status = null;
		    for (var i = 0, length = radios.length; i < length; i++)
		    {
		     if (radios[i].checked)
		     {
		      // do whatever you want with the checked radio
		       procedure_status = radios[i].value;
		      // only one radio can be logically checked, don't check the rest
		      break;
		     }
		    }

      }
    

    var radios = document.getElementsByName('service_charges');
    // alert(radios);
    var service_charge = null;
    for (var i = 0, length = radios.length; i < length; i++)
    {
	     if (radios[i].checked)
	     {
	      // do whatever you want with the checked radio
	       service_charge = radios[i].value;
	      // only one radio can be logically checked, don't check the rest
	      break;
	     }
    }
    
	// alert(service_charge);

	var checked = false;
	var checked_response = '';
    if(service_charge != null && procedure_status < 3)
    {
    	checked = true;
    }
	else if(service_charge == null && procedure_status < 3)
	{
		// checked = false;
		checked = true;
		checked_response ='Please select a a service charge on the list';
	}
	else if(service_charge == null && procedure_status == 3)
	{
		checked = true;
		// checked_response ='Please select a a service charge on the list';
	}
	

	if(checked)
	{
    	if(cavity_status == 3 && surface_id === '')
    	{
    		alert('Kindly select a surface of the tooth before you proceed ');
    	}
    	else
    	{


		     var url = "<?php echo base_url();?>dental/save_dentine/"+visit_id+"/"+patient_id;
		     //
		     $.ajax({
		     type:'POST',
		     url: url,
		     data:{tooth_id: tooth_id,patient_id: patient_id,cavity_status: cavity_status,teeth_section: teeth_section,service_charge_id: service_charge,visit_id: visit_id,surface_id:surface_id,plan_status: procedure_status},
		     dataType: 'text',
		     success:function(data){
		       // var prescription_view = document.getElementById("prescription_view");
		       // prescription_view.style.display = 'none';

		         var data = jQuery.parseJSON(data);
		            
		            var status = data.status;

		            if(status == 'success')
		            {
		              // alert(data.message);
		           
		              // display_patient_history(visit_id,patient_id);
		              // close_side_bar(); 
		              if(tooth_item_checked == 1)
		              {


		                if(tooth_id <= 38)
						{
							var newcol = 'chart_type_two';
							get_page_item(7,patient_id,1);
						}
						else
						{
							var newcol = 'chart_type_one';
							get_page_item(7,patient_id,0);
						}
						// alert(newcol);
						// $('a').on('click', function () {
					    $('#' + newcol).prop('checked',true);
					 
						// });
					 }
					 else
					 {
					 	alert(data.message);
					 	visit_display_billing(visit_id);
					 }


		            }
		            else
		            {
		              alert(data.message);
		            }

		     
		     },
		     error: function(xhr, status, error) {
		     alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		     
		     }
		     });

	 	}
	}
	else
	{
		alert(checked_response);
	}
	 if(tooth_item_checked == 1)
	 {
	 	check_department_type(tooth_id,teeth_section=null);
	 }
     


     return false;
    }

     function get_page_item(page_id,patient_id,chart_type = null)
    {
        // alert(page_id);
        // get_page_links(page_id,patient_id);
        // if(page_id > 1)
        // {
            var visit_id = <?php echo $visit_id;?>;//window.localStorage.getItem('visit_id');
             // alert(visit_id);
            if(visit_id > 0)
            {

              var visit_id = visit_id;
              // get_page_header(visit_id); 
              var url = "<?php echo base_url();?>dental/get_page_item/"+page_id+"/"+patient_id+"/"+visit_id+"/"+chart_type;  
              // alert(url);
              $.ajax({
              type:'POST',
              url: url,
              data:{page_id: page_id,patient_id: patient_id},
              dataType: 'text',
              success:function(data){
                  var data = jQuery.parseJSON(data);
                  var page_item = data.page_item;

                  // alert(page_item);
                  $('#page_item').html(data.page_item);
              },
              error: function(xhr, status, error) {
              // alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                  // display_patient_bill(visit_id);
              }
              });                
            }
            else
            {
              var visit_id = null;

              if(page_id > 1)
              {
                alert("Please select a date of a visit you wish to see");
              }
              else
              {
                var url = "<?php echo base_url();?>dental/get_page_item/"+page_id+"/"+patient_id+"/"+visit_id;  
            
                $.ajax({
                type:'POST',
                url: url,
                data:{page_id: page_id,patient_id: patient_id},
                dataType: 'text',
                success:function(data){
                    var data = jQuery.parseJSON(data);
                    var page_item = data.page_item;
                    $('#page_item').html(data.page_item);
                },
                error: function(xhr, status, error) {
                alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                    // display_patient_bill(visit_id);
                }
                });   
              }
              
            }
       
       	visit_display_billing(visit_id);
       // return false;

    }

    function set_tooth_number(teeth_number,teeth_section=null)
  	{ 	

	    var visit_id = document.getElementById("visit_id").value;
	    var patient_id = document.getElementById("patient_id").value;

		var config_url = document.getElementById("config_url").value;
		// alert(teeth_section);
     	var url = config_url+"dental/set_tooth_surfaces/"+teeth_number+"/"+visit_id+"/"+patient_id+"/"+teeth_section;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			var data = jQuery.parseJSON(data);
			// document.getElementById("current-sidebar-div").style.display = "block"; 
			// $("#current-sidebar-div").html(data);
			// alert(data);
			document.getElementById("surface_id").value = data;
			// $("#dentine-list-div").html(data);
		
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});

  }

    function check_department_type(teeth_number,teeth_section=null)
  	{
  		
  		window.localStorage.setItem('tooth_item_checked',0);

	    var visit_id = document.getElementById("visit_id").value;
	    var patient_id = document.getElementById("patient_id").value;


	    var old_tooth = window.localStorage.getItem('teeth_number_old');
	    // alert(old_tooth);
		if(old_tooth > 0)
		{
			document.getElementById(''+old_tooth+'').style.color = "#000";
		}
		

		// document.getElementById("sidebar-right").style.display = "block"; 
		// document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = document.getElementById("config_url").value;
     	var url = config_url+"dental/display_dental_formula/"+teeth_number+"/"+visit_id+"/"+patient_id+"/"+teeth_section;
		// window.alert(data_url);


		$.ajax({
		type:'POST',
		url: url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			var data = jQuery.parseJSON(data);
			// document.getElementById("current-sidebar-div").style.display = "block"; 
			// $("#current-sidebar-div").html(data);

			// alert(url);
			window.localStorage.setItem('tooth_item_checked',1);
			$("#dentine-list-div").html(data.dental_formula);
			document.getElementById(''+teeth_number+'').style.color = "#FF0000";
			// alert(teeth_number);
			$("#toothnumber").html('TOOTH # '+teeth_number);

			$("#teeth-marker").html(data.teeth_marker);




			window.localStorage.setItem('teeth_number_old',teeth_number);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});

		procedure_lists(visit_id,patient_id);

     // var XMLHttpRequestObject = false;
          
     //  if (window.XMLHttpRequest) {
      
     //      XMLHttpRequestObject = new XMLHttpRequest();
     //  } 
          
     //  else if (window.ActiveXObject) {
     //      XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     //  }
      
     //  var config_url = document.getElementById("config_url").value;
     //  var url = config_url+"dental/display_dental_formula/"+teeth_number+"/"+visit_id+"/"+patient_id+"/"+teeth_section;
     //  // alert(url);
     //  if(XMLHttpRequestObject) {
                  
     //      XMLHttpRequestObject.open("GET", url);
                  
     //      XMLHttpRequestObject.onreadystatechange = function(){
              
     //          if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

     //              document.getElementById("dental-formula").innerHTML=XMLHttpRequestObject.responseText;
     //          }
     //      }
                  
     //      XMLHttpRequestObject.send(null);
     //  }
  }




  function save_other_sick_off(visit_id)
	{
		 // start of saving rx
        var config_url = $('#config_url').val();
        var data_url = config_url+"dental/save_other_patient_sickoff/"+visit_id;
        //window.alert(data_url);
         var doctor_notes_rx = tinymce.get('deductions_and_other'+visit_id).getContent();
         // var doctor_notes_rx = $('#deductions_and_other').val();//document.getElementById("vital"+vital_id).value;
        $.ajax({
        type:'POST',
        url: data_url,
        data:{notes: doctor_notes_rx},
        dataType: 'text',
        success:function(data){
        //obj.innerHTML = XMLHttpRequestObject.responseText;
           window.alert("You have successfully updated the payment information");
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });
      // end of saving rx
	}

  function prescription_view()
  {

    // var myTarget = document.getElementById("add_item");
    // myTarget.style.display = 'block';

    var visit_id = document.getElementById("visit_id").value;
    var patient_id = document.getElementById("patient_id").value;
    // alert(patient_id);
     var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"dental/display_patient_prescription/"+visit_id+"/"+patient_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("visit-prescription").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
  }


	function save_prescription(patient_id,visit_id)
	{
		 // start of saving rx
		 // var prescription = tinymce.get('#visit_prescription').getContent();
        var config_url = $('#config_url').val();
        var data_url = config_url+"dental/save_prescription/"+patient_id+"/"+visit_id;
        // window.alert(data_url);
         // var prescription = $('#visit_prescription').val();//document.getElementById("vital"+vital_id).value;
         		// console.debug(tinymce.activeEditor.getContent());

        var prescription = tinymce.get('visit_prescription'+visit_id).getContent();

        
			 // alert(visit_id);

         // alert(prescription);
        $.ajax({
        type:'POST',
        url: data_url,
        data:{prescription: prescription},
        dataType: 'text',
        success:function(data){
        //obj.innerHTML = XMLHttpRequestObject.responseText;
           window.alert("You have successfully updated the prescription");
           close_side_bar();
           prescription_view();
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });
      // end of saving rx
	}
  
	function close_side_bar()
	{
		// $('html').removeClass('sidebar-right-opened');
		document.getElementById("sidebar-right").style.display = "none"; 
		document.getElementById("current-sidebar-div").style.display = "none"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 
		tinymce.remove();
	}
	function get_chart_type(chart_type)
	{

		var config_url = $('#config_url').val();
		var patient_id = $('#patient_id').val();
        var data_url = config_url+"dental/set_chart_type/"+patient_id+"/"+chart_type;

        
        $.ajax({
        type:'POST',
        url: data_url,
        data:{chart_type: chart_type},
        dataType: 'text',
		success:function(data)
		{

		},
        error: function(xhr, status, error) {
	        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        alert(error);

        }

        });


        window.localStorage.setItem('tooth_item_checked',0);
		if(chart_type == 0)
		{
			$('#children-div').css('display', 'block');
			$('#adult-div').css('display', 'none');
		}
		else
		{
			$('#children-div').css('display', 'none');
			$('#adult-div').css('display', 'block');
		}

	}
	function procedure_lists(visit_id,patient_id)
	{

		 // start of saving rx
		 // var prescription = tinymce.get('#visit_prescription').getContent();
        var config_url = $('#config_url').val();
        var data_url = config_url+"dental/get_visit_procedures/"+visit_id+"/"+patient_id;

         var lab_test = $('#q').val();
        
        $.ajax({
        type:'POST',
        url: data_url,
        data:{visit_id: visit_id,query: lab_test},
        dataType: 'text',
		success:function(data)
		{
			$("#procedure-lists").html(data);		
		},
        error: function(xhr, status, error) {
	        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        alert(error);
        }

        });
      // end of saving rx
	}
	function update_service_charge_bill(visit_id,patient_id,service_charge_id,amount,visit_type_id)
	{

		 // start of saving rx
		 // var prescription = tinymce.get('#visit_prescription').getContent();
        var config_url = $('#config_url').val();
        var data_url = config_url+"dental/add_visit_bill/"+visit_id+"/"+patient_id+"/"+service_charge_id+"/"+amount+"/"+visit_type_id;

        
        $.ajax({
        type:'POST',
        url: data_url,
        data:{visit_id: visit_id},
        dataType: 'text',
		success:function(data)
		{
			// $("#procedure-lists").html(data);		
			visit_display_billing(visit_id);
		},
        error: function(xhr, status, error) {
	        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        alert(error);
        }

        });
      // end of saving rx
	}

	function visit_display_billing(visit_id)
	{



		var config_url = $('#config_url').val();
        var data_url = config_url+"dental/view_billing/"+visit_id;

        // alert(data_url);
        $.ajax({
        type:'POST',
        url: data_url,
        data:{visit_id: visit_id},
        dataType: 'text',
		success:function(data)
		{
			// alert(data);
			$("#billing").html(data);

		},
        error: function(xhr, status, error) {
	        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        alert(error);
        }

        });
        var patient_id = $('#patient_id').val();
        // visit_display_billing(visit_id);
        // get_days_bill(visit_id,patient_id);
        // procedure_lists(visit_id,patient_id);
	    
	}




	// accounts


	function change_payer(visit_charge_id, service_charge_id, v_id)
	{

		var res = confirm('Do you want to change who is being billed ? ');

		if(res)
		{

			var config_url = document.getElementById("config_url").value;
		    var data_url = config_url+"accounts/change_payer/"+visit_charge_id+"/"+service_charge_id+"/"+v_id;
		   
		      // var tooth = document.getElementById('tooth'+procedure_id).value;
		     // alert(data_url);
		    $.ajax({
		    type:'POST',
		    url: data_url,
		    data:{visit_charge_id: visit_charge_id},
		    dataType: 'text',
		    success:function(data){
		     // get_medication(visit_id);
		         visit_display_billing(v_id);
		     alert('You have successfully updated your billing');
		    //obj.innerHTML = XMLHttpRequestObject.responseText;
		    },
		    error: function(xhr, status, error) {
		    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		        visit_display_billing(v_id);
		    	alert(error);
		    }

		    });

		}

	}
	//Calculate procedure total
	function calculatetotal(amount, id, procedure_id, v_id){
	       
	    var units = document.getElementById('units'+id).value;  
	    var billed_amount = document.getElementById('billed_amount'+id).value;  
	   // alert(billed_amount);
	    grand_total(id, units, billed_amount, v_id);

	}
	function grand_total(procedure_id, units, amount, v_id){



		 var config_url = document.getElementById("config_url").value;
	     var data_url = config_url+"accounts/update_service_total/"+procedure_id+"/"+units+"/"+amount+"/"+v_id;
	   
	      // var tooth = document.getElementById('tooth'+procedure_id).value;
	     // alert(data_url);
	    $.ajax({
	    type:'POST',
	    url: data_url,
	    data:{procedure_id: procedure_id},
	    dataType: 'text',
	    success:function(data){
	     // get_medication(visit_id);
	         visit_display_billing(v_id);
	     alert('You have successfully updated your billing');
	    //obj.innerHTML = XMLHttpRequestObject.responseText;
	    },
	    error: function(xhr, status, error) {
	    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        visit_display_billing(v_id);
	    alert(error);
	    }

	    });
	}

	function delete_procedure(id, visit_id,dentine_id = null){

		var res = confirm('Are you sure you want to delete this procedure ? ');

		if(res)
		{
			var XMLHttpRequestObject = false;
	        
		    if (window.XMLHttpRequest) {
		    
		        XMLHttpRequestObject = new XMLHttpRequest();
		    } 
		        
		    else if (window.ActiveXObject) {
		        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		    }
		     var config_url = document.getElementById("config_url").value;
		    var url = config_url+"nurse/delete_procedure/"+id+"/"+dentine_id;
		    
		    if(XMLHttpRequestObject) {
		                
		        XMLHttpRequestObject.open("GET", url);
		                
		        XMLHttpRequestObject.onreadystatechange = function(){
		            
		            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
		            	close_side_bar();
		                visit_display_billing(visit_id);
		            }
		        }
		                
		        XMLHttpRequestObject.send(null);
		    }
		}
	}

	function get_procedure_information(visit_charge_id,visit_id)
	{

	    var visit_id = document.getElementById("visit_id").value;
	    var patient_id = document.getElementById("patient_id").value;



		document.getElementById("sidebar-right").style.display = "block"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = document.getElementById("config_url").value;
     	var url = config_url+"dental/visit_charge_information/"+visit_charge_id+"/"+visit_id;
		// window.alert(url);
		$.ajax({
		type:'POST',
		url: url,
		data:{patient_id: patient_id},
		dataType: 'text',
		success:function(data){

			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);

			tinymce.init({
                selector: ".cleditor",
                height: "300"
            });
		
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}


	function update_visit_charges(visit_id,visit_charge_id)
	{
		var config_url = $('#config_url').val();		

		var notes = tinymce.get('notes'+visit_charge_id).getContent();	
		var units = $('#units'+visit_charge_id).val();
		var teeth_id = $('#teeth_id'+visit_charge_id).val();
		var dentine_id = $('#dentine_id'+visit_charge_id).val();
		var billed_amount = $('#billed_amount'+visit_charge_id).val();

		var patient_id = $('#patient_id'+visit_charge_id).val();

		var radios = document.getElementsByName('work_status');
	    var work_status = null;
	    for (var i = 0, length = radios.length; i < length; i++)
	    {
	     if (radios[i].checked)
	     {
	      // do whatever you want with the checked radio
	       work_status = radios[i].value;
	      // only one radio can be logically checked, don't check the rest
	      break;
	     }
	    }


		var url = config_url+"accounts/update_charges_account/"+visit_id+"/"+visit_charge_id;	
		 
		$.ajax({
		type:'POST',
		url: url,
		data:{notes: notes,visit_id: visit_id,visit_charge_id: visit_charge_id,units: units,billed_amount: billed_amount,work_status: work_status,dentine_id: dentine_id, teeth_id: teeth_id,patient_id: patient_id},
		dataType: 'text',
		
		success:function(data)
		{
		  	var data = jQuery.parseJSON(data);

		  	if(data.message == "success")
			{	

				close_side_bar();	

			}
			else
			{
				alert(data.result);
			}
			visit_display_billing(visit_id);

			if(teeth_id <= 38)
			{
				var newcol = 'chart_type_two';
				get_page_item(7,patient_id,1);
			}
			else
			{
				var newcol = 'chart_type_one';
				get_page_item(7,patient_id,0);
			}
			// alert(newcol);
			// $('a').on('click', function () {
		    $('#' + newcol).prop('checked',true);
					 

		},
		error: function(xhr, status, error) {

				alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
		 
		visit_display_billing(visit_id);	
	   
		
	// });
	}


	function add_patient_prescription(visit_id,patient_id)
	{



		document.getElementById("sidebar-right").style.display = "block"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = document.getElementById("config_url").value;
     	var url = config_url+"dental/patient_prescription/"+visit_id+"/"+patient_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){

			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);

			tinymce.init({
                selector: ".cleditor",
                height: "300"
            });
		
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}

	function sicksheet_view()
	{
		 // var myTarget = document.getElementById("add_item");
    // myTarget.style.display = 'block';

    var visit_id = document.getElementById("visit_id").value;
    var patient_id = document.getElementById("patient_id").value;
    // alert(patient_id);
     var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"dental/display_patient_sick_leave/"+visit_id+"/"+patient_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("sick-sheet").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
	}


	function save_leave_note(patient_id,visit_id)
	{
		 // start of saving rx
		 // var prescription = tinymce.get('#visit_prescription').getContent();
        var config_url = $('#config_url').val();
        var data_url = config_url+"dental/save_sick_leave_note/"+patient_id+"/"+visit_id;
        // window.alert(data_url);
         // var prescription = $('#visit_prescription').val();//document.getElementById("vital"+vital_id).value;
         		// console.debug(tinymce.activeEditor.getContent());

        var sick_leave = tinymce.get('sick_leave'+visit_id).getContent();

        
			 // alert(visit_id);

         // alert(sick_leave);
        $.ajax({
        type:'POST',
        url: data_url,
        data:{sick_leave: sick_leave},
        dataType: 'text',
        success:function(data){
        //obj.innerHTML = XMLHttpRequestObject.responseText;
           window.alert("You have successfully updated the prescription");
           close_side_bar();
           sicksheet_view();
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });
      // end of saving rx
	}
	function add_patient_sick_note(visit_id,patient_id)
	{

		document.getElementById("sidebar-right").style.display = "block"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = document.getElementById("config_url").value;
     	var url = config_url+"dental/patient_sick_note/"+visit_id+"/"+patient_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){

			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);

			tinymce.init({
                selector: ".cleditor",
                height: "300"
            });
		
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}


	// uploads


	function uploads_view()
	{
		 // var myTarget = document.getElementById("add_item");
    // myTarget.style.display = 'block';

    var visit_id = document.getElementById("visit_id").value;
    var patient_id = document.getElementById("patient_id").value;
    // alert(patient_id);
     var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"dental/display_patient_uploads/"+visit_id+"/"+patient_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("patients-uploads").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
	}


	$(document).on("submit","form#uploads-form",function(e)
	{
		e.preventDefault();
		
		var form_data = new FormData(this);

		// alert(form_data);
		var patient_id = $('#patient_id').val();
		var visit_id = $('#visit_id').val();

		var config_url = $('#config_url').val();	

		var url = config_url+"dental/upload_documents/"+patient_id+"/"+visit_id;
		
		 
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:form_data,
	   dataType: 'text',
	   processData: false,
	   contentType: false,
       success:function(data){
        //obj.innerHTML = XMLHttpRequestObject.responseText;
         
	      var data = jQuery.parseJSON(data);
	    
	      	if(data.message == "success")
			{
				
				
				close_side_bar();
				uploads_view();
				
			}
			else
			{
				alert(data.result);
			}
	   
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });
		 
		
	   
		
	});
	function add_patient_upload(visit_id,patient_id)
	{

		document.getElementById("sidebar-right").style.display = "block"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = document.getElementById("config_url").value;
     	var url = config_url+"dental/patient_uploads/"+visit_id+"/"+patient_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){

			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);

			tinymce.init({
                selector: ".cleditor",
                height: "300"
            });
		
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}
	function delete_upload_file(document_upload_id,visit_id)
	{
		var res = confirm('Are you sure you want to delete this file ? ');

		if(res)
		{
			var config_url = document.getElementById("config_url").value;
	     	var url = config_url+"dental/delete_document_scan/"+document_upload_id+"/"+visit_id;
			//window.alert(data_url);
			$.ajax({
			type:'POST',
			url: url,
			data:{appointment_id: 1},
			dataType: 'text',
			success:function(data){

				uploads_view();
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
			}

			});

		}
		
		
	}

	function bill_charge(visit_charge_id,visit_id,patient_id)
	{

		var res = confirm('Are you sure you want to bill this procedure ? ');

		if(res)
		{
			var config_url = document.getElementById("config_url").value;
	     	var url = config_url+"dental/bill_procedure/"+visit_charge_id;
			//window.alert(data_url);
			$.ajax({
			type:'POST',
			url: url,
			data:{appointment_id: 1},
			dataType: 'text',
			success:function(data){

				visit_display_billing(visit_id);


			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
			}

			});

		}
		get_days_bill(visit_id,patient_id);
	}

	function unbill_charge(visit_charge_id,visit_id,patient_id)
	{

		var res = confirm('Are you sure you want to unbill this procedure ? ');

		if(res)
		{
			var config_url = document.getElementById("config_url").value;
	     	var url = config_url+"dental/remove_bill_procedure/"+visit_charge_id;
			//window.alert(data_url);
			$.ajax({
			type:'POST',
			url: url,
			data:{appointment_id: 1},
			dataType: 'text',
			success:function(data){

				visit_display_billing(visit_id);

			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
			}

			});

		}
		get_days_bill(visit_id,patient_id);
	}
	function get_days_bill(visit_id,patient_id)
	{
		var config_url = $('#config_url').val();
        var data_url = config_url+"dental/view_days_billing/"+patient_id+"/"+visit_id;

        // alert(data_url);
        $.ajax({
        type:'POST',
        url: data_url,
        data:{visit_id: visit_id},
        dataType: 'text',
		success:function(data)
		{
			// alert(data);
			$("#days-bill").html(data);		
		},
        error: function(xhr, status, error) {
	        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        alert(error);
        }

        });
	}

	function add_new_procedure(visit_id, patient_id)
	{

		$('#procedure-div').css('display', 'none');
		// $('#procedure-list-div').css('display', 'none');	
		$('#service_charge-div').css('display', 'block');	
		$('#procedure-search-div').css('display', 'none');	
		$('#new-button-div').css('display', 'none');	
		$('#add-button-div').css('display', 'block');
		
		
	}

	function back_procedure_list(visit_id, patient_id)
	{

		$('#procedure-div').css('display', 'block');
		// $('#procedure-list-div').css('display', 'block');

		$('#service_charge-div').css('display', 'none');	
		$('#procedure-search-div').css('display', 'block');	
		$('#new-button-div').css('display', 'block');	
		$('#add-button-div').css('display', 'none');	
		
	}
	function add_service_charge(visit_id, patient_id)
	{

		var res = confirm('Are you sure you want to add this as a service charge ?');


		if(res)
		{
			var config_url = $('#config_url').val();

			var service_charge_name = $('#service_charge_name').val();
	        var data_url = config_url+"dental/add_new_service";

	        // alert(data_url);
	        $.ajax({
	        type:'POST',
	        url: data_url,
	        data:{service_charge_name: service_charge_name},
	        dataType: 'text',
			success:function(data)
			{
				var data = jQuery.parseJSON(data);
	    
				// alert(data);

				if(data.status == "success")
				{
					alert('You have successfully added the procedure');

					back_procedure_list(visit_id, patient_id);

				}
				else
				{
					alert(data.message);
				}
				
					
			},
	        error: function(xhr, status, error) {
		        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		        alert(error);
	        }

	        });
		}
	}



// prescription
function prescription_sidebar(visit_id)
{
  open_sidebar();
   var config_url = $('#config_url').val();
    var data_url = config_url+"nurse/prescription_sidebar/"+visit_id;
    // window.alert(data_url);
    $.ajax({
    type:'POST',
    url: data_url,
    data:{visit_id: visit_id},
    dataType: 'text',
    success:function(data){
    //window.alert("You have successfully updated the symptoms");
    //obj.innerHTML = XMLHttpRequestObject.responseText;
     $("#sidebar-div").html(data);
      // alert(data);
    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });

}


function search_drug_directory(visit_id)
{

    var config_url = $('#config_url').val();
    var data_url = config_url+"nurse/search_drug_directory/"+visit_id;
    //window.alert(data_url);
    var drug = $('#q').val();
    $.ajax({
    type:'POST',
    url: data_url,
    data:{visit_id: visit_id, drug : drug},
    dataType: 'text',
    success:function(data){
    //window.alert("You have successfully updated the symptoms");
    //obj.innerHTML = XMLHttpRequestObject.responseText;
    $("#searched-drugs").html(data);
    // alert(data);
    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });
}


function add_prescribed_drug(product_id, status, visit_id){

  

    var config_url = $('#config_url').val();
    var data_url = config_url+"nurse/add_prescribed_drug/"+product_id+"/"+status+"/"+visit_id;
    //window.alert(data_url);
    // var drug = $('#q').val();
    $.ajax({
    type:'POST',
    url: data_url,
    data:{visit_id: visit_id},
    dataType: 'text',
    success:function(data){
         var data = jQuery.parseJSON(data);
        // alert(data.message);
    //window.alert("You have successfully updated the symptoms");
    //obj.innerHTML = XMLHttpRequestObject.responseText;
    $('#adding-list-product').css('display', 'block');
    document.getElementById("medicine_id").value = data.product_id;
    document.getElementById("drug_name").value = data.product_name;
    get_drug_to_prescribe(visit_id);
    // alert(data);
    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });

   
}




 function get_drug_to_prescribe(visit_id)
 {
 var XMLHttpRequestObject = false;
       
   if (window.XMLHttpRequest) {
   
       XMLHttpRequestObject = new XMLHttpRequest();
   } 
       
   else if (window.ActiveXObject) {
       XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
   }
   var drug_id = document.getElementById("medicine_id").value;
 
   var url = "<?php echo site_url();?>pharmacy/inpatient_prescription/"+visit_id+"/"+drug_id+"/2";
     // alert(url);
    if(XMLHttpRequestObject) {
               
       XMLHttpRequestObject.open("GET", url);
               
       XMLHttpRequestObject.onreadystatechange = function(){
           
           if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
             var prescription_view = document.getElementById("prescription_view");
            
             document.getElementById("prescription_view").innerHTML=XMLHttpRequestObject.responseText;
              prescription_view.style.display = 'block';
           }
       }
               
       XMLHttpRequestObject.send(null);
   }
 
 
 }


function check_frequency_type()
{

 var x = document.getElementById("x_value").value;
 var type_of_drug = document.getElementById("type_of_drug").value;
   
 var number_of_days = document.getElementById("number_of_days_value").value;
 var quantity = document.getElementById("quantity_value").value;

 // alert(quantity);
 var dose_value = document.getElementById("dose_value").value;

var service_charge_id = document.getElementById("drug_id").value;
var visit_id = document.getElementById("visit_id").value;
var consumption = document.getElementById("consumption_value").value;

 if(x == "" || x == 0)
 {

   // alert("Please select the frequency of the medicine");
   x = 1;
 }
 
 if(number_of_days == "" || number_of_days == 0)
 {
    number_of_days = 1;
 }
 
     
  var url = "<?php echo base_url();?>pharmacy/get_values";
   $.ajax({
   type:'POST',
   url: url,
   data:{quantity: quantity, x: x, service_charge_id : service_charge_id, visit_id : visit_id, number_of_days: number_of_days,dose_value:dose_value,consumption: consumption,type_of_drug: type_of_drug},
   dataType: 'text',
   success:function(data){
      var data = jQuery.parseJSON(data);

      var amount = data.amount;
      var frequency = data.frequency;
       var item = data.item;

       var total_units = data.total_units;
       var total_amount = data.total_amount;

      // {"message":"success","amount":"35","frequency":"5"}

      // if(type_of_drug == 3)
      // {
       
      //   var total_units = number_of_days * frequency * quantity;
      //   var total_amount =  number_of_days * frequency * amount * quantity;
      // }
      // else
      // {

      //    var total_units =   quantity;
      //    var total_amount =   amount * quantity;
      // }
    

    // document.getElementById("total_units").innerHTML = "<h2>"+ amount +" units</h2>";
     $( "#total_units" ).html("<h2>"+ total_units +" units</h2>");
     $( "#total_amount" ).html("<h3>Ksh. "+ total_amount +"</h3>");
     $( "#item_description" ).html("<p> "+ item +"</p>");

     document.getElementById("input-total-value").value = total_units;
     // document.getElementById("total_amount").innerHTML = "<h3>Ksh. "+ frequency +" units</h3>";


   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });

    
 
}


function pass_prescription()
   {

   var quantity = document.getElementById("quantity_value").value;
   var x = document.getElementById("x_value").value;
   var dose_value = document.getElementById("dose_value").value;
   var duration = 1;//document.getElementById("duration_value").value;
   var consumption = document.getElementById("consumption_value").value;
   var number_of_days = document.getElementById("number_of_days_value").value;
   var service_charge_id = document.getElementById("drug_id").value;
   var visit_id = document.getElementById("visit_id").value;
   var input_total_units = document.getElementById("input-total-value").value;
   var module = document.getElementById("module").value;
   var passed_value = document.getElementById("passed_value").value;
   var type_of_drug = document.getElementById("type_of_drug").value;
   
   var url = "<?php echo base_url();?>pharmacy/prescribe_prescription";
   
  
   $.ajax({
   type:'POST',
   url: url,
   data:{quantity: quantity, x: x, duration: duration,consumption: consumption, service_charge_id : service_charge_id, visit_id : visit_id, number_of_days: number_of_days,module: module,passed_value:passed_value,input_total_units:input_total_units,dose_value: dose_value,type_of_drug: type_of_drug},
   dataType: 'text',
   success:function(data){
   
   var prescription_view = document.getElementById("prescription_view");
   prescription_view.style.display = 'none';
   display_inpatient_prescription(visit_id,0);
   close_side_bar();
   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });
   
   return false;
   }

   function display_inpatient_prescription(visit_id,module)
   {
   
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     
     var config_url = document.getElementById("config_url").value;
     var url = config_url+"pharmacy/display_inpatient_prescription/"+visit_id+"/"+module;
     
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
   
                 document.getElementById("visit_prescription").innerHTML=XMLHttpRequestObject.responseText;
             }
         }
                 
         XMLHttpRequestObject.send(null);
     }
   }

   function delete_prescription(prescription_id, visit_id,visit_charge_id,module)
   {
   var res = confirm('Are you sure you want to delete this from the  prescription ?');
   
   if(res)
   {
     var XMLHttpRequestObject = false;
     
     if (window.XMLHttpRequest) {
       XMLHttpRequestObject = new XMLHttpRequest();
     } 
     
     else if (window.ActiveXObject) {
       XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
      var config_url = document.getElementById("config_url").value;
     var url = config_url+"pharmacy/delete_inpatient_prescription/"+prescription_id+"/"+visit_id+"/"+visit_charge_id+"/"+module;
     
     if(XMLHttpRequestObject) {
       
       XMLHttpRequestObject.open("GET", url);
       
       XMLHttpRequestObject.onreadystatechange = function(){
         
         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
           
            display_inpatient_prescription(visit_id,0);
          
         }
       }
       XMLHttpRequestObject.send(null);
     }
   }
   }

</script>




  <script type="text/javascript">

function appointment_sidebar(patient_id,visit_id,department_id,personnel_id)
  {
      open_sidebar();
     var config_url = $('#config_url').val();
      var data_url = config_url+"dental/appointment_sidebar/"+patient_id+"/"+visit_id+"/"+department_id+"/"+personnel_id;
      // window.alert(data_url);
      $.ajax({
      type:'POST',
      url: data_url,
      data:{visit_id: visit_id},
      dataType: 'text',
      success:function(data){
      //window.alert("You have successfully updated the symptoms");
      //obj.innerHTML = XMLHttpRequestObject.responseText;
       $("#sidebar-div").html(data);

        $('.datepicker').datepicker({
              format: 'yyyy-mm-dd',
              minDate: "dateToday"
          });

        // $('.datepicker').datepicker();
        $('.timepicker').timepicker();
        // alert(data);
      },
      error: function(xhr, status, error) {
      //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
      alert(error);
      }

      });

  }

function save_appointment(visit_id)
{

// $(document).on("submit","form#next_appointment_detail",function(e)
// {
  // alert("sdasdjakgdaskjdag");
  // e.preventDefault();
  
  // var form_data = new FormData(this);

  // alert(form_data);


  var visit_id = $('#visit_id').val();
  var patient_id = $('#patient_id'+visit_id).val();
  var appointment_note = $('#appointment_note'+visit_id).val();
  var next_appointment_date = $('#next_appointment_date'+visit_id).val();
  var doctor_id = $('#doctor_id'+visit_id).val();
  var department_id = $('#department_id'+visit_id).val();
  var event_duration = $('#event_duration'+visit_id).val();
  var timepicker_start = $('#timepicker_start'+visit_id).val();
  
  var config_url = $('#config_url').val();  

   var url = config_url+"reception/save_appointment_accounts/"+patient_id+"/"+visit_id;
       $.ajax({
       type:'POST',
       url: url,
       data:{visit_id: visit_id,patient_id: patient_id,appointment_note: appointment_note,visit_date: next_appointment_date,doctor_id: doctor_id,department_id: department_id,event_duration:event_duration,timepicker_start:timepicker_start},
       dataType: 'text',
       // processData: false,
       // contentType: false,
       success:function(data)
       {
          var data = jQuery.parseJSON(data);
          
          if(data.status == "success")
          {
            
            alert('You have successfully added an appointment');

            display_next_appointment(patient_id,visit_id);
            close_side_bar();
          }
          else
          {

            alert(data.message);
          }
       
       },
       error: function(xhr, status, error) {
       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
       
       }
       });
}

function display_next_appointment(patient_id,visit_id)
{
  // alert(patient_id);

  var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     
     var config_url = document.getElementById("config_url").value;
     var url = config_url+"dental/display_appointment_view/"+patient_id+"/"+visit_id;
     
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
   
                 document.getElementById("next-appointment-view").innerHTML=XMLHttpRequestObject.responseText;
             }
         }
                 
         XMLHttpRequestObject.send(null);
     }
}


 function view_schedule(visit_id,doctor_id)
 {
     var datess=document.getElementById("next_appointment_date"+visit_id).value;
    var doctor_id= document.getElementById("doctor_id"+visit_id).value;

 
       if(datess && doctor_id){
        load_schedule(visit_id);
        load_patient_appointments_two(visit_id);
      // $('#show_doctor').fadeToggle(1000); return false;
     }
     else{
      alert('Select Date and a Doctor First')
     }
 }

 function load_schedule(visit_id){
    var config_url = $('#config_url').val();
    var datess=document.getElementById("next_appointment_date"+visit_id).value;
    var doctor= document.getElementById("doctor_id"+visit_id).value;

    var url= config_url+"reception/doc_schedule/"+doctor+"/"+datess;


    
      $('#doctors_schedule'+visit_id).load(url);
      $('#doctors_schedule'+visit_id).fadeIn(1000); return false; 
  }
  function load_patient_appointments(visit_id){
    var patient_id = $('#patient_id'+visit_id).val();
    var current_date = $('#next_appointment_date'+visit_id).val();

    var url= config_url+"reception/patient_schedule/"+patient_id+"/"+current_date;
    
    $('#patient_schedule'+visit_id).load(url);
    $('#patient_schedule'+visit_id).fadeIn(1000); return false; 

    $('#patient_schedule2'+visit_id).load(url);
    $('#patient_schedule2'+visit_id).fadeIn(1000); return false;  
  }
  function load_patient_appointments_two(visit_id){
    var patient_id = $('#patient_id'+visit_id).val();
    var current_date = $('#next_appointment_date'+visit_id).val();

    var url= config_url+"reception/patient_schedule/"+patient_id+"/"+current_date;
    
    $('#patient_schedule2'+visit_id).load(url);
    $('#patient_schedule2'+visit_id).fadeIn(1000); return false;  
  }
   function delete_event_details(appointment_id,status,visit_id,patient_id)
  {
    var config_url = $('#config_url').val();  
    var res = confirm('Are you sure you want to delete this schedule ?');

    if(res)
    {

       var url = config_url+"calendar/delete_event_details/"+appointment_id+"/"+status;
           $.ajax({
           type:'POST',
           url: url,
           data:{appointment_id: appointment_id,status: status},
           dataType: 'text',
           processData: false,
           contentType: false,
           success:function(data){
              var data = jQuery.parseJSON(data);
            
              if(data.message == "success")
              {
                display_next_appointment(patient_id,visit_id);
              }
              else
              {
                alert('Please ensure you have added included all the items');
              }
           
           },
           error: function(xhr, status, error) {
           alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
           
           }
           });
     }
     else
     {
     
     }

  }





</script>