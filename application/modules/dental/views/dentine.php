

<br>
<?php
	$one_child = $this->dental_model->get_dentine_value($patient_id,55);
	$two_child = $this->dental_model->get_dentine_value($patient_id,54);
	$three_child = $this->dental_model->get_dentine_value($patient_id,53);
	$four_child = $this->dental_model->get_dentine_value($patient_id,52);
	$five_child = $this->dental_model->get_dentine_value($patient_id,51);
	$six_child = $this->dental_model->get_dentine_value($patient_id,61);
	$seven_child = $this->dental_model->get_dentine_value($patient_id,62);
	$eight_child = $this->dental_model->get_dentine_value($patient_id,63);
	$nine_child = $this->dental_model->get_dentine_value($patient_id,64);
	$ten_child = $this->dental_model->get_dentine_value($patient_id,65);
	$eleven_child = $this->dental_model->get_dentine_value($patient_id,85);
	$twelve_child = $this->dental_model->get_dentine_value($patient_id,84);
	$thirteen_child = $this->dental_model->get_dentine_value($patient_id,83);
	$fourteen_child = $this->dental_model->get_dentine_value($patient_id,82);
	$fifteen_child = $this->dental_model->get_dentine_value($patient_id,81);
	$sixteen_child = $this->dental_model->get_dentine_value($patient_id,71);
	$seventeen_child = $this->dental_model->get_dentine_value($patient_id,72);
	$eighteen_child = $this->dental_model->get_dentine_value($patient_id,73);
	$nineteen_child = $this->dental_model->get_dentine_value($patient_id,74);
	$twenty_child = $this->dental_model->get_dentine_value($patient_id,75);



if($chart_type == 0)
{
	$child_chart = 'display: block;';
	$adult_chart = 'display: none;';
}
else if($chart_type == 1)
{
	$child_chart = 'display: none;';
	$adult_chart = 'display: block;';
}
else
{
	$child_chart = 'display: none;';
	$adult_chart = 'display: none;';
}


?>



<div id="children-div" style="<?php echo $child_chart?>"  >
	<div class="col-print-12" style="border-left: 2px solid #000; border-right: 2px solid #000;border-bottom: 2px solid #000;">
	 		
	 		<table align='center' class='table table-striped table-condensed' style="margin-bottom: 0px !important;">
	 			
	 			<tr style="background-color:#968f9f;">
	 				<td  class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$one_child?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td  class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$two_child?>')  no-repeat center center !important ;height:206px;" ></td>
	 				<td  class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$three_child?>')  no-repeat center center !important ;height:206px;" ></td>
	 				<td  class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$four_child?>')  no-repeat center center !important ;height:206px;" ></td>
	 				<td  class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$five_child?>')  no-repeat center center !important ;height:206px;" ></td>
	 				<td  class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$six_child?>')  no-repeat center center !important ;height:206px;"> </td>
	 				<td  class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$seven_child?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td  class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$eight_child?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td  class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$nine_child?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td  class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$ten_child?>')  no-repeat center center !important ;height:206px;"></td>
	 			</tr>
	 			<tr >
	 				<td class="center-align" style="height: 35px;width: 15px;" onclick="check_department_type(55)" id="55"> 55 </td>
	 				<td class="center-align" style="height: 35px;width: 15px;" onclick="check_department_type(54)" id="54"> 54 </td>
	 				<td class="center-align" style="height: 35px;width: 15px;" onclick="check_department_type(53)" id="53"> 53 </td>
	 				<td class="center-align" style="height: 35px;width: 15px;" onclick="check_department_type(52)" id="52"> 52 </td>
	 				<td class="center-align" style="height: 35px;width: 15px;" onclick="check_department_type(51)" id="51" > 51 </td>
	 				<td class="center-align" style="height: 35px;width: 15px;"  onclick="check_department_type(61)" id="61"> 61 </td>
	 				<td class="center-align" style="height: 35px;width: 15px;"  onclick="check_department_type(62)" id="62"> 62 </td>
	 				<td class="center-align" style="height: 35px;width: 15px;"  onclick="check_department_type(63)" id="63"> 63 </td>
	 				<td class="center-align" style="height: 35px;width: 15px;"  onclick="check_department_type(64)" id="64"> 64 </td>
	 				<td class="center-align" style="height: 35px;width: 15px;" onclick="check_department_type(65)" id="65"> 65 </td>
	 				
	 			</tr>

	 		</table>
	 	<!-- </div> -->
	</div>
	
	<div class="col-print-12" style="border-left: 2px solid #000; border-right: 2px solid #000;">
	 	<!-- <div class="col-md-12"> -->
	 		<table align='center' class='table table-striped table-condensed' style="margin-bottom: 0px !important;">
	 			<tr >
	 				<td class="center-align" style="height: 35px;width: 15px;"  onclick="check_department_type(85)" id="85"> 85 </td>
	 				<td class="center-align" style="height: 35px;width: 15px;"  onclick="check_department_type(84)" id="84"> 84 </td>
	 				<td class="center-align" style="height: 35px;width: 15px;"  onclick="check_department_type(83)" id="83"> 83 </td>
	 				<td class="center-align" style="height: 35px;width: 15px;"  onclick="check_department_type(82)" id="82"> 82 </td>
	 				<td class="center-align" style="height: 35px;width: 15px;"  onclick="check_department_type(81)" id="81"> 81 </td>
	 				<td class="center-align" style="height: 35px;width: 15px;"  onclick="check_department_type(71)" id="71"> 71 </td>
	 				<td class="center-align" style="height: 35px;width: 15px;"  onclick="check_department_type(72)" id="72"> 72 </td>
	 				<td class="center-align" style="height: 35px;width: 15px;"  onclick="check_department_type(73)" id="73"> 73 </td>
	 				<td class="center-align" style="height: 35px;width: 15px;"  onclick="check_department_type(74)" id="74"> 74 </td>
	 				<td class="center-align" style="height: 35px;width: 15px;"  onclick="check_department_type(75)" id="75"> 75 </td>
	 				
	 			</tr>
	 			<tr style="background-color:#968f9f;">
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$eleven_child?>')  no-repeat center center !important ;height:206px;"> </td>
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$twelve_child?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$thirteen_child?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$fourteen_child?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$fifteen_child?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$sixteen_child?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$seventeen_child?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$eighteen_child?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$nineteen_child?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$twenty_child?>')  no-repeat center center !important ;height:206px;"></td>
	 			</tr>
	 			
	 			
	 		</table>
	 	<!-- </div> -->
	</div>

</div>
<?php
	$one = $this->dental_model->get_dentine_value($patient_id,18);
	$two = $this->dental_model->get_dentine_value($patient_id,17);
	$three = $this->dental_model->get_dentine_value($patient_id,16);
	$four = $this->dental_model->get_dentine_value($patient_id,15);
	$five = $this->dental_model->get_dentine_value($patient_id,14);
	$six = $this->dental_model->get_dentine_value($patient_id,13);
	$seven = $this->dental_model->get_dentine_value($patient_id,12);
	$eight = $this->dental_model->get_dentine_value($patient_id,11);
	$nine = $this->dental_model->get_dentine_value($patient_id,21);
	$ten = $this->dental_model->get_dentine_value($patient_id,22);
	$eleven = $this->dental_model->get_dentine_value($patient_id,23);
	$twelve = $this->dental_model->get_dentine_value($patient_id,24);
	$thirteen = $this->dental_model->get_dentine_value($patient_id,25);
	$fourteen = $this->dental_model->get_dentine_value($patient_id,26);
	$fifteen = $this->dental_model->get_dentine_value($patient_id,27);
	$sixteen = $this->dental_model->get_dentine_value($patient_id,28);
	$seventeen = $this->dental_model->get_dentine_value($patient_id,48);
	$eighteen = $this->dental_model->get_dentine_value($patient_id,47);
	$nineteen = $this->dental_model->get_dentine_value($patient_id,46);
	$twenty = $this->dental_model->get_dentine_value($patient_id,45);
	$twenty_one = $this->dental_model->get_dentine_value($patient_id,44);
	$twenty_two = $this->dental_model->get_dentine_value($patient_id,43);
	$twenty_three = $this->dental_model->get_dentine_value($patient_id,42);
	$twenty_four = $this->dental_model->get_dentine_value($patient_id,41);
	$twenty_five = $this->dental_model->get_dentine_value($patient_id,31);
	$twenty_six = $this->dental_model->get_dentine_value($patient_id,32);
	$twenty_seven = $this->dental_model->get_dentine_value($patient_id,33);
	$twenty_eight = $this->dental_model->get_dentine_value($patient_id,34);
	$twenty_nine = $this->dental_model->get_dentine_value($patient_id,35);
	$thirty = $this->dental_model->get_dentine_value($patient_id,36);
	$thirty_one = $this->dental_model->get_dentine_value($patient_id,37);
	$thirty_two = $this->dental_model->get_dentine_value($patient_id,38);



	

?>

<div id="adult-div" style="<?php echo $adult_chart?>" >
	<div class="col-print-12" style="border-left: 2px solid #000;border-right: 2px solid #000;border-bottom: 2px solid #000;">
	 	<!-- <div class="col-md-12"> -->
	 		
	 		<table align='center' class='table table-striped table-condensed' style="margin-bottom: 0px !important;">
	 			<tr style="background-color:#968f9f;">
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$one?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td class="center-align eighteen"  style="background: url('<?php echo site_url().'assets/dentine/'.$two?>')  no-repeat center center !important ;height:206px;"> </td>

	 				<td class="center-align eighteen"  style="background: url('<?php echo site_url().'assets/dentine/'.$three?>')  no-repeat center center !important ;height:206px;"> </td>
	 				<td class="center-align eighteen"  style="background: url('<?php echo site_url().'assets/dentine/'.$four?>')  no-repeat center center !important ;height:206px;"> </td>
	 				<td class="center-align eighteen"  style="background: url('<?php echo site_url().'assets/dentine/'.$five?>')  no-repeat center center !important ;height:206px;"> </td>
	 				<td class="center-align eighteen"  style="background: url('<?php echo site_url().'assets/dentine/'.$six?>')  no-repeat center center !important ;height:206px;"> </td>
	 				<td class="center-align eighteen"  style="background: url('<?php echo site_url().'assets/dentine/'.$seven?>')  no-repeat center center !important ;height:206px;"> </td>
	 				<td class="center-align eighteen"  style="background: url('<?php echo site_url().'assets/dentine/'.$eight?>')  no-repeat center center !important ;height:206px;"> </td>

	 				<td class="center-align eighteen"  style="background: url('<?php echo site_url().'assets/dentine/'.$nine?>')  no-repeat center center !important ;height:206px;"> </td>
	 				<td class="center-align eighteen"  style="background: url('<?php echo site_url().'assets/dentine/'.$ten?>')  no-repeat center center !important ;height:206px;"> </td>
	 				<td class="center-align eighteen"  style="background: url('<?php echo site_url().'assets/dentine/'.$eleven?>')  no-repeat center center !important ;height:206px;"> </td>
	 				<td class="center-align eighteen"  style="background: url('<?php echo site_url().'assets/dentine/'.$twelve?>')  no-repeat center center !important ;height:206px;"> </td>
	 				<td class="center-align eighteen"  style="background: url('<?php echo site_url().'assets/dentine/'.$thirteen?>')  no-repeat center center !important ;height:206px;"> </td>
	 				<td class="center-align eighteen"  style="background: url('<?php echo site_url().'assets/dentine/'.$fourteen?>')  no-repeat center center !important ;height:206px;"> </td>
	 				<td class="center-align eighteen"  style="background: url('<?php echo site_url().'assets/dentine/'.$fifteen?>')  no-repeat center center !important ;height:206px;"> </td>
	 				<td class="center-align eighteen"  style="background: url('<?php echo site_url().'assets/dentine/'.$sixteen?>')  no-repeat center center !important ;height:206px;"> </td>


	 			</tr>
	 			
	 			<tr >
	 				<td class="center-align" onclick="check_department_type(18)" id="18"> 18 </td>
	 				<td class="center-align" onclick="check_department_type(17)" id="17"> 17 </td>
	 				<td class="center-align" onclick="check_department_type(16)" id="16"> 16 </td>
	 				<td class="center-align" onclick="check_department_type(15)" id="15"> 15 </td>
	 				<td class="center-align" onclick="check_department_type(14)" id="14"> 14 </td>
	 				<td class="center-align" onclick="check_department_type(13)" id="13"> 13 </td>
	 				<td class="center-align" onclick="check_department_type(12)" id="12"> 12 </td>
	 				<td class="center-align" onclick="check_department_type(11)" id="11"> 11 </td>


	 				<td class="center-align" onclick="check_department_type(21)" id="21"> 21 </td>
	 				<td class="center-align" onclick="check_department_type(22)" id="22"> 22 </td>
	 				<td class="center-align" onclick="check_department_type(23)" id="23"> 23 </td>
	 				<td class="center-align" onclick="check_department_type(24)" id="24"> 24 </td>
	 				<td class="center-align" onclick="check_department_type(25)" id="25"> 25 </td>
	 				<td class="center-align" onclick="check_department_type(26)" id="26"> 26 </td>
	 				<td class="center-align" onclick="check_department_type(27)" id="27"> 27 </td>
	 				<td class="center-align" onclick="check_department_type(28)" id="28"> 28 </td>
	 				
	 			</tr>


	 		</table>
	 	<!-- </div> -->
	</div>
	
	<div class="col-print-12" style="border-left: 2px solid #000;border-right: 2px solid #000;">
	 	<!-- <div class="col-md-12"> -->
	 		<table align='center' class='table table-striped table-condensed' style="margin-bottom: 0px !important;">
	 			<tr >
	 				<td class="center-align"  onclick="check_department_type(48)" id="48"> 48 </td>
	 				<td class="center-align"  onclick="check_department_type(47)" id="47"> 47 </td>
	 				<td class="center-align"  onclick="check_department_type(46)" id="46"> 46 </td>
	 				<td class="center-align"  onclick="check_department_type(45)" id="45"> 45 </td>
	 				<td class="center-align"  onclick="check_department_type(44)" id="44"> 44 </td>
	 				<td class="center-align"  onclick="check_department_type(43)" id="43"> 43 </td>
	 				<td class="center-align"  onclick="check_department_type(42)" id="42"> 42 </td>
	 				<td class="center-align"  onclick="check_department_type(41)" id="41"> 41 </td>

	 				<td class="center-align"  onclick="check_department_type(31)" id="31"> 31 </td>
	 				<td class="center-align"  onclick="check_department_type(32)" id="32"> 32 </td>
	 				<td class="center-align"  onclick="check_department_type(33)" id="33"> 33 </td>
	 				<td class="center-align"  onclick="check_department_type(34)" id="34"> 34 </td>
	 				<td class="center-align"  onclick="check_department_type(35)" id="35"> 35 </td>
	 				<td class="center-align"  onclick="check_department_type(36)" id="36"> 36 </td>
	 				<td class="center-align"  onclick="check_department_type(37)" id="37"> 37 </td>
	 				<td class="center-align"  onclick="check_department_type(38)" id="38"> 38 </td>
	 				
	 			</tr>
	 			<tr style="background-color:#968f9f;">
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$seventeen?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$eighteen?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$nineteen?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$twenty?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$twenty_one?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$twenty_two?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$twenty_three?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$twenty_four?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$twenty_five?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$twenty_six?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$twenty_seven?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$twenty_eight?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$twenty_nine?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$thirty?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$thirty_one?>')  no-repeat center center !important ;height:206px;"></td>
	 				<td class="center-align eighteen"   style="background: url('<?php echo site_url().'assets/dentine/'.$thirty_two?>')  no-repeat center center !important ;height:206px;"></td>
	 				
	 			</tr>
	 			
	 			
	 		</table>
	 	<!-- </div> -->
	</div>
	
</div>
	
