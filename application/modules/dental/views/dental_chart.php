<?php
// get visit details


$visit_rs = $this->reception_model->get_visit_details($visit_id);

foreach ($visit_rs as $key) {
	# code...

	$time_end=$key->time_end;
	$time_start=$key->time_start;
	$visit_date=$key->visit_date;
	$personnel_id=$key->personnel_id;
	$room_id=$key->room_id;
	$patient_id=$key->patient_id;
	// $scheme_name=$key->scheme_name;
	$insurance_description=$key->insurance_description;
	$patient_insurance_number=$key->patient_insurance_number;
	$insurance_limit=$key->insurance_limit;
	$principal_member=$key->principal_member;
}

if($chart_type == 1)
{
	$child_items = '';
	$adult_items = 'checked';
}
else
{
	$child_items = 'checked';
	$adult_items = '';
}
// var_dump($patient_id);die();
?>
<div class="row">
    <div class="col-md-12">
		<div class="col-print-6" >
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-lg-4 control-label">Type of chart? </label>
			        <div class="col-lg-4">
			            <div class="radio">
			                <label>
			                    <input id="optionsRadios1" type="radio" name="chart_type" <?php echo $child_items;?> id="chart_type_one" onclick="get_chart_type(0)" value="0">
			                    Child
			                </label>
			            </div>
			        </div>
			        
			        <div class="col-lg-4">
			            <div class="radio">
			                <label>
			                    <input id="optionsRadios1" type="radio" name="chart_type" <?php echo $adult_items;?> id="chart_type_two" onclick="get_chart_type(1)"  value="1">
			                    Adult
			                </label>
			            </div>
			        </div>
				</div>

			</div>
			<div id="page_item"></div>
			
			
			
		</div>
		<div class="col-print-6" >
			
			<div class="col-md-12" >
				<div class="col-md-12">
						<h1 class="center-align"><i id="toothnumber"></i></h1>
					</div>
				<!-- <hr> -->
				<div class="col-md-12" >
					<div class="col-md-4">  
						<div id="dentine-list-div"></div>
					</div>
					<div class="col-md-4">
						
						<h4>Plans</h4>
						<div class="form-group left-align">
			                <div class="radio">
			                	<label>
			                        <input id="optionsRadios2" type="radio" name="procedure_status" checked  id="procedure_status"  value="1">
			                         Treatment Plan
			                    </label>
			                    <br>
			                    <label>
			                        <input id="optionsRadios2" type="radio" name="procedure_status"  id="procedure_status"  value="2">
			                         Completed
			                    </label>
			                    <br>
			                    <label>
			                        <input id="optionsRadios2" type="radio" name="procedure_status"  id="procedure_status"  value="3">
			                        Exam
			                    </label>
			                	
			                    
			                </div>
			            </div>
					</div>
					
					<div class="col-md-4">
						<div id="teeth-marker"></div>
						<div class="col-md-12" >
							<div class="center-align">
								<a class="btn btn-sm btn-success  " onclick="pass_tooth()"> Update Dental</a>	
							</div>
											 	
						</div>
					</div>
				</div>


			</div>
		</div>
	</div>
</div>