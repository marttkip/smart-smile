<?php

if($query->num_rows() > 0)
{
	foreach ($query->result() as $key => $value) {
		# code...
		$visit_type_name = $value->visit_type_name;
		$batch_date_from = $value->batch_date_from;
		$batch_date_to = $value->batch_date_to;
		$created = $value->created;
		$batch_number = $value->batch_number;
		$batch_amount = $value->batch_amount;
		$debtor_id = $value->visit_type_id;
	}
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Batch</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
        	body
        	{
        		font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
        	}
			.receipt_spacing{letter-spacing:0px; font-size: 14px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			
			.col-md-6 {
			    width: 50%;
			 }
		
			h3
			{
				font-size: 30px;
			}
			p
			{
				margin: 0 0 0px !important;
			}
			
			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{ margin:0 auto;}
			.col-print-1 {width:8%;  float:left;}
			.col-print-2 {width:16%; float:left;}
			.col-print-3 {width:25%; float:left;}
			.col-print-4 {width:33%; float:left;}
			.col-print-5 {width:42%; float:left;}
			.col-print-6 {width:50%; float:left;}
			.col-print-7 {width:58%; float:left;}
			.col-print-8 {width:66%; float:left;}
			.col-print-9 {width:75%; float:left;}
			.col-print-10{width:83%; float:left;}
			.col-print-11{width:92%; float:left;}
			.col-print-12{width:100%; float:left;}

			table.table{
			    border-spacing: 0 !important;
			    font-size:14px;
				margin-top:0px;
				margin-bottom: 10px !important;
				border-collapse: inherit !important;
				font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
			}
			th, td {
			    border: 1.5px solid #000 !important;
			    padding: 0.5em 1em !important;
			}
			tr, td {
			    border: 1.5px solid #000 !important;
			    padding: 0.5em 1em !important;
			}
			/* the first 'th' within the first 'tr' of the 'thead': */
			thead tr:first-child th:first-child {
			    /*border-radius: 10px 0 0 0 !important;*/
			}
			/* the last 'th' within the first 'tr' of the 'thead': */
			thead tr:first-child th:last-child {
			    /*border-radius: 0 10px 0 0 !important;*/
			}
			/* the first 'td' within the last 'tr' of the 'tbody': */
			tbody tr:last-child td:first-child {
			    /*border-radius: 0 0 0 10px !important;*/
			}
			/* the last 'td' within the last 'tr' of the 'tbody': */
			tbody tr:last-child td:last-child {
			    /*border-radius: 0 0 10px 10px !important;*/
			}
			tbody tr:first-child td:first-child {
			    /*border-radius: 10px 10px 0 0 !important;*/
			    /*border-bottom: 0px solid #000 !important;*/
			}
			.padd
			{
				padding:10px;
			}
			
		</style>
    </head>
    <body>
    	
 		

    	<div class="padd " >
    		<div class="row">
    			<div class="col-md-12">
		    		<div class="col-print-6" style="margin-bottom: 10px;">
		            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive" />
		            	<!-- <p style="font-size: 13px !important;margin-top:10px !important;">Dr. Lorna Sande | Dr. Kinoti M. Dental Surgeons</p> -->
		            	
		            	<!-- <p style="font-size: 13px;">Pin. PO51455684R </p> -->
		            </div>
		            <div class="col-print-6" style="margin-bottom: 10px;padding: 10px">
		            	<h3 class="pull-right"><strong>Batch# <?php echo $batch_number;?></strong></h3>
		            </div>
		        	<!-- <div  class="center-align">
		            	<p style="font-size: 40px;">
		                	<h3 style="text-decoration: underline;"><?php echo $contacts['company_name'];?></h3> <br/>
		                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
		                    Address :<strong> P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></strong> <br/>
		                    Office Line: <strong> <?php echo $contacts['phone'];?></strong> <br/>
		                    Website: <strong> http://www.irisdental.co.ke </strong> E-mail: <strong><?php echo $contacts['email'];?>.</strong><br/>
		                </p>
		            </div> -->
		        </div>
		    </div>
    
        
	      	<div class="row" >
	      		<div class="col-md-12">
	      			<div class="col-print-6 left-align">
	      				
		            	<table class="table">
		            		<tr>
		            			<td><strong>Dispatch to</strong></td>
		            		</tr>
		            		<tr>
		            			
		            			<td>
		            				
		            				<strong>INSURANCE COMPANY:</strong> <?php echo $visit_type_name; ?> <br>
		            				<strong>BATCH DATE:</strong> <?php echo $batch_date_from.' - '.$batch_date_to; ?>

		            			</td>
		            		</tr>
		            	</table>
		            </div>
		            <div class="col-print-3">
		            	&nbsp;
		            </div>
		            <div class="col-print-3">
		            	<p style="font-size: 12px;"> &nbsp;</p>
		            	<table class="table">
		            		<tr>
		            			<td>
		            				<span class="pull-left">Date Created:</span> 
		            				<span class="pull-right"> <strong><?php echo date('jS M Y',strtotime($created)); ?> </strong></span>
		            			</td>
		            		</tr>
		            		<tr>
		            			<td>
		            				<span class="pull-left">Batch Number:</span> 
		            				<span class="pull-right"> <strong><?php echo $batch_number; ?> </strong></span>
		            			</td>
		            		</tr>
		            		
		            	</table>
		            </div>
	      			
	      		</div>
	        	
	        </div>
	       
		    
	      	<div class="row">
	      		<div class="col-md-12">
	      			<?php


					$creditor_result = $this->debtors_model->get_allocated_invoices($debtor_batch_id);
					?>
				 	<table class="table table-hover table-bordered ">
					 	<thead>
							<tr>
							  <th>#</th>
							  <th>File No.</th>
							  <th>Patient</th>
							  <th>Invoice Date</th>	
							  <th>Member No</th>
							  <th>Scheme</th>
							  <th>Invoice Number</th>  
							  <th>Invoice Amount</th>				
							</tr>
						 </thead>
					  	<tbody>
					  		<?php
					  		$x=0;
					  		$result_items='';
					  		// var_dump($creditor_result->num_rows());die();
					  		if($creditor_result->num_rows() > 0)
					  		{
					  			foreach ($creditor_result->result() as $key => $value) {
					  				# code...
					  				$patient_number = $value->patient_number;
					  				$scheme_name = $value->scheme_name;
					  				$member_number = $value->member_number;
					  				$visit_invoice_id = $value->visit_invoice_id;
					  				$created = $value->invoice_date;
					  				$patient_surname = $value->patient_surname;
					  				$patient_onames = $value->patient_onames;
					  				$patient_first_name = $value->patient_first_name;
					  				$visit_invoice_number = $value->visit_invoice_number;
					  				$preauth_amount = $value->total_amount;
					  				$name = $patient_first_name.' '.$patient_onames.' '.$patient_surname;

					  				$checkbox_data = array(
	                                        'name'        => 'creditor_invoice_items[]',
	                                        'id'          => 'checkbox',
	                                        'class'          => 'css-checkbox  lrg ',
	                                        'checked'=>'checked',
	                                        'value'       => $visit_invoice_id
	                                      );


					  				$x++;
					  				$result_items .= '
					  									<tr>
					  										
					  										<td>'.$x.'</td>
					  										<td>'.$patient_number.'</td>
					  										<td>'.$name.'</td>
					  										<td>'.date('Y.m.d',strtotime($created)).'</td>
					  										<td>'.$member_number.'</td>
					  										<td>'.$scheme_name.'</td>
					  										<td>'.$visit_invoice_number.'</td>
					  										<td>'.number_format($preauth_amount,2).'</td>
					  									</tr>
					  									';

					  			}
					  			$result_items .= '
					  									<tr>
					  										
					  										<th></th>
					  										<th></th>
					  										<th></th>
					  										<th></th>
					  										<th></th>
					  										<th></th>
					  										<th>Total Batch Amount</th>
					  										<th>'.number_format($batch_amount,2).'</th>
					  									</tr>
					  									';
					  		}
					  		echo $result_items;
					  		?>
						</tbody>
					</table>
		      

	      		</div>
	      	</div>
	      	<div class="row" style="margin-top: 20px; ">

	      		<br>
	          	<br>
	      		<div class="col-md-12">
	                <div class="col-print-4">
						Approved By: ...................................................................
	                </div>
	                <div class="col-print-4">
	                	
	                  	Signature: .....................................................................
	                       
	                </div>
	                <div class="col-print-4">
	                	
	                  	Date: .........................................................................
	                        
	                </div>
	          	</div>
	          	<br>
	          	<br>
	          	<br>
	          	<br>
	          	<br>
	          	<br>
	          	<div class="col-md-12">
	                <div class="col-print-4">
						Received By: ...................................................................
	                </div>
	                <div class="col-print-4">
	                	
	                  	Signature: .....................................................................
	                       
	                </div>
	                <div class="col-print-4">
	                	
	                  	Date: .........................................................................
	                        
	                </div>
	          	</div>
	      	</div>
	  


    </body>
    
</html>