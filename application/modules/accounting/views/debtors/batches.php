<!-- search -->
<?php echo $this->load->view('search/search_debtor_batches', '', TRUE);?>
<!-- end search -->

<div class="row">
	<div class="col-md-12"> 

		<section class="panel">
			<header class="panel-heading">

				<h2 class="panel-title"><?php echo $title;?></h2>



			</header>

			<div class="panel-body">



				<?php
				$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');

				if(!empty($error))
				{
					echo '<div class="alert alert-danger">'.$error.'</div>';
					$this->session->unset_userdata('error_message');
				}

				if(!empty($success))
				{
					echo '<div class="alert alert-success">'.$success.'</div>';
					$this->session->unset_userdata('success_message');
				}

				$search = $this->session->userdata('creditor_payment_search');
				$search_title = $this->session->userdata('creditor_search_title');



				if(!empty($search))
				{
					echo '
					<a href="'.site_url().'accounting/creditors/close_creditor_search/'.$creditor_id.'" class="btn btn-warning btn-sm ">Close Search</a>
					';
					echo $search_title;
				}	
			// var_dump($creditor_id); die();
				//$creditor_result = $this->debtors_model->get_unallocated_invoices($debtor_id);
				//var_dump($creditor_result);
				//die();
				?>
				<input type="hidden" name="debtor_id" id="debtor_id" value="<?php echo $debtor_id;  ?>">


				<div id="ubatched_table"> </div>


				<!-- <div class="col-md-12">
					<div class="col-md-6">
						<input type="hidden" name="batch_amount" id="batch_amount">
					</div>
					<div class="col-md-6">
						<?php
						$next_purchase_number = $this->debtors_model->create_batch_number();

						?>
						<div class="table-responsive" id="complete-batch" style="display: none;margin: 0 auto;" >
							<table class="table table-responsive table-condensed table-striped table-bordered">
								<thead>
									<th>Title</th>
									<th>Description</th>
								</thead>
								<tbody>
									<tr>
										<td>Batch Number</td>
										<td><input type="text" class="form-control" name="batch_number" value="<?php echo $next_purchase_number?>" id="batch_number" readonly></td>
									</tr>

									<tr>

										<td>Batch Amount</td>
										<td><span id="amount_reconcilled"></span></td>
									</tr>


									<tr>


									</tr>

								</tbody>
							</table>
							<div class="col-md-12">
								<div class="center-align" >
									<button type="submit" id="submit-button" class="btn btn-sm btn-success" onclick="return confirm('Are you sure you want to complete this batch')"> Create Batch </button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form> -->
			<hr>
			<h2 class="panel-title">Insurance Batches</h2>
			<?php
			$debtor_batches_rs = $this->debtors_model->get_debtor_batches($debtor_id);
			?>
			<table class="table table-hover table-bordered ">
				<thead>
					<tr>
						<th>#</th>
						<th>Batch Number</th>	
						<th>Batch Dates</th>						  
						<th>Total Invoices</th>
						<th>Batch Amount</th>
						<th>Created</th>
						<th>Status</th>
						<th>Document</th>
						<th colspan="3">Action</th>			
					</tr>
				</thead>
				<tbody>
					<?php
					$batch_result = '';
					$y=0;
					if($debtor_batches_rs->num_rows() > 0)
					{
						foreach ($debtor_batches_rs->result() as $key => $value_two) {
				  				# code...
							$debtor_batch_id = $value_two->debtor_batch_id;
							$batch_number = $value_two->batch_number;
							$batch_date_from = $value_two->batch_date_from;
							$batch_date_to = $value_two->batch_date_to;
							$batch_amount = $value_two->batch_amount;
							$batch_status = $value_two->batch_status;
							$created = $value_two->created;
							$total_invoice_count = $value_two->total_invoice_count;
							$approved_by = $value_two->approved_by;
							$created_by = $value_two->created_by;
							$document_name = $value_two->document_name;

							if($batch_status == 1)
							{
								$status = 'Not Approved';
								$personnel_id = $this->session->userdata('personnel_id');
								$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');

								if($authorize_invoice_changes OR $personnel_id == 0)
								{
									$button = '<a href="'.site_url().'approve-to-dispach/'.$debtor_batch_id.'/'.$debtor_id.'" class="btn btn-sm btn-danger" onclick="confirm(\' Are you sure you want to approve to dispatch ? \')"> Approve for dispatch</a>';
								}
								else
								{
									$button = '';
								}


							}
							else if($batch_status == 2)
							{

								$status = 'Ready for Dispatch';

								$button = '<a href="'.site_url().'print-dispatch/'.$debtor_batch_id.'" target="_blank" class="btn btn-sm btn-warning" > Print Dispatch Note</a>';
							}
							else if($batch_status == 3)
							{
								$status = 'Closed';
								$button = '';
							}

							$y++;
							$batch_result .= '<tr>
							<td>'.$y.'</td>
							<td>'.$batch_number.'</td>
							<td>'.$batch_date_from.' - '.$batch_date_to.'</td>
							<td>'.$total_invoice_count.'</td>
							<td>'.number_format($batch_amount,2).'</td>
							<td>'.date('jS M Y',strtotime($created)).'</td>
							<td>'.$status.'</td>
							<td>'.$document_name.'</td>
							<td>'.$button.' <a  class="btn btn-sm btn-success" onclick="view_dispatch_invoices('.$debtor_batch_id.')"> View Invoices</a></td>

							</tr>';
						}
					}
					echo $batch_result;
					?>
				</tbody>
			</table>
		</div>
	</section>
</div>
</div>
<script type="text/javascript">
	$(function() {
		$("#billed_account_id").customselect();
	});
</script>

<script type="text/javascript">

	function get_unbatched_invoices(){
		//debtor_id
		var debtor_id = document.getElementById('debtor_id').value;
		var tbl = document.getElementById('ubatched_table');
		//console.log(debtor_id);
		var config_url = $('#config_url').val();
		var url = config_url+"accounting/debtors/get_unallocated_invoices_data/" + debtor_id;

        // alert(total_bill);
        $.ajax({
        	type:'GET',
        	url: url,
        	//data:{billed: total_bill},
        	dataType: 'text',
        // processData: false,
        // contentType: false,
        success:function(data){
        	//var data = jQuery.parseJSON(data);
        	//console.log(data);
        	tbl.innerHTML = data;
        },
        error: function(xhr, status, error) {
        	alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

        }
    });

    }

    window.onload = (event) => {
    	console.log('page is fully loaded');
    	get_unbatched_invoices();
    };

    function get_values(visit_invoice_id)
    {
    	var favorite = [];
    	$.each($("input[id='checkbox']:checked"), function(){    

    		var invoiced_value = $("#preauth_amount"+$(this).val()).val(); 

            // favorite.push($(this).val());

            favorite.push(invoiced_value);
        });
        // alert("My favourite sports are: " + favorite.join(", "));
        var total_bill = favorite.join(", ");


        var config_url = $('#config_url').val();
        var url = config_url+"accounting/debtors/calculate_billed_items";

        // alert(total_bill);
        $.ajax({
        	type:'POST',
        	url: url,
        	data:{billed: total_bill},
        	dataType: 'text',
        // processData: false,
        // contentType: false,
        success:function(data){
        	var data = jQuery.parseJSON(data);
          // alert(data);
          if(data.message == 'success')  
          {
                 // alert(data.billing);
                 document.getElementById("batch_amount").value = data.billing;
                // document.getElementById("difference").value = data.balance;
                $("#amount_reconcilled").html(data.billing);
                // alert(data.billing);

                if(data.billing == "0")
                {
                    // alert('herer');
                    $('#complete-batch').css('display', 'none');
                }
                else
                {
                	$('#complete-batch').css('display', 'block');
                }


            }
            else
            {
            	alert(data.result);
            }


        },
        error: function(xhr, status, error) {
        	alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

        }
    });
    }

    function view_dispatch_invoices(debtor_batch_id)
    {
    	document.getElementById("sidebar-right").style.display = "block"; 
            // document.getElementById("existing-sidebar-div").style.display = "none"; 

            var config_url = $('#config_url').val();
            var data_url = config_url+"accounting/debtors/view_dispatch_invoices/"+debtor_batch_id;
        // window.alert(data_url);
        $.ajax({
        	type:'POST',
        	url: data_url,
        	data:{account_payment_id: 1},
        	dataType: 'text',
        	success:function(data){

        		document.getElementById("current-sidebar-div").style.display = "block"; 
        		$("#current-sidebar-div").html(data);

     //         $('.datepicker').datepicker({
					//     format: 'yyyy-mm-dd'
					// });

				},
				error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
    }

});
    }

    function close_side_bar()
    {
        // $('html').removeClass('sidebar-right-opened');
        document.getElementById("sidebar-right").style.display = "none"; 
        document.getElementById("current-sidebar-div").style.display = "none"; 
        document.getElementById("existing-sidebar-div").style.display = "none"; 
        tinymce.remove();
    }

    function clearUserdata(){
    	var config_url = $('#config_url').val();
    	var url = config_url+"accounting/debtors/clear_session_search_filter";

        // alert(total_bill);
        $.ajax({
        	type:'GET',
        	url: url,
        	//data:{billed: total_bill},
        	dataType: 'text',
        // processData: false,
        // contentType: false,
        success:function(data){
        	//var data = jQuery.parseJSON(data);
        	//console.log(data);
        	//tbl.innerHTML = data;
        	get_unbatched_invoices();
        },
        error: function(xhr, status, error) {
        	alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

        }
    });

    }



    $(document).on("submit","form#submit-search-data",function(e)
    {
    	//alert('clicked');
    	e.preventDefault();
    	var config_url = $('#config_url').val();
    	var data_url1 = config_url+"accounting/debtors/search_creditor_account";
    	// alert(data_url);
    	var form_data_search = new FormData(this);

    	//alert(form_data.values());

    	$.ajax({
    		type:'POST',
    		url: data_url1,
    		data:form_data_search,
    		dataType: 'text',
    		processData: false,
            contentType: false,
    		success:function(data){
    			get_unbatched_invoices();

				},
			error: function(xhr, status, error) {
        			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
      			alert(error);
      			// console.log(error);
    		}
		});
    });






</script>