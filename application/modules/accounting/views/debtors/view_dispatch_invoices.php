<?php


$creditor_result = $this->debtors_model->get_allocated_invoices($debtor_batch_id);
?>

<div class="col-md-12" style="margin-top:40px !important;">
	<section class="panel">
		<div class="panel-body">
			 <div class="padd">
			 	<table class="table table-hover table-bordered ">
				 	<thead>
						<tr>
						  <th>#</th>
						  <th>File No.</th>
						  <th>Patient</th>
						  <th>Invoice Date</th>	
						  <th>Member No</th>
						  <th>Scheme</th>
						  <th>Invoice Number</th>  
						  <th>Invoice Amount</th>				
						</tr>
					 </thead>
				  	<tbody>
				  		<?php
				  		$x=0;
				  		$result_items='';
				  		// var_dump($creditor_result->num_rows());die();
				  		if($creditor_result->num_rows() > 0)
				  		{
				  			foreach ($creditor_result->result() as $key => $value) {
				  				# code...
				  				$patient_number = $value->patient_number;
				  				$scheme_name = $value->scheme_name;
				  				$member_number = $value->member_number;
				  				$visit_invoice_id = $value->visit_invoice_id;
				  				$created = $value->invoice_date;
				  				$patient_surname = $value->patient_surname;
				  				$patient_onames = $value->patient_onames;
				  				$patient_first_name = $value->patient_first_name;
				  				$visit_invoice_number = $value->visit_invoice_number;
				  				$preauth_amount = $value->total_amount;
				  				$name = $patient_first_name.' '.$patient_onames.' '.$patient_surname;

				  				$checkbox_data = array(
                                        'name'        => 'creditor_invoice_items[]',
                                        'id'          => 'checkbox',
                                        'class'          => 'css-checkbox  lrg ',
                                        'checked'=>'checked',
                                        'value'       => $visit_invoice_id
                                      );


				  				$x++;
				  				$result_items .= '
				  									<tr>
				  										
				  										<td>'.$x.'</td>
				  										<td>'.$patient_number.'</td>
				  										<td>'.$name.'</td>
				  										<td>'.date('jS M Y',strtotime($created)).'</td>
				  										<td>'.$member_number.'</td>
				  										<td>'.$scheme_name.'</td>
				  										<td>'.$visit_invoice_number.'</td>
				  										<td>'.number_format($preauth_amount,2).'<input type="hidden" class="form-control" colspan="3" name="preauth_amount'.$visit_invoice_id.'" id="preauth_amount'.$visit_invoice_id.'" value="'.$preauth_amount.'"/></td>
				  									</tr>
				  									';

				  			}
				  		}
				  		echo $result_items;
				  		?>
					</tbody>
				</table>
		      </div>
		</div>
	</section>
</div>
<br/>
<div class="row" style="margin-top: 5px;">
		<ul>
			<li style="margin-bottom: 5px;">
				<div class="row">
			        <div class="col-md-12 center-align">
				        <a  class="btn btn-sm btn-info" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR</a>
				        		
			               
			        </div>
			    </div>
				
			</li>
		</ul>
	</div>
