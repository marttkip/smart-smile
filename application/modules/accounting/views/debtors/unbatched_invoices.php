<?php

	$search_filter_from = $this->session->userdata('date_from');
	$search_filter_to = $this->session->userdata('date_to');

	$clearbtn = '';
	if(!empty($search_filter_from))
	{
	
		$clearbtn .= '<button class="btn btn-primary btn-s" style="margin: 10px;" onclick="clearUserdata()">Clear</button>';
	}


?>


<div style="display: flex; justify-content: space-between;">
	<h2 class="panel-title">Unbatched Invoices</h2>
	<?php echo $clearbtn; ?>
</div>
<?php
	            echo form_open("accounting/debtors/create_debtor_batch/".$debtor_id, array("class" => "form-horizontal"));
	            ?>
<table class="table table-hover table-bordered ">
	<thead>
		<tr>
			<th></th>
			<th>#</th>
			<th>File No.</th>
			<th>Patient</th>
			<th>Invoice Date</th>	
			<th>Member No</th>
			<th>Scheme</th>
			<th>Invoice Number</th>

			<th>Invoice Amount</th>				
		</tr>
	</thead>
	<tbody>
		<?php
		$x=0;
		$result_items='';
		$total_invoiced = 0;
  		// var_dump($creditor_result->num_rows());die();
		if($creditor_result->num_rows() > 0)
		{
			foreach ($creditor_result->result() as $key => $value) {
  				# code...
				$patient_number = $value->patient_number;
				$scheme_name = $value->scheme_name;
				$member_number = $value->member_number;
				$visit_invoice_id = $value->visit_invoice_id;
				$created = $value->invoice_date;
				$patient_surname = $value->patient_surname;
				$patient_onames = $value->patient_onames;
				$patient_first_name = $value->patient_first_name;
				$visit_invoice_number = $value->visit_invoice_number;
				$preauth_amount = $value->total_amount;
				$name = $patient_first_name.' '.$patient_onames.' '.$patient_surname;
				$total_invoiced += $preauth_amount;
				$checkbox_data = array(
					'name'        => 'creditor_invoice_items[]',
					'id'          => 'checkbox',
					'class'          => 'css-checkbox  lrg ',
                        // 'checked'=>'checked',
					'value'       => $visit_invoice_id,
					'onclick'=>'get_values('.$visit_invoice_id.')'
				);


				$x++;
				$result_items .= '
				<tr>
				<td>'.form_checkbox($checkbox_data).'<label for="checkbox'.$visit_invoice_id.'" name="checkbox79_lbl" class="css-label lrg klaus"></label>'.'</td>
				<td>'.$x.'</td>
				<td>'.$patient_number.'</td>
				<td>'.$name.'</td>
				<td>'.date('jS M Y',strtotime($created)).'</td>
				<td>'.$member_number.'</td>
				<td>'.$scheme_name.'</td>
				<td>'.$visit_invoice_number.'</td>
				<td>'.number_format($preauth_amount,2).'<input type="hidden" class="form-control" colspan="3" name="preauth_amount'.$visit_invoice_id.'" id="preauth_amount'.$visit_invoice_id.'" value="'.$preauth_amount.'"/></td>
				</tr>
				';

			}
			$result_items .= '
					<tr>
					<td colspan="7"></td>
					<th>TOTAL AMOUNT</th>
					<th>'.number_format($total_invoiced,2).'</th>
					</tr>
					';
		}
		echo $result_items;
		?>
	</tbody>
</table>
<div class="col-md-12">
		            <div class="col-md-6">
		               <input type="hidden" name="batch_amount" id="batch_amount">
		            </div>
		            <div class="col-md-6">
		            	<?php
		            	$next_purchase_number = $this->debtors_model->create_batch_number();

		            	?>
		                <div class="table-responsive" id="complete-batch" style="display: none;margin: 0 auto;" >
		                    <table class="table table-responsive table-condensed table-striped table-bordered">
		                        <thead>
		                            <th>Title</th>
		                            <th>Description</th>
		                        </thead>
		                         <tbody>
		                         	<tr>
		                                <td>Batch Number</td>
		                                <td><input type="text" class="form-control" name="batch_number" value="<?php echo $next_purchase_number?>" id="batch_number" readonly></td>
		                            </tr>
		                            
		                            <tr>
		                                
		                                <td>Batch Amount</td>
		                                <td><span id="amount_reconcilled"></span></td>
		                            </tr>


		                            <tr>
		                               
		                                
		                            </tr>
		                            
		                        </tbody>
		                    </table>
		                    <div class="col-md-12">
		                    	 <div class="center-align" >
                                    <button type="submit" id="submit-button" class="btn btn-sm btn-success" onclick="return confirm('Are you sure you want to complete this batch')"> Create Batch </button>
                                </div>
		                    </div>
		                </div>
		            </div>
		        </div>
</form>
