<?php
class debtors_model extends CI_Model 
{

	public function get_all_visits_view($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('patients.*,v_patient_account_balances.*');
		$this->db->where($where);
		$this->db->order_by('patients.patient_id','DESC');
		// $this->db->group_by('patients.patient_id');
		// $this->db->join('v_patient_balances','v_patient_balances.patient_id = visit.visit_id','LEFT');
		// $this->db->join('personnel','visit.personnel_id = personnel.personnel_id','left');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	function export_debtors()
	{
		$this->load->library('excel');
		
		
		$where = 'v_transactions_by_date.transactionCategory = "Revenue" AND visit_invoice.visit_invoice_id = v_transactions_by_date.transaction_id';
		
		$table = 'v_transactions_by_date,visit_invoice';
		$visit_search = $this->session->userdata('debtors_search_query');
		// var_dump($visit_search);die();
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		
			
			
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'" ';
			$where .= ' AND v_transactions_by_date.transaction_date = "'.date('Y-m-d').'" ';
		}
		

		$branch_session = $this->session->userdata('branch_id');

		if($branch_session > 0)
		{
			$where .= ' AND v_transactions_by_date.branch_id = '.$branch_session;
			// $where .= $visit_search;
		
		}
		
		$this->db->select('v_transactions_by_date.*, patients.*, payment_method.*, personnel.personnel_fname, personnel.personnel_onames,visit_invoice.visit_invoice_number,branch.branch_name,branch.branch_code,visit.visit_id,visit_invoice.visit_invoice_id');
		$this->db->join('patients', 'patients.patient_id = v_transactions_by_date.patient_id', 'left');
		$this->db->join('payment_method', 'payment_method.payment_method_id = v_transactions_by_date.payment_method_id', 'left');
		$this->db->join('payments', 'payments.payment_id = v_transactions_by_date.transaction_id', 'left');
		$this->db->join('visit', 'visit.visit_id = visit_invoice.visit_id', 'left');
		$this->db->join('personnel', 'visit.personnel_id = personnel.personnel_id', 'left');
		$this->db->join('branch', 'branch.branch_id = v_transactions_by_date.branch_id', 'left');

		$this->db->where($where);

		$visits_query = $this->db->get($table);
		
		$title = 'Transactions Export '.date('jS M Y H:i a',strtotime(date('Y-m-d H:i:s')));
		$col_count = 0;
		
		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/
			$row_count = 0;
			$report[$row_count][$col_count] = '#';
			$col_count++;
			$report[$row_count][$col_count] = 'Invoice Date';
			$col_count++;
			$report[$row_count][$col_count] = 'Patient No.';
			$col_count++;
			$report[$row_count][$col_count] = 'Patient';
			$col_count++;
			$report[$row_count][$col_count] = 'Category';
			$col_count++;
			$report[$row_count][$col_count] = 'Doctor';
			$col_count++;
			$report[$row_count][$col_count] = 'Invoice No.';
			$col_count++;
			$report[$row_count][$col_count] = 'Branch Code';
			$col_count++;
			$report[$row_count][$col_count] = 'Invoice Amount';
			$col_count++;
			$report[$row_count][$col_count] = 'Payments';
			$col_count++;
			$report[$row_count][$col_count] = 'Balance';
			$col_count++;	
			//display all patient data in the leftmost columns
			$total_waiver = 0;
			$total_payments = 0;
			$total_invoice = 0;
			$total_balance = 0;
			$total_rejected_amount = 0;
			$total_cash_balance = 0;
			$total_insurance_payments =0;
			$total_insurance_invoice =0;
			$total_payable_by_patient = 0;
			$total_payable_by_insurance = 0;
			$total_debit_notes = 0;
			$total_credit_notes= 0;
			foreach ($visits_query->result() as $row)
			{
				$row_count++;
				$total_invoiced = 0;
				$visit_date = date('jS M Y',strtotime($row->transaction_date));
				$visit_time = date('H:i a',strtotime($row->visit_time));
				if($row->visit_time_out != '0000-00-00 00:00:00')
				{
					$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
				}
				else
				{
					$visit_time_out = '-';
				}
				
				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$personnel_id = $row->personnel_id;
				$dependant_id = $row->dependant_id;
				$strath_no = $row->strath_no;
				$visit_type_id = $row->visit_type;
				$patient_number = $row->patient_number;
				$visit_type = $row->visit_type;
				$visit_table_visit_type = $visit_type;
				$patient_table_visit_type = $visit_type_id;
				$rejected_amount = $row->amount_rejected;
				$visit_invoice_number = $row->visit_invoice_number;
				$visit_invoice_id = $row->visit_invoice_id;
				$parent_visit = $row->parent_visit;
				$branch_code = $row->branch_code;

				if(empty($rejected_amount))
				{
					$rejected_amount = 0;
				}
				// $coming_from = $this->reception_model->coming_from($visit_id);
				// $sent_to = $this->reception_model->going_to($visit_id);
				$visit_type_name = $row->payment_type_name;
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
				$patient_date_of_birth = $row->patient_date_of_birth;

				$doctor = $row->personnel_fname;
				$count++;
				$invoice_total = $row->dr_amount;
				$payments_value = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
				$balance  = $this->accounts_model->balance($payments_value,$invoice_total);

				$total_payable_by_patient += $invoice_total;
				$total_payments += $payments_value;
				$total_balance += $balance;


				$report[$row_count][$col_count] = $count;
				$col_count++;
				$report[$row_count][$col_count] = $visit_date;
				$col_count++;
				$report[$row_count][$col_count] = $patient_number;
				$col_count++;
				$report[$row_count][$col_count] = ucwords(strtolower($patient_surname));
				$col_count++;
				$report[$row_count][$col_count] = $visit_type_name;
				$col_count++;
				$report[$row_count][$col_count] = $doctor;
				$col_count++;
				$report[$row_count][$col_count] = $visit_invoice_number;
				$col_count++;
				$report[$row_count][$col_count] = $branch_code;
				$col_count++;
				$report[$row_count][$col_count] = number_format($invoice_total,2);
				$col_count++;
				$report[$row_count][$col_count] = (number_format($payments_value,2));
				$col_count++;
				$report[$row_count][$col_count] = (number_format($balance,2));
				$col_count++;				
				
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}


	/*
	*	Retrieve all creditor
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_debtors($table, $where, $per_page, $page, $order = 'visit_type_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$this->db->join('visit_type_account','visit_type.visit_type_id = visit_type_account.visit_type','left');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}


	public function update_debtor_account($visit_type_id)
	{
		$account = array(
			'visit_type'=>$visit_type_id,//$this->input->post('account_to_id'),
			'opening_balance'=>$this->input->post('opening_balance'),
			'debit_id'=>$this->input->post('debit_id'),
			'created'=>$this->input->post('start_date')
			);

		// check if it exists

		$this->db->where('visit_type',$visit_type_id);
		$query = $this->db->get('visit_type_account');

		if($query->num_rows() > 0)
		{
			// update
			$this->db->where('visit_type',$visit_type_id);
			if($this->db->update('visit_type_account',$account))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}

		}
		else
		{
			// insert
			if($this->db->insert('visit_type_account',$account))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		// var_dump($account); die();
		
		
	}

	public function update_debtor_batch_date($visit_type_id)
	{
		$account = array(
			
			'batch_start_date'=>$this->input->post('start_date')
			);

		// check if it exists

		$this->db->where('visit_type_id',$visit_type_id);
		if($this->db->update('visit_type',$account))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}	
	}

	public function get_unallocated_invoices($visit_type_id)
	{
		$date_from = $this->session->userdata('date_from');
		$date_to = $this->session->userdata('date_to');

		if(!empty($date_from)){

			$from = ' AND  visit_invoice.created between "'.$date_from.'" and "'.$date_to.'"';
		}


		$this->db->where('visit_type_id',$visit_type_id);
		$query = $this->db->get('visit_type');
		$created = null;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$created = $value->batch_start_date;
			}
		}
		
		$add = '';
		// $batch_start_date = $this->config->item('batch_start_date');


		if(!empty($created))
		{
			$add = ' AND visit_invoice.created >= "'.$created.'"';
		}

		$this->db->where('visit_invoice.patient_id = patients.patient_id AND visit_invoice.visit_invoice_status <> 1 AND visit_charge.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_charge.visit_charge_delete = 0 AND visit_charge.charged = 1 AND visit_invoice.batch_id = 0 AND visit_invoice.bill_to = '.$visit_type_id.$from.$add);
		$this->db->select('visit_invoice.*,SUM(visit_charge.visit_charge_amount*visit_charge.visit_charge_units) AS total_amount,patients.*,visit_invoice.created as invoice_date');
		$this->db->order_by('visit_invoice.created','ASC');
		$this->db->group_by('visit_charge.visit_invoice_id');
		$query = $this->db->get('visit_invoice,visit_charge,patients');

		

		return $query;
	}

	public function create_debtor_batch($visit_type_id)
	{
		$batch_number = $this->input->post('batch_number');
		$batch_amount = $this->input->post('batch_amount');
		// var_dump($batch_amount);die();
		// $total_visits = sizeof($_POST['creditor_invoice_items']);
		// $visit = $_POST['creditor_invoice_items'];
		// var_dump($visit);die();

		$suffix = str_replace('FHDC-BAT', '', $batch_number);

		$array['suffix'] = $suffix;
		$array['batch_number'] = $batch_number;
		$array['created_by'] = $this->session->userdata('personnel_id');
		$array['batch_amount'] = $batch_amount;
		$array['created'] = date('Y-m-d');
		$array['visit_type_id'] = $visit_type_id;
		$array['batch_status'] = 1;
		// var_dump($array);die();
		if($this->db->insert('debtor_batches',$array))
		{
			$debtor_batch_id = $this->db->insert_id();


			// 

			$total_visits = sizeof($_POST['creditor_invoice_items']);
	        //check if any checkboxes have been ticked
	        if($total_visits > 0)
	        {
	          for($r = 0; $r < $total_visits; $r++)
	          {
	            $visit = $_POST['creditor_invoice_items'];
	            $visit_invoice_id = $visit[$r];
	            //check if card is held
	            if($r == 0)
	            {
	            	$visit_start_date = $this->get_visit_invoice_date($visit_invoice_id);
	            }

	            if($r == ($total_visits-1))
	            {
	            	$visit_end_date = $this->get_visit_invoice_date($visit_invoice_id);

	            }
	            // echo $r.' '.$total_visits.' '.$visit_end_date.'<br>';
	            $service = array(
	                      'batch_id'=>$debtor_batch_id,
	                    );
	            $this->db->where('visit_invoice_id',$visit_invoice_id);
	            $this->db->update('visit_invoice',$service);

	            
	          }
	        }

	        // update debtor_invoice

	        $array_two['batch_date_from'] = $visit_start_date;
	        $array_two['batch_date_to'] = $visit_end_date;
	        $array_two['total_invoice_count'] = $total_visits;

	        $this->db->where('debtor_batch_id',$debtor_batch_id);
	        $this->db->update('debtor_batches',$array_two);

	        return TRUE;
		}
		else
		{
			return FALSE;
		}
		// die();
	}
	public function get_visit_invoice_date($visit_invoice_id)
	{
		$this->db->where('visit_invoice_id',$visit_invoice_id);
		$this->db->from('visit_invoice');
		$this->db->select('created');
		// $this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
		  $result = $query->result();
		  $created =  $result[0]->created;
		}

		return $created;
	}
	public function create_batch_number()
	{
	  //select product code
		$branch_code = $this->session->userdata('branch_code');
		$this->db->where('debtor_batch_id > 0');
		$this->db->from('debtor_batches');
		$this->db->select('MAX(suffix) AS number');
		$this->db->order_by('suffix','ASC');
		// $this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
		  $result = $query->result();
		  $number =  $result[0]->number;

		   // var_dump($number); die();

		   
		    $number++;
		    

		 
		  
		}
		else{//start generating rece$number= 1;ipt numbers
		  
		 
		      $number = 1;
		    
		}

		// var_dump($number); die();
		return $branch_code.'-BAT'.$number;
	}

	public function get_debtor_batches($visit_type_id)
	{

		$this->db->where('visit_type_id',$visit_type_id);
		$this->db->from('debtor_batches');
		$this->db->select('*');
		$query = $this->db->get();

		return $query;

	}
	public function get_allocated_invoices($debtor_batch_id)
	{


		$this->db->where('visit_invoice.patient_id = patients.patient_id AND visit_charge.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_charge.visit_charge_delete = 0  AND visit_invoice.visit_invoice_delete = 0  AND visit_charge.charged = 1 AND visit_invoice.batch_id = '.$debtor_batch_id);
		$this->db->select('visit_invoice.*,SUM(visit_charge.visit_charge_amount*visit_charge.visit_charge_units) AS total_amount,patients.*,visit_invoice.created as invoice_date');
		$this->db->order_by('visit_invoice.created','ASC');
		$this->db->group_by('visit_charge.visit_invoice_id');
		$query = $this->db->get('visit_invoice,visit_charge,patients');


		// $this->db->where('visit_invoice.patient_id = patients.patient_id AND visit_invoice.batch_id = '.$debtor_batch_id);
		// $this->db->select('visit_invoice.*,patients.*,visit_invoice.created as invoice_date');
		// $this->db->order_by('visit_invoice.created','ASC');
		// $query = $this->db->get('visit_invoice,patients');

		return $query;
	}
	public function get_receivables_aging_report()
	{
		//retrieve all users
		$this->db->from('visit_type');
		$this->db->select('*');
		$this->db->join('visit_type_account','visit_type.visit_type_id = visit_type_account.visit_type','LEFT');
		$this->db->order_by('visit_type.visit_type_name','ASC');
		$query = $this->db->get();

		return $query;
	}

	public function get_total_invoices($bill_to,$status,$batch_start_date=NULL)
	{

			// var_dump($status)
		if($status == 0)
		{
			$add = ' AND visit_invoice.batch_id = 0';
		}
		else
		{
			$add = ' AND visit_invoice.batch_id > 0';
		}

		if(!empty($batch_start_date))
		{
			$add .= ' AND visit_invoice.created >= "'.$batch_start_date.'" ';
		}

		$this->db->where('visit_invoice.patient_id = patients.patient_id AND visit_charge.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_charge.visit_charge_delete = 0 AND visit_charge.charged = 1  AND visit_invoice.visit_invoice_delete = 0 AND visit_invoice.bill_to = '.$bill_to.$add);
		$this->db->select('SUM(visit_charge.visit_charge_amount*visit_charge.visit_charge_units) AS total_amount');
		// $this->db->order_by('visit_invoice.created','ASC');
		// $this->db->group_by('visit_charge.visit_invoice_id');
		$query = $this->db->get('visit_invoice,visit_charge,patients');

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		if(empty($total_amount))
		{
			$total_amount = 0;
		}
		return $total_amount;
	}

	public function get_total_payment_batches($bill_to)
	{

		
		$this->db->where('batch_receipts.insurance_id = '.$bill_to);
		$this->db->select('SUM(total_amount_paid) AS total_amount');
		$query = $this->db->get('batch_receipts');

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		if(empty($total_amount))
		{
			$total_amount = 0;
		}
		return $total_amount;
	}


	public function get_total_unallocated_payment($bill_to)
	{

		
		$this->db->where('batch_unallocations.batch_receipt_id = batch_receipts.batch_receipt_id AND batch_unallocations.unallocated_payment_delete = 0 AND batch_receipts.batch_receipt_delete = 0 AND batch_receipts.insurance_id = '.$bill_to);
		$this->db->select('SUM(batch_unallocations.amount_paid) AS total_amount');
		$query = $this->db->get('batch_unallocations,batch_receipts');

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		if(empty($total_amount))
		{
			$total_amount = 0;
		}
		return $total_amount;
	}




	// remmitances 

	public function get_transacting_accounts($parent_account_name,$type=null)
	{
	  $branch_session = $this->session->userdata('branch_id');
	  $personnel_id = $this->session->userdata('personnel_id');

	  $branch_add = '';
	  // if($branch_session > 0)
	  // {
	  //   $branch_add = ' AND (branch_id = '.$branch_session.')';
	  // }
	  $this->db->from('account');
	  $this->db->select('*');
	  $this->db->where('(parent_account = 2 OR parent_account =19) AND paying_account = 0'.$branch_add);
	  $query = $this->db->get();     

	  return $query;     

	}
	public function get_receipt_amount($batch_receipt_id)
	{

		$this->db->from('batch_payments');
		$this->db->select('SUM(amount) AS total_amount_paid');
		$this->db->where('batch_receipt_id',$batch_receipt_id);
		$query = $this->db->get();  
		$total_amount_paid = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount_paid = $value->total_amount_paid;
			}
		}
		if(empty($total_amount_paid))
		{
			$total_amount_paid = 0;
		}
		return $total_amount_paid;

	}

	public function get_receipt_amount_paid($batch_receipt_id)
	{
		$this->db->from('payments,batch_receipts');
		$this->db->select('SUM(amount_paid) AS total_amount_paid');
		$this->db->where('batch_receipts.batch_receipt_id = payments.batch_receipt_id AND payments.cancel = 0 AND batch_receipts.current_payment_status = 1 AND payments.batch_receipt_id = '.$batch_receipt_id);
		$query = $this->db->get();  
		$total_amount_paid = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount_paid = $value->total_amount_paid;
			}
		}
		if(empty($total_amount_paid))
		{
			$total_amount_paid = 0;
		}
		return $total_amount_paid;
	}

	/*
	*	Retrieve all departments
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_batch_receipts_payments($table, $where, $per_page, $page, $order = 'batch_payments.invoice_number', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('batch_payments.*,visit_invoice.visit_invoice_number,payments.confirm_number,account.account_name');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$this->db->join('payments','batch_payments.payment_id = payments.payment_id','LEFT');
		$this->db->join('payment_item','payment_item.payment_id = payments.payment_id','LEFT');
		$this->db->join('visit_invoice','visit_invoice.visit_invoice_id = payment_item.visit_invoice_id','LEFT');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	
	/*
	*	Retrieve all departments
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_current_payments($table, $where, $per_page, $page, $order = 'receipt_number', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		// $this->db->group_by('receipt_number');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	public function confirm_batch_payments($batch_receipt_id)
	{

			$receipt_number = $this->input->post('receipt_number');
			$payment_date = $this->input->post('payment_date');
			$bank_id = $this->input->post('bank_id');
			$batch_receipt_id = $this->input->post('batch_receipt_id');
			$payment_method = 9;




			$total_visits = sizeof($_POST['visit_invoices']);
	        //check if any checkboxes have been ticked
	        if($total_visits > 0)
	        {
	          for($r = 0; $r < $total_visits; $r++)
	          {
	            $visit = $_POST['visit_invoices'];
	            $visit_invoice_id = $visit[$r];
	            //check if card is held
	            
	            $amount_paid = $this->input->post('amount_paid'.$visit_invoice_id);
	            $patient_id = $this->input->post('patient_id'.$visit_invoice_id);


	            if($amount_paid > 0)
	            {
		            $data = array(
								'patient_id' => $patient_id,
								'payment_method_id'=>9,
								'amount_paid'=>$amount_paid,
								'personnel_id'=>$this->session->userdata("personnel_id"),
								'payment_type'=>1,
								'transaction_code'=>$receipt_number,
								'reason'=>'Insurance Payment',
								'payment_service_id'=>1,
								'change'=>0,
								'payment_date'=>$payment_date,
								'payment_created_by'=>$this->session->userdata("personnel_id"),
								'approved_by'=>$this->session->userdata("personnel_id"),
								'date_approved'=>date('Y-m-d'),
								'bank_id'=>$bank_id,
								'batch_receipt_id'=>$batch_receipt_id
							);
		            
					$prefix = $suffix = $this->accounts_model->create_receipt_number();
					// var_dump($prefix);die();
					$branch_code = $this->session->userdata('branch_code');
					$branch_id = $this->session->userdata('branch_id');

					$branch_code = str_replace("IDC", "CT", $branch_code);

					if($branch_code == "CT")
					{
						$branch_code = "CTR";
					}

					
					if($prefix < 10)
					{
						$number = '00000'.$prefix;
					}
					else if($prefix < 100 AND $prefix >= 10)
					{
						$number = '0000'.$prefix;
					}
					else if($prefix < 1000 AND $prefix >= 100)
					{
						$number = '000'.$prefix;
					}
					else if($prefix < 10000 AND $prefix >= 1000)
					{
						$number = '00'.$prefix;
					}
					else if($prefix < 100000 AND $prefix >= 10000)
					{
						$number = '0'.$prefix;
					}

					$invoice_number = $branch_code.$number;
					$data['confirm_number'] = $invoice_number;
					$data['suffix'] = $suffix;
					$data['branch_id'] = $branch_id;


					if($this->db->insert('payments', $data))
					{
						$payment_id = $this->db->insert_id();


						 $service = array(
							              'visit_invoice_id'=>$visit_invoice_id,
							              'invoice_type'=>1,
							              'patient_id' => $patient_id,
							              'created_by' => $this->session->userdata('personnel_id'),
							              'created' => $payment_date,
							              'payment_item_amount'=>$amount_paid,
							              'payment_id'=>$payment_id
							              // 'payment_id'=>NULL
							            );
					    $this->db->insert('payment_item',$service);


					    $this->accounts_model->update_visit_invoice_data($payment_id);
					}
	          	}
	        }
			
			return TRUE;
		}
	}

	public function get_all_unallocated_batch_payments($batch_receipt_id)
	{
		$this->db->where('unallocated_payment_delete = 0 AND batch_receipt_id = '.$batch_receipt_id);
		$query = $this->db->get('batch_unallocations');

		return $query;
	}

	public function get_all_unallocated_payments($batch_receipt_id)
	{
		$this->db->select('SUM(amount_paid) AS total_amount');
		$this->db->where('unallocated_payment_delete = 0 AND batch_receipt_id = '.$batch_receipt_id);
		$query = $this->db->get('batch_unallocations');

		$total_amount = 0;

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		if(empty($total_amount))
		{
			$total_amount = 0;
		}
		return $total_amount;
	}
	public function get_all_unpaid_invoices($table, $where,$group_by=NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		// $this->db->order_by('visit_invoice.dentist_id','DESC');
		$query = $this->db->get('');
		
		return $query;
		
	}

	public function get_paid_invoices($table, $where,$group_by=NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('visit_invoice.created AS invoice_date,payments.*,payment_item.payment_item_amount,patients.patient_surname,patients.patient_othernames,visit_invoice.visit_invoice_number');
		$this->db->where($where);
		// $this->db->join('visit_charge','visit_invoice.visit_invoice_id','DESC');
		$query = $this->db->get('');
		
		return $query;
		
	}


	public function get_personnel_name($personnel_id)
	{
		//retrieve all users


		$this->db->from('personnel');
		$this->db->select('*');
		$this->db->where('personnel_id = '.$personnel_id);
		$query = $this->db->get();

		$names = '';
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$personnel_fname = $value->personnel_fname;
				$personnel_onames = $value->personnel_onames;
				$names = $personnel_fname.' '.$personnel_onames;
			}
		}

		if($personnel_id == 0)
		{
			$names = 'Administrator';
		}
		
		return $names;
	}

	public function upload_any_file($path, $location, $name, $upload, $edit = NULL)
	{
		if(!empty($_FILES[$upload]['tmp_name']))
		{
			$image = $this->session->userdata($name);
			
			if((!empty($image)) || ($edit != NULL))
			{
				if($edit != NULL)
				{
					$image = $edit;
				}
				
				//delete any other uploaded image
				if($this->file_model->delete_file($path."\\".$image, $location))
				{
					//delete any other uploaded thumbnail
					$this->file_model->delete_file($path."\\thumbnail_".$image, $location);
				}
				
				else
				{
					$this->file_model->delete_file($path."/".$image, $location);
					$this->file_model->delete_file($path."/thumbnail_".$image, $location);
				}
			}
			//Upload image
			$response = $this->file_model->upload_any_file($path, $upload);
			if($response['check'])
			{
				$file_name = $response['file_name'];
					
				//Set sessions for the image details
				$this->session->set_userdata($name, $file_name);
			
				return TRUE;
			}
		
			else
			{
				$this->session->set_userdata('upload_error_message', $response['error']);
				
				return FALSE;
			}
		}
		
		else
		{
			$this->session->set_userdata('upload_error_message', '');
			return FALSE;
		}
	}
	public function upload_batched_documents($document,$debtor_batch_id)
	{
		$array['document_name'] = $document;
		$array['received_by'] = $this->input->post('received_by');
		$array['received_date'] = $this->input->post('received_date');

		$this->db->where('debtor_batch_id',$debtor_batch_id);
		if($this->db->update('debtor_batches',$array))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function get_debtor_statement_value($visit_type_id,$date,$checked)
	{
		// invoices
		$invoice = '';
		$start_date = '2018-03-01';
		$first_date = date('Y-m').'-01';
		if($checked == 1)
		{
			// 30 days
			$invoice = ' AND visit.visit_date >= "'.$start_date.'" AND visit.visit_date >= "'.$first_date.'" AND visit.visit_date <= "'.$date.'" ';
		
		}
		else if($checked == 2)
		{
			// 30 days
			$sixty_months = date('Y-m-01');
			$sixty_months = date('Y-m-d',strtotime ( '-1 month' , strtotime ( $sixty_months ) ) );
			// var_dump($sixty_months); die();
			$newdate = date('Y-m-t',strtotime ( '+0 month' , strtotime ( $sixty_months ) ) );
			$last_date = date('Y-m-t', strtotime($newdate));

			// $last_date = date('Y-m-d', strtotime('-2 months'));
			// var_dump($last_date); die();
			$invoice = ' AND visit.visit_date >= "'.$start_date.'" AND visit.visit_date >= "'.$sixty_months.'" AND visit.visit_date <= "'.$last_date.'" ';
		}
		else if($checked == 3)
		{
			// 60 days
			// var_dump($checked); die();
			// 30 days
			$sixty_months = date('Y-m-01');
			$sixty_months = date('Y-m-d',strtotime ( '-2 month' , strtotime ( $sixty_months ) ) );
			// var_dump($sixty_months); die();
			$newdate = date('Y-m-t',strtotime ( '+0 month' , strtotime ( $sixty_months ) ) );
			$last_date = date('Y-m-t', strtotime($newdate));


			$invoice = ' AND visit.visit_date >= "'.$start_date.'"AND visit.visit_date >= "'.$sixty_months.'" AND visit.visit_date <= "'.$last_date.'" ';
		}

		else if($checked == 4)
		{
			// over 90 days

			$sixty_months = date('Y-m-01');
			$sixty_months = date('Y-m-d',strtotime ( '-3 month' , strtotime ( $sixty_months ) ) );
			// var_dump($sixty_months); die();
			$newdate = date('Y-m-t',strtotime ( '+0 month' , strtotime ( $sixty_months ) ) );
			$last_date = date('Y-m-t', strtotime($newdate));

			$invoice = ' AND visit.visit_date >= "'.$start_date.'"AND visit.visit_date >= "'.$sixty_months.'" AND visit.visit_date <= "'.$last_date.'" ';
			// $invoice = ' AND visit.visit_date >= "'.$start_date.'" AND visit.visit_date <= "'.$last_date.'" ';
		}
		else if($checked == 5)
		{
			// over 120 days

			$sixty_months = date('Y-m-01');
			$sixty_months = date('Y-m-d',strtotime ( '-4 month' , strtotime ( $sixty_months ) ) );
			// var_dump($sixty_months); die();
			$newdate = date('Y-m-t',strtotime ( '+0 month' , strtotime ( $sixty_months ) ) );
			$last_date = date('Y-m-t', strtotime($newdate));

			$invoice = ' AND visit.visit_date >= "'.$start_date.'" AND visit.visit_date >= "'.$sixty_months.'" AND visit.visit_date <= "'.$last_date.'" ';
			// $invoice = ' AND visit.visit_date >= "'.$start_date.'" AND visit.visit_date <= "'.$last_date.'" ';
		}
		else if($checked == 6)
		{
			// over 120 days

			$sixty_months = date('Y-m-01');
			$sixty_months = date('Y-m-d',strtotime ( '-5 month' , strtotime ( $sixty_months ) ) );
			// var_dump($sixty_months); die();
			$newdate = date('Y-m-t',strtotime ( '+0 month' , strtotime ( $sixty_months ) ) );
			$last_date = date('Y-m-t', strtotime($newdate));

			// var_dump($last_date); die();

			$invoice = ' AND visit.visit_date >= "'.$start_date.'" AND visit.visit_date <= "'.$last_date.'" ';
			// $invoice = ' AND visit.visit_date >= "'.$start_date.'" AND visit.visit_date <= "'.$last_date.'" ';
		}
	

		$this->db->where('visit_charge.visit_charge_delete = 0 AND (visit.parent_visit IS NULL OR visit.parent_visit = 0) AND visit.visit_id = visit_charge.visit_id AND visit.visit_delete = 0 AND visit_charge.charged = 1 AND visit.visit_type = '.$visit_type_id.' '.$invoice);
		$this->db->select('SUM(visit_charge_amount*visit_charge_units) AS total_invoice');
		$query = $this->db->get('visit_charge,visit');
		$total_invoice = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_invoice = $value->total_invoice;
			}
		}


		$this->db->where('visit.visit_id = visit_bill.visit_parent AND (visit.parent_visit = 0 OR visit.parent_visit IS NULL) AND visit.visit_delete = 0 AND visit_bill.visit_type_id = '.$visit_type_id.' '.$invoice);
		$this->db->select('SUM(visit_bill_amount) AS total_invoice');
		$query = $this->db->get('visit_bill,visit');
		$total_rejected_invoice = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_rejected_invoice = $value->total_invoice;
			}
		}




		$this->db->where('(visit.parent_visit = 0 OR visit.parent_visit IS NULL) AND visit.visit_delete = 0 AND visit.visit_type = '.$visit_type_id.' '.$invoice);
		$this->db->select('SUM(rejected_amount) AS total_invoice');
		$rejected = $this->db->get('visit');
		$rejections = 0;
		if($rejected->num_rows() > 0)
		{
			foreach ($rejected->result() as $key => $value) {
				# code...
				$rejections = $value->total_invoice;
			}
		}

		$total_rejected_amount += $rejections;

		$this->db->where('cancel = 0 AND visit.visit_id = payments.visit_id AND visit.visit_delete = 0 AND payments.payment_type = 2 AND visit.visit_type = '.$visit_type_id.' '.$invoice);
		$this->db->select('SUM(amount_paid) AS total_payments');
		$query_waiver = $this->db->get('payments,visit');
		$total_waiver = 0;
		if($query_waiver->num_rows() > 0)
		{
			foreach ($query_waiver->result() as $key => $value) {
				# code...
				$total_waiver = $value->total_payments;
			}
		}




		$this->db->where('cancel = 0 AND visit.visit_id = payments.visit_id AND visit.visit_delete = 0 AND payments.payment_type = 1 AND visit.visit_type = '.$visit_type_id.' '.$invoice);
		$this->db->select('SUM(amount_paid) AS total_payments');
		$query_payments = $this->db->get('payments,visit');
		$total_payments = 0;
		if($query_payments->num_rows() > 0)
		{
			foreach ($query_payments->result() as $key => $value) {
				# code...
				$total_payments = $value->total_payments;
			}
		}
		
		$amount = ($total_invoice) - ($total_payments + $total_waiver);
		// if($visit_type_id == 1 AND $checked == 2)
		// {
		// 	var_dump($total_invoice); die();
		// }
		// if($amount < 0)
		// {
		// 	$amount = -$amount;
		// }

		return $amount;

	}

	public function get_debtor_total_payments($visit_type_id)
	{
		$start_date = date('2018-03-01');
		$invoice = ' AND visit_invoice.created >= "'.$start_date.'"';

		$this->db->where('payments.cancel = 0 AND payments.payment_id = payment_item.payment_id AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id AND visit_invoice.visit_invoice_delete = 0 AND payments.payment_type = 1 AND visit_invoice.bill_to = '.$visit_type_id.' '.$invoice);
		$this->db->select('SUM(payment_item.payment_item_amount) AS total_payments');
		$query_payments = $this->db->get('payments,payment_item,visit_invoice');
		$total_payments = 0;
		if($query_payments->num_rows() > 0)
		{
			foreach ($query_payments->result() as $key => $value) {
				# code...
				$total_payments = $value->total_payments;
			}
		}

		return $total_payments;

	}

	public function get_debtor_statement($visit_type_id)
	{
		$creditor_query = $this->get_opening_debtor_balance($visit_type_id);
		// $bills = $this->get_all_provider_invoices($visit_type_id);
		$all_collections = $this->get_all_provider_work_done($visit_type_id);
		// var_dump($all_collections); die();
		// $payments = $this->get_all_payments_provider($visit_type_id);

		// $brought_forward_balance = $this->get_provider_balance_brought_forward($visit_type_id);

		// var_dump($creditor_query);die();


		$x=0;

		$bills_result = '';
		$last_date = '';
		$visit_last_date = '';
		$current_year = date('Y');
		// $total_invoices = $bills->num_rows();
		$invoices_count = 0;
		$total_invoice_balance = 0;
		$total_arrears = 0;
		$total_payment_amount = 0;
		$result = '';
		$total_pardon_amount = 0;


		$opening_balance = 0;

		$total_invoice_amount = 0;
			$total_transfer_amount = 0;
			$total_credit_amount = 0;
			$total_bill_amount = 0;
			$total_payment_amount = 0;
			$total_rejected_amount = 0;
			$total_arrears_amount = 0;

		$opening_date = date('Y-m-d');
		$debit_id = 2;
		// var_dump($creditor_query->num_rows()); die();
		if($creditor_query->num_rows() > 0)
		{
			$row = $creditor_query->row();
			$opening_balance = $row->opening_balance;
			$opening_date = $row->created;
			$debit_id = $row->debit_id;
			// var_dump($debit_id); die();
			if($debit_id == 2)
			{
				// this is deniget_all_provider_credit_month
				$result .= 
							'
								<tr>
									<td>'.date('d M Y',strtotime($created)).' </td>
									<td colspan=5>Opening Balance</td>
									<td>'.number_format($opening_balance, 2).'</td>
								</tr> 
							';
				$total_arrears_amount += $opening_balance;

			}
			else
			{
				// this is a prepayment
				$result .= 
							'
								<tr>
									<td>'.date('d M Y',strtotime($created)).' </td>
									<td colspan=5>Opening Balance</td>
									<td>'.number_format($opening_balance, 2).'</td>
								</tr> 
							';
				$total_payment_amount = $opening_balance;
				$total_arrears_amount -= $opening_balance;
			}
		}
		

		if($brought_forward_balance == FALSE)
		{
			$result .='';
		}

		else
		{
			$search_title = $this->session->userdata('creditor_search_title');
			if($brought_forward_balance < 0)
			{
				$positive = -$brought_forward_balance;
				$result .= 
							'
								<tr>
									<td colspan=5> B/F</td>
									<td>'.number_format($positive, 2).'</td>
									<td></td>
								</tr> 
							';
				$total_invoice_balance += $positive;

			}
			else
			{
				$result .= 
							'
								<tr>
									<td colspan=6> B/F</td>
									<td></td>
									<td>'.number_format($brought_forward_balance, 2).'</td>
								</tr> 
							';


				$total_invoice_balance += $brought_forward_balance;
			}
		}
		if($all_collections->num_rows() > 0)
		{
			
			foreach ($all_collections->result() as $collections_key) {
				# code...
				$visit_date = $collections_key->visit_date;
				$bill_explode = explode('-', $visit_date);
				$billing_year = $bill_explode[0];
				$billing_month = $bill_explode[1];
				$start_date = $billing_year.'-'.$billing_month.'-01';

				$end_date =  date("Y-m-t", strtotime($start_date));
				$invoice_amount = $this->get_total_invoice_collection($visit_type_id,$start_date,$end_date,$week);
			
				$payments = $this->get_all_payments_debtor_monthly($visit_type_id,$start_date,$end_date,$week);
				$credit = $this->get_all_provider_waiver_month($visit_type_id,$start_date,$end_date,$week);
				// $total_payment_amount += $payments;

				// var_dump($invoice_amount);die();

				$total_bill = ($invoice_amount) - $credit;

				$total_invoice_amount += $invoice_amount;
				$total_waiver_amount += $credit;
				$total_rejected_amount += $rejected_amount;
				$total_payment_amount += $payments;
				$total_bill_amount += $total_bill;
				$total_arrears = $total_bill - $payments - $rejected_amount;
				$total_arrears_amount += $total_arrears;
				
					$result .= 
					'
						<tr>
							<td>'.date('M Y',strtotime($visit_date)).' Invoice </td>
							<td>'.number_format($invoice_amount - $credit, 2).'</td>
							<td>'.number_format($rejected_amount, 2).'</td>
							<td>('.number_format($credit, 2).')</td>
							<td>'.number_format($total_bill, 2).'</td>
							<td>('.number_format($payments, 2).')</td>
							<td>'.number_format($total_arrears, 2).'</td>
							<td><a href="'.site_url().'export-debtor-invoices/'.$visit_type_id.'/'.$start_date.'/'.$end_date.'"  class="btn btn-xs btn-success" >export invoices</a></td>
						</tr> 
					';
				
				$total_invoice_balance += $amount_value;

					
				$visit_last_date = $end_month;
			}

			$result .= 
					'
						<tr>
							<td><strong>Total Amount</strong> </td>
							<td><strong>'.number_format($total_invoice_amount, 2).'</strong></td>
							<td><strong>'.number_format($total_rejected_amount, 2).'</strong></td>
							<td><strong>('.number_format($total_credit_amount, 2).')</strong></td>
							<td><strong>'.number_format($total_bill_amount, 2).'</strong></td>
							<td><strong>('.number_format($total_payment_amount, 2).')</strong></td>
							<td><strong>'.number_format($total_arrears_amount, 2).'</strong></td>
						</tr> 
					';
		}

		
		
	



		$response['total_arrears'] = $total_arrears;
		$response['total_invoice_balance'] = $total_invoice_balance;
		$response['invoice_date'] = $invoice_date;
		$response['opening_balance'] = $opening_balance;
		$response['opening_date'] = $opening_date;
		$response['debit_id'] = $debit_id;
		$response['result'] = $result;
		$response['total_payment_amount'] = $total_payment_amount;

		// var_dump($response); die();

		return $response;
	}

	public function get_opening_debtor_balance($visit_type_id)
	{
		$this->db->select('*'); 
		$this->db->where('visit_type = '.$visit_type_id.'' );
		$query = $this->db->get('visit_type_account');
		
		return $query;
	}

	public function get_all_provider_work_done($visit_type)
	{
		$search = $this->session->userdata('provider_invoice_search');

		if(!empty($search))
		{
			$invoice_search = $search;
		}
		else
		{
			$invoice_search = '';
		}
		$start_date = date('2018-03-01');
		$invoice = ' AND visit_invoice.created >= "'.$start_date.'"';
		
		$this->db->from('visit_invoice,visit');
		$this->db->select('created AS visit_date');
		$this->db->where('visit.visit_delete = 0 AND visit.visit_id = visit_invoice.visit_id AND visit_invoice.visit_invoice_delete = 0  AND visit_invoice.bill_to = '.$visit_type.''.$invoice);
		$this->db->order_by('YEAR(visit_invoice.created),MONTH(visit_invoice.created)','ASC');
		$this->db->group_by('YEAR(visit_invoice.created),MONTH(visit_invoice.created)');
		$query = $this->db->get();
		return $query;
	}

		public function get_total_invoice_collection($visit_type_id,$start_date,$end_date,$week)
	{
		if(!empty($start_date) AND !empty($end_date))
		{
			$search_add =  ' AND (visit_invoice.created >= \''.$start_date.'\' AND visit_invoice.created <= \''.$end_date.'\') ';
			$search_payment_add =  ' AND (visit_invoice.created >= \''.$start_date.'\' AND visit_invoice.created <= \''.$end_date.'\') ';
		}
		else if(!empty($start_date))
		{
			$search_add = ' AND visit_invoice.created = \''.$start_date.'\'';
			$search_payment_add = ' AND visit_invoice.created = \''.$start_date.'\'';
		}
		else if(!empty($end_date))
		{
			$search_add = ' AND visit_invoice.created = \''.$end_date.'\'';
			$search_payment_add = ' AND visit_invoice.created = \''.$end_date.'\'';
		}
		$start_date = date('2018-03-01');
		$invoice = ' AND visit.visit_date >= "'.$start_date.'"';
		$this->db->from('visit,visit_charge,visit_invoice');
		$this->db->select('SUM(visit_charge.visit_charge_amount*visit_charge.visit_charge_units) AS total_charged_amount');
		$this->db->where('visit.visit_id = visit_invoice.visit_id AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id AND visit_invoice.visit_invoice_delete = 0 AND visit_charge.charged = 1 AND visit_charge.visit_charge_delete = 0  AND visit.visit_delete = 0 AND visit_invoice.bill_to = '.$visit_type_id.''.$invoice.''.$search_add);
		$query = $this->db->get();
		$total_charged_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_charged_amount = $value->total_charged_amount;
			}
		}
		// var_dump($total_charged_amount); die();
		return $total_charged_amount;
	}

	public function get_all_payments_debtor_monthly($visit_type,$start_date,$end_date,$payment_week)
	{
		$search = $this->session->userdata('provider_payment_search');

		if(!empty($search))
		{
			$payment_search = $search;
		}
		else
		{
			$payment_search = '';
		}

		$date_from = $this->session->userdata('providers_date_from');
		$date_to = $this->session->userdata('providers_date_to');

		if(!empty($start_date) AND !empty($end_date))
		{
			$search_add =  ' AND (visit_invoice.created >= \''.$start_date.'\' AND visit_invoice.created <= \''.$end_date.'\') ';
			$search_payment_add =  ' AND (visit_invoice.created >= \''.$start_date.'\' AND visit_invoice.created <= \''.$end_date.'\') ';
		}
		else if(!empty($start_date))
		{
			$search_add = ' AND visit_invoice.created = \''.$start_date.'\'';
			$search_payment_add = ' AND visit_invoice.created = \''.$start_date.'\'';
		}
		else if(!empty($end_date))
		{
			$search_add = ' AND visit_invoice.created = \''.$end_date.'\'';
			$search_payment_add = ' AND visit_invoice.created = \''.$end_date.'\'';
		}
		$start_date = date('2018-03-01');
			$invoice = ' AND visit_invoice.created >= "'.$start_date.'"';

		
		$this->db->from('visit_invoice,payments,payment_item');
		$this->db->select('SUM(payment_item.payment_item_amount) AS total_payments');
		$this->db->where('visit_invoice.visit_invoice_delete = 0 AND payments.payment_id = payment_item.payment_id AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id  AND payments.payment_type = 1 AND payments.cancel = 0 AND visit_invoice.bill_to = '.$visit_type.$search_add.$invoice.'');
		$waiver_query = $this->db->get('');
		$total_amount = 0;
		if($waiver_query->num_rows() > 0)
		{
			foreach ($waiver_query->result() as $key => $value) {
				# code...
				$total_amount =$value->total_payments;

			}
		}
		// var_dump($total_amount); die();
		return $total_amount;
	}
	public function get_all_provider_waiver_month($visit_type,$start_date,$end_date,$payment_week)
	{
		$search = $this->session->userdata('provider_payment_search');

		if(!empty($search))
		{
			$payment_search = $search;
		}
		else
		{
			$payment_search = '';
		}

		$date_from = $this->session->userdata('providers_date_from');
		$date_to = $this->session->userdata('providers_date_to');

		if(!empty($start_date) AND !empty($end_date))
		{
			$search_add =  ' AND (visit_invoice.created >= \''.$start_date.'\' AND visit_invoice.created <= \''.$end_date.'\') ';
			$search_payment_add =  ' AND (visit_invoice.created >= \''.$start_date.'\' AND visit_invoice.created <= \''.$end_date.'\') ';
		}
		else if(!empty($start_date))
		{
			$search_add = ' AND visit_invoice.created = \''.$start_date.'\'';
			$search_payment_add = ' AND visit_invoice.created = \''.$start_date.'\'';
		}
		else if(!empty($end_date))
		{
			$search_add = ' AND visit_invoice.created = \''.$end_date.'\'';
			$search_payment_add = ' AND visit_invoice.created = \''.$end_date.'\'';
		}


		
		$this->db->from('visit_invoice,visit_credit_note,visit_credit_note_item');
		$this->db->select('SUM(visit_credit_note_item.visit_cr_note_amount) AS total_payments');
		$this->db->where('visit_invoice.visit_invoice_delete = 0  AND visit_credit_note.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_credit_note.visit_credit_note_id = visit_credit_note_item.visit_credit_note_id AND visit_credit_note.visit_cr_note_delete = 0  AND visit_invoice.bill_to = '.$visit_type.$search_payment_add.'');
		$waiver_query = $this->db->get('');
		$total_amount = 0;
		if($waiver_query->num_rows() > 0)
		{
			foreach ($waiver_query->result() as $key => $value) {
				# code...
				$total_amount =$value->total_payments;

			}
		}

		return $total_amount;
	}
	public function get_all_debtor_rejections($visit_type,$start_date,$end_date,$payment_week)
	{

		if(!empty($start_date) AND !empty($end_date))
		{
			$search_add =  ' AND (visit_date >= \''.$start_date.'\' AND visit_date <= \''.$end_date.'\') ';
			$search_payment_add =  ' AND (visit_date >= \''.$start_date.'\' AND visit_date <= \''.$end_date.'\') ';
		}
		else if(!empty($start_date))
		{
			$search_add = ' AND visit_date = \''.$start_date.'\'';
			$search_payment_add = ' AND visit_date = \''.$start_date.'\'';
		}
		else if(!empty($end_date))
		{
			$search_add = ' AND visit_date = \''.$end_date.'\'';
			$search_payment_add = ' AND visit_date = \''.$end_date.'\'';
		}
		$start_date = date('2018-03-01');
		$invoice = ' AND visit.visit_date >= "'.$start_date.'"';

		$this->db->where('visit.visit_id = visit_bill.visit_parent AND (visit.parent_visit = 0 OR visit.parent_visit IS NULL) AND visit.visit_delete = 0 AND visit.visit_type = '.$visit_type.' '.$search_add.''.$invoice);
		$this->db->select('SUM(visit_bill_amount) AS total_invoice');
		$rejected_query = $this->db->get('visit_bill,visit');
		$total_rejected_invoice = 0;
		if($rejected_query->num_rows() > 0)
		{
			foreach ($rejected_query->result() as $key => $value) {
				# code...
				$total_rejected_invoice = $value->total_invoice;
			}
		}
		return $total_rejected_invoice;

	}

	function export_debtor_statement($visit_type_id,$start_date,$end_date)
	{
		$this->load->library('excel');
		

		$where = 'v_transactions_by_date.transactionCategory = "Revenue" AND visit_invoice.visit_invoice_id = v_transactions_by_date.transaction_id AND visit_invoice.bill_to = '.$visit_type_id.' AND (visit_invoice.created >= "'.$start_date.'" AND visit_invoice.created <= "'.$end_date.'" )';
		
		$table = 'v_transactions_by_date,visit_invoice';
		
		
		$this->db->select('v_transactions_by_date.*, patients.*, payment_method.*, personnel.personnel_fname, personnel.personnel_onames,visit_invoice.visit_invoice_number,visit_invoice.created AS invoice_date,branch.branch_name,branch.branch_code,visit.visit_id,visit_invoice.visit_invoice_id');
		$this->db->join('patients', 'patients.patient_id = v_transactions_by_date.patient_id', 'left');
		$this->db->join('payment_method', 'payment_method.payment_method_id = v_transactions_by_date.payment_method_id', 'left');
		$this->db->join('payments', 'payments.payment_id = v_transactions_by_date.transaction_id', 'left');
		$this->db->join('visit', 'visit.visit_id = visit_invoice.visit_id', 'left');
		$this->db->join('personnel', 'visit.personnel_id = personnel.personnel_id', 'left');
		$this->db->join('branch', 'branch.branch_id = v_transactions_by_date.branch_id', 'left');

		$this->db->where($where);
		$this->db->order_by('v_transactions_by_date.created_at','DESC');
		$visits_query = $this->db->get($table);

		// var_dump($visits_query); die();
		
		$title = 'Debtors Summary for  '.date('jS M Y',strtotime($start_date)).' '.date('jS M Y',strtotime($end_date));
		$col_count = 0;
		
		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/
			$row_count = 0;
			$report[$row_count][$col_count] = '#';
			$col_count++;
			$report[$row_count][$col_count] = 'Visit Date';
			$col_count++;
			$report[$row_count][$col_count] = 'Name';
			$col_count++;
			$report[$row_count][$col_count] = 'Invoice Number';
			$col_count++;
			$report[$row_count][$col_count] = 'Procedures';
			$col_count++;
			$report[$row_count][$col_count] = 'Doctor';
			$col_count++;
			$report[$row_count][$col_count] = 'Invoice Amount';
			$col_count++;
			$report[$row_count][$col_count] = 'Balance';
			$col_count++;
			//display all patient data in the leftmost columns
			foreach($visits_query->result() as $row)
			{
				$row_count++;
				$total_invoiced = 0;
				$total_invoiced = 0;
				$visit_date = date('jS M Y',strtotime($row->transaction_date));
				$visit_time = date('H:i a',strtotime($row->visit_time));
				if($row->visit_time_out != '0000-00-00 00:00:00')
				{
					$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
				}
				else
				{
					$visit_time_out = '-';
				}
				
				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$personnel_id = $row->personnel_id;
				$dependant_id = $row->dependant_id;
				$strath_no = $row->strath_no;
				$visit_type_id = $row->visit_type;
				$patient_number = $row->patient_number;
				$visit_type = $row->visit_type;
				$visit_table_visit_type = $visit_type;
				$patient_table_visit_type = $visit_type_id;
				$rejected_amount = $row->amount_rejected;
				$visit_invoice_number = $row->visit_invoice_number;
				$visit_invoice_id = $row->visit_invoice_id;
				$parent_visit = $row->parent_visit;
				$branch_code = $row->branch_code;
				$invoice_date = $row->invoice_date;

				if(empty($rejected_amount))
				{
					$rejected_amount = 0;
				}
				// $coming_from = $this->reception_model->coming_from($visit_id);
				// $sent_to = $this->reception_model->going_to($visit_id);
				$visit_type_name = $row->payment_type_name;
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
				$patient_date_of_birth = $row->patient_date_of_birth;

				$doctor = $row->personnel_fname;
				$count++;
				$invoice_total = $row->dr_amount;
				$payments_value = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
				$credit_note = $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);

				$invoice_total -= $credit_note;
				$balance  = $this->accounts_model->balance($payments_value,$invoice_total);


				$total_payable_by_patient += $invoice_total;
				$total_payments += $payments_value;
				$total_balance += $balance;



				$item_invoiced_rs = $this->accounts_model->get_patient_visit_charge_items($visit_id,$visit_invoice_id);
			
				$procedures = '';
				if(count($item_invoiced_rs) > 0)
				{
					foreach ($item_invoiced_rs as $key_items):
						$s++;
						$service_charge_name = $key_items->service_charge_name;
						$visit_charge_amount = $key_items->visit_charge_amount;
						$service_name = $key_items->service_name;
						$units = $key_items->visit_charge_units;
						$visit_total = $visit_charge_amount * $units;
						$personnel_id = $key_items->personnel_id;
						$procedures .= strtoupper($service_charge_name).',';
					endforeach;
				}

				// $result .= 
				// 	'
				// 		<tr>
				// 			<td>'.$count.'</td>
				// 			<td>'.$visit_date.'</td>
				// 			<td>'.$patient_number.'</td>
				// 			<td>'.ucwords(strtolower($patient_surname)).'</td>
				// 			<td>'.$visit_type_name.'</td>
				// 			<td>'.$doctor.'</td>
				// 			<td>'.$visit_invoice_number.'</td>
				// 			<td>'.$branch_code.'</td>
				// 			<td>'.number_format($invoice_total,2).'</td>
				// 			<td>'.(number_format($payments_value,2)).'</td>
				// 			<td>'.(number_format($balance,2)).'</td>
				// 			<td><a href="'.site_url().'print-invoice/'.$visit_invoice_id.'/'.$visit_id.'" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Print Invoice</a></td>
				// 		</tr> 
				// ';

				//display the patient data
				$report[$row_count][$col_count] = $count;
				$col_count++;
				$report[$row_count][$col_count] = $invoice_date;
				$col_count++;
				$report[$row_count][$col_count] = $patient_surname.' '.$patient_othernames;
				$col_count++;
				$report[$row_count][$col_count] = $visit_invoice_number;
				$col_count++;
				$report[$row_count][$col_count] = $procedures;
				$col_count++;
				$report[$row_count][$col_count] = $doctor;
				$col_count++;
				$report[$row_count][$col_count] = $invoice_total;
				$col_count++;
				$report[$row_count][$col_count] = $balance;
				$col_count++;
				
				
				
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}

	public function get_v_receivables()
	{
		$this->db->where('recepientId > 0');
		$this->db->order_by('receivable','ASC');
		$this->db->join('visit_type_account','v_aged_receivables.recepientId = visit_type_account.visit_type','LEFT');

		$query = $this->db->get('v_aged_receivables');

		return $query;
	}



	public function export_statements_of_accounts()
	{


		$this->load->library('excel');
		
	
	
		$where = 'v_transactions_by_date.transactionCategory = "Revenue" AND visit_invoice.visit_invoice_id = v_transactions_by_date.transaction_id AND visit_invoice.visit_invoice_status <> 1 AND visit_invoice.created >= "2018-03-01"';
		
		$table = 'v_transactions_by_date,visit_invoice';


		$visit_search = $this->session->userdata('statements_search_query');
		// var_dump($visit_search);die();
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		
			// $query = $this->accounting_model->get_statements_of_accounts($table, $where,1);

			// var_dump($query);die();
			
		}
			
		
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'" ';

		
		}


		$this->db->from($table);

		$this->db->select('v_transactions_by_date.*, patients.*, payment_method.*, personnel.personnel_fname, personnel.personnel_onames,visit_invoice.visit_invoice_number,branch.branch_name,branch.branch_code,visit.visit_id,visit_invoice.visit_invoice_id,visit_invoice.member_number,visit_invoice.authorising_officer,visit_invoice.scheme_name,visit_invoice.preauth_date');
		$this->db->join('patients', 'patients.patient_id = v_transactions_by_date.patient_id', 'left');
		$this->db->join('payment_method', 'payment_method.payment_method_id = v_transactions_by_date.payment_method_id', 'left');
		$this->db->join('payments', 'payments.payment_id = v_transactions_by_date.transaction_id', 'left');
		$this->db->join('visit', 'visit.visit_id = visit_invoice.visit_id', 'left');
		$this->db->join('personnel', 'visit.personnel_id = personnel.personnel_id', 'left');
		$this->db->join('branch', 'branch.branch_id = v_transactions_by_date.branch_id', 'left');

		$this->db->where($where);
		$this->db->order_by('v_transactions_by_date.invoice_date','DESC');
	
		$visits_query = $this->db->get();
		
		$title = 'Statements of Accounts Export ';
		// var_dump($visits_query); die();
		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/

				
			$row_count = 0;
			$report[$row_count][0] = 'Ref No';
			$report[$row_count][1] = 'FILE No';
			$report[$row_count][2] = 'Patient\'s Name. ';
			$report[$row_count][3] = 'Scheme/Company';
			$report[$row_count][4] = 'Member No./Policy No.';
			$report[$row_count][5] = 'Preauthorisation Date';
			$report[$row_count][6] = 'Preauthorisation Officer';
			$report[$row_count][7] = 'Transaction Type';
			$report[$row_count][8] = 'Ageing';
			$report[$row_count][9] = 'Invoice Date';		
			$report[$row_count][10] = 'Invoice Number';
			$report[$row_count][11] = 'Invoice Amount';
			$report[$row_count][12] = 'Invoice Balance';

			$total_waiver = 0;
			$total_payments = 0;
			$total_invoice = 0;
			$total_balance = 0;
			$total_rejected_amount = 0;
			$total_cash_balance = 0;
			$total_insurance_payments =0;
			$total_insurance_invoice =0;
			$total_payable_by_patient = 0;
			$total_payable_by_insurance = 0;
			$total_debit_notes = 0;
			$total_credit_notes= 0;

			// var_dump($visits_query->result());die();
			foreach ($visits_query->result() as $row)
			{
				$total_invoiced = 0;
				$visit_date = date('d.m.y',strtotime($row->transaction_date));
				
				
				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$personnel_id = $row->personnel_id;
				$dependant_id = $row->dependant_id;
				$patient_number = $row->patient_number;
				$visit_invoice_number = $row->visit_invoice_number;
				$scheme_name = $row->scheme_name;
				$member_number = $row->member_number;
				$preauth_date = $row->preauth_date;
				$authorising_officer = $row->authorising_officer;
				$visit_invoice_id = $row->visit_invoice_id;
				$branch_code = $row->branch_code;

				$visit_type_name = $row->payment_type_name;
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
				$patient_date_of_birth = $row->patient_date_of_birth;
				$patient_first_name = $row->patient_first_name;

				$doctor = $row->personnel_fname;
				$count++;
				$invoice_total = $row->dr_amount;
				
				$payments_value = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
				$credit_note = $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);

				$invoice_total -= $credit_note;

				$balance  = $this->accounts_model->balance($payments_value,$invoice_total);

				$preauth_date = date('d.m.y',strtotime($row->preauth_date));
				$total_payable_by_patient += $invoice_total;
				$total_payments += $payments_value;
				$total_balance += $balance;

				$row_count++;
				$date = $row->transaction_date;
									
				$ageing  = $this->get_aging_time($date);
					

				//display the patient data
				$report[$row_count][0] = $count;
				$report[$row_count][1] = $patient_number;
				$report[$row_count][2] = ucwords(strtolower($patient_surname.' '.$patient_othernames.' '.$patient_first_name));
				$report[$row_count][3] = $scheme_name;
				$report[$row_count][4] = $preauth_date;
				$report[$row_count][5] = $visit_invoice_number;
				$report[$row_count][6] = $authorising_officer;
				$report[$row_count][7] = 'Invoice';
				$report[$row_count][8] = $ageing;
				$report[$row_count][9] = $visit_date;
				$report[$row_count][10] = $visit_invoice_number;					
				$report[$row_count][11] = (number_format($invoice_total,2));			
				$report[$row_count][12] = (number_format($balance,2));
			}

			$row_count++;
			$report[$row_count][0] = '';
			$report[$row_count][1] = '';
			$report[$row_count][2] = '';
			$report[$row_count][3] = '';
			$report[$row_count][4] = '';
			$report[$row_count][5] = '';
			$report[$row_count][6] = '';
			$report[$row_count][7] = '';
			$report[$row_count][8] = '';
			$report[$row_count][9] = '';					
			$report[$row_count][10] = '';			
			$report[$row_count][11] = '';
			$report[$row_count][12] = '';



			$row_count++;
			$report[$row_count][0] = '';
			$report[$row_count][1] = '';
			$report[$row_count][2] = '';
			$report[$row_count][3] = '';
			$report[$row_count][4] = '';
			$report[$row_count][5] = '';
			$report[$row_count][6] = '';
			$report[$row_count][7] = '';
			$report[$row_count][8] = '';
			$report[$row_count][9] = '';
			$report[$row_count][10] = 'Total';					
			$report[$row_count][11] = number_format($total_payable_by_patient,2);			
			$report[$row_count][12] = number_format($total_balance,2);





		}

		$row_count++;
		$report[$row_count][0] = '';
		$report[$row_count][1] = '';
		$report[$row_count][2] = 'Unallocated Payments';
		$report[$row_count][3] = '';
		$report[$row_count][4] = '';
		$report[$row_count][5] = '';
		$report[$row_count][6] = '';
		$report[$row_count][7] = '';
		$report[$row_count][8] = '';
		$report[$row_count][9] = '';					
		$report[$row_count][10] = '';			
		$report[$row_count][11] = '';
		$report[$row_count][12] = '';

		$visit_type = $this->session->userdata('statements_visit_type');

		$this->db->where('batch_unallocations.batch_receipt_id = batch_receipts.batch_receipt_id AND batch_unallocations.unallocated_payment_delete = 0 AND batch_receipts.insurance_id = '.$visit_type);
		// $this->db->get('batch_unallocations.*,batch_receipts.*');
		$query = $this->db->get('batch_unallocations,batch_receipts');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...

				$payment_date = $value->payment_date;
				$amount = $value->amount_paid;
				$payment_date = $value->payment_date;
				$receipt_number = $value->receipt_number;

				$payment_date = date('d.m.y',strtotime($value->payment_date));

				$total_balance -= $amount;

				$visit_type_name = $this->session->userdata('statements_visit_type_name');
				$row_count++;
				$report[$row_count][0] = '';
				$report[$row_count][1] = '';
				$report[$row_count][2] = 'Insurance - '.$visit_type_name;
				$report[$row_count][3] = '';
				$report[$row_count][4] = '';
				$report[$row_count][5] = '';
				$report[$row_count][6] = '';
				$report[$row_count][7] = 'Payment';
				$report[$row_count][8] = '';
				$report[$row_count][9] = $payment_date;					
				$report[$row_count][10] = $receipt_number;			
				$report[$row_count][11] = number_format($amount,2);
				$report[$row_count][12] = number_format($amount,2);
			}

			$row_count++;
			$report[$row_count][0] = '';
			$report[$row_count][1] = '';
			$report[$row_count][2] = '';
			$report[$row_count][3] = '';
			$report[$row_count][4] = '';
			$report[$row_count][5] = '';
			$report[$row_count][6] = '';
			$report[$row_count][7] = '';
			$report[$row_count][8] = '';
			$report[$row_count][9] = '';					
			$report[$row_count][10] = '';			
			$report[$row_count][11] = '';
			$report[$row_count][12] = '';


			$row_count++;
			$report[$row_count][0] = '';
			$report[$row_count][1] = '';
			$report[$row_count][2] = '';
			$report[$row_count][3] = '';
			$report[$row_count][4] = '';
			$report[$row_count][5] = '';
			$report[$row_count][6] = '';
			$report[$row_count][7] = '';
			$report[$row_count][8] = '';
			$report[$row_count][9] = '';
			$report[$row_count][10] = 'Total';					
			$report[$row_count][11] = number_format($total_balance,2);			
			$report[$row_count][12] = number_format($total_balance,2);
		}
		// var_dump($report);die();
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}

	public function get_aging_time($date_from)
	{
		$date_to = date('Y-m-d');

		$diff = abs(strtotime($date_to)-strtotime($date_from));
		$years = floor($diff / (365*60*60*24));
		// var_dump($years);die();


		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));


		if($years > 0)
		{
			$months += $years*12;
		}

		return $months;
	}

		public function get_statements_of_accounts($table,$where)
	{
		//retrieve all users
		$this->db->from($table);

		$this->db->select('v_transactions_by_date.*, patients.*, payment_method.*, personnel.personnel_fname, personnel.personnel_onames,visit_invoice.visit_invoice_number,branch.branch_name,branch.branch_code,visit.visit_id,visit_invoice.visit_invoice_id,visit_invoice.member_number,visit_invoice.authorising_officer,visit_invoice.scheme_name,visit_invoice.preauth_date,visit_invoice.created AS invoice_date');
		$this->db->join('patients', 'patients.patient_id = v_transactions_by_date.patient_id', 'left');
		$this->db->join('payment_method', 'payment_method.payment_method_id = v_transactions_by_date.payment_method_id', 'left');
		$this->db->join('payments', 'payments.payment_id = v_transactions_by_date.transaction_id', 'left');
		$this->db->join('visit', 'visit.visit_id = visit_invoice.visit_id', 'left');
		$this->db->join('personnel', 'visit.personnel_id = personnel.personnel_id', 'left');
		$this->db->join('branch', 'branch.branch_id = v_transactions_by_date.branch_id', 'left');

		$this->db->where($where);
		$this->db->order_by('visit_invoice.created','DESC');


		$query = $this->db->get('');
		
		return $query;
	}
}

?>