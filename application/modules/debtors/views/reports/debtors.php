<!-- search -->
<?php echo $this->load->view('search_debtors', '', TRUE);?>
<!-- end search -->
<?php //echo $this->load->view('transaction_statistics_invoices', '', TRUE);?>
 
<div class="row">
    <div class="col-md-12">

        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title"><?php echo $title;?></h2>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
          <h5 class="center-align"><?php echo $this->session->userdata('search_title');?></h5>
<?php
		$result = '<a href="'.site_url().'export-statements-of-accounts" target="_blank" class="btn btn-sm btn-success pull-right">Export Statement </a>
		<a href="'.site_url().'print-statements-of-accounts" target="_blank" class="btn btn-sm btn-warning pull-right" style="margin-left:5px;">Print Statement </a>';
		$search = $this->session->userdata('statements_search_query');
		if(!empty($search))
		{
			echo '<a href="'.site_url().'debtors/close_statement_reports_search" class="btn btn-sm btn-warning">Close Search</a>';
		}
		
		if(!empty($query))
		{
			//if users exist display them
			if ($query->num_rows() > 0)
			{
				$count = 0;
			
				$result .= 
					'
						<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
						  <thead>
							<tr>
								<th>Ref No.</th>
								<th>FILE No.</th>
								<th>Patient\'s Name.</th>
								<th>Scheme/Company </th>
								<th>Member No./Policy No.</th>
								<th>Ageing</th>
								<th>Invoice Date</th>
								<th>Invoice Number</th>
								<th>Invoice Amount</th>
								<th>Total Paid</th>
								<th>Invoice Balance</th>
							</tr>
						  </thead>
						  <tbody>
				';
				
				// $personnel_query = $this->accounting_model->get_all_personnel();
				$total_waiver = 0;
				$total_payments = 0;
				$total_invoice = 0;
				$total_balance = 0;
				$total_rejected_amount = 0;
				$total_cash_balance = 0;
				$total_insurance_payments =0;
				$total_insurance_invoice =0;
				$total_payable_by_patient = 0;
				$total_payable_by_insurance = 0;
				$total_debit_notes = 0;
				$total_credit_notes= 0;
				foreach ($query->result() as $row)
				{
					$total_invoiced = 0;
					$visit_date = date('d.m.y',strtotime($row->transaction_date));
					
					
					$visit_id = $row->visit_id;
					$patient_id = $row->patient_id;
					$personnel_id = $row->personnel_id;
					$dependant_id = $row->dependant_id;
					$patient_number = $row->patient_number;
					$visit_invoice_number = $row->visit_invoice_number;
					$scheme_name = $row->scheme_name;
					$member_number = $row->member_number;
					$preauth_date = $row->preauth_date;
					$authorising_officer = $row->authorising_officer;
					$visit_invoice_id = $row->visit_invoice_id;
					$branch_code = $row->branch_code;

					$visit_type_name = $row->payment_type_name;
					$patient_othernames = $row->patient_othernames;
					$patient_surname = $row->patient_surname;
					$patient_date_of_birth = $row->patient_date_of_birth;
					$patient_first_name = '';//$row->patient_first_name;
					$preauth_date = date('d.m.y',strtotime($row->preauth_date));
					$doctor = $row->personnel_fname;
					$count++;
					$invoice_total = $row->dr_amount;
					$payments_value = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
					$credit_note = $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);

					$invoice_total -= $credit_note;

					$balance  = $this->accounts_model->balance($payments_value,$invoice_total);


					$total_payable_by_patient += $invoice_total;
					$total_payments += $payments_value;
					$total_balance += $balance;

					$date = $row->transaction_date;
					// $date = $row->invoice_date;
									
					$ageing  = $this->debtors_model->get_aging_time($date);


					$result .= 
						'
								<tr>
									<td>'.$count.'</td>
									<td>'.$patient_number.'</td>
									<td>'.strtoupper(strtolower($patient_surname.' '.$patient_othernames.' '.$patient_first_name)).'</td>
									<td>'.$scheme_name.'</td>
									<td>'.$member_number.'</td>
									<td>'.$ageing.'</td>
									<td>'.$date.'</td>
									<td>'.$visit_invoice_number.'</td>
									<td>'.(number_format($invoice_total,2)).'</td>
									<td>'.(number_format($payments_value,2)).'</td>
									<td>'.(number_format($balance,2)).'</td>
								</tr> 
						';
					
				}

				$result .= 
						'
							<tr>
								<td colspan=8> Totals</td>
								<td><strong>'.number_format($total_payable_by_patient,2).'</strong></td>
								<td><strong>'.number_format($total_payments,2).'</strong></td>
								<td><strong>'.number_format($total_balance,2).'</strong></td>
							</tr> 
					';


				$result .= 
						'
							<tr>
								<td colspan=11> Unallocated Payments</td>
								
							</tr> 
						';

				$visit_type = $this->session->userdata('statements_visit_type');

				$this->db->where('batch_unallocations.batch_receipt_id = batch_receipts.batch_receipt_id AND batch_unallocations.unallocated_payment_delete = 0 AND batch_receipts.insurance_id = '.$visit_type);
				// $this->db->get('batch_unallocations.*,batch_receipts.*');
				$query = $this->db->get('batch_unallocations,batch_receipts');
				$visit_type_name = $this->session->userdata('statements_visit_type_name');

				if($query->num_rows() > 0)
				{
					$counting =0;
					foreach ($query->result() as $key => $value) {
						# code...

						$payment_date = $value->payment_date;
						$amount = $value->amount_paid;
						$payment_date = $value->payment_date;
						$receipt_number = $value->receipt_number;

						$payment_date = date('d.m.y',strtotime($value->payment_date));

						$total_balance -= $amount;
						$counting++;
						$result .= 
							'
									<tr>
										<td>'.$counting.'</td>
										<td></td>
										<td>Insurance - '.$visit_type_name.'</td>
										<td></td>
										<td>Payment</td>
										<td></td>
										<td></td>
										<td>'.$payment_date.'</td>
										<td>'.$receipt_number.'</td>
										<td>('.(number_format($amount,2)).')</td>
										<td>('.(number_format($amount,2)).')</td>
									</tr> 
							';
					}
					$result .= 
						'
							<tr>
								<td colspan=11></td>
								
							</tr> 
						';
					$result .= 
							'
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<th>Total</th>
										<th>'.(number_format($total_balance,2)).'</th>
										<th>'.(number_format($total_balance,2)).'</th>
									</tr> 
							';
				}
				
				$result .= 
				'
							  </tbody>
							</table>
				';
			}
			
			else
			{
				$result .= "There are no visits";
			}

		}
		else
		{
			$result = "There are no records found";
		}
		
		
		echo $result;
?>
          </div>
          <div class="row">

          	
          </div>
        
		</section>
    </div>
  </div>