<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/accounts/controllers/accounts.php";
error_reporting(0);



class Debtors extends accounts 
{
	var $document_upload_path;
	var $document_upload_location;
	function __construct()
	{
		parent:: __construct();
		
		$this->load->model('debtors/debtors_model');
		$this->load->model('reception/reception_model');
		$this->load->model('admin/file_model');

		$this->document_upload_path = realpath(APPPATH . '../assets/debtor_batches');
		$this->document_upload_location = base_url().'assets/debtor_batches/';
	}


	public function index()
	{

		
		$branch_code = $this->session->userdata('search_branch_code');
		
		if(empty($branch_code))
		{
			$branch_code = $this->session->userdata('branch_code');
		}
		
		$this->db->where('visit_type_id > 0');
		$query = $this->db->get('visit_type');
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$visit_type_name = $row->visit_type_name;
		}
		
		else
		{
			$visit_type_name = '';
		}
		$where = 'visit_type.visit_type_id >0 ';
		$search = $this->session->userdata('search_hospital_debtors');		
		$where .= $search;		
		$table = 'visit_type';
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounting/debtors-statements';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 40;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["page"] = $page;
         $v_data["visit_type_id"] = $visit_type_id;

         // var_dump($visit_type_id);die();
        $v_data["links"] = $this->pagination->create_links();
		$v_data['query'] = $this->debtors_model->get_all_debtors($table, $where, $config["per_page"], $page);
		$data['title'] = $v_data['title'] = 'Debtors';
		$data['content'] = $this->load->view('debtors/debtors/all_debtors', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function debtor_statement($debtor_id)
	{

		$where = 'visit_type_id = '.$debtor_id;
		$table = 'visit_type';
		
		
		// echo $where;die();
		// $v_data['balance_brought_forward'] = $this->creditors_model->calculate_balance_brought_forward($date_from,$creditor_id);
		// $creditor = $this->creditors_model->get_creditor($creditor_id);
		// $row = $creditor->row();
		// $creditor_name = $row->creditor_name;
		// $opening_balance = $row->opening_balance;
		// $debit_id = $row->debit_id;
		// // var_dump($opening_balance); die();
		// $v_data['module'] = 1;
		// $v_data['creditor_name'] = $creditor_name;
		
		// $v_data['creditor_id'] = $creditor_id;
		// $v_data['date_from'] = $date_from;
		// $v_data['date_to'] = $date_to;
		// $v_data['opening_balance'] = $opening_balance;
		// $v_data['debit_id'] = $debit_id;
		// $v_data['query'] = $this->creditors_model->get_creditor_account($where, $table);
		$v_data['title'] = 'Debtor ';
		$v_data['debtor_id'] = $debtor_id;
		$data['title'] = 'Statement';
		$data['content'] = $this->load->view('debtors/debtors/statement', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);

	}

	public function export_debtor_statement($visit_type_id,$start_date,$end_date)
	{
		$this->debtors_model->export_debtor_statement($visit_type_id,$start_date,$end_date);
	}

	

	public function all_debtors()
	{
		$module = NULL;
		
		$v_data['branch_name'] = $branch_name;
		
		// $where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0  AND visit.close_card <> 2 AND (visit.parent_visit = 0 OR visit.parent_visit IS NULL)';
		// $table = 'visit, patients, visit_type';


		$where = 'patients.patient_id = v_patient_account_balances.patient_id';
		$table = 'patients,v_patient_account_balances';

		$visit_search = $this->session->userdata('all_debtors_search_query');
		// var_dump($visit_search);die();
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		
			
			
		}
		else
		{
			$where .= '';

		}
		$segment = 3;
		
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounts/patients-accounts';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 30;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->debtors_model->get_all_visits_view($table, $where, $config["per_page"], $page, 'ASC');
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['search'] = $visit_search;
		$v_data['total_patients'] = $config['total_rows'];		
		
		$page_title = $this->session->userdata('page_title');
		if(empty($page_title))
		{
			$page_title = 'Debtors';
		}
		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['debtors'] = $this->session->userdata('debtors');
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		$v_data['total_visits'] = $config['total_rows'];
		
		$v_data['module'] = $module;
		
		$data['content'] = $this->load->view('accounts/all_debtors_view', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}


	public function search_all_debtors_report()
	{
		// alert("dasdhakjh");
		$patient_number = $this->input->post('patient_number');
		$patient_phone = $this->input->post('patient_phone');
		$patient_name = $this->input->post('patient_name');
		$this->session->set_userdata('search_branch_code', $branch_code);
		
		$search_title = 'Showing reports for: ';
		
		if(!empty($patient_phone))
		{

			$patient_phone = ' AND patients.patient_phone1 LIKE \'%'.$patient_phone.'%\' ';


		}
		
		if(!empty($patient_number))
		{
			$patient_number = ' AND patients.patient_number LIKE \'%'.$patient_number.'%\' ';
			
			$search_title .= 'Patient number. '.$patient_number;
		}
		
		
		
		$surname = '';

		//search surname
		if(!empty($_POST['patient_name']))
		{
			$search_title .= ' first name <strong>'.$_POST['patient_name'].'</strong>';
			$surnames = explode(" ",$_POST['patient_name']);
			$total = count($surnames);
			
			$count = 1;
			$surname = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$surname .= ' (patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\' OR patients.patient_othernames LIKE \'%'.addslashes($surnames[$r]).'%\')';
				}
				
				else
				{
					$surname .= ' (patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\' OR patients.patient_othernames LIKE \'%'.addslashes($surnames[$r]).'%\') AND ';
				}
				$count++;
			}
			$surname .= ') ';
		}
		
		else
		{
			$surname = '';
		}
		
		$search = $patient_number.$surname.$patient_phone;
		// var_dump($search); die();
		$this->session->set_userdata('all_debtors_search_query', $search);
		$this->session->set_userdata('search_title', $search_title);
		
		redirect('accounts/patients-accounts');
	}

	public function close_all_reports_search()
	{
		$this->session->unset_userdata('all_debtors_search_query');

		// redirect('hospital-reports/debtors');
		redirect('accounts/patients-accounts');
	}
	public function export_debtors()
	{
		$this->debtors_model->export_debtors();
	}






	// debtors accounts

	public function debtors_accounts()
	{
		$branch_code = $this->session->userdata('search_branch_code');
		
		if(empty($branch_code))
		{
			$branch_code = $this->session->userdata('branch_code');
		}
		
		$this->db->where('visit_type_id > 0');
		$query = $this->db->get('visit_type');
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$visit_type_name = $row->visit_type_name;
		}
		
		else
		{
			$visit_type_name = '';
		}
		$where = 'visit_type.visit_type_id > 0 ';
		$search = $this->session->userdata('search_hospital_debtors');		
		$where .= $search;		
		$table = 'visit_type';
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounts/debtors-accounts';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 40;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["page"] = $page;
        $v_data["links"] = $this->pagination->create_links();
		$v_data['query'] = $this->debtors_model->get_all_debtors($table, $where, $config["per_page"], $page);
		$data['title'] = $v_data['title'] = 'Debtors';
		$data['content'] = $this->load->view('debtors/debtors/debtors_accounts', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function debtor_batches($debtor_id)
	{

		$where = 'visit_type_id = '.$debtor_id;
		$table = 'visit_type';
		
		

		// $v_data['creditor_id'] = $creditor_id;
		// $v_data['date_from'] = $date_from;
		// $v_data['date_to'] = $date_to;
		// $v_data['opening_balance'] = $opening_balance;
		// $v_data['debit_id'] = $debit_id;
		// $v_data['query'] = $this->creditors_model->get_creditor_account($where, $table);
		$v_data['title'] = 'Debtor ';
		$v_data['debtor_id'] = $debtor_id; 
		$data['title'] = 'Statement';
		$data['content'] = $this->load->view('debtors/debtors/batches', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);

	}

	public function calculate_billed_items()
	{
		$billed_items = $this->input->post('billed');
		

		$billed_items = str_replace(' ', '', $billed_items);
		$split = explode(",", $billed_items);
		$total_count = count($split);
		$total_amount = 0;
		if(is_array($split))
		{
			if($total_count > 0)
			{
				for ($i=0; $i <= $total_count; $i++) { 
					# code...

					$total_amount += $split[$i];


				}
			}
		}

		// var_dump($total_amount);die();
	
		$response['message'] ='success';
		$response['billing'] = $total_amount;
		echo json_encode($response);
	}


	public function clear_session_search_filter()
	{
		$this->session->unset_userdata('date_from');
		$this->session->unset_userdata('date_from');

		echo "success";

	}

	public function get_unallocated_invoices_data($id)
	{
		//var_dump($id);
		//die();

		$creditor_result = $this->debtors_model->get_unallocated_invoices($id);
		//var_dump($creditor_result);
		//die();
		// if($creditor_result->num_rows() > 0)
		// {
		// 	echo $creditor_result->result();
		// 	//var_dump($creditor_result->result());
		// 	//die();
		// }
		$data['creditor_result'] = $creditor_result;
		$data['debtor_id'] = $id;

		$page = $this->load->view('debtors/debtors/unbatched_invoices',$data,true);
		echo $page;
	}

	public function create_debtor_batch($visit_type_id)
	{
		if($this->debtors_model->create_debtor_batch($visit_type_id))
		{ 
			$this->session->set_userdata('success_message', 'Batch has been successfully created');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Sorry could not create batch. Please try again');
		}

		redirect('accounts/debtors-batches/'.$visit_type_id);
	}

	public function approve_batch($debtor_batch_id,$visit_type_id)
	{
		$array['batch_status'] = 2;
		$array['date_approved'] = date('Y-m-d');
		$array['approved_by'] = $this->session->userdata('personnel_id');

		$this->db->where('debtor_batch_id',$debtor_batch_id);
		if($this->db->update('debtor_batches',$array))
		{
			$this->session->set_userdata('success_message', 'Batch has been successfully approved');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Sorry could not approve batch. Please try again');
		}

		redirect('accounts/debtors-batches/'.$visit_type_id);

	}
	public function view_dispatch_invoices($debtor_batch_id)
	{
		$data['debtor_batch_id'] = $debtor_batch_id;
		$where = 'debtor_batches.visit_type_id = visit_type.visit_type_id AND debtor_batch_id = '.$debtor_batch_id;
		$table = 'debtor_batches,visit_type';

		$this->db->where($where);
		$this->db->select('visit_type.visit_type_name,debtor_batches.*');
		$query = $this->db->get($table);
		

		$data['query'] = $query;
		$data['debtor_batch_id'] = $debtor_batch_id;
		
		$page = $this->load->view('debtors/debtors/view_dispatch_invoices',$data,true);
		// var_dump($page);die();
		echo $page;
	}
	public function print_batch($debtor_batch_id)
	{
		$where = 'debtor_batches.visit_type_id = visit_type.visit_type_id AND debtor_batch_id = '.$debtor_batch_id;
		$table = 'debtor_batches,visit_type';

		$this->db->where($where);
		$this->db->select('visit_type.visit_type_name,debtor_batches.*');
		$query = $this->db->get($table);
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$visit_type_name = $value->visit_type_name;
				$batch_date_from = $value->batch_date_from;
				$batch_date_to = $value->batch_date_to;
				$debtor_id = $value->visit_type_id;
			}
		}
		$v_data['contacts'] = $this->site_model->get_contacts();
		$v_data['title'] = $visit_type_name.' Batch '.$batch_date_from.' '.$batch_date_to;
		$v_data['debtor_id'] = $debtor_id;
		$v_data['query'] = $query;
		$v_data['debtor_batch_id'] = $debtor_batch_id;
		$data['title'] = $v_data['title'];
		echo $this->load->view('debtors/debtors/print_debtor_batch', $v_data);
		
		// $this->load->view('admin/templates/general_page', $data);
	}






	// remittance reconcilliations


	function import_payments()
	{


		$where = 'account.account_id = batch_receipts.bank_id AND visit_type.visit_type_id = batch_receipts.insurance_id';


		$search_item = $this->session->userdata('batch_payments_search');

		if(!empty($search_item))
		{
			$where .= $search_item;
		}

		// var_dump($where);die();
		$table = 'batch_receipts,account,visit_type';
		//pagination
		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounts/remittance-reconcilliations';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->debtors_model->get_current_payments($table, $where, $config["per_page"], $page);
		
		//change of order method 
		
		
		$data['title'] = 'Payments';		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		// var_dump($query);die();
		//open the add new product
		$v_data['title'] = 'Import payments';
		$data['title'] = 'Import payments';
		$data['content'] = $this->load->view('remittances/payments', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	
	function do_payment_import()
	{
		$data_where = array(
							'receipt_number' => $this->input->post('receipt_number'),
							'payment_date'=>$this->input->post('payment_date'),
							'payment_method_id'=>$payment_method_id,
							'bank_id'=>$this->input->post('bank_id'),
							'insurance_id'=>$this->input->post('insurance_id'),
							'total_amount_paid'=>$this->input->post('total_amount_paid')
						);

		
		$this->db->where($data_where);
		$query = $this->db->get('batch_receipts');
		// var_dump($query); die();
		if($query->num_rows() === 1)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$batch_receipt_id = $value->batch_receipt_id;
			}
		}
		else
		{

			$data_checked = array(
									'receipt_number' => $this->input->post('receipt_number'),
									'payment_date'=>$this->input->post('payment_date'),
									'payment_method_id'=>$payment_method_id,
									'bank_id'=>$this->input->post('bank_id'),
									'insurance_id'=>$this->input->post('insurance_id'),
									'total_amount_paid'=>$this->input->post('total_amount_paid')
								  );

			// var_dump($data); die();
			$this->db->insert('batch_receipts',$data_checked);
			$batch_receipt_id = $this->db->insert_id();



		}
		redirect('accounts/remittance-reconcilliations');
	}

	public function search_batch_payments()
	{

		$payment_date_from = $this->input->post('payment_date_from');
		$payment_date_to = $this->input->post('payment_date_to');
		$receipt_number = $this->input->post('receipt_number');
		$bank_id = $this->input->post('bank_id');

		$search_title = '';
		if(!empty($bank_id))
		{
			$bank = ' AND bank_id = '.$bank_id;
		}
		else
		{
			$bank = '';
		}

		if(!empty($insurance_id))
		{
			$insurance = ' AND insurance_id = '.$insurance_id;
		}
		else
		{
			$insurance = '';
		}


		if(!empty($receipt_number))
		{
			$receipt = ' AND receipt_number LIKE \'%'.$receipt_number.'%\'';
		}
		else
		{
			$receipt = '';
		}


		if(!empty($payment_date_from) && !empty($payment_date_to))
		{
			$dates = ' AND DATE(payment_date) BETWEEN \''.$payment_date_from.'\' AND \''.$payment_date_to.'\'';
			$search_title .= 'Payments from '.date('jS M Y', strtotime($payment_date_from)).' to '.date('jS M Y', strtotime($payment_date_to)).' ';
		}
		
		else if(!empty($payment_date_from))
		{
			$dates = ' AND DATE(payment_date) = \''.$payment_date_from.'\'';
			$search_title .= 'Payments of '.date('jS M Y', strtotime($payment_date_from)).' ';
		}
		
		else if(!empty($payment_date_to))
		{
			$dates = ' AND DATE(payment_date) = \''.$payment_date_to.'\'';
			$search_title .= 'Payments of '.date('jS M Y', strtotime($payment_date_to)).' ';
		}
		
		else
		{
			$dates = '';
		}



		$search = $dates.$insurance.$bank.$receipt;

		$this->session->set_userdata('batch_payments_search',$search);
		$this->session->set_userdata('batch_payment_search_title', $search_title);
		
		redirect('accounts/remittance-reconcilliations');



	}

	public function close_batch_search()
	{

		$this->session->unset_userdata('batch_payments_search');
		$this->session->unset_userdata('batch_payment_search_title');
		
		redirect('accounts/remittance-reconcilliations');
	}

	public function calculate_billed_items_amount()
	{
		$billed_items = $this->input->post('billed');
		$batch_receipt_id = $this->input->post('batch_receipt_id');
		$total_paid = $this->input->post('total_paid');

		$billed_items = str_replace(' ', '', $billed_items);
		$split = explode(",", $billed_items);
		$total_count = count($split);
		$total_amount = 0;
		if(is_array($split))
		{
			if($total_count > 0)
			{
				for ($i=0; $i < $total_count; $i++) { 
					# code...
					$total_amount += $split[$i];
				}
			}
		}

		$total_unallocated = $this->debtors_model->get_all_unallocated_payments($batch_receipt_id);
		$total_amount += $total_unallocated;
		$balance = $total_paid - $total_amount;
		$response['message'] ='success';
		$response['billing'] = $total_amount;
		$response['balance'] = $balance;
		echo json_encode($response);
	}

	public function confirm_payments($batch_receipt_id)
	{
		if($this->debtors_model->confirm_batch_payments($batch_receipt_id))
		{

			$array_update['current_payment_status'] = 1;
			$this->db->where('batch_receipt_id',$batch_receipt_id);
			$this->db->update('batch_receipts',$array_update);


			$this->session->set_userdata('success_message', 'Remittance successfully added');
			redirect('accounts/remittance-reconcilliations');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Sorry could not update remittance. Please try again');
			redirect('accounts/remittance-reconcilliations');
		}

		
	}
	public function add_unallocated_payment_view($batch_receipt_id)
	{
		$v_data['batch_receipt_id']   = $batch_receipt_id;

		$page = $this->load->view('remittances/unallocated_payment_view',$v_data);

		echo $page;


	}
	public function add_unallocated_payment()
	{
		$array['amount_paid'] = $this->input->post('amount_paid_unreconcilled');
		$array['invoice_number'] = $this->input->post('invoice_referenced');
		$array['reason'] = $this->input->post('payment_description');
		$array['batch_receipt_id'] = $this->input->post('batch_receipt_id');

		if($this->db->insert('batch_unallocations',$array))
		{
			$response['message'] ='success';
			$response['result'] = 'You have successfully added this payment item';
		}
		else
		{
			$response['message'] ='success';
			$response['result'] = 'Sorry could not add this payment';
		}

		echo json_encode($response);
	}

	public function get_unreconcilled_payments($batch_receipt_id)
	{
		$v_data['batch_receipt_id']   = $batch_receipt_id;

		$response['message'] = 'success';
		$response['results'] = $this->load->view('remittances/unallocated_payments',$v_data,true);

		echo json_encode($response);
	}
	public function delete_unallocated_payment($unallocated_payment_id)
	{
		$array_update['current_payment_status'] = 0;
		$this->db->where('batch_receipt_id',$batch_receipt_id);
		$this->db->update('batch_receipts',$array_update);


		$update['unallocated_payment_delete'] = 1;
		$this->db->where('unallocated_payment_id',$unallocated_payment_id);
		if($this->db->update('batch_unallocations',$update))
		{
			$response['message'] = 'success';
			$response['results'] = 'You successfully removed the item';

		}
		else
		{
			$response['message'] = 'fail';
			$response['results'] = 'Sorry could not remove the item. Please try again';
		}

		echo json_encode($response);
	}


	public function update_visit_invoice($visit_invoice_id)
	{
		// $update_array['visit_invoice_id'] = $visit_invoice_id;
		$array_update['current_payment_status'] = 1;
		$this->db->where('batch_receipt_id',$batch_receipt_id);
		$this->db->update('batch_receipts',$array_update);


		$this->db->where('batch_receipt_id > 0 AND visit_invoice_id ='.$visit_invoice_id);
		$query = $this->db->get('visit_invoice');


		if($query->num_rows() > 0)
		{
		


			$update_array['batch_receipt_id'] = 0;
			$update_array['amount_to_pay'] = 0;
			$this->db->where('visit_invoice_id',$visit_invoice_id);
			if($this->db->update('visit_invoice',$update_array))
			{
				$response['message'] = 'success';
				$response['results'] = 'You successfully removed the item';

			}
			else
			{
				$response['message'] = 'fail';
				$response['results'] = 'Sorry could not remove the item. Please try again';
			}

		}
		else
		{
			$update_array['batch_receipt_id'] = $this->input->post('batch_receipt_id');
			$update_array['amount_to_pay'] = $this->input->post('amount_payable');

			$this->db->where('visit_invoice_id',$visit_invoice_id);

			if($this->db->update('visit_invoice',$update_array))
			{
				$response['message'] = 'success';
				$response['results'] = 'You successfully removed the item';

			}
			else
			{
				$response['message'] = 'fail';
				$response['results'] = 'Sorry could not remove the item. Please try again';
			}
		}



		

		echo json_encode($response);
	}

	public function view_batch_items($batch_receipt_id,$insurance_id,$location=NULL)
	{

		
		
		$data['title'] = 'Payments';		
		$v_data['batch_receipt_id'] = $batch_receipt_id;
		$v_data['insurance_id'] = $insurance_id;

		if($location == 1)
		{

		}
		else
		{
			$update_array['batch_receipt_id'] = 0;
			$update_array['amount_to_pay'] = 0;
			$this->db->where('visit_invoice_status <> 1 AND  bill_to = '.$insurance_id);
			$this->db->update('visit_invoice',$update_array);
		}
		// var_dump($query);die();
		//open the add new product
		$v_data['title'] = 'Batch Items';
		$data['title'] = 'Batch Items';
		$data['content'] = $this->load->view('remittances/view_batch_items', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);

	}


	public function unsettled_invocies($insurance_id,$batch_receipt_id)
	{
		$v_data['insurance_id'] = $insurance_id;
		$v_data['batch_receipt_id'] = $batch_receipt_id;
		$v_data['title'] = 'Batch Items';
		$data['title'] = 'Batch Items';
		$data['message']='success';
		$data['result'] = $this->load->view('remittances/unsettled_invocies', $v_data,true);
		
		echo json_encode($data);
	}

	public function picked_invocies($insurance_id,$batch_receipt_id)
	{
		$v_data['insurance_id'] = $insurance_id;
		$v_data['batch_receipt_id'] = $batch_receipt_id;
		$v_data['title'] = 'Batch Items';
		$data['title'] = 'Batch Items';
		$data['message']='success';
		$data['result'] = $this->load->view('remittances/selected_invocies', $v_data,true);
		
		echo json_encode($data);
	}

	public function get_total_amount_reconcilled($batch_receipt_id)
	{

		// check if the batch is closed 
		$this->db->from('payments,payment_item,batch_receipts');
		$this->db->select('SUM(payment_item.payment_item_amount) AS total_amount_paid');
		$this->db->where('batch_receipts.batch_receipt_id = payments.batch_receipt_id AND payments.cancel = 0 AND payments.payment_id = payment_item.payment_id AND batch_receipts.current_payment_status = 1 AND payments.batch_receipt_id = '.$batch_receipt_id);
		$query = $this->db->get();  
		$total_amount = 0;

		// var_dump($query->result());die();
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount_paid;
			}

			if(empty($total_amount))
			{
				$this->db->where('batch_receipt_id',$batch_receipt_id);
				$this->db->select('SUM(visit_invoice.amount_to_pay) AS total_amount');
				$query=$this->db->get('visit_invoice');

				$total_amount = 0;
				if($query->num_rows() > 0)
				{
					foreach ($query->result() as $key => $value) {
						# code...
						$total_amount = $value->total_amount;
					}
				}

				if(empty($total_amount))
				{
					$total_amount = 0;
				}

			}


		}
		else
		{
			$this->db->where('batch_receipt_id',$batch_receipt_id);
			$this->db->select('SUM(visit_invoice.amount_to_pay) AS total_amount');
			$query=$this->db->get('visit_invoice');

			$total_amount = 0;
			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value) {
					# code...
					$total_amount = $value->total_amount;
				}
			}

			if(empty($total_amount))
			{
				$total_amount = 0;
			}


		}

		
		
		$amount_unallocated = $this->debtors_model->get_all_unallocated_payments($batch_receipt_id);

		$total_paid = $this->input->post('total_paid');
		$balance = $total_paid - ($total_amount +$amount_unallocated);
	
		$result['billing'] = $total_amount+$amount_unallocated;
		$result['balance'] = $balance;
		$result['message'] = 'success';


		echo json_encode($result);
	}
	public function update_amount_to_pay($visit_invoice_id,$batch_receipt_id)
	{




		$update_array['batch_receipt_id'] = $batch_receipt_id;
		$update_array['amount_to_pay'] = $this->input->post('billed');

		$this->db->where('visit_invoice_id',$visit_invoice_id);

		if($this->db->update('visit_invoice',$update_array))
		{
			$response['message'] = 'success';
			$response['results'] = 'You successfully removed the item';

		}
		else
		{
			$response['message'] = 'fail';
			$response['results'] = 'Sorry could not remove the item. Please try again';
		}

		echo json_encode($response);
	}

	public function paid_invoices($insurance_id,$batch_receipt_id)
	{
		$v_data['insurance_id'] = $insurance_id;
		$v_data['batch_receipt_id'] = $batch_receipt_id;
		$v_data['title'] = 'Batch Items';
		$data['title'] = 'Batch Items';
		$data['message']='success';
		$data['result'] = $this->load->view('remittances/paid_invoices', $v_data,true);
		
		echo json_encode($data);
	}

	public function open_debtor_payment($insurance_id,$batch_receipt_id)
	{


		// get all payments
		$where = 'payments.batch_receipt_id = '.$batch_receipt_id.' AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id AND payments.payment_id = payment_item.payment_id AND visit_invoice.bill_to = '.$insurance_id;
        
        $table = 'visit_invoice,payment_item,payments';


		$this->db->where($where);
		$query = $this->db->get($table);

		// var_dump($query->result());die();

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$visit_invoice_id = $value->visit_invoice_id;
				$payment_item_amount = $value->payment_item_amount;
				$payment_id = $value->payment_id;
				


				// cancel the payments

				$payment_array['cancel'] = 1;
				$payment_array['cancelled_by'] = $this->session->userdata('personnel_id');

				$this->db->where('payment_id',$payment_id);
				$this->db->update('payments',$payment_array);



				// set status of the visit invoice as not cleared

				$visit_invoice_array['visit_invoice_status'] = 0;
				$visit_invoice_array['batch_receipt_id'] = $batch_receipt_id;
				$visit_invoice_array['amount_to_pay'] = $payment_item_amount;

				$this->db->where('visit_invoice_id',$visit_invoice_id);
				$this->db->update('visit_invoice',$visit_invoice_array);
			}

			$array_update['current_payment_status'] = 0;
			$this->db->where('batch_receipt_id',$batch_receipt_id);
			$this->db->update('batch_receipts',$array_update);
		}
		

		redirect('debtors/view_batch_items/'.$batch_receipt_id.'/'.$insurance_id.'/1');

	}

	public function update_opening_balance($visit_type_id)
	{

		// var_dump($_POST); die();
		$this->form_validation->set_rules('start_date', 'Description', 'trim|required|xss_clean');
		$this->form_validation->set_rules('opening_balance', 'Amount', 'trim|required|xss_clean');
		
		if ($this->form_validation->run())
		{
			if($this->debtors_model->update_debtor_account($visit_type_id))
			{
				$this->session->set_userdata('success_message', 'Updated provider account successfully');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Unable to update. Please try again');
			}
		}
		
		else
		{
			$this->session->set_userdata('error_message', validation_errors());
		}
		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}



	public function update_debtor_batch_date($visit_type_id)
	{

		// var_dump($_POST); die();
		$this->form_validation->set_rules('start_date', 'Description', 'trim|required|xss_clean');
		
		if ($this->form_validation->run())
		{
			if($this->debtors_model->update_debtor_batch_date($visit_type_id))
			{
				$this->session->set_userdata('success_message', 'Updated provider account successfully');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Unable to update. Please try again');
			}
		}
		
		else
		{
			$this->session->set_userdata('error_message', validation_errors());
		}
		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}

	public function close_batch($debtor_batch_id)
	{
		$image_error = '';
		$this->session->unset_userdata('upload_error_message');
		$document_name = 'document_scan';
		
		//upload image if it has been selected
		$response = $this->debtors_model->upload_any_file($this->document_upload_path, $this->document_upload_location, $document_name, 'document_scan');
		if($response)
		{
			$document_upload_location = $this->document_upload_location.$this->session->userdata($document_name);
		}
		
		//case of upload error
		else
		{
			$image_error = $this->session->userdata('upload_error_message');
			$this->session->unset_userdata('upload_error_message');
		}

		$document = $this->session->userdata($document_name);
		
		$this->form_validation->set_rules('received_date', 'Date Received', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->debtors_model->upload_batched_documents($document,$debtor_batch_id))
			{
				$this->session->set_userdata('success_message', 'Document uploaded successfully');
				$this->session->unset_userdata($document_name);

				$response2['message'] = 'success';
				$response2['result'] = 'You have successfully uploaded the result';
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not upload document. Please try again');
				$response2['message'] = 'fail';
				$response2['result'] = 'Sorry could not upload the document. Please try again';
			}
		}
		else
		{
			$this->session->set_userdata('error_message', 'Could not upload document. Please try again');
			$response2['message'] = 'fail';
			$response2['result'] = 'Sorry could not upload the document. Please try again';
		}
		
		$debtor_id = $this->input->post('debtor_id');
		
		redirect('accounts/debtors-batches/'.$debtor_batch_id);

		// echo json_encode($response2);
	}



	// statements of accounts 


	public function statements_of_accounts()
	{
		$module = NULL;
		
		
		$where = 'v_transactions_by_date.transactionCategory = "Revenue" AND visit_invoice.visit_invoice_id = v_transactions_by_date.transaction_id AND visit_invoice.visit_invoice_status <> 1 AND visit_invoice.created >= "2018-03-01"';
		
		$table = 'v_transactions_by_date,visit_invoice';


		$visit_search = $this->session->userdata('statements_search_query');
		// var_dump($visit_search);die();
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		
			$query = $this->debtors_model->get_statements_of_accounts($table, $where,1);

			// var_dump($query);die();
			
		}
		else
		{
			$where .= ' AND v_transactions_by_date.transaction_date = "'.date('Y-m-d').'" ';
		
			$visit_payments = ' AND v_transactions_by_date.transaction_date = \''.date('Y-m-d').'\'';
			$visit_invoices = ' AND v_transactions_by_date.transaction_date = \''.date('Y-m-d').'\'';
			$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

			$this->session->set_userdata('visit_invoices', $visit_invoices);
			// $this->session->set_userdata('debtors_search_query', $visit_invoices);
			$this->session->set_userdata('visit_payments', $visit_payments);
		

			$query = NULL;

		}

	


		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = 'Statements of accounts';
		$v_data['debtors'] = $this->session->userdata('debtors');
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		// $v_data['branches'] = $this->reports_model->get_all_active_branches();
		$v_data['query'] = $query;
		
		$v_data['module'] = $module;
		
		$data['content'] = $this->load->view('reports/debtors', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_statements_of_accounts()
	{
		$visit_type_id = $visit_type_idd = $this->input->post('visit_type_id');
		$branch_id = $branch_idd= $this->input->post('branch_id');
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');

		$this->session->set_userdata('search_branch_code', $branch_code);
		
		$search_title = 'Showing reports for: ';
		
		if(!empty($visit_type_id))
		{
			$visit_type = $visit_type_id;
			$visit_type_id = ' AND v_transactions_by_date.payment_type = '.$visit_type_id.' ';


			
			$this->db->where('visit_type_id', $visit_type_idd);
			$query = $this->db->get('visit_type');
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->visit_type_name.' ';

				$visit_type_name =$row->visit_type_name;
			}
		}
		
	
		//date filter for cash report
		$prev_search = '';
		$prev_table = '';
		
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_payments = ' AND v_transactions_by_date.transaction_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$visit_invoices = ' AND v_transactions_by_date.transaction_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$search_title .= 'Visit date from '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit_payments = ' AND v_transactions_by_date.transaction_date = \''.$visit_date_from.'\'';
			$visit_invoices = ' AND v_transactions_by_date.transaction_date = \''.$visit_date_from.'\'';
			$search_title .= 'Visit date of '.date('jS M Y', strtotime($visit_date_from)).' ';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_payments = ' AND v_transactions_by_date.transaction_date = \''.$visit_date_to.'\'';
			$visit_invoices = ' AND v_transactions_by_date.transaction_date = \''.$visit_date_to.'\'';
			$search_title .= 'Visit date of '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else
		{
			$visit_payments = '';
			$visit_invoices = '';
		}

		$surname = '';

		
		
		$search = $visit_type_id.$visit_invoices;
		
		$this->session->set_userdata('statements_search_query', $search);
		$this->session->set_userdata('statements_visit_invoices', $visit_invoices);
		$this->session->set_userdata('statements_visit_payments', $visit_payments);
		$this->session->set_userdata('statements_visit_type_id', $visit_type_id);
		$this->session->set_userdata('statements_visit_type', $visit_type);
		$this->session->set_userdata('statements_patient_number', $patient_number);
		$this->session->set_userdata('statements_search_title', $search_title);
		$this->session->set_userdata('statements_visit_type', $visit_type);
		$this->session->set_userdata('statements_visit_type_name', $visit_type_name);

		
		
		redirect('accounts/statements-of-accounts');
	}

	public function close_statement_reports_search()
	{
		$this->session->unset_userdata('statements_search_query');
		$this->session->unset_userdata('statements_visit_invoices');
		$this->session->unset_userdata('statements_visit_payments');
		$this->session->unset_userdata('statements_visit_type_id');
		$this->session->unset_userdata('statements_visit_type');
		$this->session->unset_userdata('statements_patient_number');
		$this->session->unset_userdata('statements_search_title');
		$this->session->unset_userdata('statements_visit_type');
		$this->session->unset_userdata('statements_visit_type_name');

		redirect('accounts/statements-of-accounts');
	}

	public function export_statements_of_accounts()
	{
		$this->debtors_model->export_statements_of_accounts();
	}

	public function print_statements_of_accounts()
	{


		$module = NULL;
		
		
		$where = 'v_transactions_by_date.transactionCategory = "Revenue" AND visit_invoice.visit_invoice_id = v_transactions_by_date.transaction_id AND visit_invoice.visit_invoice_status <> 1 AND visit_invoice.created >= "2018-03-01"';
		
		$table = 'v_transactions_by_date,visit_invoice';


		$visit_search = $this->session->userdata('statements_search_query');
		// var_dump($visit_search);die();
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		
			$query = $this->debtors_model->get_statements_of_accounts($table, $where,1);

			// var_dump($query);die();
			
		}
		else
		{
			$where .= ' AND v_transactions_by_date.transaction_date = "'.date('Y-m-d').'" ';
		
			$visit_payments = ' AND v_transactions_by_date.transaction_date = \''.date('Y-m-d').'\'';
			$visit_invoices = ' AND v_transactions_by_date.transaction_date = \''.date('Y-m-d').'\'';
			$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

			$this->session->set_userdata('visit_invoices', $visit_invoices);
			// $this->session->set_userdata('debtors_search_query', $visit_invoices);
			$this->session->set_userdata('visit_payments', $visit_payments);
		

			$query = NULL;

		}

	
		$v_data['contacts'] = $this->site_model->get_contacts();

		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = 'Statements of accounts';
		$v_data['debtors'] = $this->session->userdata('debtors');
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		// $v_data['branches'] = $this->reports_model->get_all_active_branches();
		$v_data['query'] = $query;
		
		$v_data['module'] = $module;
		
		$this->load->view('debtors/reports/print_statement_of_accounts', $v_data);
		
	}


}
?>