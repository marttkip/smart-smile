<?php

class Hospital_administration_model extends CI_Model 
{	

	function import_invoice_template()
	{
		$this->load->library('Excel');
		
		$title = 'Invoices Import Template';
		$count=1;
		$row_count=0;
		
		$report[$row_count][0] = 'Visit ID';
		$report[$row_count][1] = 'Invoice Number';
		$report[$row_count][2] = 'Patient No';
		$report[$row_count][3] = 'Invoice Date';
		$report[$row_count][4] = 'Invoiced amount';
		
		$row_count++;
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}

	public function import_csv_invoices($upload_path)
	{
		//load the file model
		$this->load->model('admin/file_model');
		/*
			-----------------------------------------------------------------------------------------
			Upload csv
			-----------------------------------------------------------------------------------------
		*/
		$response = $this->file_model->upload_csv($upload_path, 'import_csv');
		
		if($response['check'])
		{
			$file_name = $response['file_name'];
			
			$array = $this->file_model->get_array_from_csv($upload_path.'/'.$file_name);
			//var_dump($array); die();
			$response2 = $this->sort_csv_invoices_data($array);
		
			if($this->file_model->delete_file($upload_path."\\".$file_name, $upload_path))
			{
			}
			
			return $response2;
		}
		
		else
		{
			$this->session->set_userdata('error_message', $response['error']);
			return FALSE;
		}
	}
	public function sort_csv_invoices_data($array)
	{
		//count total rows
		$total_rows = count($array);
		$total_columns = count($array[0]);//var_dump($array);die();
		
		//if products exist in array
		if(($total_rows > 0) && ($total_columns == 5))
		{
			$count = 0;
			$comment = '';
			$items['modified_by'] = $this->session->userdata('personnel_id');
			$response = '
				<table class="table table-hover table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Charge name</th>
						  <th>Amount</th>
						</tr>
					  </thead>
					  <tbody>
			';
			
				//retrieve the data from array
				for($r = 1; $r < $total_rows; $r++)
				{
					$visit_id = $array[$r][0];
					$invoice_number = $array[$r][1];
					$patient_id = $array[$r][2];
					$visit_date = $array[$r][3];
					$amount_invoiced = $array[$r][4];



					$visit_data = array(
										"branch_code" => $this->session->userdata('branch_code'),
										"visit_date" => $visit_date,
										"patient_id" => $patient_id,
										"personnel_id" => $this->session->userdata('personell_id'),
										"insurance_limit" => '',
										"patient_insurance_number" => '',
										"visit_type" => 1,
										"time_start"=>date('H:i:s'),
										"time_end"=>date('H:i:s'),
										"appointment_id"=>0,
										"close_card"=>0,
										"procedure_done"=>'',
										"insurance_description"=>'',
										"visit_id"=>$visit_id
										//"room_id"=>$room_id,
									);
					if($this->db->insert('visit', $visit_data))
					{
						// insert into the visit charge table
			
						
						$visit_charge_data = array(
							"visit_id" => $visit_id,
							"service_charge_id" => 1,
							"created_by" => $this->session->userdata("personnel_id"),
							"date" => $visit_date,
							"visit_charge_amount" => $amount_invoiced,
							"charged"=>1,
							"visit_charge_delete"=>0,
							"visit_charge_units"=>1,
							"visit_charge_qty"=>1
						);
						if($this->db->insert('visit_charge', $visit_charge_data))
						{

						}
					}
					$count++;
					$response .= '
									<tr class="">
										<td>'.$count.'</td>
										<td>'.$invoice_number.'</td>
										<td>'.$patient_id.'</td>
										<td>'.$visit_date.'</td>
										<td>'.$amount_invoiced.'</td>
									</tr> 
							';
				
				$response .= '</table>';
				
				$return['response'] = $response;
				$return['check'] = TRUE;
			}
		}
		
		//if no products exist
		else
		{
			$return['response'] = 'Charges data not found';
			$return['check'] = FALSE;
		}
		
		return $return;
	}


	// payments
	function import_payment_template()
	{
		$this->load->library('Excel');
		
		$title = 'payments Import Template';
		$count=1;
		$row_count=0;
		
		$report[$row_count][0] = 'Receipt No';
		$report[$row_count][1] = 'Invoice number';
		$report[$row_count][2] = 'Payment Date';
		$report[$row_count][3] = 'Payed By';
		$report[$row_count][4] = 'payment type';
		$report[$row_count][5] = 'payment amount';
		
		$row_count++;
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}

	public function import_csv_payments($upload_path)
	{
		//load the file model
		$this->load->model('admin/file_model');
		/*
			-----------------------------------------------------------------------------------------
			Upload csv
			-----------------------------------------------------------------------------------------
		*/
		$response = $this->file_model->upload_csv($upload_path, 'import_csv');
		
		if($response['check'])
		{
			$file_name = $response['file_name'];
			
			$array = $this->file_model->get_array_from_csv($upload_path.'/'.$file_name);
			//var_dump($array); die();
			$response2 = $this->sort_csv_payments_data($array);
		
			if($this->file_model->delete_file($upload_path."\\".$file_name, $upload_path))
			{
			}
			
			return $response2;
		}
		
		else
		{
			$this->session->set_userdata('error_message', $response['error']);
			return FALSE;
		}
	}
	public function sort_csv_payments_data($array)
	{
		//count total rows
		$total_rows = count($array);
		$total_columns = count($array[0]);//var_dump($array);die();
		
		//if products exist in array
		if(($total_rows > 0) && ($total_columns == 6))
		{
			$count = 0;
			$comment = '';
			$items['modified_by'] = $this->session->userdata('personnel_id');
			$response = '
				<table class="table table-hover table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Charge name</th>
						  <th>Amount</th>
						</tr>
					  </thead>
					  <tbody>
			';
			
				//retrieve the data from array
				for($r = 1; $r < $total_rows; $r++)
				{
					$receipt_number = $array[$r][0];
					$visit_id = $array[$r][1];
					$payment_date = $array[$r][2];
					$payed_by = $array[$r][3];
					$payment_type = $array[$r][4];
					$amount_paid = $array[$r][5];

					if($payment_type == 1)
					{
						$payment_method = 1;
					}
					else
					{
						$payment_method = 9;
					}

					$type_payment = 1;
					$transaction_code = $receipt_number;
					$payment_service_id = 3;
					$change = 0;



					$this->db->where('visit_id = '.$visit_id.' AND amount_paid = "'.$amount_paid.'" AND payment_created = "'.$payment_date.'" ');
					$query_amount = $this->db->get('payments');
					if($query_amount->num_rows() == 0)
					{
						$data = array(
							'visit_id' => $visit_id,
							'payment_method_id'=>$payment_method,
							'amount_paid'=>$amount_paid,
							'personnel_id'=>$this->session->userdata("personnel_id"),
							'payment_type'=>$type_payment,
							'transaction_code'=>$transaction_code,
							'payment_service_id'=>$payment_service_id,
							'change'=>$change,
							'payment_created'=>$payment_date,
							'payed_by'=>$payed_by,
							'payment_created_by'=>$this->session->userdata("personnel_id"),
							'approved_by'=>$this->session->userdata("personnel_id"),
							'date_approved'=>$payment_date,
							'is_new'=>1
						);

						$this->db->insert('payments', $data);
					}
					else
					{
						$array_gift['is_new'] = 2;
						$this->db->where('visit_id = '.$visit_id.' AND amount_paid = "'.$amount_paid.'" AND payment_created = "'.$payment_date.'"');
						$this->db->update('payments',$array_gift);
					}	
					
					$count++;
					$response .= '
									<tr class="">
										<td>'.$count.'</td>
										<td>'.$receipt_number.'</td>
										<td>'.$amount_paid.'</td>
										<td>'.$payment_date.'</td>
									</tr> 
							';
				
				
				
				
			}
			$response .= '</tbody></table>';

			$return['response'] = $response;
			$return['check'] = TRUE;
		}
		
		//if no products exist
		else
		{
			$return['response'] = 'Charges data not found';
			$return['check'] = FALSE;
		}
		
		return $return;
	}

	function import_patients_data_template()
	{
		$this->load->library('Excel');
		
		$title = 'payments Import Template';
		$count=1;
		$row_count=0;
		
		$report[$row_count][0] = 'Patient No';
		$report[$row_count][1] = 'Phone ';
		$report[$row_count][2] = 'Email';
		
		$row_count++;
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}

	public function import_csv_patients_update($upload_path)
	{
		//load the file model
		$this->load->model('admin/file_model');
		/*
			-----------------------------------------------------------------------------------------
			Upload csv
			-----------------------------------------------------------------------------------------
		*/
		$response = $this->file_model->upload_csv($upload_path, 'import_csv');
		
		if($response['check'])
		{
			$file_name = $response['file_name'];
			
			$array = $this->file_model->get_array_from_csv($upload_path.'/'.$file_name);
			//var_dump($array); die();
			$response2 = $this->sort_csv_patient_update_data($array);
		
			if($this->file_model->delete_file($upload_path."\\".$file_name, $upload_path))
			{
			}
			
			return $response2;
		}
		
		else
		{
			$this->session->set_userdata('error_message', $response['error']);
			return FALSE;
		}
	}
	public function sort_csv_patient_update_data($array)
	{
		//count total rows
		$total_rows = count($array);
		$total_columns = count($array[0]);//var_dump($array);die();
		
		//if products exist in array
		if(($total_rows > 0) && ($total_columns == 3))
		{
			$count = 0;
			$comment = '';
			$items['modified_by'] = $this->session->userdata('personnel_id');
			$response = '
				<table class="table table-hover table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Patient Number</th>
						  <th>Phone</th>
						</tr>
					  </thead>
					  <tbody>
			';
			
				//retrieve the data from array
				for($r = 1; $r < $total_rows; $r++)
				{
					$patient_number = $array[$r][0];
					$phone = $array[$r][1];
					$email = $array[$r][2];

			



					$data = array(
								'patient_number' => $patient_number,
								'patient_phone1'=>$phone,
							);
					$this->db->where('patient_number',$patient_number);

					$this->db->update('patients', $data);
					
					$count++;
					$response .= '
									<tr class="">
										<td>'.$count.'</td>
										<td>'.$patient_number.'</td>
										<td>'.$phone.'</td>
									</tr> 
							';
				
				
				
				
			}
			$response .= '</tbody></table>';

			$return['response'] = $response;
			$return['check'] = TRUE;
		}
		
		//if no products exist
		else
		{
			$return['response'] = 'Charges data not found';
			$return['check'] = FALSE;
		}
		
		return $return;
	}


	public function get_transacting_accounts($parent_account_name,$type=null)
	{
	  $branch_session = $this->session->userdata('branch_id');
	  $personnel_id = $this->session->userdata('personnel_id');

	  $branch_add = '';
	  // if($branch_session > 0)
	  // {
	  //   $branch_add = ' AND (branch_id = '.$branch_session.')';
	  // }
	  $this->db->from('account');
	  $this->db->select('*');
	  $this->db->where('(parent_account = 2 OR parent_account =19) AND paying_account = 0'.$branch_add);
	  $query = $this->db->get();     

	  return $query;     

	}
	public function get_receipt_amount($batch_receipt_id)
	{

		$this->db->from('batch_payments');
		$this->db->select('SUM(amount) AS total_amount_paid');
		$this->db->where('batch_receipt_id',$batch_receipt_id);
		$query = $this->db->get();  
		$total_amount_paid = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount_paid = $value->total_amount_paid;
			}
		}
		if(empty($total_amount_paid))
		{
			$total_amount_paid = 0;
		}
		return $total_amount_paid;

	}

	public function get_receipt_amount_paid($batch_receipt_id)
	{
		$this->db->from('payments');
		$this->db->select('SUM(amount_paid) AS total_amount_paid');
		$this->db->where('batch_receipt_id = '.$batch_receipt_id);
		$query = $this->db->get();  
		$total_amount_paid = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount_paid = $value->total_amount_paid;
			}
		}
		if(empty($total_amount_paid))
		{
			$total_amount_paid = 0;
		}
		return $total_amount_paid;
	}

	/*
	*	Retrieve all departments
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_batch_receipts_payments($table, $where, $per_page, $page, $order = 'batch_payments.invoice_number', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('batch_payments.*,visit_invoice.visit_invoice_number,payments.confirm_number,account.account_name');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$this->db->join('payments','batch_payments.payment_id = payments.payment_id','LEFT');
		$this->db->join('payment_item','payment_item.payment_id = payments.payment_id','LEFT');
		$this->db->join('visit_invoice','visit_invoice.visit_invoice_id = payment_item.visit_invoice_id','LEFT');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	
	/*
	*	Retrieve all departments
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_current_payments($table, $where, $per_page, $page, $order = 'receipt_number', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		// $this->db->group_by('receipt_number');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	public function confirm_batch_payments($batch_receipt_id)
	{

			$receipt_number = $this->input->post('receipt_number');
			$payment_date = $this->input->post('payment_date');
			$bank_id = $this->input->post('bank_id');
			$batch_receipt_id = $this->input->post('batch_receipt_id');
			$payment_method = 9;




			$total_visits = sizeof($_POST['visit_invoices']);
	        //check if any checkboxes have been ticked
	        if($total_visits > 0)
	        {
	          for($r = 0; $r < $total_visits; $r++)
	          {
	            $visit = $_POST['visit_invoices'];
	            $visit_invoice_id = $visit[$r];
	            //check if card is held
	            
	            $amount_paid = $this->input->post('amount_paid'.$visit_invoice_id);
	            $patient_id = $this->input->post('patient_id'.$visit_invoice_id);


	            if($amount_paid > 0)
	            {
		            $data = array(
								'patient_id' => $patient_id,
								'payment_method_id'=>9,
								'amount_paid'=>$amount_paid,
								'personnel_id'=>$this->session->userdata("personnel_id"),
								'payment_type'=>1,
								'transaction_code'=>$receipt_number,
								'reason'=>'Insurance Payment',
								'payment_service_id'=>1,
								'change'=>0,
								'payment_date'=>$payment_date,
								'payment_created_by'=>$this->session->userdata("personnel_id"),
								'approved_by'=>$this->session->userdata("personnel_id"),
								'date_approved'=>date('Y-m-d'),
								'bank_id'=>$bank_id,
								'batch_receipt_id'=>$batch_receipt_id
							);
		            
					$prefix = $suffix = $this->accounts_model->create_receipt_number();
					// var_dump($prefix);die();
					$branch_code = $this->session->userdata('branch_code');
					$branch_id = $this->session->userdata('branch_id');

					$branch_code = str_replace("IDC", "CT", $branch_code);

					if($branch_code == "CT")
					{
						$branch_code = "CTR";
					}

					
					if($prefix < 10)
					{
						$number = '00000'.$prefix;
					}
					else if($prefix < 100 AND $prefix >= 10)
					{
						$number = '0000'.$prefix;
					}
					else if($prefix < 1000 AND $prefix >= 100)
					{
						$number = '000'.$prefix;
					}
					else if($prefix < 10000 AND $prefix >= 1000)
					{
						$number = '00'.$prefix;
					}
					else if($prefix < 100000 AND $prefix >= 10000)
					{
						$number = '0'.$prefix;
					}

					$invoice_number = $branch_code.$number;
					$data['confirm_number'] = $invoice_number;
					$data['suffix'] = $suffix;
					$data['branch_id'] = $branch_id;


					if($this->db->insert('payments', $data))
					{
						$payment_id = $this->db->insert_id();


						 $service = array(
							              'visit_invoice_id'=>$visit_invoice_id,
							              'invoice_type'=>1,
							              'patient_id' => $patient_id,
							              'created_by' => $this->session->userdata('personnel_id'),
							              'created' => $payment_date,
							              'payment_item_amount'=>$amount_paid,
							              'payment_id'=>$payment_id
							              // 'payment_id'=>NULL
							            );
					    $this->db->insert('payment_item',$service);
					}
	          	}
	        }
			
			return TRUE;
		}
	}

	public function get_all_unallocated_batch_payments($batch_receipt_id)
	{
		$this->db->where('unallocated_payment_delete = 0 AND batch_receipt_id = '.$batch_receipt_id);
		$query = $this->db->get('batch_unallocations');

		return $query;
	}

	public function get_all_unallocated_payments($batch_receipt_id)
	{
		$this->db->select('SUM(amount_paid) AS total_amount');
		$this->db->where('unallocated_payment_delete = 0 AND batch_receipt_id = '.$batch_receipt_id);
		$query = $this->db->get('batch_unallocations');

		$total_amount = 0;

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		if(empty($total_amount))
		{
			$total_amount = 0;
		}
		return $total_amount;
	}



}
?>