<?php echo $this->load->view('search/search_invoices_list', '', TRUE);?>
<?php
$error = $this->session->userdata('error_message');
$success = $this->session->userdata('success_message');

if(!empty($error))
{
  echo '<div class="alert alert-danger">'.$error.'</div>';
  $this->session->unset_userdata('error_message');
}

if(!empty($success))
{
  echo '<div class="alert alert-success">'.$success.'</div>';
  $this->session->unset_userdata('success_message');
}

?>

<div class="row">
  <div class="col-md-12">
    <section class="panel">
        <header class="panel-heading">
            <h3 class="panel-title">Invoices </h3>
        </header>
        <div class="panel-body">
          <?php
          $search = $this->session->userdata('search_creditor_invoices_list');
          $search_title = $this->session->userdata('creditor_invoices_title_search');

          if(!empty($search))
          {
            echo '
            <a href="'.site_url().'finance/creditors/close_creditor_invoices_search" class="btn btn-warning btn-sm ">Close Search</a>
            ';
            echo $search_title;
          }
          ?>
            <table class="table table-hover table-bordered col-md-12">
              <thead>
                <tr>
                  <th>#</th>
                   <th>Creditor</th>
                  <th>Invoice Date</th>
                  <th>Invoice Number</th>
                  <th>Status</th>
                  <th>Invoice Amount</th>
                  <th>Amount Paid</th>
                  <th>Balance</th>
                  <!-- <th colspan="1">Actions</th> -->
                </tr>
              </thead>
              <tbody>
                <?php
                	// $creditor_invoices = $this->creditors_model->get_creditor_invoice($lease_id,10);
                  // var_dump($tenant_query);die();
                if($creditor_invoices->num_rows() > 0)
                {
                  $y = $page;
                  // $count = $page;
                  foreach ($creditor_invoices->result() as $key) {
                    # code...
                    // $total_amount = $key->total_amount;
                    $creditor_invoice_id = $key->creditor_invoice_id;
                    $creditor_name = $key->creditor_name;
                    $transaction_date = $key->invoice_date;
                    $start_date = $key->start_date;
                    $dr_amount = $key->dr_amount;
                    $cr_amount = $key->cr_amount;
                    $balance = $key->balance;
                    $invoice_number = $key->invoice_number;
                    // $vat_charged = $key->vat_charged;
                    $created = $key->invoice_date;
                    // $created_by = $key->created_by;

                    if($dr_amount == $cr_amount)
                    {
                      $custom = 'fully paid';
                      $custom_color = 'success';
                    }
                    else if($dr_amount > $cr_amount AND $cr_amount > 0)
                    {
                      $custom = 'partially paid';
                      $custom_color = 'warning';

                    }
                    else if($dr_amount > $cr_amount AND $cr_amount == 0)
                    {
                      $custom = 'Not paid';
                      $custom_color = '';

                    }

                    else
                    {
                      $custom = 'over paid';
                      $custom_color = 'danger';
                    }



                    $payment_explode = explode('-', $transaction_date);

                    $invoice_note_date = date('jS M Y',strtotime($transaction_date));
                    $created = date('jS M Y',strtotime($created));
                    $y++;

                    ?>
                    <tr>
                      <td><?php echo $y?></td>
                      <td><?php echo strtoupper($creditor_name);?></td>
                      <td><?php echo date('jS M Y',strtotime($transaction_date));?></td>
                      <td><?php echo $invoice_number?></td>
                      <td><?php echo strtoupper($custom);?></td>
                      <td><?php echo number_format($dr_amount,2);?></td>
                      <td><?php echo number_format($cr_amount,2);?></td>
                      <td><?php echo number_format($balance,2);?></td>
                     

                    </tr>
                    <?php

                  }
                }
                ?>

              </tbody>
            </table>

            <div class="widget-foot">
                                
                <?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>

        </div>
      </section>
    </div>
  </div>


<script>
function display_payment_model()
{
  $('#modal-defaults').modal('show');
  $('#datepicker').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd',
  })
  // var quantity = document.getElementById("quantity");
}
function get_value()
{
  var quantity = document.getElementById("quantity").value;
  var unit_price = document.getElementById("unit_price").value;
  var tax_type_id = document.getElementById("tax_type_id").value;

  var url = "<?php echo base_url();?>finance/creditors/calculate_value";
   $.ajax({
   type:'POST',
   url: url,
   data:{quantity: quantity,unit_price : unit_price, tax_type_id : tax_type_id},
   dataType: 'text',
   success:function(data){
     var data = jQuery.parseJSON(data);
     var amount = data.amount;
      var vat = data.vat;
      $( "#vat_amount" ).html("<h4>TAX KES. "+ vat +" </h4>");
      $( "#total_units" ).html("<h3>TOTAL : KES. "+ amount +" </h3>");
     document.getElementById("input-total-value").value = amount;
     document.getElementById("vat-amount").value = vat;

   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

   }
   });

}
function check_payment_type(payment_type_id){

  var myTarget1 = document.getElementById("cheque_div");

  var myTarget2 = document.getElementById("mpesa_div");

  var myTarget3 = document.getElementById("insuarance_div");

  if(payment_type_id == 1)
  {
    // this is a check

    myTarget1.style.display = 'block';
    myTarget2.style.display = 'none';
    myTarget3.style.display = 'none';
  }
  else if(payment_type_id == 2)
  {
    myTarget1.style.display = 'none';
    myTarget2.style.display = 'none';
    myTarget3.style.display = 'none';
  }
  else if(payment_type_id == 3)
  {
    myTarget1.style.display = 'none';
    myTarget2.style.display = 'none';
    myTarget3.style.display = 'block';
  }
  else if(payment_type_id == 4)
  {
    myTarget1.style.display = 'none';
    myTarget2.style.display = 'none';
    myTarget3.style.display = 'none';
  }
  else if(payment_type_id == 5)
  {
    myTarget1.style.display = 'none';
    myTarget2.style.display = 'block';
    myTarget3.style.display = 'none';
  }
  else
  {
    myTarget2.style.display = 'none';
    myTarget3.style.display = 'block';
  }

}
</script>
