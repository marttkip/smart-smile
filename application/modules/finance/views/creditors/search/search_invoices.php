 <section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Search</h2>
    </header>
    
    <!-- Widget content -->
    <div class="panel-body">
        <div class="padd">
            <?php
            echo form_open("finance/creditors/search_vendor_invoices", array("class" => "form-horizontal"));
            ?>
            <div class="row">
            	<div class="col-md-4">
            		<div class="form-group">
            			<div class="col-lg-4">
			                <div class="radio">
			                    <label>
			                        <input id="optionsRadios1" type="radio" name="payment_status" value="1" >
			                       	All Invoice
			                    </label>
			                </div>
			            </div>
			            <div class="col-lg-4">
			                <div class="radio">
			                    <label>
			                        <input id="optionsRadios1" type="radio" name="payment_status" value="2"  >
			                        Paid
			                    </label>
			                </div>
			            </div>
			            <div class="col-lg-4">
			                <div class="radio">
			                    <label>
			                        <input id="optionsRadios1" type="radio" name="payment_status" value="3" >
			                        Unpaid
			                    </label>
			                </div>
			            </div>
			             <div class="col-lg-4">
			                <div class="radio">
			                    <label>
			                        <input id="optionsRadios1" type="radio" name="payment_status" value="4" >
			                        Over Payment
			                    </label>
			                </div>
			            </div>
			            
			            
					</div>
            	</div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Date from: </label>
                        
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_from" placeholder="Date from" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
                <div class="col-md-3">
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Date to: </label>
                        
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_to" placeholder="Date to" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                	<div class="center-align">
		                <button type="submit" class="btn btn-info btn-sm">Search</button>
		            </div>
                </div>
            </div>
            
            <?php
            echo form_close();
            ?>
        </div>
    </div>
</section>