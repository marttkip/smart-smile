<?php

//patient details
$visit_type = $patient['visit_type'];
$patient_type = $patient['patient_type'];
$patient_othernames = $patient['patient_othernames'];
$patient_surname = $patient['patient_surname'];
$patient_surname = $patient['patient_surname'];
$patient_number = $patient['patient_number'];
$gender = $patient['gender'];
$patient_insurance_number = $patient['patient_insurance_number'];
$inpatient = $patient['inpatient'];
$visit_type_name = $patient['visit_type_name'];
$patient_date_of_birth = $patient['patient_date_of_birth'];

if(!empty($patient_date_of_birth) AND $patient_date_of_birth != "0000-00-00")
{
    $patient_age1 = $this->reception_model->calculate_age($patient_date_of_birth);
}
else
{
    $patient_age1 = '';

}


$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
$visit_date = date('jS F Y');

//doctor
//$doctor = $this->accounts_model->get_att_doctor($visit_id);

//served by
$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));


                   
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Treatment Statement </title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
         <style type="text/css">
            .receipt_spacing{letter-spacing:0px; font-size: 12px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            .row .col-md-12 table {
                border:solid #000 !important;
                border-width:1px 0 0 1px !important;
                font-size:12px;
            }
            .row .col-md-12 th, .row .col-md-12 td {
                border:solid #000 !important;
                border-width:0 1px 1px 0 !important;
            }
            .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
            {
                 padding: 2px;
            }
            .col-md-4
            {
                width:32%;
            }
             .col-md-6
            {
                width:50%;
            }
            .col-print-1 {width:8%;  float:left;}
            .col-print-2 {width:16%; float:left;}
            .col-print-3 {width:25%; float:left;}
            .col-print-4 {width:33%; float:left;}
            .col-print-5 {width:42%; float:left;}
            .col-print-6 {width:50%; float:left;}
            .col-print-7 {width:58%; float:left;}
            .col-print-8 {width:66%; float:left;}
            .col-print-9 {width:75%; float:left;}
            .col-print-10{width:83%; float:left;}
            .col-print-11{width:92%; float:left;}
            .col-print-12{width:100%; float:left;}
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{max-height:70px; margin:0 auto;}

        </style>
    </head>
    <body class="receipt_spacing">
    	<!-- <div class="watermark" style="background:url(<?php echo base_url().'assets/logo/'.$contacts['logo'];?>) no-repeat;"></div> -->
        <!-- <div class="watermark"> -->
          <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" width="100%" height="75%" style="position:fixed; opacity: 0.1;z-index: -1; position: fixed;" />
        <!-- </div> -->
            <div class="content-view">
                
          
	           <div class="row receipt_bottom_border" >
	                <div class="col-md-12" style="margin-left: 2px;">
	                     <div class="col-print-4 ">
	                         <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"  />
	                    </div>
	                    <div class="col-print-4">
	                        &nbsp;
	                    </div>
	                    <div class="col-print-4">
	                        <p style="font-size: 11px;margin-top:10px;">
	                        
	                            <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
	                            Address :<strong> P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></strong> <br/>
	                            Office Line: <strong> <?php echo $contacts['phone'];?></strong> <br/>
	                            E-mail: <strong><?php echo $contacts['email'];?>.</strong><br/>
	                        </p>
	                    </div>
	                   
	                    
	                </div>
	            </div>
	          <div class="row receipt_bottom_border" >
	                <div class="col-md-12 center-align">
	                    <h3>TREATMENT SHEET</h3>
	                </div>
	            </div>
	            
	            <!-- Patient Details -->
	            <div class="row " style="margin-bottom: 10px; padding-right: 20px;padding-left: 20px;">
	                <div class="col-md-12 pull-left">
	                    <h4><strong >Patient's Name:</strong> 
	                        <span style="text-decoration: dotted underline ;"><?php echo $patient_surname.' '.$patient_othernames; ?></span>
	                    </h4>
	                </div>
	                
	                
	                <div class="col-md-12">
	                   
	                    <h4><strong>Patients No. </strong><span style="text-decoration: dotted underline ;" > <?php echo $patient_number; ?> </span>
	                        <!-- <strong>Date </strong> <span style="text-decoration: dotted underline ;"><?php echo $visit_date; ?></span> -->
	                        <strong>Age </strong> <span style="text-decoration: dotted underline ;"><?php echo $patient_age1; ?></span>

	                    </h4>
	                        <h4><strong>Gender. </strong><span style="text-decoration: dotted underline ;" > <?php echo $gender; ?> </span>
	                        
	                    </h4>
	                       
	                </div>
	            </div>
	    
     
        <div class="row">
            <div class="col-md-12" style="padding:20px;">
            	<?php
            		$result = '';
            		if ($query->num_rows() > 0)
						{
							$count = 0;
							
							$result .= 
								'
									<table class="table table-hover table-bordered ">
									  <thead>
										<tr>
										  <th width="5%">#</th>
										  <th width="15%">Visit Date</th>
										  <th width="80%">Doctors Notes</th>
										</tr>
									  </thead>
									  <tbody>
								';
							
							$personnel_query = $this->personnel_model->retrieve_personnel();
							$count = 1;
							foreach ($query->result() as $row)
							{
								$visit_date = date('jS M Y',strtotime($row->visit_date));
								$visit_time = date('H:i a',strtotime($row->visit_time));
								$visit_id1 = $row->visit_id;
								$waiting_time = $this->nurse_model->waiting_time($visit_id1);
								$patient_id = $row->patient_id;
								$personnel_id = $row->personnel_id;
								$dependant_id = $row->dependant_id;
								$strath_no = $row->strath_no;
								$created_by = $row->created_by;
								$modified_by = $row->modified_by;
								$visit_type_id = $row->visit_type_id;
								$visit_type = $row->visit_type;
								$created = $row->patient_date;
								$last_modified = $row->last_modified;
								$last_visit = $row->last_visit;
								
								
									$patient_type = $this->reception_model->get_patient_type($visit_type_id);
									
									if($visit_type == 3)
									{
										$visit_type = 'Other';
									}
									else if($visit_type == 4)
									{
										$visit_type = 'Insurance';
									}
									else
									{
										$visit_type = 'General';
									}
									
									$patient_othernames = $row->patient_othernames;
									$patient_surname = $row->patient_surname;
									$title_id = $row->title_id;
									$patient_date_of_birth = $row->patient_date_of_birth;
									$civil_status_id = $row->civil_status_id;
									$patient_address = $row->patient_address;
									$patient_post_code = $row->patient_postalcode;
									$patient_town = $row->patient_town;
									$patient_phone1 = $row->patient_phone1;
									$patient_phone2 = $row->patient_phone2;
									$patient_email = $row->patient_email;
									$patient_national_id = $row->patient_national_id;
									$religion_id = $row->religion_id;
									$gender_id = $row->gender_id;
									$patient_kin_othernames = $row->patient_kin_othernames;
									$patient_kin_sname = $row->patient_kin_sname;
									$relationship_id = $row->relationship_id;
									$visit_id = $visit_id1;
								
								
								//creators and editors
								if($personnel_query->num_rows() > 0)
								{
									$personnel_result = $personnel_query->result();
									
									foreach($personnel_result as $adm)
									{
										$personnel_id2 = $adm->personnel_id;
										
										if($personnel_id == $personnel_id2)
										{
											$doctor = $adm->personnel_fname;
											break;
										}
										
										else
										{
											$doctor = '-';
										}
									}
								}
								
								else
								{
									$doctor = '-';
								}

								$item_to_display2 ='';
								$item_to_display = '';

								//  start of plan
								$plan_rs = $this->nurse_model->get_plan($visit_id1);
								$plan_num_rows = count($plan_rs);
								$plan = "";
								if($plan_num_rows > 0){
									foreach ($plan_rs as $key_plan):
										$visit_plan = $key_plan->visit_plan;
									endforeach;
									$plan .="".$visit_plan."";
									
								}
								else
								{
									$plan .="-";
								}
								// end of plan

								// start of diagnosis todays records 
								// start of diagnosis todays records 
								$notes_rs = $this->nurse_model->get_doctors_patient_notes($visit_id1);
								$notes_num_rows = count($notes_rs);
								// echo $notes_num_rows;
								// $todays_new_notes = '<form id="opening_form" method="POST" class="form-horizontal">';
								$todays_new_notes = form_open("dental/save-new-notes/".$visit_id, array("class" => "form-horizontal"));
								$notes = "";

								$todays_notes = "";
								$doctor_notes ="";
								if($notes_num_rows > 0){
									foreach ($notes_rs as $notes_key):
										$doctor_notes_id = $notes_key->doctor_notes_id;
										$doctor_notes = $notes_key->doctor_notes;
										$todays_notes .="".$doctor_notes."";
									endforeach;
								}
								else
								{
									$notes .= "-";
								}
								// end of diagnosis
								

								//  notes 
								$notes = '';
								$get_medical_rs = $this->nurse_model->get_hpco_notes($visit_id1);
								$hpco_num_rows = count($get_medical_rs);
								// echo $hpco_num_rows;

								if($hpco_num_rows > 0){
									foreach ($get_medical_rs as $key2) :
										$hpco = $key2->hpco_description;
										$todays_new_notes .= '
											                	<div class="col-md-12">
												                	<div class="form-group">
												                		<label class="col-lg-2 control-label">HP C/O : </label>
												                		<div class="col-lg-8">
																			<textarea id="hpco'.$visit_id.'" name="hpco'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$hpco.'</textarea>
																		</div>
																	</div>
																</div>
															';
									endforeach;

									$notes .= '<span class="bold">Hp C/O</span> '.$hpco.'';
									
								}
								else
								{
									$notes .= '';
									$todays_new_notes .= '	
														
														<div class="row">
															<div class="col-md-12">
											                	<div class="form-group">
											                		<label class="col-lg-2 control-label">HP C/O : </label>
											                		<div class="col-lg-8">
																		<textarea id="hpco'.$visit_id.'" name="hpco'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																	</div>
																</div>
															</div>
														</div><br/>';
								}





								
								$get_histories_rs = $this->nurse_model->get_histories_notes($visit_id1);
								$history_num_rows = count($get_histories_rs);
								//echo $history_num_rows;

								if($history_num_rows > 0){
									foreach ($get_histories_rs as $key3) :
										$past_dental_history = $key3->past_dental_history;
										$past_medical_history = $key3->past_medical_history;
									endforeach;
									$notes .= '<p><span class="bold">PM Hx</span> '.$past_medical_history.'</p>';
									$notes .= '<p><span class="bold">PD Hx</span> '.$past_dental_history.'</p>';

									$todays_new_notes .= '
														<br/>
														<div class="row">
										                	<div class="col-md-12">
											                	<div class="form-group">
											                		<label class="col-lg-2 control-label">PD Hx : </label>
											                		<div class="col-lg-8">
											                			<p>-Any previous dental treatment </p>
											                			<p>-Any adverse reactions & experience to dental Rx </p>
											                			<p>- Frequency of brushing </p>
											                			<p>- Food packing, bleeding gums,sensitivity </p>
																		<textarea id="past_dental_hx'.$visit_id1.'" name="past_dental_hx'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$past_dental_history.'</textarea>
																	</div>
																</div>
															</div>
														</div>
														<br/>
														<div class="row">

															<div class="col-md-12">
																<div class="form-group">
											                		<label class="col-lg-2 control-label">PM Hx : </label>
																	<div class="col-lg-8">
																		<p>-Allergy to any medicine </p>
											                			<p>- Currently on any medication</p>
											                			<p>- Any chronic disease/History of radiation </p>
											                			<p>- Bleeding gums,sensitivity </p>
																		<textarea id="past_medical_hx'.$visit_id1.'" name="past_medical_hx'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$past_medical_history.'</textarea>
																	</div>
																</div>
															</div>
														</div>
													';
								}
								else
								{
									$notes .='';
									$todays_new_notes .= '
														<br/>
														<div class="row">
										                	<div class="col-md-12">
											                	<div class="form-group">
											                		<label class="col-lg-2 control-label">PD Hx : </label>
											                		<div class="col-lg-8">
											                			<p>-Any previous dental treatment </p>
											                			<p>-Any adverse reactions & experience to dental Rx </p>
											                			<p>- Frequency of brushing </p>
											                			<p>- Food packing, bleeding gums,sensitivity </p>
																		<textarea id="past_dental_hx'.$visit_id1.'" name="past_dental_hx'.$visit_id1.'"  rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																	</div>
																</div>
															</div>
														</div>
														<br/>
														<div class="row">
															<div class="col-md-12">
																<div class="form-group">
											                		<label class="col-lg-2 control-label">PM Hx : </label>
																	<div class="col-lg-8">
																		<p>- Allergy to any medicine </p>
											                			<p>- Currently on any medication</p>
											                			<p>- Any chronic disease/History of radiation </p>
											                			<p>- Bleeding gums,sensitivity </p>
																		<textarea id="past_medical_hx'.$visit_id1.'" name="past_medical_hx'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																	</div>
																</div>
															</div>
														</div> <br/>';
								}


								// get the medicals this s
								$get_general_rs = $this->nurse_model->get_general_exam_notes($visit_id1);
								$general_num_rows = count($get_general_rs);
								// echo $general_num_rows;

								if($general_num_rows > 0){
									foreach ($get_general_rs as $key2) :
										$general_exam = $key2->general_exam_description;
										$todays_new_notes .= '
											                	<div class="col-md-12">
												                	<div class="form-group">
												                		<label class="col-lg-2 control-label"> General Exam: </label>
												                		<div class="col-lg-8">
												                			<p>(Pallour , Jaundice, Swelling , Fever , Lymphadenopathy)</p>
																			<textarea id="general_exam'.$visit_id.'" name="general_exam'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$general_exam.'</textarea>
																		</div>
																	</div>
																</div>
															';
									endforeach;

									$notes .= '<span class="bold">General Examp</span> '.$general_exam.'';
									
								}
								else
								{
									$notes .= '';
									$todays_new_notes .= '	
														
														<div class="row">
															<div class="col-md-12">
											                	<div class="form-group">
											                		<label class="col-lg-2 control-label">General Exam: </label>
											                		<div class="col-lg-8">
											                			<p>(Pallour , Jaundice, Swelling , Fever , Lymphadenopathy)</p>
																		<textarea id="general_exam'.$visit_id.'" name="general_exam'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																	</div>
																</div>
															</div>
														</div><br/>';
								}

								$get_oc_rs = $this->nurse_model->get_oc_notes($visit_id1);
								$oc_num_rows = count($get_oc_rs);
								//echo $oc_num_rows;

								if($oc_num_rows > 0){
									foreach ($get_oc_rs as $key2) :
										$soft_tissue = $key2->soft_tissue;
										$decayed = $key2->decayed;
										$filled = $key2->filled;
										$missing = $key2->missing;
										$general = $key2->general;
										$other = $key2->other;
									endforeach;
									$notes .= '<p><span class="bold">Soft Tissue</span> '.$soft_tissue.'</p>';
									$notes .= '<p><span class="bold">Hard Tissue</span></p>';
									$notes .= '<div style="margin-left:20px">
													<p><span class="bold">General : </span> '.$general.'</p>';
									$notes .= '		<p><span class="bold">Decayed : </span> '.$decayed.'</p>';
									$notes .= '		<p><span class="bold">Filled :</span> '.$filled.'</p>';
									$notes .= '		<p><span class="bold">Missing :</span> '.$missing.'</p>';
									$notes .= '		<p><span class="bold">Other : </span> '.$other.'</p>
												</div>';
									$todays_new_notes .= '
														<br/>
														<div class="row">
										                	<div class="col-md-12">
											                	<div class="form-group">
											                		<label class="col-lg-2 control-label">Soft Tissue : </label>
											                		<div class="col-lg-8">
																		<textarea id="soft_tissue'.$visit_id1.'" name="soft_tissue'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$soft_tissue.'</textarea>
																	</div>
																</div>
															</div>
														</div>
														<br/>
														<div class="row">
															<div class="col-md-12">
																<div class="form-group">
											                		<label class="col-lg-2 control-label">Hard Tissue : </label>
											                		<div class="col-md-10">
												                		<div class="row">
												                			<div class="col-md-12">
													                			<label class="col-lg-2 control-label">General : </label>
																				<div class="col-lg-8">
																					<textarea id="general'.$visit_id1.'" name="general'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$general.'</textarea>
																				</div>
																			</div>
																		</div>
																		<br/>
																		<div class="row">
												                			<div class="col-md-12">
													                			<label class="col-lg-2 control-label">Decayed : </label>
																				<div class="col-lg-8">
																					<textarea id="decayed'.$visit_id1.'" name="decayed'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$decayed.'</textarea>
																				</div>
																			</div>
																		</div>
																		<br/>
																		<div class="row">
																			<div class="col-md-12">
																				<label class="col-lg-2 control-label">Missing : </label>
																				<div class="col-lg-8">
																					<textarea id="missing'.$visit_id1.'" name="missing'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$missing.'</textarea>
																				</div>
																			</div>
																		</div>
																		<br/>
																		<div class="row">
																			<div class="col-md-12">
																				<label class="col-lg-2 control-label">Filled : </label>
																				<div class="col-lg-8">
																					<textarea id="filled'.$visit_id1.'" name="filled'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$filled.'</textarea>
																				</div>
																			</div>
																		</div>
																		<br/>
																		<div class="row">
																			<div class="col-md-12">
													                			<label class="col-lg-2 control-label">Others : </label>
																				<div class="col-lg-8">
																					<p>(Impacted, Abrasion, Florosis, Traumatic bite) </p>
																					<textarea id="others'.$visit_id1.'" name="others'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$other.'</textarea>
																				</div>
																			</div>
																		</div>

																	</div>
																</div>
															</div>
														</div>
														<br>				
													';
								}
								else
								{
									$notes .='';
									$todays_new_notes .='
														<div class="row">
										                	<div class="col-md-12">
											                	<div class="form-group">
											                		<label class="col-lg-2 control-label">Soft Tissue : </label>
											                		<div class="col-lg-8">
																		<textarea id="soft_tissue'.$visit_id1.'" name="soft_tissue'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="form-group">
											                		<label class="col-lg-2 control-label">Hard Tissue : </label>
											                		<div class="col-md-10">
												                		<div class="row">
												                			<div class="col-md-12">
													                			<label class="col-lg-2 control-label">General : </label>
																				<div class="col-lg-8">
																					<textarea id="general'.$visit_id1.'" name="general'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																				</div>
																			</div>
																		</div>
																		<br/>
																		<div class="row">
												                			<div class="col-md-12">
													                			<label class="col-lg-2 control-label">Decayed : </label>
																				<div class="col-lg-8">
																					<textarea id="decayed'.$visit_id1.'" name="decayed'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																				</div>
																			</div>
																		</div>
																		<br/>
																		<div class="row">
																			<div class="col-md-12">
																				<label class="col-lg-2 control-label">Missing : </label>
																				<div class="col-lg-8">
																					<textarea id="missing'.$visit_id1.'" name="missing'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																				</div>
																			</div>
																		</div>
																		<br/>
																		<div class="row">
																			<div class="col-md-12">
																				<label class="col-lg-2 control-label">Filled : </label>
																				<div class="col-lg-8">
																					<textarea id="filled'.$visit_id1.'" name="filled'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																				</div>
																			</div>
																		</div>
																		<br/>
																		<div class="row">
																			<div class="col-md-12">
													                			<label class="col-lg-2 control-label">Others : </label>

																				<div class="col-lg-8">
																					<p>(Impacted, Abrasion, Florosis, Traumatic bite) </p>
																					<textarea id="others'.$visit_id1.'" name="others'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<br>
														';
								}
								$get_oral_examination_rs = $this->nurse_model->get_oe_notes($visit_id1);
								$oe_num_rows = count($get_oral_examination_rs);
								if($oe_num_rows > 0){
									foreach ($get_oral_examination_rs as $key3) :
										$oral_examination = $key3->oe_description;
										$item_to_display2 .= '
											                	<div class="col-md-12">
												                	<div class="form-group">
												                		<label class="col-lg-2 control-label">O/E : </label>
												                		<div class="col-lg-8">
																			<textarea id="oral_examination'.$visit_id1.'" name="oral_examination'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$oral_examination.'</textarea>
																		</div>
																	</div>
																</div>
															';
									endforeach;

									$notes .= '<span class="bold">Oral Examination</span> '.$oral_examination.'';
									
								}
								else
								{
									$notes .= '';
								}
								$get_inves_rs = $this->nurse_model->get_investigations_notes($visit_id1);
								$invest_num_rows = count($get_inves_rs);
								//echo $invest_num_rows;

								if($invest_num_rows > 0){
									foreach ($get_inves_rs as $key4) :
										$investigation = $key4->investigation;
									endforeach;
									$notes .= '<p><span class="bold">Investigations : </span> '.$investigation.'</p>';
									$todays_new_notes .= '
														<br/>
														<div class="row">
															<div class="col-md-12">
																<div class="form-group">
																	<label class="col-lg-2 control-label">Investigations : </label>
																	<div class="col-lg-8">
																		<p> (Please enumerate the items)</p>
																		<textarea id="investigations'.$visit_id1.'" name="investigations'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$investigation.'</textarea>
																	</div>
																</div>
															</div>
														</div>
													';
								}
								else
								{
									$notes .= '';
									$todays_new_notes .= '
														<br/>
														<div class="row">
															<div class="col-md-12">
																<div class="form-group">
																	<label class="col-lg-2 control-label">Investigations : </label>
																	<div class="col-lg-8">
																		<p> (Please enumerate the items)</p>
																		<textarea id="investigations'.$visit_id1.'" name="investigations'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																	</div>
																</div>
															</div>
														</div> <br/>';
								}

								$get_occlusal_exam_rs = $this->nurse_model->get_occlusal_exam_notes($visit_id1);
								$occlusal_num_rows = count($get_occlusal_exam_rs);
								//echo $occlusal_num_rows;

								if($occlusal_num_rows > 0){
									foreach ($get_occlusal_exam_rs as $key4) :
										$occlusal_exam = $key4->occlusal_exam_description;
									endforeach;
									$notes .= '<p><span class="bold">Occlusal Exam : </span> '.$occlusal_exam.'</p>';
									$todays_new_notes .= '
														<br/>
															<div class="row">
															<div class="col-md-12">
																<div class="form-group">
																	<label class="col-lg-2 control-label">Occlusal Exam : </label>
																	<div class="col-lg-8">
																		<p> (Please enumerate the items)</p>
																		<textarea id="occlusal_exam'.$visit_id1.'" name="occlusal_exam'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6" >'.$occlusal_exam.'</textarea>
																	</div>
																</div>
															</div>
														</div>
														<br/>
														';
								}
								else
								{
									$notes .= '';
									$todays_new_notes .= '
														<br/>
														<div class="row">
															<div class="col-md-12">
																<div class="form-group">
																	<label class="col-lg-2 control-label">Occlusal Exam : </label>
																	<div class="col-lg-8">
																		<p> (Please enumerate the items)</p>
																		<textarea id="occlusal_exam'.$visit_id1.'" name="occlusal_exam'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6" ></textarea>
																	</div>
																</div>
															</div>
															</div>
															<br/>';
								}
								$get_findings_rs = $this->nurse_model->get_findings_notes($visit_id1);
								$find_num_rows = count($get_findings_rs);
								//echo $find_num_rows;

								if($find_num_rows > 0){
									foreach ($get_findings_rs as $key4) :
										$findings = $key4->finding_description;
									endforeach;
									$notes .= '<p><span class="bold">Findings : </span> '.$findings.'</p>';
									$todays_new_notes .= '
														<br/>
															<div class="row">
															<div class="col-md-12">
																<div class="form-group">
																	<label class="col-lg-2 control-label">Findings : </label>
																	<div class="col-lg-8">
																		<p> (Please enumerate the items)</p>
																		<textarea id="findings'.$visit_id1.'" name="findings'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$findings.'</textarea>
																	</div>
																</div>
															</div>
														</div>
														<br/>
														';
								}
								else
								{
									$notes .= '';
									$todays_new_notes .= '
														<br/>
														<div class="row">
															<div class="col-md-12">
																<div class="form-group">
																	<label class="col-lg-2 control-label">Findings : </label>
																	<div class="col-lg-8">
																		<p> (Please enumerate the items)</p>
																		<textarea id="findings'.$visit_id1.'" name="findings'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																	</div>
																</div>
															</div>
															</div>
															<br/>';
								}

								$get_plan_rs = $this->nurse_model->get_plan_notes($visit_id1);
								$plannum_rows = count($get_plan_rs);
								//echo $plannum_rows;

								if($plannum_rows > 0){
									foreach ($get_plan_rs as $key2) :
										$plan_description = $key2->plan_description;
									endforeach;
									$notes .= '<p><span class="bold">Plan : </span> '.$plan_description.'</p>';
									$todays_new_notes .='<br/>
															<div class="row">
															<div class="col-md-12">
																<div class="form-group">
																	<label class="col-lg-2 control-label">Plan Description : </label>
																	<div class="col-lg-8">
																		<p> (Please enumerate the items)</p>
																		<textarea id="plan'.$visit_id1.'" name="plan'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$plan_description.'</textarea>
																	</div>
																</div>
															</div>
														</div>
														<br/>			
													';
								}
								else
								{
									$notes .= '';
									$todays_new_notes .= '<br/>
														<div class="row">
															<div class="col-md-12">
																<div class="form-group">
																<label class="col-lg-2 control-label"> Plan  : </label>
																	<div class="col-lg-8">
																		<p> (Please enumerate the items)</p>
																		<textarea id="plan'.$visit_id1.'" name="plan'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																	</div>
																</div>
															</div>
														</div>
														<br/>
														';
								}

								$get_rx_rs = $this->nurse_model->get_rxdone_notes($visit_id1);
								$rs_num_rows = count($get_rx_rs);
								//echo $rs_num_rows;

								if($rs_num_rows > 0){
									foreach ($get_rx_rs as $key6) :
										$rx_description = $key6->rx_description;
									endforeach;
									$notes .= '<p><span class="bold">Rx Done : </span> '.$rx_description.'</p>';
									$todays_new_notes .= '<br/>
														<div class="row">
															<div class="col-md-12">
																<div class="form-group">
																	<label class="col-lg-2 control-label">Rx Done : </label>
																	<div class="col-lg-8">
																		<p> (Please enumerate the items)</p>
																		<textarea id="rx'.$visit_id1.'" name="rx'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$rx_description.'</textarea>
																	</div>
																</div>
															</div>
														</div>
														<br/>	
													';
								}
								else
								{
									$notes .= '';
									$todays_new_notes .= '<br/>
														<div class="row">
															<div class="col-md-12">
																<div class="form-group">
																	<label class="col-lg-2 control-label">RX Done : </label>
																	<div class="col-lg-8">
																		<p> (Please enumerate the items)</p>
																		<textarea id="rx'.$visit_id1.'" name="rx'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																	</div>
																</div>
															</div>
														</div>
														<br/>
														';
								}
								$get_tca_rs = $this->nurse_model->get_tca_notes($visit_id1);
								$tca_num_rows = count($get_tca_rs);
								//echo $tca_num_rows;



								if($tca_num_rows > 0){
									foreach ($get_tca_rs as $key7):
										$tca_description = $key7->tca_description;
									endforeach;
									$notes .= '<p><span class="bold">TCA : </span> '.$tca_description.'</p>';
									$todays_new_notes .= '<br/>
														<div class="row">
															<div class="col-md-12">
																<div class="form-group">
																	<label class="col-lg-2 control-label">TCA : </label>
																	<div class="col-lg-8">	
																		<p> (Please enumerate the items)</p>
																		<textarea id="tca'.$visit_id1.'" name="tca'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$tca_description.'</textarea>
																	</div>
																</div>
															</div>
														</div>	
													';
								}
								else
								{
									$notes .= '';
									$todays_new_notes .= '<br/>
														<div class="row">
															<div class="col-md-12">
																<div class="form-group">
																	<label class="col-lg-2 control-label">TCA : </label>
																	<div class="col-lg-8">
																		<p> (Please enumerate the items)</p>
																		<textarea id="tca'.$visit_id1.'" name="tca'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																	</div>
																</div>
															</div>
														</div>
														<br/>	
												';
								}
									$todays_new_notes .= '<br/><div class="row">
															<div class="col-md-12">
																<div class="form-group">
																	<div class="center-align col-lg-12 ">
																		  <!--<a hred="#" class="btn btn-large btn-info" onclick="save_all_soap('.$visit_id1.')">Save Doctors Notes</a>-->
																		  <input type="hidden" name="visit_id_form" value="'.$visit_id.'">
																		  <button type="submit" class="btn btn-large btn-info">Save doctor\'s notes</button>
																	</div>
																</div>
															</div>
														</div>';
									$todays_new_notes .= form_close();

								
								if($visit_id == $visit_id1 && $number_items > 1 && $count == $number_items)
								{
									// means that there are previous visits already done 
									// this should only include the small additional notes text area
									$visit_personnel_id = $this->session->userdata('personnel_id');
									if($visit_personnel_id == $personnel_id OR $visit_personnel_id == 0)
									{
										
										$changer = '<td>	
														<a  class="btn btn-sm btn-success" id="open_visit'.$visit_id.'" onclick="get_visit_trail('.$visit_id.');">Add visit notes</a>
														<a  class="btn btn-sm btn-danger" id="close_visit'.$visit_id.'" style="display:none;" onclick="close_visit_trail('.$visit_id.');">Close tab</a>
													</td>';
									}
									else
									{
										$changer = 'No action to perform';
									}

										$item_to_display2 = '';
										$get_medical_rs = $this->nurse_model->get_hpco_notes($visit_id);
										$hpco_num_rows = count($get_medical_rs);
										// $todays_notes = $doctor_notes;
										if($hpco_num_rows > 0){
											foreach ($get_medical_rs as $key2) :
												$hpco = $key2->hpco_description;
												$item_to_display2 .= '
													                	<div class="col-md-12">
														                	<div class="form-group">
														                		<label class="col-lg-2 control-label">Presenting Complaint : </label>
														                		<div class="col-lg-8">
																					<textarea id="hpco'.$visit_id.'" name="hpco'.$visit_id.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$hpco.'</textarea>
																				</div>
																			</div>
																		</div>
																	';
											endforeach;

											$todays_notes .= '<span class="bold">Presenting Complaint</span> '.$hpco.'';
											
										}
										else
										{
											$todays_notes .= '';
											$item_to_display2 .= '	
																
																<div class="row">
																	<div class="col-md-12">
													                	<div class="form-group">
													                		<label class="col-lg-2 control-label">HP C/O : </label>
													                		<div class="col-lg-8">
																				<textarea id="hpco'.$visit_id.'" name="hpco'.$visit_id.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																			</div>
																		</div>
																	</div>
																</div><br/>';
										}
										$get_oral_examination_rs = $this->nurse_model->get_oe_notes($visit_id);
										$oe_num_rows = count($get_oral_examination_rs);
										if($oe_num_rows > 0){
											foreach ($get_oral_examination_rs as $key3) :
												$oral_examination = $key3->oe_description;
												$item_to_display2 .= '
													                	<div class="col-md-12">
														                	<div class="form-group">
														                		<label class="col-lg-2 control-label">Presenting Complaint : </label>
														                		<div class="col-lg-8">
																					<textarea id="oral_examination'.$visit_id.'" name="oral_examination'.$visit_id.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$oral_examination.'</textarea>
																				</div>
																			</div>
																		</div>
																	';
											endforeach;

											$todays_notes .= '<span class="bold">Oral Examination</span> '.$oral_examination.'';
											
										}
										else
										{
											$todays_notes .= '';
											$item_to_display2 .= '	
																
																<div class="row">
																	<div class="col-md-12">
													                	<div class="form-group">
													                		<label class="col-lg-2 control-label">HP C/O : </label>
													                		<div class="col-lg-8">
																				<textarea id="oral_examination'.$visit_id.'" name="oral_examination'.$visit_id.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																			</div>
																		</div>
																	</div>
																</div><br/>';
										}

										$get_inves_rs = $this->nurse_model->get_investigations_notes($visit_id);
										$invest_num_rows = count($get_inves_rs);
										//echo $invest_num_rows;

										if($invest_num_rows > 0){
											foreach ($get_inves_rs as $key4) :
												$investigation = $key4->investigation;
											endforeach;
											$todays_notes .= '<p><span class="bold">Investigations : </span> '.$investigation.'</p>';
											$item_to_display2 .= '
																<br/>
																<div class="row">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label class="col-lg-2 control-label">Investigations : </label>
																			<div class="col-lg-8">
																				<p> (Please enumerate the items)</p>
																				<textarea id="investigations'.$visit_id.'" name="investigations'.$visit_id.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$investigation.'</textarea>
																			</div>
																		</div>
																	</div>
																</div>
															';
										}
										else
										{
											$todays_notes .= '';
											$item_to_display2 .= '
																<br/>
																<div class="row">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label class="col-lg-2 control-label">Investigations : </label>
																			<div class="col-lg-8">
																				<p> (Please enumerate the items)</p>
																				<textarea id="investigations'.$visit_id.'" name="investigations'.$visit_id.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																			</div>
																		</div>
																	</div>
																</div> <br/>';
										}

										$get_rx_rs = $this->nurse_model->get_rxdone_notes($visit_id);
										$rs_num_rows = count($get_rx_rs);
										//echo $rs_num_rows;

										if($rs_num_rows > 0){
											foreach ($get_rx_rs as $key6) :
												$rx_description = $key6->rx_description;
											endforeach;
											$todays_notes .= '<p><span class="bold">Rx Done : </span> '.$rx_description.'</p>';
											$item_to_display2 .= '<br/>
																<div class="row">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label class="col-lg-2 control-label">Rx Done : </label>
																			<div class="col-lg-8">
																				<p> (Please enumerate the items)</p>
																				<textarea id="rx'.$visit_id.'" name="rx'.$visit_id.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$rx_description.'</textarea>
																			</div>
																		</div>
																	</div>
																</div>
																<br/>	
															';
										}
										else
										{
											$todays_notes .= '';
											$item_to_display2 .= '<br/>
																<div class="row">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label class="col-lg-2 control-label">RX Done : </label>
																			<div class="col-lg-8">
																				<p> (Please enumerate the items)</p>
																				<textarea id="rx'.$visit_id.'" name="rx'.$visit_id.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																			</div>
																		</div>
																	</div>
																</div>
																<br/>
																';
										}
										$get_tca_rs = $this->nurse_model->get_tca_notes($visit_id);
										$tca_num_rows = count($get_tca_rs);
										//echo $tca_num_rows;



										if($tca_num_rows > 0){
											foreach ($get_tca_rs as $key7):
												$tca_description = $key7->tca_description;
											endforeach;
											$todays_notes .= '<p><span class="bold">TCA : </span> '.$tca_description.'</p>';
											$item_to_display2 .= '<br/>
																<div class="row">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label class="col-lg-2 control-label">TCA : </label>
																			<div class="col-lg-8">	
																				<p> (Please enumerate the items)</p>
																				<textarea id="tca'.$visit_id.'" name="tca'.$visit_id.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$tca_description.'</textarea>
																			</div>
																		</div>
																	</div>
																</div>	
															';
										}
										else
										{
											$todays_notes .= '';
											$item_to_display2 .= '<br/>
																<div class="row">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label class="col-lg-2 control-label">TCA : </label>
																			<div class="col-lg-8">
																				<p> (Please enumerate the items)</p>
																				<textarea id="tca'.$visit_id.'" name="tca'.$visit_id.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																			</div>
																		</div>
																	</div>
																</div>
																<br/>	
														';
										}

										$notes = $todays_notes;
										// $item_to_display2 = '<div class="col-md-12">
										// 							<div class="form-group">
										// 								<div class="col-lg-2">
										// 									Todays Visit 
										// 								</div>
										// 								<div class="col-lg-8">
										// 									<textarea id="todays_notes'.$visit_id.'" name="todays_notes'.$visit_id.'" rows="5" cols="40" class="form-control col-md-6 cleditor" >'.$notes.'</textarea>
										// 								</div>
										// 							</div>
										// 						</div>';


								}
								else if($visit_id == $visit_id1 && $number_items == 1 && $count == $number_items)
								{
									// this is the first visit
									$changer = '<td>
														<a  class="btn btn-sm btn-success" id="open_todays_visit'.$visit_id.'" onclick="get_patient_first_trail('.$visit_id.');">Open patient first card</a>
														<a  class="btn btn-sm btn-danger" id="close_todays_visit'.$visit_id.'" style="display:none;" onclick="close_patient_first_trail('.$visit_id.');">Close patient first card</a>
													</td>';
									$notes = $notes;
									$item_to_display = $todays_new_notes;
								}
								else if($visit_id > $visit_id1 && $number_items > $count AND $number_items > 1)
								{

									// past visit checker 
										// if($count < ($number_items -1))
										// {
										// 	$changer = '<td></td>';

										// }
										// else
										// {
											// $visit_personnel_id = $this->session->userdata('personnel_id');
											// if($visit_personnel_id == $personnel_id)
											// {

												// $changer = '<td>	
												// 		<a  class="btn btn-sm btn-success" id="open_visit'.$visit_id.'" onclick="get_visit_trail('.$visit_id.');">Add visit notes</a>
												// 		<a  class="btn btn-sm btn-danger" id="close_visit'.$visit_id.'" style="display:none;" onclick="close_visit_trail('.$visit_id.');">Close tab</a>
												// 	</td>';
											$changer = '<td>No action to perform </td>';

											// }
											// else
											// {
											// 	$changer = '<td>You are not allowed to perform an action</td>';
											// }
											
										// }

										$item_to_display2 = '';
										$get_medical_rs = $this->nurse_model->get_hpco_notes($visit_id1);
										$hpco_num_rows = count($get_medical_rs);
										// $todays_notes = $doctor_notes;
										if($hpco_num_rows > 0){
											foreach ($get_medical_rs as $key2) :
												$hpco = $key2->hpco_description;
												$item_to_display2 .= '
													                	<div class="col-md-12">
														                	<div class="form-group">
														                		<label class="col-lg-2 control-label">Presenting Complaint : </label>
														                		<div class="col-lg-8">
																					<textarea id="hpco'.$visit_id1.'" name="hpco'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$hpco.'</textarea>
																				</div>
																			</div>
																		</div>
																	';
											endforeach;

											$todays_notes .= '<span class="bold">Presenting Complaint</span> '.$hpco.'';
											
										}
										else
										{
											$todays_notes .= '';
											$item_to_display2 .= '	
																
																<div class="row">
																	<div class="col-md-12">
													                	<div class="form-group">
													                		<label class="col-lg-2 control-label">HP C/O : </label>
													                		<div class="col-lg-8">
																				<textarea id="hpco'.$visit_id1.'" name="hpco'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																			</div>
																		</div>
																	</div>
																</div><br/>';
										}


										$get_oral_examination_rs = $this->nurse_model->get_oe_notes($visit_id1);
										$oe_num_rows = count($get_oral_examination_rs);
										if($oe_num_rows > 0){
											foreach ($get_oral_examination_rs as $key3) :
												$oral_examination = $key3->oe_description;
												$item_to_display2 .= '
													                	<div class="col-md-12">
														                	<div class="form-group">
														                		<label class="col-lg-2 control-label">O/E : </label>
														                		<div class="col-lg-8">
																					<textarea id="oral_examination'.$visit_id1.'" name="oral_examination'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$oral_examination.'</textarea>
																				</div>
																			</div>
																		</div>
																	';
											endforeach;

											$todays_notes .= '<span class="bold">Oral Examination</span> '.$oral_examination.'';
											
										}
										else
										{
											$todays_notes .= '';
											$item_to_display2 .= '	
																
																<div class="row">
																	<div class="col-md-12">
													                	<div class="form-group">
													                		<label class="col-lg-2 control-label">O/E: </label>
													                		<div class="col-lg-8">
																				<textarea id="oral_examination'.$visit_id1.'" name="oral_examination'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																			</div>
																		</div>
																	</div>
																</div><br/>';
										}

										$get_inves_rs = $this->nurse_model->get_investigations_notes($visit_id1);
										$invest_num_rows = count($get_inves_rs);
										//echo $invest_num_rows;


										if($invest_num_rows > 0){
											foreach ($get_inves_rs as $key4) :
												$investigation = $key4->investigation;
											endforeach;
											$todays_notes .= '<p><span class="bold">Investigations : </span> '.$investigation.'</p>';
											$item_to_display2 .= '
																<br/>
																<div class="row">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label class="col-lg-2 control-label">Investigations : </label>
																			<div class="col-lg-8">
																				<p> (Please enumerate the items)</p>
																				<textarea id="investigations'.$visit_id1.'" name="investigations'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$investigation.'</textarea>
																			</div>
																		</div>
																	</div>
																</div>
															';
										}
										else
										{
											$todays_notes .= '';
											$item_to_display2 .= '
																<br/>
																<div class="row">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label class="col-lg-2 control-label">Investigations : </label>
																			<div class="col-lg-8">
																				<p> (Please enumerate the items)</p>
																				<textarea id="investigations'.$visit_id1.'" name="investigations'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																			</div>
																		</div>
																	</div>
																</div> <br/>';
										}


							

										$get_rx_rs = $this->nurse_model->get_rxdone_notes($visit_id1);
										$rs_num_rows = count($get_rx_rs);
										//echo $rs_num_rows;

										if($rs_num_rows > 0){
											foreach ($get_rx_rs as $key6) :
												$rx_description = $key6->rx_description;
											endforeach;
											$todays_notes .= '<p><span class="bold">Rx Done : </span> '.$rx_description.'</p>';
											$item_to_display2 .= '<br/>
																<div class="row">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label class="col-lg-2 control-label">Rx Done : </label>
																			<div class="col-lg-8">
																				<p> (Please enumerate the items)</p>
																				<textarea id="rx'.$visit_id1.'" name="rx'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$rx_description.'</textarea>
																			</div>
																		</div>
																	</div>
																</div>
																<br/>	
															';
										}
										else
										{
											$todays_notes .= '';
											$item_to_display2 .= '<br/>
																<div class="row">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label class="col-lg-2 control-label">RX Done : </label>
																			<div class="col-lg-8">
																				<p> (Please enumerate the items)</p>
																				<textarea id="rx'.$visit_id1.'" name="rx'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																			</div>
																		</div>
																	</div>
																</div>
																<br/>
																';
										}
										$get_tca_rs = $this->nurse_model->get_tca_notes($visit_id1);
										$tca_num_rows = count($get_tca_rs);
										//echo $tca_num_rows;



										if($tca_num_rows > 0){
											foreach ($get_tca_rs as $key7):
												$tca_description = $key7->tca_description;
											endforeach;
											$todays_notes .= '<p><span class="bold">TCA : </span> '.$tca_description.'</p>';
											$item_to_display2 .= '<br/>
																<div class="row">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label class="col-lg-2 control-label">TCA : </label>
																			<div class="col-lg-8">	
																				<p> (Please enumerate the items)</p>
																				<textarea id="tca'.$visit_id1.'" name="tca'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$tca_description.'</textarea>
																			</div>
																		</div>
																	</div>
																</div>	
															';
										}
										else
										{
											$todays_notes .= '';
											$item_to_display2 .= '<br/>
																<div class="row">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label class="col-lg-2 control-label">TCA : </label>
																			<div class="col-lg-8">
																				<p> (Please enumerate the items)</p>
																				<textarea id="tca'.$visit_id1.'" name="tca'.$visit_id1.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																			</div>
																		</div>
																	</div>
																</div>
																<br/>	
														';
										}
										
										if($rs_num_rows == 0 && $visit_id == $visit_id1)
										{
											$notes = $todays_notes;
										}
										else if($rs_num_rows == 0 && $visit_id != $visit_id1)
										{
											// echo "sdjkasda";
											$notes = $todays_notes;
										}
										else
										{
											$notes = $notes;
										}
										
										// $item_to_display2 = '<div class="col-md-12">
										// 							<div class="form-group">
										// 								<div class="col-lg-2">
										// 									Todays Visit
										// 								</div>
										// 								<div class="col-lg-8">
										// 									<textarea id="todays_notes'.$visit_id.'" name="todays_notes'.$visit_id.'" rows="5" cols="40" class="form-control col-md-6 cleditor" >'.$todays_notes.'</textarea>
										// 								</div>
										// 							</div>
										// 						</div>';

									
								}
								else if($visit_id > $visit_id1 && $number_items == $count AND $number_items > 1)
								{
									$notes = $notes;
								}
								else
								{
									$notes .= $todays_notes;
									$visit_personnel_id = $this->session->userdata('personnel_id');
									if($visit_personnel_id == $personnel_id OR $visit_personnel_id == 0)
									{
										$changer = '<td>
															<a  class="btn btn-sm btn-success" id="open_visit'.$visit_id.'" onclick="get_visit_trail('.$visit_id.');">Add visit notes </a>
															<a  class="btn btn-sm btn-danger" id="close_visit'.$visit_id.'" style="display:none;" onclick="close_visit_trail('.$visit_id.');">Close tab</a>
														</td>';
									}
									else
									{
										$changer = 'No action to perform';
									}
									// $item_to_display2 = '<div class="row">
									// 						<div class="col-md-12">
									// 							<div class="form-group">
									// 								<div class="col-lg-2">
									// 									Todays Visit
									// 								</div>
									// 								<div class="col-lg-8">
									// 									<textarea id="todays_notes'.$visit_id.'" name="todays_notes'.$visit_id.'" rows="5" cols="40" class="form-control col-md-6 cleditor" ></textarea>
									// 								</div>
									// 							</div>
									// 						</div>
									// 					</div><br>';
										$item_to_display2 = '';
										$get_medical_rs = $this->nurse_model->get_hpco_notes($visit_id);
										$hpco_num_rows = count($get_medical_rs);
										// $todays_notes = $doctor_notes;
										if($hpco_num_rows > 0){
											foreach ($get_medical_rs as $key2) :
												$hpco = $key2->hpco_description;
												$item_to_display2 .= '
													                	<div class="col-md-12">
														                	<div class="form-group">
														                		<label class="col-lg-2 control-label">Presenting Complaint : </label>
														                		<div class="col-lg-8">
																					<textarea id="hpco'.$visit_id.'" name="hpco'.$visit_id.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$hpco.'</textarea>
																				</div>
																			</div>
																		</div>
																	';
											endforeach;

											$todays_notes .= '<span class="bold">Presenting Complaint</span> '.$hpco.'';
											
										}
										else
										{
											$todays_notes .= '';
											$item_to_display2 .= '	
																
																<div class="row">
																	<div class="col-md-12">
													                	<div class="form-group">
													                		<label class="col-lg-2 control-label">P/C : </label>
													                		<div class="col-lg-8">
																				<textarea id="hpco'.$visit_id.'" name="hpco'.$visit_id.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																			</div>
																		</div>
																	</div>
																</div><br/>';
										}
										$get_oral_examination_rs = $this->nurse_model->get_oe_notes($visit_id);
										$oe_num_rows = count($get_oral_examination_rs);
										if($oe_num_rows > 0){
											foreach ($get_oral_examination_rs as $key3) :
												$oral_examination = $key3->oe_description;
												$item_to_display2 .= '
													                	<div class="col-md-12">
														                	<div class="form-group">
														                		<label class="col-lg-2 control-label">O/E : </label>
														                		<div class="col-lg-8">
																					<textarea id="oral_examination'.$visit_id.'" name="oral_examination'.$visit_id.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$oral_examination.'</textarea>
																				</div>
																			</div>
																		</div>
																	';
											endforeach;

											$todays_notes .= '<span class="bold">Oral Examination</span> '.$oral_examination.'';
											
										}
										else
										{
											$todays_notes .= '';
											$item_to_display2 .= '	
																
																<div class="row">
																	<div class="col-md-12">
													                	<div class="form-group">
													                		<label class="col-lg-2 control-label">O/E : </label>
													                		<div class="col-lg-8">
																				<textarea id="oral_examination'.$visit_id.'" name="oral_examination'.$visit_id.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																			</div>
																		</div>
																	</div>
																</div><br/>';
										}

										$get_inves_rs = $this->nurse_model->get_investigations_notes($visit_id);
										$invest_num_rows = count($get_inves_rs);
										//echo $invest_num_rows;

										if($invest_num_rows > 0){
											foreach ($get_inves_rs as $key4) :
												$investigation = $key4->investigation;
											endforeach;
											$todays_notes .= '<p><span class="bold">Investigations : </span> '.$investigation.'</p>';
											$item_to_display2 .= '
																<br/>
																<div class="row">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label class="col-lg-2 control-label">Investigations : </label>
																			<div class="col-lg-8">
																				<p> (Please enumerate the items)</p>
																				<textarea id="investigations'.$visit_id.'" name="investigations'.$visit_id.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$investigation.'</textarea>
																			</div>
																		</div>
																	</div>
																</div>
															';
										}
										else
										{
											$todays_notes .= '';
											$item_to_display2 .= '
																<br/>
																<div class="row">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label class="col-lg-2 control-label">Investigations : </label>
																			<div class="col-lg-8">
																				<p> (Please enumerate the items)</p>
																				<textarea id="investigations'.$visit_id.'" name="investigations'.$visit_id.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																			</div>
																		</div>
																	</div>
																</div> <br/>';
										}
								
										$get_rx_rs = $this->nurse_model->get_rxdone_notes($visit_id);
										$rs_num_rows = count($get_rx_rs);
										//echo $rs_num_rows;

										if($rs_num_rows > 0){
											foreach ($get_rx_rs as $key6) :
												$rx_description = $key6->rx_description;
											endforeach;
											$todays_notes .= '<p><span class="bold">Rx Done : </span> '.$rx_description.'</p>';
											$item_to_display2 .= '<br/>
																<div class="row">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label class="col-lg-2 control-label">Rx Done : </label>
																			<div class="col-lg-8">
																				<p> (Please enumerate the items)</p>
																				<textarea id="rx'.$visit_id.'" name="rx'.$visit_id.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$rx_description.'</textarea>
																			</div>
																		</div>
																	</div>
																</div>
																<br/>	
															';
										}
										else
										{
											$todays_notes .= '';
											$item_to_display2 .= '<br/>
																<div class="row">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label class="col-lg-2 control-label">RX Done : </label>
																			<div class="col-lg-8">
																				<p> (Please enumerate the items)</p>
																				<textarea id="rx'.$visit_id.'" name="rx'.$visit_id.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																			</div>
																		</div>
																	</div>
																</div>
																<br/>
																';
										}
										$get_tca_rs = $this->nurse_model->get_tca_notes($visit_id);
										$tca_num_rows = count($get_tca_rs);
										//echo $tca_num_rows;



										if($tca_num_rows > 0){
											foreach ($get_tca_rs as $key7):
												$tca_description = $key7->tca_description;
											endforeach;
											$todays_notes .= '<p><span class="bold">TCA : </span> '.$tca_description.'</p>';
											$item_to_display2 .= '<br/>
																<div class="row">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label class="col-lg-2 control-label">TCA : </label>
																			<div class="col-lg-8">	
																				<p> (Please enumerate the items)</p>
																				<textarea id="tca'.$visit_id.'" name="tca'.$visit_id.'" rows="4" cols="40" class="form-control col-md-6 cleditor" >'.$tca_description.'</textarea>
																			</div>
																		</div>
																	</div>
																</div>	
															';
										}
										else
										{
											$todays_notes .= '';
											$item_to_display2 .= '<br/>
																<div class="row">
																	<div class="col-md-12">
																		<div class="form-group">
																			<label class="col-lg-2 control-label">TCA : </label>
																			<div class="col-lg-8">
																				<p> (Please enumerate the items)</p>
																				<textarea id="tca'.$visit_id.'" name="tca'.$visit_id.'" rows="4" cols="40" class="form-control col-md-6 cleditor" ></textarea>
																			</div>
																		</div>
																	</div>
																</div>
																<br/>	
														';
										}

									
								}


								$result .= 
									'
										<tr>
											<td>'.$count.'</td>
											<td>'.$visit_date.'</td>
											<td>'.$notes.'</td>
										</tr> 
									';
									$count++;


							}
						
							 
								$result .=
										'<tr id="visit_trail'.$visit_id.'" style="display:none;">
											<td colspan="5">';
												$result .= form_open("dental/save-current-notes/".$visit_id, array("class" => "form-horizontal"));
												$result .= '
															<div class="row">
																<div class="col-md-12">
																	<div class="form-group">
																		<div class="col-lg-2">
																			Todays Visit
																		</div>
																		<div class="col-lg-8">
																			'.$item_to_display2.'
																		</div>
																		
																	</div>
																</div>
															</div>
															<br/>
															<div class="row">
																<div class="col-md-12">
																	<div class="form-group">
																		<div class="center-align col-lg-12 ">
																			<button class="btn btn-info btn-lg" type="submit"> Save Doctors Current Notes</button>
																		</div>
																	</div>
																</div>
															</div>';
												 $result .= form_close();
										$result .= '		 
											</td>
										</tr>';
								
								 

								$result .=
										'<tr id="todays_trail'.$visit_id.'" style="display:none;">
											<td colspan="5">
												'.$item_to_display.'
												
												 
											</td>
										</tr>';
							
							$result .= 
							'
										  </tbody>
										</table>
							';
						}
						
						else
						{
							$result .= "There are no patient treatment";
						}
						
							echo $result;

            	?>
            </div>
            </div>
        </div>
        <?php
		$this->session->unset_userdata('selected_drugs');
		
		
        if($doctor == '-')
        {

        }
        else
        {
        ?>
            <div class="row" style="font-style:bold; font-size:11px;">
                <div class="col-md-10 pull-left">
                    <div class="col-md-6 pull-left">
                       
                        <p> <p>Approved By: </p><strong>Dr. <?php echo $doctor;?></strong></p>
                        <br>

                        <p>Sign : ................................ </p>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>
    	<div class="row" style="font-style:italic; font-size:11px;">
        	<div class="col-md-12 ">
                <div class="col-md-6 pull-left">
                    	Prepared by: <?php echo $served_by;?> 
                </div>
                <div class="col-md-6 pull-right">
            			<?php echo date('jS M Y H:i a'); ?> Thank you
            	</div>
          	</div>
        	
        </div>
    </body>
    
</html>