<?php


$branch_id = $branch_session = $this->session->userdata('branch_id');
$doctors_schedule = $this->calendar_model->get_branch_doctors($branch_id);

$resources = '';
$resources_items = '';
$notes_one ='';
$count = $doctors_schedule->num_rows()+1;

$width = ($count+1)/100;

// var_dump($count);die();
if($doctors_schedule->num_rows() > 0)
{
	$letter = 'a';
	foreach ($doctors_schedule->result() as $key => $value) {
		# code...

		
		$fname = $value->personnel_fname;
		$onames = $value->personnel_onames;
		$personnel_id = $value->personnel_id;
		$authorize_invoice_changes = $value->authorize_invoice_changes;
		$branch_id = $value->branch_id;
		$name = $fname.' '.$onames;
		
		if($branch_session == $branch_id OR $authorize_invoice_changes == 1)
		{
			$resources_items .= '<th style="width: '.$width.'%;">NOTE</th>';
			
			$notes_one .= '<td>
							<table class="table borderless">
								<tbody>';
			// $resources .= '{ id: "'.$personnel_id.'", title: "'.$name.'"},';
			// $resources_items .= '<option value="'.$personnel_id.'">'.$name.'</option>';


			$query_one = $this->reception_model->get_todays_calendar_notes($todays_date,2,$personnel_id,0,$branch_id);
			// $notes_one = '';
			// var_dump($query_one); die();
			if($query_one->num_rows() > 0)
			{
				foreach ($query_one->result() as $key => $value) {
					# code...
					$note = $value->note;
					$calendar_note_id = $value->calendar_note_id;
					$notes_one .= '
										<tr>
										<td><span onclick="get_note_details('.$calendar_note_id.')"> '.$note.'</span>  <a class="pull-right" onclick="delete_note_details('.$calendar_note_id.',1)"><i class="fa fa-trash"></i></a></td>
									  </tr>';
				}
			}


			$query_one_patients = $this->reception_model->get_todays_rescheduled_patients($todays_date,2,$personnel_id,0,$branch_id);
			if($personnel_id == 45)
			{
				// var_dump($query_one_patients); die();

			}
			
			if($query_one_patients->num_rows() > 0)
			{
				foreach ($query_one_patients->result() as $key => $value) {
					# code...
					$event_name = $value->event_name;
					$event_description = $value->event_description;
					$appointment_id = $value->appointment_id;
					$notes_one .= '<tr>
										<td><span> '.$event_name.' '.$event_description.'</span>  <a class="pull-right" onclick="delete_event_details('.$appointment_id.',1)"><i class="fa fa-trash"></i></a>
										</td>
								  	</tr>';
				}
			}
			$notes_one .= '</tbody>
				</table>
			</td>';
			$letter++;
		}
		
		else
		{
			// if($authorize_invoice_changes == 1)
			// {
			// 	echo "<option value='".$personnel_id."'> ".$fname." ".$onames."</option>";
			// }
			
		}

		// $resources_items .= '<option value="'.$personnel_id.'">'.$name.'</option>';
		// $fname = $value->personnel_fname;
		// $onames = $value->personnel_onames;
		// $personnel_id = $value->personnel_id;

		// $name = $fname.' '.$onames;
								

		

		 
		    

	}
	// $resources .= '{ id: "a", title: "Online Bookings"},';
}

$branches_rs = $this->reception_model->get_branches();


if($branches_rs->num_rows() > 0)
{
	foreach ($branches_rs->result() as $key => $value) {
		# code...
		$branch_idd = $value->branch_id;
		$branch_code = $value->branch_code;
		// var_dump($branch_code);die();

		if($branch_session == $branch_idd)
		{
			$resources_items .= '<th style="width: '.$width.'%;">NOTE</th>';
			// $resources .= '{ id: "'.$branch_code.'", title: "Online Bookings"},';
			$query_one = $this->reception_model->get_todays_calendar_notes($todays_date,2,$branch_code,0,$branch_id);
			// $notes_one = '';
			// var_dump($query_one); die();
			$notes_one .= '<td>
							<table class="table borderless">
								<tbody>';
			if($query_one->num_rows() > 0)
			{
				foreach ($query_one->result() as $key => $value) {
					# code...
					$note = $value->note;
					$calendar_note_id = $value->calendar_note_id;
					$notes_one .= '
										<tr>
											<td><span onclick="get_note_details('.$calendar_note_id.')"> '.$note.'</span>  <a class="pull-right" onclick="delete_note_details('.$calendar_note_id.',1)"><i class="fa fa-trash"></i></a></td>
										  </tr>';
				}
			}


			$query_one_patients = $this->reception_model->get_todays_rescheduled_patients($todays_date,2,$branch_code,0,$branch_id);

			// var_dump($query_one); die();
			if($query_one_patients->num_rows() > 0)
			{
				foreach ($query_one_patients->result() as $key => $value) {
					# code...
					$event_name = $value->event_name;
					$event_description = $value->event_description;
					$appointment_id = $value->appointment_id;
					$notes_one .= '
										<tr>
											<td><span> '.$event_name.' '.$event_description.'</span>  <a class="pull-right" onclick="delete_event_details('.$appointment_id.',1)"><i class="fa fa-trash"></i></a>
											</td>
									  	</tr>';
				}
			}
			$notes_one .= '</tbody>
				</table>
			</td>';

		}
		
	}
}

?>
<div style='margin-left: 40px;'>
	<table class='table table-bordered table-condensed' id='bottom-table'>
		<thead>
			<?php echo $resources_items?>
		</thead>
		<tbody>

			<?php echo $notes_one?>
					
		</tbody>
	</table>
</div>

