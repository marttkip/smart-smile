
 <section class="panel ">
	<header class="panel-heading">
		<div class="panel-title">
		<strong>Name:</strong> <?php echo $patient_surname.' '.$patient_othernames;?>. <strong> Patient Visit: </strong><?php echo $visit_type_name;?>. 
		

		</div>
		<div class="pull-right">
			  <?php
				if($inpatient > 0)
				{
					?>
					<a href="<?php echo site_url();?>queues/inpatient-queue" class="btn btn-info btn-sm pull-right " style="margin-top:-25px"><i class="fa fa-arrow-left"></i> Back to Inpatient Queue</a>
			
					<?php
				}
				else
				{
					?>
					<a href="<?php echo site_url();?>queues/walkins" class="btn btn-info btn-sm pull-right " style="margin-top:-25px"><i class="fa fa-arrow-left"></i> Back to Walkins Queue</a>
			
					<?php
				}
				?>
		</div>
	</header>
	
	<!-- Widget content -->
	
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
			<?php
				$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');
				
				if(!empty($error))
				{
				  echo '<div class="alert alert-danger">'.$error.'</div>';
				  $this->session->unset_userdata('error_message');
				}
				
				if(!empty($success))
				{
				  echo '<div class="alert alert-success">'.$success.'</div>';
				  $this->session->unset_userdata('success_message');
				}
			 ?>
			</div>
		</div>
		
		
        <!--<div class="row">
        	<div class="col-sm-3 col-sm-offset-3">
            	<a href="<?php echo site_url().'doctor/print_prescription'.$visit_id;?>" class="btn btn-warning">Print prescription</a>
            </div>
            
        	<div class="col-sm-3">
            	<a href="<?php echo site_url().'doctor/print_lab_tests'.$visit_id;?>" class="btn btn-danger">Print lab tests</a>
            </div>
        </div>-->
      
        
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-12">
					<section class="panel panel-featured panel-featured-info">
						<header class="panel-heading">
							
							<h2 class="panel-title">Invoices Charges</h2>
						</header>
						<div class="panel-body">
                            <div class="row">
                            	<?php echo form_open("pharmacy/bill_patient/".$visit_id, array("class" => "form-horizontal"));?>
					            	<div class="col-md-10 ">
					                    <div class="col-md-12" style="margin-bottom: 10px">
						                  <div class="form-group">
						                  <label class="col-md-2 control-label">Service: </label>
						                  	<div class="col-md-10">
							                    <select id='service_id_item' name='service_charge_id' class='form-control custom-select ' >
							                      <option value=''>None - Please Select a service</option>
							                       <?php echo $services_list;?>
							                    </select>

							                    <input type="hidden" name="visit_id_checked" id="visit_id_checked">
						                    </div>
						                  </div>
						                </div>
						                <br>
						                <input type="hidden" name="provider_id" value="0">
						               
						                <input data-format="yyyy-MM-dd" type="hidden" data-plugin-datepicker class="form-control" name="visit_date_date" id="visit_date_date" placeholder="Admission Date" value="<?php echo date('Y-m-d');?>">
						            </div>
						            <div class="col-md-2" >
						            	<div class="center-align">
											<button type="submit" class='btn btn-info btn-sm' type='submit' >Add to Bill</button>
										</div>
						            </div>
						         <?php echo form_close();?>
						    </div>
						    <hr>
                            <?php

								$result = "
								<table align='center' class='table table-striped table-condensed'>
									<tr>
										<th>#</th>
										<th >Drug</th>
										<th >Units</th>
										<th >Unit Cost (Ksh)</th>
										<th > Total</th>
										<th > </th>
									</tr>		
								"; 

								$total= 0; 
								$count = 0;
								
								$total_amount= 0; 
								$sub_total = 0;
								$counted =0;
								$personnel_query = $this->personnel_model->retrieve_personnel();
									
								if($charge_sheet_query->num_rows() >0){
									
									$visit_date_day = '';
									foreach ($charge_sheet_query->result() as $value => $key1):
										$v_procedure_id = $key1->visit_charge_id;
										$procedure_id = $key1->service_charge_id;
										$service_name = $key1->service_name;
										$product_id = $key1->drug_id;
										$date = $key1->date;
										$time = $key1->time;
										$visit_charge_timestamp = $key1->visit_charge_timestamp;
										$visit_charge_amount = $key1->visit_charge_amount;
										$units = $key1->visit_charge_units;
										$procedure_name = $key1->service_charge_name;
										$service_id = $key1->service_id;
										$provider_id = $key1->provider_id;
										$charged = $key1->charged;
										$prescription_id = $key1->prescription_id;
									
										$sub_total= $sub_total +($units * $visit_charge_amount);
										$visit_date = date('l d F Y',strtotime($date));
										$visit_time = date('H:i A',strtotime($visit_charge_timestamp));
										
										if($visit_date_day != $visit_date)
										{
											
											$visit_date_day = $visit_date;
										}
										else
										{
											$visit_date_day = '';
										}

										// echo 'asdadsa'.$visit_date_day;

										if($personnel_query->num_rows() > 0)
										{
											$personnel_result = $personnel_query->result();
											
											foreach($personnel_result as $adm)
											{
												$personnel_id = $adm->personnel_id;
												

												if($personnel_id == $provider_id)
												{
													$provider_id = '[ Dr. '.$adm->personnel_fname.' '.$adm->personnel_lname.']';


													$procedure_name = $procedure_name.$provider_id;
												}
												
												
												
											}
										}
										
										else
										{
											$provider_id = '';
										}


										if($charged == 0)
										{
											$color = 'default';
											$buttons = "<td>
															<button class='btn btn-sm btn-success' type='submit' onclick='return confirm(\"Update Prescription ?\");'><i class='fa fa-pencil'></i> Update Charge </button>
															
															<a href='".site_url()."pharmacy/delete_prescription/".$prescription_id."/".$visit_id."/".$v_procedure_id."/5' class='btn btn-sm btn-warning'  onclick='return confirm(\"Do you want to remove this charge ?\");'><i class='fa fa-pencil'></i> Remove Drug </a>
														</td>";
										}
										else
										{
											$color = 'success';
											$buttons = '';
										}


										$counted++;
										$result .= form_open("update-charge-sheet/".$v_procedure_id."/".$product_id."/".$visit_id."/".$prescription_id, array("class" => "form-horizontal"));
										$result .="
												<tr class='".$color."'> 
													<td>".$counted."</td>		
													<td >".$procedure_name."</td>
													<td align='center'>
														<input type='text' id='units".$v_procedure_id."' name='units".$v_procedure_id."' class='form-control' value='".$units."' size='3' />
													</td>
													<td align='center'><input type='text' id='billed_amount".$v_procedure_id."' name='billed_amount".$v_procedure_id."' class='form-control'  size='5' value='".$visit_charge_amount."'></div>
													</td>
													<td >".number_format($visit_charge_amount*$units)."</td>
													".$buttons."
												</tr>	
										";
										$result .=form_close();
										$visit_date_day = $visit_date;
										$total_amount += $visit_charge_amount*$units;
										endforeach;
										

								}		

								$result .='<tr>
											<td colspan="4">Total Amount</td>
											<td>'.number_format($total_amount).'</td>
										   </tr>';
					$result .="
					 </table>
					";

					echo $result;
					?>
						
						</div>
					</section>


					<!-- END OF FIRST ROW -->

			
				</div>
				<!-- END OF THE SPAN 7 -->
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 center-align">
				<?php
				$query = $this->reception_model->get_visit($visit_id);
				$closed = 0;
				if($query->num_rows() == 1)
				{
					foreach ($query->result() as $key => $value) {
						# code...
						$closed = $value->closed;
					}
				}
				if($closed == 1)
				{
					?>
						<a href="<?php echo site_url();?>pharmacy/close_visit/<?php echo $visit_id?>" class='btn btn-info btn-sm' onclick="return confirm('Do you want to post sale ?')" > Post Sale </a>
					<?php
				}
				else
				{
					?>
					<a href="<?php echo site_url();?>pharmacy/send_to_walkin_accounts/<?php echo $visit_id?>" class='btn btn-success btn-sm'  onclick="return confirm('Do you send to accounts ?')" > Send to accounts </a>
					<?php

				}
				?>
				
				
			</div>
		</div>
			
	
	</div>
</section>
  <!-- END OF ROW -->
<script type="text/javascript">

 
   $(function() {
       $("#service_id_item").customselect();
       $("#provider_id_item").customselect();
       $("#parent_service_id").customselect();

   });
   $(document).ready(function(){
   		display_patient_bill(<?php echo $visit_id;?>);
   });
     
  function getservices(id){

        var myTarget1 = document.getElementById("service_div");
        var myTarget2 = document.getElementById("username_div");
        var myTarget3 = document.getElementById("password_div");
        var myTarget4 = document.getElementById("service_div2");
        var myTarget5 = document.getElementById("payment_method");
		
        if(id == 1)
        {
          myTarget1.style.display = 'none';
          myTarget2.style.display = 'none';
          myTarget3.style.display = 'none';
          myTarget4.style.display = 'block';
          myTarget5.style.display = 'block';
        }
        else
        {
          myTarget1.style.display = 'block';
          myTarget2.style.display = 'block';
          myTarget3.style.display = 'block';
          myTarget4.style.display = 'none';
          myTarget5.style.display = 'none';
        }
        
  }
  function check_payment_type(payment_type_id){
   
    var myTarget1 = document.getElementById("cheque_div");

    var myTarget2 = document.getElementById("mpesa_div");

    var myTarget3 = document.getElementById("insuarance_div");

    if(payment_type_id == 1)
    {
      // this is a check
     
      myTarget1.style.display = 'block';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 2)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 3)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';
    }
    else if(payment_type_id == 4)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 5)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'block';
      myTarget3.style.display = 'none';
    }
    else
    {
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';  
    }

  }

   function display_patient_bill(visit_id){

      var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/view_patient_bill/"+visit_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("patient_bill").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
  }

	//Calculate procedure total
	function calculatetotal(amount, id, procedure_id, v_id){
	       
	    var units = document.getElementById('units'+id).value;  
	    var billed_amount = document.getElementById('billed_amount'+id).value;  

	    grand_total(id, units, billed_amount, v_id);
	}
	function grand_total(procedure_id, units, amount, v_id){
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var config_url = document.getElementById("config_url").value;
	    var url = config_url+"accounts/update_service_total/"+procedure_id+"/"+units+"/"+amount;
	    // alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
				{
	    			display_patient_bill(v_id);
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}
	function delete_service(id, visit_id){
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	     var config_url = document.getElementById("config_url").value;
	    var url = config_url+"accounts/delete_service_billed/"+id;
	    
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                display_patient_bill(visit_id);
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}
	function save_service_items(visit_id)
	{
		var provider_id = $('#provider_id'+visit_id).val();
		var service_id = $('#service_id'+visit_id).val();
		var visit_date = $('#visit_date_date'+visit_id).val();
		var url = "<?php echo base_url();?>accounts/add_patient_bill/"+visit_id;
		
		$.ajax({
		type:'POST',
		url: url,
		data:{provider_id: provider_id, service_charge_id: service_id, visit_date: visit_date},
		dataType: 'text',
		success:function(data){
			alert("You have successfully billed");
			display_patient_bill(visit_id);
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			display_patient_bill(visit_id);
		}
		});
		return false;
	}

 
</script>
