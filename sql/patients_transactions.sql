CREATE OR REPLACE VIEW v_transactions AS
SELECT
visit_invoice.visit_invoice_id AS transaction_id,
visit.visit_id AS reference_id,
visit_invoice.visit_invoice_number AS reference_code,
'' AS transactionCode,
visit.patient_id AS patient_id,
'' AS supplier_id,
'' AS supplier_name,
'' AS parent_service,
visit_charge.service_charge_id AS child_service,
visit.personnel_id AS personnel_id,
CONCAT(personnel.personnel_fname,' ',personnel.personnel_onames) AS personnel_name,
visit_invoice.bill_to AS payment_type,
visit_type.visit_type_name AS payment_type_name,
'' AS payment_method_id,
'' AS payment_method_name,
'' AS account_parent,
'0' AS account_id,
'' AS account_classification,
'' AS account_name,
'' AS transaction_name,
CONCAT( "<strong>Invoice </strong> <br> Invoice Number :  ", visit_invoice.visit_invoice_number,"<br> <strong>Bill to:</strong>  ", visit_type.visit_type_name ) AS transaction_description,
SUM(visit_charge.visit_charge_amount*visit_charge.visit_charge_units) AS dr_amount,
'0' AS cr_amount,
visit_invoice.created AS invoice_date,
visit_invoice.created AS transaction_date,
visit_charge.visit_charge_timestamp AS created_at,
visit_charge.charged AS `status`,
visit.branch_id AS branch_id,
'1' AS party,
'Revenue' AS transactionCategory,
'Invoice Patients' AS transactionClassification,
'visit_charge' AS transactionTable,
'visit' AS referenceTable
FROM
	visit_charge,visit_invoice,visit,service_charge,visit_type,personnel
	WHERE visit.visit_delete = 0 AND visit_charge.charged = 1  
	AND visit_invoice.visit_invoice_delete = 0
	AND visit_charge.visit_charge_delete = 0 
	AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id
	AND service_charge.service_charge_id = visit_charge.service_charge_id
	AND visit_type.visit_type_id = visit_invoice.bill_to
	AND visit.visit_id = visit_charge.visit_id
	AND visit_invoice.dentist_id = personnel.personnel_id

GROUP BY visit_charge.visit_invoice_id

UNION ALL

SELECT
	visit_credit_note.visit_credit_note_id AS transaction_id,
	visit_invoice.visit_invoice_id AS reference_id,
	visit_credit_note.visit_cr_note_number AS reference_code,
	visit_credit_note.visit_cr_note_number AS transactionCode,
	visit_credit_note.patient_id AS patient_id,
	'' AS supplier_id,
	'' AS supplier_name,
	'' AS parent_service,
	'' AS child_service,
	'' AS personnel_id,
	'' AS personnel_name,
	visit_invoice.bill_to AS payment_type,
	'' AS payment_type_name,
	'' AS payment_method_id,
	'' AS payment_method_name,
	'' AS account_parent,
	'' AS account_id,
	'' AS account_classification,
	'' AS account_name,
	CONCAT( "Credit Note ", " : " ) AS transaction_name,
	CONCAT( "<strong>Credit Note for </strong> <br> Invoice Number: ", visit_invoice.visit_invoice_number,"<br><strong>Credit Note No:</strong> ",visit_credit_note.visit_cr_note_number) AS transaction_description,
	'0' AS dr_amount,
	SUM(visit_credit_note_item.visit_cr_note_amount*visit_credit_note_item.visit_cr_note_units) AS cr_amount,
	visit_invoice.created AS invoice_date,
	visit_credit_note.created AS transaction_date,
	visit_credit_note.created AS created_at,
	visit_credit_note.visit_cr_note_delete AS `status`,
	2 AS branch_id,
	'2' AS party,
	'Credit Note' AS transactionCategory,
	'Invoice Patients' AS transactionClassification,
	'creditnte' AS transactionTable,
	'visit' AS referenceTable
FROM
	visit_credit_note_item,visit_credit_note,visit_invoice
	WHERE visit_credit_note.visit_cr_note_delete = 0 AND visit_credit_note_item.visit_cr_note_item_delete = 0 

	AND visit_invoice.visit_invoice_id = visit_credit_note.visit_invoice_id 
	AND visit_credit_note_item.visit_credit_note_id = visit_credit_note.visit_credit_note_id 
	
GROUP BY visit_credit_note_item.visit_credit_note_id


UNION ALL

-- Invocie based on patient Vist
SELECT
	payments.payment_id AS transaction_id,
	visit_invoice.visit_invoice_id AS reference_id,
	payments.confirm_number AS reference_code,
	payments.transaction_code AS transactionCode,
	payments.patient_id AS patient_id,
	'' AS supplier_id,
	'' AS supplier_name,
	'' AS parent_service,
	'' AS child_service,
	visit_invoice.dentist_id AS personnel_id,
	CONCAT(personnel.personnel_fname,' ',personnel.personnel_onames) AS personnel_name,
	visit_invoice.bill_to AS payment_type,
	visit_type.visit_type_name AS payment_type_name,
	payments.payment_method_id AS payment_method_id,
	payment_method.payment_method AS payment_method_name,
	account.parent_account AS account_parent,
	account.account_id AS account_id,
	account.account_type_id AS account_classification,
	account.account_name AS account_name,
	CONCAT( "Invoice Payment", " : " ) AS transaction_name,
	CONCAT( "<strong>Payment</strong> <br> Invoice Number: ", visit_invoice.visit_invoice_number,"<br><strong>Method:</strong> ",payment_method.payment_method,"<br><strong>Receipt No:</strong> ",payments.confirm_number) AS transaction_description,
	'0' AS dr_amount,
	SUM(payment_item.payment_item_amount) AS cr_amount,
	visit_invoice.created AS invoice_date,
	payments.payment_date AS transaction_date,
	DATE(payments.time) AS created_at,
	payments.payment_status AS `status`,
	visit.branch_id AS branch_id,
	'2' AS party,
	'Revenue Payment' AS transactionCategory,
	'Invoice Patients Payment' AS transactionClassification,
	'payments' AS transactionTable,
	'visit' AS referenceTable
FROM
	payments,payment_item,visit_invoice,payment_method,visit,account,visit_type,personnel
	WHERE payments.cancel = 0 AND payments.payment_type = 1 AND payment_item.invoice_type = 1
	AND payments.payment_id = payment_item.payment_id 
	AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id 
	AND payments.payment_method_id = payment_method.payment_method_id
	AND visit.visit_id = visit_invoice.visit_id
	AND payment_method.account_id = account.account_id
	AND visit.visit_type = visit_type.visit_type_id
	AND visit_invoice.dentist_id = personnel.personnel_id
GROUP BY payment_item.payment_id

UNION ALL

-- Invocie based on patient Vist
SELECT
	payments.payment_id AS transaction_id,
	'' AS reference_id,
	payments.confirm_number AS reference_code,
	payments.transaction_code AS transactionCode,
	payments.patient_id AS patient_id,
	'' AS supplier_id,
	'' AS supplier_name,
	'' AS parent_service,
	'' AS child_service,
	'' AS personnel_id,
	'' AS personnel_name,
	'' AS payment_type,
	'' AS payment_type_name,
	payments.payment_method_id AS payment_method_id,
	payment_method.payment_method AS payment_method_name,
	account.parent_account AS account_parent,
	account.account_id AS account_id,
	account.account_type_id AS account_classification,
	account.account_name AS account_name,
	CONCAT( "Payment on account", " : " ) AS transaction_name,
	CONCAT( "<strong>Payment</strong> <br> On Account: <br><strong>Method:</strong> ",payment_method.payment_method,"<br><strong>Receipt No:</strong> ",payments.confirm_number) AS transaction_description,
	'0' AS dr_amount,
	SUM(payment_item.payment_item_amount) AS cr_amount,
	payments.payment_date AS invoice_date,
	payments.payment_date AS transaction_date,
	DATE(payments.time) AS created_at,
	payments.payment_status AS `status`,
	payments.branch_id AS branch_id,
	'2' AS party,
	'Revenue Payment' AS transactionCategory,
	'On account Patients Payment' AS transactionClassification,
	'payments' AS transactionTable,
	'visit' AS referenceTable
FROM
	payments,payment_item,payment_method,account
	WHERE payments.cancel = 0 AND payments.payment_type = 1 AND payment_item.invoice_type = 3
	AND payments.payment_id = payment_item.payment_id 
	AND payments.payment_method_id = payment_method.payment_method_id
	AND payment_method.account_id = account.account_id
GROUP BY payment_item.payment_id;
CREATE OR REPLACE VIEW v_transactions_by_date AS SELECT * FROM v_transactions ORDER BY transaction_date ASC;

CREATE OR REPLACE VIEW v_patient_account_balances AS 

select
   `v_transactions`.`patient_id` AS `patient_id`,
   sum(`v_transactions`.`dr_amount`) AS `total_debits`,
   sum(`v_transactions`.`cr_amount`) AS `total_credit`,
   sum(`v_transactions`.`dr_amount`) - sum(`v_transactions`.`cr_amount`) AS `total_balance` 
from
   `v_transactions` 
group by
   `v_transactions`.`patient_id`;







   
CREATE OR REPLACE VIEW v_statement_of_accounts AS 


SELECT
	visit_invoice.visit_invoice_id AS transaction_id,
	visit_invoice.visit_invoice_number AS reference_code,
	visit_invoice.patient_id AS patient_id,
	CONCAT(patients.patient_othernames," ",patients.patient_surname) AS patient_name,
	visit_invoice.dentist_id AS personnel_id,
	visit_invoice.bill_to AS payment_type,
	visit_type.visit_type_name AS payment_type_name,
	CONCAT( "<strong>Invoice </strong> <br> Invoice Number :  ", visit_invoice.visit_invoice_number,"<br> <strong>Bill to:</strong>  ", visit_type.visit_type_name ) AS transaction_description,
	SUM(visit_charge.visit_charge_amount*visit_charge.visit_charge_units) AS dr_amount,
	(SELECT COALESCE (SUM(payment_item.payment_item_amount),0) FROM payments,payment_item WHERE payments.payment_id = payment_item.payment_id AND cancel = 0 AND payment_item.visit_invoice_id = visit_invoice.visit_invoice_id) AS cr_amount,
	visit_invoice.created AS invoice_date,
	visit_charge.charged AS `status`,
	2 AS branch_id
FROM
	visit_charge,visit_invoice,service_charge,visit_type,patients
	WHERE visit_charge.charged = 1  
	AND visit_invoice.visit_invoice_delete = 0
	AND visit_charge.visit_charge_delete = 0 
	AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id
	AND service_charge.service_charge_id = visit_charge.service_charge_id
	AND visit_type.visit_type_id = visit_invoice.bill_to
	AND patients.patient_id = visit_invoice.patient_id
	
GROUP BY visit_charge.visit_invoice_id;




CREATE OR REPLACE VIEW v_aged_receivables AS
-- Creditr Invoices
SELECT
	visit_type.visit_type_id AS recepientId,
	visit_type.visit_type_name as receivable,
	2 as branch_id,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) )  = 0, v_transactions.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) )  = 0, v_transactions.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) )  = 0, v_transactions.dr_amount, 0 ))
        - Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) )  = 0, v_transactions.cr_amount, 0 ))
        END
  ) AS `coming_due`,
	(
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) BETWEEN 1 AND 30, v_transactions.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) BETWEEN 1 AND 30, v_transactions.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) BETWEEN 1 AND 30, v_transactions.dr_amount, 0 ))
				  - Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) )  BETWEEN 1 AND 30, v_transactions.cr_amount, 0 ))
			END
  ) AS `thirty_days`,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) BETWEEN 31 AND 60, v_transactions.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) BETWEEN 31 AND 60, v_transactions.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) BETWEEN 31 AND 60, v_transactions.dr_amount, 0 ))
				   - Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) BETWEEN 31 AND 60, v_transactions.cr_amount, 0 ))
			END
  ) AS `sixty_days`,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) BETWEEN 61 AND 90, v_transactions.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) BETWEEN 61 AND 90, v_transactions.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) BETWEEN 61 AND 90, v_transactions.dr_amount, 0 ))
				   - Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) )  BETWEEN 61 AND 90, v_transactions.cr_amount, 0 ))
		END
  ) AS `ninety_days`,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) >90, v_transactions.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) >90, v_transactions.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) >90, v_transactions.dr_amount, 0 ))
				 - Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) )  >90, v_transactions.cr_amount, 0 ))
			END
  ) AS `over_ninety_days`,

  (

    (
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) )  = 0, v_transactions.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) )  = 0, v_transactions.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) )  = 0, v_transactions.dr_amount, 0 ))
					 - Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) )  = 0, v_transactions.cr_amount, 0 ))
  		END
    )-- Getting the Value for 0 Days
    + (
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) BETWEEN 1 AND 30, v_transactions.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) BETWEEN 1 AND 30, v_transactions.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) BETWEEN 1 AND 30, v_transactions.dr_amount, 0 ))
 - Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) )  BETWEEN 1 AND 30, v_transactions.cr_amount, 0 ))
  		END
    ) --  AS `1-30 Days`
    +(
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) BETWEEN 31 AND 60, v_transactions.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) BETWEEN 31 AND 60, v_transactions.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) BETWEEN 31 AND 60, v_transactions.dr_amount, 0 ))

				   - Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) )  BETWEEN 31 AND 60, v_transactions.cr_amount, 0 ))
				END

    ) -- AS `31-60 Days`
    +(
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) BETWEEN 61 AND 90, v_transactions.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) BETWEEN 61 AND 90, v_transactions.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) BETWEEN 61 AND 90, v_transactions.dr_amount, 0 ))
				   - Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) BETWEEN 61 AND 90, v_transactions.cr_amount, 0 ))
  		END
    ) -- AS `61-90 Days`
    +(
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) >90, v_transactions.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) >90, v_transactions.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) ) >90, v_transactions.dr_amount, 0 ))
				  - Sum(IF( DATEDIFF( CURDATE( ), date( v_transactions.invoice_date ) )  >90, v_transactions.cr_amount, 0 ))
  		END
    ) -- AS `>90 Days`
  ) AS `Total`,
	SUM(v_transactions.dr_amount) AS total_dr,
	SUM(v_transactions.cr_amount) AS total_cr

	FROM
		visit_type
	LEFT JOIN v_transactions ON v_transactions.payment_type = visit_type.visit_type_id 
	AND v_transactions.payment_type > 0 
	
	WHERE v_transactions.invoice_date > '2018-03-01'
	 GROUP BY visit_type.visit_type_id;



